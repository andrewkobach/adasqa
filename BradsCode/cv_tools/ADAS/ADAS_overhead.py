import os
#import io
import sys
import argparse
import cv2
import numpy as np
from ADAS_projective_transform import AdasProjectiveTransform
from sde_utils.sde_tar_sr4 import SdeTarSr4
#from cv_utils.intersections import  intersectLines
import tempfile
import json
#import time
#import datetime
import configparser
import matplotlib.pyplot as plt
import pandas as pd
import math
#import scipy.signal
#import boto3
#from sde_utils.sedsync_loging import get_sdemd_list
#import tempfile
#from auto_calibrate import filter_lanes, min_cal, calc_diff, remove_outliers
#from multiprocessing import pool, freeze_support, cpu_count




def Smooth(a, w, n):
    # a is the array which is to be linearly smoothed, which may be padded with nan values at the beginning and end 
    # w is the window size over which the smoothing is defined.  Must be an odd integer. 
    # n is the order of the polynomial that is used for the smoothing
    if(w%2==0): 
        print('Smooth() function only takes w as an odd integer!')
        exit()
    x = [i for i in range(w)]
    wby2 = math.floor(w/2)
    asmoothed = a.copy()
    for i in range(len(a)-2*wby2):
        window = a[i:i+w]
        if(np.isnan(window).any() == True): 
            continue
        fits = np.polyfit(x, window, n)
        fittedwindow = [np.polyval(fits, j) for j in x]
        asmoothed[i+wby2] = fittedwindow[wby2]
    return asmoothed
        

def generate_dual_view(vid_file, sde, cal, out_file, offset, overhead_feet=180, debug=True, verbose=False, fourccstg=None):
    """
    Generate the dual view where the left side is the overhead and the right side is
    the original view.

    :param vid_file: Video file
    :param trans_file: Transform file name. Trnasform represented as json ext and int params.
    :param out_file: Output file name.
    :param overhead_feet: number of feet ahead to show.
    :return: Nothing
    """
    if not os.path.exists(vid_file):
        return
    vidinfo, cref, isstiched = sde.get_video_info('Forward')
    resizevideo = False
    vcap = cv2.VideoCapture(vid_file)
    ret, frame = vcap.read()
    if frame is None:
        print("Missing Video Codec")
        return
    maxx = frame.shape[1]
    maxy = frame.shape[0]    
    
    vcap.release()
    if isstiched or  maxx < 1024 or maxy < 576:
        resizevideo = True
        badvideo = False
        if cref['@X2'] != "1012" or cref['@Y2'] != "566":
            badvideo = True
        maxx = 1024
        maxy = 576

    over_xl = 0
    over_xr = 0
    imgscl = 1280 / maxx
    
    
    distparm = np.array([-8.2944760069433592e+00, 6.9050435757954415e+02, 0., 0.,
              2.9883315472690225e+02, -7.8481132058431520e+00,
              6.8504116360425655e+02, 5.9495900344134816e+02])
    
    cm = np.array([[cal["int_fx"], 0, cal["int_cx"]], [0, cal["int_fy"], cal["int_cy"]], [0.0, 0.0, 1.0]])
    


    have_trans = False
    if cal is not None:
        

        have_trans = True

        # get the transform to the road.

        adast = AdasProjectiveTransform(cal["int_fx"], cal["int_fy"], cal["int_cx"], cal["int_cy"], cal["ext_rx"], cal["ext_ry"],
                                          cal["ext_rz"], cal["ext_tx"], cal["ext_ty"], cal["ext_tz"])
        t = adast.get_image_to_road()
        retinv, invbc = cv2.invert(t, flags=cv2.DECOMP_LU)
        oldrd = np.array([0.0,-30480])
        ci_pts = pointhtrans(oldrd, invbc)
        newrd = pointhtrans(np.array([ci_pts[0]+1,ci_pts[1]+1]), t)
        if debug:
            print("road pixel Size 100 feet out in mm: "+str(np.max(np.abs(oldrd-newrd))))
        oldrd = np.array([0.0,-24384])
        ci_pts = pointhtrans(oldrd, invbc)
        newrd = pointhtrans(np.array([ci_pts[0]+1,ci_pts[1]+1]), t)
        if debug:
            print("road pixel Size 80 feet out in mm: "+str(np.max(np.abs(oldrd-newrd))))


        # scale by the image scaling size
        t = np.dot(t,np.array([[imgscl, 0.0, 0.0],
                               [0.0, imgscl, 0.0], [0.0, 0.0, 1.0]]))
        torg = t.copy()
        retinv, invbc = cv2.invert(t, flags=cv2.DECOMP_LU)
        img_cent_x = int(cal["int_cx"]*maxx/1280.0)

        # Scale so many feet out in front.
        newscale = (maxy)/(overhead_feet*304.8)

        scaletrans = np.array([[newscale, 0.0, 0.0],
                               [0.0, newscale, 0.0], [0.0, 0.0, 1.0]])
        # combine the scaling with the transform.
        t = np.dot(scaletrans, t)
        # now translate to the center of the screen.
        
        '''
        # shift overhead view down to see extrapolated lane lines at bumper distance
        cent_road_bumper = pointhtrans(np.array([0,0]), torg)
        
        ptcentbot = pointhtrans([maxx / 2, maxy], t)
        justtrans = np.array([[1.0, 0.0, (maxx / 2) - ptcentbot[0]],
                              [0.0, 1.0, (maxy) - ptcentbot[1] - newscale*cent_road_bumper[1]], [0.0, 0.0, 1.0]])
        t = np.dot(justtrans, t)
        '''
        
        ptcentbot = pointhtrans([maxx / 2, maxy], t)
        justtrans = np.array([[1.0, 0.0, (maxx / 2) - ptcentbot[0]],
                              [0.0, 1.0, (maxy) - ptcentbot[1]], [0.0, 0.0, 1.0]])
        t = np.dot(justtrans, t)

        # combine the scale and translate.
        totaltransscale = np.dot(justtrans, scaletrans)
        over_xl = 0
        over_xr = maxx

        # find the center point on the road.
        cent_road = pointhtrans(np.array([0,maxy]), torg)
        
        
        # calculate the red lines on the road.
        halfwidth = cal["car_width"]/2
        yteninc = -10*304.8
        sixfeet = 6*304.8
        rdshape = []
        rdshape.extend([[-halfwidth,0,1],[-halfwidth,-60*304.8,1]])
        rdshape.extend([[halfwidth, 0,1], [halfwidth, -60 * 304.8,1]])
        rdshape.extend([[sixfeet,cent_road[1],1],[sixfeet,cent_road[1]-304.8,1]])
        rdshape.extend([[-sixfeet, cent_road[1],1], [-sixfeet, cent_road[1] - 304.8,1]])
        yten = yteninc
        for i in range(0,10):
            rdshape.extend([[-halfwidth, yten, 1], [halfwidth, yten, 1]])
            yten += yteninc
        # translate and scale on the road.
        tview = np.dot(totaltransscale, np.array(rdshape).transpose())

        # get tick marks for the un-rectified showing the edge of the vehicle and 4 inch increments
        # make points to transform.
        img_sides = []
        fourinch = 101.6
        onefoot = 304.8
        
        img_sides.append([pointhtrans([-halfwidth,cent_road[1]],invbc),
                          pointhtrans([-halfwidth,cent_road[1]-onefoot],invbc)])
        img_sides.append([pointhtrans([halfwidth, cent_road[1]],invbc),
                          pointhtrans([halfwidth, cent_road[1] - onefoot],invbc)])
        img_sides.append([pointhtrans([-halfwidth+onefoot,cent_road[1]],invbc),
                          pointhtrans([-halfwidth+onefoot,cent_road[1]-onefoot],invbc)])
        img_sides.append([pointhtrans([halfwidth-onefoot, cent_road[1]],invbc),
                          pointhtrans([halfwidth-onefoot, cent_road[1] - onefoot],invbc)])
        img_sides.append([pointhtrans([-halfwidth+2*onefoot,cent_road[1]],invbc),
                          pointhtrans([-halfwidth+2*onefoot,cent_road[1]-onefoot],invbc)])
        img_sides.append([pointhtrans([halfwidth-2*onefoot, cent_road[1]],invbc),
                          pointhtrans([halfwidth-2*onefoot, cent_road[1] - onefoot],invbc)])

        img_sides.append([pointhtrans([-halfwidth-onefoot,cent_road[1]],invbc),
                          pointhtrans([-halfwidth-onefoot,cent_road[1]-onefoot],invbc)])
        img_sides.append([pointhtrans([halfwidth+onefoot, cent_road[1]],invbc),
                          pointhtrans([halfwidth+onefoot, cent_road[1] - onefoot],invbc)])
        img_sides.append([pointhtrans([-halfwidth-2*onefoot,cent_road[1]],invbc),
                          pointhtrans([-halfwidth-2*onefoot,cent_road[1]-onefoot],invbc)])
        img_sides.append([pointhtrans([halfwidth+2*onefoot, cent_road[1]],invbc),
                          pointhtrans([halfwidth+2*onefoot, cent_road[1] - onefoot],invbc)])


        for k in [1,2]:
            img_sides.append([pointhtrans([-halfwidth+k*fourinch, cent_road[1]], invbc),
                              pointhtrans([-halfwidth+k*fourinch, cent_road[1] - onefoot*2/3], invbc)])
            img_sides.append([pointhtrans([halfwidth-k*fourinch, cent_road[1]], invbc),
                              pointhtrans([halfwidth-k*fourinch, cent_road[1] - onefoot*2/3], invbc)])
            img_sides.append([pointhtrans([-halfwidth + onefoot+k*fourinch, cent_road[1]], invbc),
                              pointhtrans([-halfwidth + onefoot+k*fourinch, cent_road[1] - onefoot*2/3], invbc)])
            img_sides.append([pointhtrans([halfwidth - onefoot-k*fourinch, cent_road[1]], invbc),
                              pointhtrans([halfwidth - onefoot-k*fourinch, cent_road[1] - onefoot*2/3], invbc)])

        for k in [1,2]:
            img_sides.append([pointhtrans([-halfwidth-k*fourinch, cent_road[1]], invbc),
                              pointhtrans([-halfwidth-k*fourinch, cent_road[1] - onefoot*2/3], invbc)])
            img_sides.append([pointhtrans([halfwidth+k*fourinch, cent_road[1]], invbc),
                              pointhtrans([halfwidth+k*fourinch, cent_road[1] - onefoot*2/3], invbc)])
            img_sides.append([pointhtrans([-halfwidth - onefoot-k*fourinch, cent_road[1]], invbc),
                              pointhtrans([-halfwidth - onefoot-k*fourinch, cent_road[1] - onefoot*2/3], invbc)])
            img_sides.append([pointhtrans([halfwidth + onefoot+k*fourinch, cent_road[1]], invbc),
                              pointhtrans([halfwidth + onefoot+k*fourinch, cent_road[1] - onefoot*2/3], invbc)])
         

    fourcc = cv2.VideoWriter_fourcc(*'vp80') if fourccstg is None else cv2.VideoWriter_fourcc(*fourccstg)
    out = cv2.VideoWriter(out_file,fourcc, 4, ((over_xr-over_xl)+ maxx, maxy),True)
    vcap = cv2.VideoCapture(vid_file)

    # midtime = int(time.mktime(time.strptime(sde.get_event_date_time(),"%Y-%m-%dT%H:%M:%S")))*1000
    sdefps = int(sde.get_video_fps("Forward"))
    mspf = int(1000/sdefps)
    msec_dur = sde.get_total_duration()*1000

    veh_data = sde.get_vehicleDetection()
    veh_data_timestamp = np.array([td['timestamp'] for td in veh_data])
    lane_data = sde.get_laneDetection()
    lane_data_timestamp = np.array([td['timestamp'] for td in lane_data])
    mstarttime = (( np.min(lane_data_timestamp)+np.max(lane_data_timestamp))/2.0)-(msec_dur/2.0)
    font = cv2.FONT_HERSHEY_SIMPLEX
    imgscl = 1.0/imgscl
    bottomLeftCornerOfText = i_scl((1080,710),imgscl)
    fontScale = 0.5
    fontColor = (255, 255, 0)
    yellow_color = (0, 255, 255)
    redcolor = (0,0,255)
    textlineType =1
    NoneType = type(None);
    frame_cnt = 0
    mstarttime = mstarttime-mspf
    
    
    
    
    
    
    # study distances in vehicle avro files
    # distances to vehicles
    # add in yDistanceSD to vehicle dictionary
    vehiclesinevent = set([])
    for fm in veh_data:
        for veh in fm['vehicleResult']['vehicles']:
            veh['yDistanceSD'] = int(-pointhtrans(i_scl(((veh['right']+veh['left'])/2., veh['bottom']), imgscl), np.linalg.inv(invbc))[1])
            vehiclesinevent.add(veh['id'])
            
            
    # make dataframes of vehicles, filled with nan
    ySD = pd.DataFrame(columns=list(vehiclesinevent), index=[i for i in range(len(veh_data))])
    yNS = pd.DataFrame(columns=list(vehiclesinevent), index=[i for i in range(len(veh_data))])
    ySDsmoothed = pd.DataFrame(columns=list(vehiclesinevent), index=[i for i in range(len(veh_data))])

    
    
    # fill df with yDistanceSD for all 300 frames, for every vehicle in event.  If vehicle not present, keep as nan
    for i, fm in enumerate(veh_data):
        for veh in fm['vehicleResult']['vehicles']:
            ySD[veh['id']][i] = veh['yDistanceSD']
            yNS[veh['id']][i] = veh['yDistance']
    
    '''
    # smooth yDistanceSD and fill a dataframe with smoothed values
    windowsize = 5
    for v in vehiclesinevent:
        y = Smooth(np.asarray(ySD[v].values, dtype=float), windowsize, 1) 
        ySDsmoothed[v] = y 
    '''    
    
    
    # make ydistance plot
    x = [i for i in range(len(veh_data))]
    colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
    for i, v in enumerate(vehiclesinevent):
        plt.scatter(x, ySD[v].values, s=5, c=colors[i%len(colors)])
        plt.scatter(x, yNS[v].values, s=5, c=colors[i%len(colors)], alpha=0.2)
        #plt.plot(x, ySDsmoothed[v].values, color='k', linestyle='--')
    plt.xlabel('frame number')
    plt.ylabel('forward distance [mm]')
    plt.xlim(0, 300)
    plt.savefig(out_file[:-4] + '_vehicles.png', bbox_inches='tight', dpi=300)
    
    
    
    
    
    
    
    
    # rolling array, 3 elements long, of SD_ydist and vehicle ID
    rolling_ydist = [np.nan] * 3
    rolling_id = [np.nan] * 3
    
    SFtrigger = -1
    LDWtrigger = -1
    FCWtrigger = -1
    
    SFtrigframecnt = 0
    LDWtrigframecntL = 0
    LDWtrigframecntR = 0   
    
    xdistL_NS = []
    xdistL_SD = []
    xdistR_NS = []
    xdistR_SD = []

    
    
    while True:
        ret, frame = vcap.read()
        if not ret:
            break
        if resizevideo:
            if frame.shape[0] == 570 and frame.shape[1] ==2040:
                frame = cv2.copyMakeBorder(frame[0:570, 0:1020], 4, 2, 4, 0, cv2.BORDER_CONSTANT, (0, 0, 0))
            else:
                frame = cv2.copyMakeBorder(frame[0:566, 0:1012],8,2,10,2,cv2.BORDER_CONSTANT,(0,0,0))
                if badvideo:
                    cv2.putText(frame, "Bad Video", (512,250), font, 2.0,
                                (0,0,255), textlineType)

        if offset[0] != 0:
            scloffset = int(offset[0]*imgscl)
            frame = cv2.copyMakeBorder(frame[:,0:-scloffset],0,0,scloffset,0,cv2.BORDER_CONSTANT,(0,0,0))
        frame_cnt += 1
        mstarttime += mspf

        if have_trans:
            # get the overhead view.
            dst = cv2.warpPerspective(frame,t,(maxx,maxy))
            # Draw the red lines.
            if len(rdshape) > 0:
                for k in range(0,len(rdshape),2):
                    cv2.line(dst, (int(tview[0, k]/tview[2, k]), int(tview[1, k]/tview[2, k])), (int(tview[0, k+1]/tview[2, k+1]), int(tview[1, k+1]/tview[2, k+1])), (0, 0, 255), 2)
            # draw green center line
            #cv2.line(frame, (img_cent_x,0),(img_cent_x,maxy), (0, 255, 0), 1)


        # Now draw the cars and lanes.
        if verbose:
            cv2.putText(frame, "Frame #: " + str(frame_cnt), bottomLeftCornerOfText, font, fontScale,
                    fontColor, textlineType)

        # LANES
        l_data_ix = int(np.argsort(np.absolute(lane_data_timestamp - mstarttime))[0])
        tmdiff = abs(lane_data[l_data_ix]['timestamp']-mstarttime)
        if tmdiff <= mspf:
            
            data_idx = l_data_ix
            veh_item = veh_data[data_idx]
            mm_sec = -277.778 * max(0.0, veh_item['speedFloat'])
            
            lane_item = lane_data[data_idx]
            
            turnsigveto = 0
            
            ahatavg = []
            for l_dict in lane_item['laneResult']['lanes']:
                
                if l_dict['position'] == 1:
                    cv2.putText(frame, "Left Neighbor: " + str(l_dict['XDistance']), i_scl((60, 600),imgscl),
                                font, fontScale, fontColor, textlineType)
                if l_dict['position'] == 2: # left lane line
                    if(veh_data[data_idx]['turnSigLeft'] in (1,2)): turnsigveto = 1 # turn signal veto
                    cv2.putText(frame, "NS xDist: " + str(l_dict['XDistance']*10) + ' mm, ' + str(l_dict['confidence'])+'%', i_scl((380, 600),imgscl),
                            font, fontScale, fontColor, textlineType)
                if l_dict['position'] == 3: # right lane line
                    if(veh_data[data_idx]['turnSigRight'] in (1,2)): turnsigveto = 1 # turn signal veto
                    cv2.putText(frame, "NS xDist: " + str(l_dict['XDistance']*10) + ' mm, ' + str(l_dict['confidence'])+'%', i_scl((700, 600),imgscl),
                            font, fontScale, fontColor, textlineType)
                if l_dict['position'] == 4:
                    cv2.putText(frame, "Right Neighbor: " + str(l_dict['XDistance']), i_scl((1020, 600),imgscl),
                            font, fontScale, fontColor, textlineType)


                
                # draw NeuSoft lane lines
                pnts = np.array([l_dict['points']['pointX'], l_dict['points']['pointY']]).transpose()
                pnts = np.int32(pnts.reshape((-1, 1, 2))*imgscl)
                cv2.polylines(frame, [pnts], False, (255, 0, 0), thickness=2)
                
                
                
                    
                # study the curvature of the road
                pntsp = np.array([l_dict['points']['pointX'], l_dict['points']['pointY']])
                
                xpnts = pntsp[0]
                ypnts = pntsp[1]
                
                roadpts = []
                roadptsx = []
                roadptsy = []
                
                roadptsx_road = []
                roadptsy_road = []
                
                
                # get points for lane lines in dst
                for x, y in zip(xpnts, ypnts):
                    x = x*imgscl
                    y = y*imgscl
                    
                    pts = np.array([x,y], dtype=np.float32)
                    pts.shape = (-1, 1, 2)
                    pts_undist = cv2.undistortPoints(pts, cm, distparm, None, cm)[0][0]
                    
                    roadptsx.append(pointhtrans([x, y], t)[0])
                    roadptsy.append(pointhtrans([x, y], t)[1])
                    
                    #roadptsx_road.append(pointhtrans([x,y], np.linalg.inv(invbc))[0])
                    #roadptsy_road.append(pointhtrans([x,y], np.linalg.inv(invbc))[1])
                    
                    roadptsx_road.append(pointhtrans([pts_undist[0], pts_undist[1]], np.linalg.inv(invbc))[0])
                    roadptsy_road.append(pointhtrans([pts_undist[0], pts_undist[1]], np.linalg.inv(invbc))[1])
                    
                    roadpts.append(pointhtrans([x, y], t))
                
                roadpts = np.int32(np.array(roadpts).reshape((-1,1,2)))
                
                # find y in dst of 0,0 in road
                bumperY = pointhtrans([0, 0], t.dot(invbc))[1]
                
                # fit lane with 2nd order polynomial
                ahat = np.polyfit(roadptsy, roadptsx, 2)
                ahat_road = np.polyfit(roadptsy_road, roadptsx_road, 2)
                ypts_fit = np.linspace(bumperY, maxy*0.1, 100)
                ypts_fit_road = np.linspace(0., roadptsy_road[-1], 100)
                xpts_fit = [ahat[0]*y**2 + ahat[1]*y + ahat[2] for y in ypts_fit]
                xpts_fit_road = [ahat_road[0]*y**2 + ahat_road[1]*y + ahat_road[2] for y in ypts_fit_road]
                
                # calculate xDist in road coordinates 
                xDist = pointhtrans([xpts_fit[0], ypts_fit[0]], np.linalg.inv(t.dot(invbc)))[0]
                xDist = abs(xDist) - halfwidth # distance from side of bumper to lane in mm
                
                xDist_road = abs(xpts_fit_road[0]) - halfwidth
                
                # save to dictionary
                #l_dict['SD_xDist'] = xDist/10. # save in dictionary in cm instead of mm
                l_dict['SD_xDist'] = xDist_road/10. # save in dictionary in cm instead of mm 
                
                # use the value of XDist with the lens disortion correction
                xDist = xDist_road
                
                
                # converting from mm to inches
                #xDist = xDist*0.0393701 
                
                # draw calculated xDist on image
                if l_dict['position'] == 2: # left lane line
                    cv2.putText(frame, "SD xDist: " + str(int(xDist))+ ' mm', i_scl((380, 625),imgscl),
                            font, fontScale, fontColor, textlineType)
                if l_dict['position'] == 3:
                    cv2.putText(frame, "SD xDist: " + str(int(xDist)) + ' mm', i_scl((700, 625),imgscl),
                            font, fontScale, fontColor, textlineType)
                
                # reshape and draw magenta fitted lane lines in dst
                laneext = np.array([xpts_fit, ypts_fit]).transpose()
                laneext = np.int32(np.array(laneext).reshape((-1,1,2)))
                cv2.polylines(dst, [laneext], False, (255, 0, 255))
                
                # draw green line that only has linear part of magenta fit
                #xpts_fit_lin = [ahat[1]*y**1 + ahat[2] for y in ypts_fit]
                #laneext_lin = np.array([xpts_fit_lin, ypts_fit]).transpose()
                #laneext_lin = np.int32(np.array(laneext_lin).reshape((-1,1,2)))
                #cv2.polylines(dst, [laneext_lin], False, (0, 255, 0))
                
                # draw blue lines from NeuSoft on top of magenta 
                cv2.polylines(dst, [roadpts], False, (255, 0, 0), thickness=2)
                
                # calculate tangent lines
                w = (xpts_fit[1] - xpts_fit[0])/(ypts_fit[1] - ypts_fit[0])
                tangenty = roadptsy[3]
                tangentx = w*(tangenty - ypts_fit[0]) + xpts_fit[0]
                
                
                
                
                
                # LANE LINE FILTER
                
                # slope cutoff in road coordinates
                wrc = args.laneslopemax
                
                # confidence limit for lane lines
                cfd = args.lanelineconfidence
                
                w = abs(w)
                
                # get maximum time associated with NeuSoft's lane lines
                topindx = len(ypnts)-1
                secout = pointhtrans([xpnts[topindx]*imgscl, ypnts[topindx]*imgscl], np.linalg.inv(invbc))[1]/mm_sec
               
                goodlaneline = 0
                
                if( (w < wrc) & (cfd <= l_dict['confidence']) ): 
                    cv2.polylines(frame, [pnts], False, (255, 0, 0), thickness=2) # draw blue lane line
                    
                    # save fitting parameters for averaging later
                    if( (l_dict['position'] == 2) | ((l_dict['position'] == 3))): 
                        
                        goodlaneline = 1
                        
                        # require that the lane line extends out far enough in time to justify the extrapolation for the new in-lane Boolean
                        if(secout >= args.lanelinedistsec): 
                            ahatavg.append(ahat) 
 
                else: 
                    cv2.polylines(frame, [pnts], False, (0, 0, 255), thickness=2) # draw red lane line
                   
 
                
                # check to see if LDW trigger was satisfied
                #xDist = xDist/0.0393701 # transform from in to mm 
                
                # left lane
                if(l_dict['position'] == 2):
                    if((xDist <= -args.lanedeparture) & (goodlaneline == 1) & (turnsigveto == 0)):
                        LDWtrigframecntL += 1
                        if(LDWtrigframecntL >= args.numframesLDW): 
                            LDWtrigger = 2
                            cv2.polylines(frame, [pnts], False, (0, 255, 255), thickness=2) # draw yellow lane line
                    else: LDWtrigframecntL = 0
                
                # right lane
                if(l_dict['position'] == 3):
                    if((xDist <= -args.lanedeparture) & (goodlaneline == 1) & (turnsigveto == 0)):
                        LDWtrigframecntR += 1
                        if(LDWtrigframecntR >= args.numframesLDW): 
                            LDWtrigger = 2
                            cv2.polylines(frame, [pnts], False, (0, 255, 255), thickness=2) # draw yellow lane line
                    else: LDWtrigframecntR = 0
          
            
                
                
                
                
                
                
            # after looping through all lane lines, but still in lane module
      
            
            
            xdistR_NS.append(np.nan)
            xdistL_NS.append(np.nan)
            xdistR_SD.append(np.nan)
            xdistL_SD.append(np.nan)    
            
            
            for ld in lane_item['laneResult']['lanes']:
                
                if(ld['position'] == 2): # left lane
                    xdistL_NS[-1] = ld['XDistance']
                    xdistL_SD[-1] = ld['SD_xDist']
                    
                if(ld['position'] == 3): # right lane
                    xdistR_NS[-1] = ld['XDistance']
                    xdistR_SD[-1] = ld['SD_xDist']
        
            
            inlanepolyL = []
            inlanepolyR = []
                
            if(len(ahatavg) != 0):        
                a2 = []  
                a1 = []
                
                for a in ahatavg:
                    a2.append(a[0])
                    a1.append(a[1])
                    
                '''
                # module block for hill veto    
                if(len(ahatavg) == 2): # need both the left and right lanes for hill veto
                    ypts_fit = np.linspace(bumperY, maxy*0.01, 100)
                    
                    for a in ahatavg:
                        xpts_fit = [a[0]*y**2 + a[1]*y + a[2] for y in ypts_fit]
                        laneext = np.array([xpts_fit, ypts_fit]).transpose()
                        laneext = np.int32(np.array(laneext).reshape((-1,1,2)))
                        cv2.polylines(dst, [laneext], False, (0, 255, 0))
                  '''  
                    
                    
                # get average of fitting parameters in dst coordinates
                a2avg = np.average(a2)
                a1avg = np.average(a1)
                
                
                # get origin in road coordinates in dst coordinates
                centertruckdstL = pointhtrans([-halfwidth, 0], t.dot(invbc))
                centertruckdstR = pointhtrans([halfwidth, 0], t.dot(invbc))
                
                # sample average fits in dst
                top_sec = args.maxinlanetime
                top = pointhtrans([0, top_sec*mm_sec], t.dot(invbc))[1]
                ypts_fit = np.linspace(maxy, top, 25)
                xpts_fitL = [a2avg*y**2 + a1avg*y + (centertruckdstL[0] - a2avg*maxy**2 - a1avg*maxy) for y in ypts_fit]
                xpts_fitR = [a2avg*y**2 + a1avg*y + (centertruckdstR[0] - a2avg*maxy**2 - a1avg*maxy) for y in ypts_fit]
                
                # fill array used for in-lane boolean
                inlanepolyL.append(a2avg)
                inlanepolyL.append(a1avg)
                inlanepolyL.append(centertruckdstL[0] - a2avg*maxy**2 - a1avg*maxy)
                inlanepolyR.append(a2avg)
                inlanepolyR.append(a1avg)
                inlanepolyR.append(centertruckdstR[0] - a2avg*maxy**2 - a1avg*maxy)
                
                
                # get y position in dst coordinates of S1 and S2 following times
                y_S1_dst = pointhtrans([0, args.shortfollowingS1*mm_sec], t.dot(invbc))[1]
                y_S2_dst = pointhtrans([0, args.shortfollowingS2*mm_sec], t.dot(invbc))[1]
                y_top_dst = pointhtrans([0, top_sec*mm_sec], t.dot(invbc))[1]
                xL_S1_dst = a2avg*y_S1_dst**2 + a1avg*y_S1_dst + (centertruckdstL[0] - a2avg*maxy**2 - a1avg*maxy)
                xR_S1_dst = a2avg*y_S1_dst**2 + a1avg*y_S1_dst + (centertruckdstR[0] - a2avg*maxy**2 - a1avg*maxy)
                xL_S2_dst = a2avg*y_S2_dst**2 + a1avg*y_S2_dst + (centertruckdstL[0] - a2avg*maxy**2 - a1avg*maxy)
                xR_S2_dst = a2avg*y_S2_dst**2 + a1avg*y_S2_dst + (centertruckdstR[0] - a2avg*maxy**2 - a1avg*maxy)
                xL_top_dst = a2avg*y_top_dst**2 + a1avg*y_top_dst + (centertruckdstL[0] - a2avg*maxy**2 - a1avg*maxy)
                xR_top_dst = a2avg*y_top_dst**2 + a1avg*y_top_dst + (centertruckdstR[0] - a2avg*maxy**2 - a1avg*maxy)
                
                # reshape arrays and draw on dst
                laneextL = np.array([xpts_fitL, ypts_fit]).transpose()
                laneextL = np.int32(np.array(laneextL).reshape((-1,1,2)))
                
                laneextR = np.array([xpts_fitR, ypts_fit]).transpose()
                laneextR = np.int32(np.array(laneextR).reshape((-1,1,2)))
                
                # transform sets of points from dst to frame
                fit_frameL = []
                fit_frameR = []
                
                for x, y in zip(xpts_fitL, ypts_fit):
                    fit_frameL.append(pointhtrans([x,y], np.linalg.inv(t)))
                    
                for x, y in zip(xpts_fitR, ypts_fit):
                    fit_frameR.append(pointhtrans([x,y], np.linalg.inv(t)))    
                
                # reshape arrays
                fit_frameL = np.int32(np.array(fit_frameL).reshape((-1,1,2)))  
                fit_frameR = np.int32(np.array(fit_frameR).reshape((-1,1,2)))  
                
                # draw sides of new in-lane Boolean in frame
                cv2.polylines(frame, [fit_frameL], False, (0, 255, 0), thickness=1)
                cv2.polylines(frame, [fit_frameR], False, (0, 255, 0), thickness=1)
                
                # convert L and R following-time points from dst to frame
                S1_L_frame = pointhtrans([xL_S1_dst, y_S1_dst], np.linalg.inv(t))
                S1_R_frame = pointhtrans([xR_S1_dst, y_S1_dst], np.linalg.inv(t))
                S2_L_frame = pointhtrans([xL_S2_dst, y_S2_dst], np.linalg.inv(t))
                S2_R_frame = pointhtrans([xR_S2_dst, y_S2_dst], np.linalg.inv(t))
                top_L_frame = pointhtrans([xL_top_dst, y_top_dst], np.linalg.inv(t))
                top_R_frame = pointhtrans([xR_top_dst, y_top_dst], np.linalg.inv(t))
                
                # draw following times in frame
                cv2.line(frame, tuple(np.int32(S1_L_frame)), tuple(np.int32(S1_R_frame)), (0, 255, 255), 1)
                cv2.line(frame, tuple(np.int32(S2_L_frame)), tuple(np.int32(S2_R_frame)), (0, 255, 255), 1)
                cv2.line(frame, tuple(np.int32(top_L_frame)), tuple(np.int32(top_R_frame)), (0, 255, 0), 1)
                
            
            # if no suitable lane lines found, use in-lane triangle instead
            else:
                followingtime = [args.shortfollowingS1, args.shortfollowingS2]
                    
                for k in followingtime:
                    lspeed = pointhtrans([-halfwidth, k*mm_sec], invbc)
                    rspeed = pointhtrans([halfwidth, k*mm_sec], invbc)
                    #cv2.line(frame, tuple(np.int32(lspeed)), tuple(np.int32(rspeed)), (0, 255, 255), 1)
                    cv2.line(frame, tuple(np.int32(lspeed)), tuple(np.int32(rspeed)), (0, 255, 255), 1)
                
                top_sec = args.maxinlanetime
                
                # draw in-lane triangle
                l1 = pointhtrans([-halfwidth, 0.1*mm_sec], invbc)
                r1 = pointhtrans([halfwidth, 0.1*mm_sec], invbc)
                                    
                l2 = pointhtrans([-halfwidth, top_sec*mm_sec], invbc)
                r2 = pointhtrans([halfwidth, top_sec*mm_sec], invbc)
                
                
                cv2.line(frame, tuple(np.int32(l1)), tuple(np.int32(l2)), (0, 255, 0), 1) # left side
                cv2.line(frame, tuple(np.int32(r1)), tuple(np.int32(r2)), (0, 255, 0), 1) # right side
                cv2.line(frame, tuple(np.int32(l2)), tuple(np.int32(r2)), (0, 255, 0), 1) # top 

                # get equation of triangle sides in dst
                l1dst = pointhtrans(l1, t)
                r1dst = pointhtrans(r1, t)
                
                # fill in-lane polynomials
                inlanepolyL.append(0)
                inlanepolyL.append(0)
                inlanepolyL.append(l1dst[0])
                
                inlanepolyR.append(0)
                inlanepolyR.append(0)
                inlanepolyR.append(r1dst[0])
                
                
      

                
        # get x position in frame of vanishing point
        vanishingpointx = pointhtrans([-halfwidth, 1000*mm_sec], invbc)[0]
        cv2.line(frame, tuple(np.int32([vanishingpointx,0])), tuple(np.int32([vanishingpointx,maxy])), (255, 0, 255), 1) # vertical magenta line         

        # VEHICLE
        firmwarecolor = (0,223,255)
        cv2.putText(frame, "Firmware: "+str(sde.get_firmware_version()), i_scl((60, 45),imgscl), font, fontScale, firmwarecolor, textlineType)
        cv2.putText(frame, "SR: "+str(sde.get_sr()), i_scl((60, 65),imgscl), font, fontScale, firmwarecolor, textlineType)
        cv2.putText(frame, "Event Time: "+str(sde.get_event_time()), i_scl((60, 85),imgscl), font, fontScale, firmwarecolor, textlineType)
        
        if "speedFloat" in veh_item:
            cv2.putText(frame, "Speed: " + str(0.621371 * veh_item['speedFloat'])[:4] + " MPH", i_scl((60, maxy-100),imgscl),
                        font, fontScale, firmwarecolor, textlineType)
            if have_trans:
                mm_sec = -277.778 * max(0.0, veh_item['speedFloat'])
       
        else:
            cv2.putText(frame, "Speed: N/A", i_scl((60, maxy-100),imgscl), font, fontScale, firmwarecolor, textlineType)
        
        if "turnSigLeft" in veh_item:
            cv2.putText(frame, "TurnSig: Left=" + str(veh_item['turnSigLeft']) +
                        ", Right=" + str(veh_item['turnSigRight']),
                        i_scl((60, maxy-80),imgscl), font, fontScale, firmwarecolor, textlineType)
        else:
            cv2.putText(frame, "TurnSig: N/A", i_scl((60, maxy-80),imgscl), font, fontScale, firmwarecolor, textlineType)

        v_data_ix = int(np.argsort(np.absolute(veh_data_timestamp - mstarttime))[0])
        tmdiff = abs(veh_data[v_data_ix]['timestamp']-mstarttime)
        if tmdiff <=  mspf:
            data_idx = v_data_ix
            veh_item = veh_data[data_idx] # information about all vehicles in a given frame
            mm_sec = -277.778 * max(0.0, veh_item['speedFloat'])
            
            
            # vehicle
            for v_dict in veh_item['vehicleResult']['vehicles']:
                if have_trans:
                    lside = pointhtrans(i_scl([v_dict['left'],v_dict['bottom']],imgscl),t)
                    rside = pointhtrans(i_scl([v_dict['right'],v_dict['bottom']],imgscl),t)
                    rd_pt = pointhtrans(i_scl([(v_dict['right']+v_dict['left'])/2,v_dict['bottom']],imgscl),torg)
                    cv2.line(dst, tuple(np.int32(lside)), tuple(np.int32(rside)), (0, 255, 0), 1)
                    rside[0] = rside[0]+10
                    cv2.putText(dst, str(np.int32(-rd_pt[1])),
                                tuple(np.int32(rside)), font, fontScale, fontColor, 1)
                    v_dict['SD_yDist'] = np.int32(-rd_pt[1]) # fill vehicle dictionary with y distance from homography
                    
                if verbose:
                    cv2.rectangle(frame, i_scl((v_dict['left'], v_dict['top']),imgscl), i_scl((v_dict['right'], v_dict['bottom']),imgscl), fontColor)
                    cv2.putText(frame, "Cnf: " + str(v_dict['confidence']), i_scl((v_dict['left'], v_dict['top']),imgscl),
                                font, fontScale, fontColor, textlineType)
                    cv2.putText(frame, "InLane: " + str(v_dict['vehicleInLane']), i_scl((v_dict['left'], v_dict['top'] - 15),imgscl),
                                font, fontScale, fontColor, textlineType)
                    cv2.putText(frame, "X-Dist: " + str(v_dict['xDistance']), i_scl((v_dict['left'], v_dict['top'] - 30),imgscl),
                                font, fontScale, fontColor, textlineType)
                    cv2.putText(frame, "Y-Dist: " + str(v_dict['yDistance']), i_scl((v_dict['left'], v_dict['top'] - 45),imgscl),
                                font, fontScale, fontColor, textlineType)
                    cv2.putText(frame, "ObjID: " + str(v_dict['id']), i_scl((v_dict['left'], v_dict['top'] - 60),imgscl),
                                font, fontScale, fontColor, textlineType)
                else:
                    inlane = v_dict['vehicleInLane'] == 1
                    cv2.rectangle(frame, i_scl((v_dict['left'], v_dict['top']), imgscl),
                                  i_scl((v_dict['right'], v_dict['bottom']), imgscl), redcolor if inlane else fontColor)
                    vehtext = str(v_dict['id'])+" ("+str(v_dict['xDistance'])+", "+str(v_dict['yDistance'])+") "+str(v_dict['confidence'])+"%"
                    cv2.putText(frame, vehtext, i_scl((v_dict['left'], v_dict['top']-5),imgscl),
                                font, fontScale, fontColor, textlineType)
                    cv2.circle(frame, i_scl(((v_dict['right']+v_dict['left'])/2., v_dict['bottom']), imgscl), 6, (0, 255, 0), -1) # draw green circle
                    





                    # get vehicle position in dst
                    vposframe = i_scl(((v_dict['right']+v_dict['left'])/2., v_dict['bottom']), imgscl) # vehicle position in the frame
                    vposdst = pointhtrans(vposframe, t) # vehicle position n dst
                    
                    # max distance for in-lane boolean in dst
                    top_sec = args.maxinlanetime
                    
                    # position of top of in-lane Boolean in dst
                    top_sec_dst = pointhtrans([0, mm_sec*top_sec], t.dot(invbc))
                                        
                    # calculate left and right points on in-lane shape at y position of vehicle
                    vposdstL = inlanepolyL[0]*vposdst[1]**2 + inlanepolyL[1]*vposdst[1] + inlanepolyL[2]
                    vposdstR = inlanepolyR[0]*vposdst[1]**2 + inlanepolyR[1]*vposdst[1] + inlanepolyR[2]
                    
                    
                                        
                    # decide whether a vehicle is in-lane or not, given the new in-lane Boolean
                    v_dict['inlane'] = 0 # add entry to vehicle dictionary
                    if( (vposdst[1] > top_sec_dst[1]) & (vposdst[0] > vposdstL) & (vposdst[0] < vposdstR) ):
                        
                        # require a minimum vehicle confidence, which is used when defining a true positive for FCW
                        if(v_dict['confidence'] >= args.vehicleconfidence): 
                            v_dict['inlane'] = 1
                        
                        SFS1pix = pointhtrans([0, args.shortfollowingS1*mm_sec], invbc)
                        SFS2pix = pointhtrans([0, args.shortfollowingS2*mm_sec], invbc) 
                        vehposy = v_dict['bottom']*imgscl
                        
                        if( (vehposy >= SFS1pix[1]) & (vehposy <= SFS2pix[1]) ): 
                            cv2.circle(frame, i_scl(((v_dict['right']+v_dict['left'])/2., v_dict['bottom']), imgscl), 6, (0, 255, 255), -1) # draw yellow circle
                            
                        
                        if(v_dict['bottom']*imgscl >= SFS2pix[1]): 
                            cv2.circle(frame, i_scl(((v_dict['right']+v_dict['left'])/2., v_dict['bottom']), imgscl), 6, (0, 0, 255), -1) # draw red circle
                            SFtrigframecnt += 1
                            if(SFtrigframecnt == args.numframesSF): SFtrigger = 2
                        else: SFtrigframecnt = 0    
                        
                    
            # permute rolling lists
            rolling_ydist = [rolling_ydist[1], rolling_ydist[2], np.nan]
            rolling_id = [rolling_id[1], rolling_id[2], np.nan]
            
            # loop through all vehicle in frame
            ydist = []
            vehid = []            
            
            for veh in veh_item['vehicleResult']['vehicles']:
                
                # select only those vehicle in lane
                if(veh['inlane'] == 1):
                    ydist.append(veh['SD_yDist'])
                    vehid.append(veh['id'])
            
            # pick out closest in-lane vehicle's distance and ID
            if(len(ydist) != 0):
                minyindex = ydist.index(min(ydist))                 
                rolling_ydist[2] = ydist[minyindex]
                rolling_id[2] = vehid[minyindex]
            
            
           
            
            TTC = -1.
            if(all(x==rolling_id[0] for x in rolling_id)): # check rolling if criteria are satisfied for calculating TTC
                fit = np.polyfit(np.asarray([0., 0.25, 0.5]), np.asarray(rolling_ydist), 1) # fit with straight line over three frames
                if(fit[0] < 0):
                    TTC = 0.5*rolling_ydist[2]/abs(fit[0]) # the 0.5 is Delta-t
                    if( (TTC < args.TTCmin) and (TTC >= rolling_ydist[2]/mm_sec) ): # TTC is less than treshold and greater than following time
                        FCWtrigger = 2
                        cv2.putText(frame, 'TTC: '+str(TTC)[:3]+' s', i_scl((maxx/2., 60),imgscl), font, 1., (255,0,255), 2) # draw TTC in frame
                 
            
            
            
        

        # piece together the images
        if have_trans:
            new_img = np.concatenate((dst, frame), axis=1)
            if debug:
                cv2.imshow("dst",new_img)
                cv2.waitKey(60)
            out.write(new_img)
        else:
            if debug:
                cv2.imshow("dst",frame)
                cv2.waitKey(60)
            out.write(frame)
    
    
    
        
    xaxis = [i for i in range(len(xdistL_NS))]
    
    lanewidth_NS = [cal["car_width"] + (xL + xR)*10 for xL, xR in zip(xdistL_NS, xdistR_NS)]
    lanewidth_SD = [cal["car_width"] + (xL + xR)*10 for xL, xR in zip(xdistL_SD, xdistR_SD)]
        
    
    plt.clf()
    
    plt.scatter(xaxis, xdistL_SD, s=5, c='b', label='left lane (SD)')
    plt.scatter(xaxis, xdistL_NS, s=5, c='b', alpha=0.2, label='left lane (NS)')
    plt.scatter(xaxis, xdistR_SD, s=5, c='m', label='right lane (SD)')
    plt.scatter(xaxis, xdistR_NS, s=5, c='m', alpha=0.2, label='right lane (NS)')
    
    plt.xlabel('frame number')
    plt.title(out_file.split(os.sep)[-1][:-4]+'.SDE')
    plt.ylabel('xDist [mm]')
    plt.xlim(0, 80)
    plt.legend(loc='upper right')
    plt.savefig(out_file[:-4] + '_lanes.png', bbox_inches='tight', dpi=300)
    

            
    # find average lane width
    avglanewidth_NS = [w for w in lanewidth_NS if str(w)!='nan']
    avglanewidth_SD = [w for w in lanewidth_SD if str(w)!='nan']    
    
    
    if(len(avglanewidth_NS) != 0):
        
        avglanewidth_NS = np.average(avglanewidth_NS)
        avglanewidth_SD = np.average(avglanewidth_SD)
        
        plt.clf()
    
        plt.scatter(xaxis, lanewidth_SD, s=5, c='g', label='SmartDrive')
        plt.scatter(xaxis, lanewidth_NS, s=5, c='r', label='NeuSoft')
        plt.plot([0,80], [3657.6, 3657.6]) # 12 ft
        plt.text(81, 3658-50, str(3657) , fontsize=7)
        plt.plot([0,80],[avglanewidth_NS,avglanewidth_NS], color='r', linestyle='--', linewidth=1)
        plt.plot([0,80],[avglanewidth_SD,avglanewidth_SD], color='g', linestyle='--', linewidth=1)
        plt.text(81, avglanewidth_SD-50, str(int(avglanewidth_SD)), fontsize=7)
        plt.text(81, avglanewidth_NS-50, str(int(avglanewidth_NS)), fontsize=7)
    
        plt.xlabel('frame number')
        plt.ylabel('lane width [mm]')
        plt.title(out_file.split(os.sep)[-1][:-4]+'.SDE')
        plt.xlim(0, 80)
        plt.ylim(0,6000)
        plt.legend(loc='lower right')
        plt.savefig(out_file[:-4] + '_lanewidth.png', bbox_inches='tight', dpi=300)
        
        
        
    jsonfile = out_file[:-4] + '.json'
    
    if(os.path.exists(jsonfile)):    
        f = json.load(open(jsonfile))
        
        f['SF'] = SFtrigger
        f['LDW'] = LDWtrigger
        f['FCW'] = FCWtrigger
        f['Firmware'] = sde.get_firmware_version()
            
        json.dump(f, open(jsonfile, 'w'))

    vcap.release()
    out.release()
    # cv2.destroyAllWindows()
    return


# transform a cartesian point through a homogenious transform back to cartesian
def pointhtrans(pt,m):
    """
    MAp a point trhough the perspective transform M
    :param pt:
    :param m:
    :return:
    """
    inpt = np.asarray([pt[0],pt[1],1.0])
    outpt = np.dot(m,inpt)
    return np.asarray([outpt[0]/outpt[2],outpt[1]/outpt[2]])


def i_scl(v,s):
    """
    Scale a point.
    :param v:
    :param s:
    :return:
    """
    return (int(v[0]*s), int(v[1]*s))


def jsoncal_from_ini(calinifile):
    """
    parse ini to get the calibration values.
    :param calinifile:
    :return:
    """
    cfg = configparser.ConfigParser()
    cfg.read(calinifile)
    cfgdct = dict(cfg['shengyang_1280x720_planar_camera_param'])
    cfgdctci = dict(cfg['CarInfo'])
    cfgdct.update(cfgdctci)
    for keys in cfgdct:
        cfgdct[keys] = float(cfgdct[keys])
    return cfgdct


def make_cal_json(sdedir, calinifile):
    """
    Convert the ini data into a calibration json file.
    :param sdedir:
    :param calinifile:
    :return:
    """
    # first get the SR4 name.
    jsonfilename = os.path.join(sdedir,"cal.json")
    for root, dirs, files in os.walk(sdedir):
        for fname in files:
            if fname.endswith((".SDE", ".sde")):
                jsonfilename = os.path.join(sdedir, fname[:8]+".json")
                if os.path.exists(jsonfilename):
                    return jsonfilename
                break
            else:
                continue
        break
    cfgdct = jsoncal_from_ini(calinifile)
    with open(jsonfilename, 'w') as f:
        json.dump(cfgdct, f, indent=2)
    return jsonfilename


# def showdistortion(frame):
#     nf = frame.copy()
#     # draw lines every 10
#     imagepts = []
#     for i in range(0,frame.shape[1]+1,10):
#         cv2.line(nf, tuple([i,0]), tuple([i,frame.shape[0]]), (0, 0, 255), 1)
#     for i in range(0,frame.shape[0]+1,10):
#         cv2.line(nf, tuple([0,i]), tuple([frame.shape[1],i]), (0, 0, 255), 1)
#         imagepts.append([frame.shape[1]/2,i])
#     inset = 30
#     inset2 = 90
#     inset3 = 220
#     cv2.line(nf, tuple([0, frame.shape[0]-inset]), tuple([frame.shape[1], frame.shape[0]-inset]), (0, 255, 0), 1)
#     cv2.line(nf, tuple([0, inset]), tuple([frame.shape[1], inset]), (0, 255, 0), 1)
#     cv2.line(nf, tuple([0, frame.shape[0]-inset2]), tuple([frame.shape[1], frame.shape[0]-inset2]), (0, 255, 0), 1)
#     cv2.line(nf, tuple([0, inset2]), tuple([frame.shape[1], inset2]), (0, 255, 0), 1)
#     cv2.line(nf, tuple([0, frame.shape[0]-inset3]), tuple([frame.shape[1], frame.shape[0]-inset3]), (0, 255, 0), 1)
#     cv2.line(nf, tuple([0, inset3]), tuple([frame.shape[1], inset3]), (0, 255, 0), 1)
#     cm = np.array([[1566,0,648],[0,1566,328],[0.0,0.0,1.0]])
#     d = np.array([ -8.2944760069433592e+00, 6.9050435757954415e+02, 0., 0.,
#        2.9883315472690225e+02, -7.8481132058431520e+00,
#        6.8504116360425655e+02, 5.9495900344134816e+02, 0., 0., 0., 0.,
#        0., 0. ])
#     centerpts = np.array(imagepts, dtype=np.float32)
#     centerpts.shape = (-1, 1, 2)
#     centerpts = np.ascontiguousarray(centerpts)
#     undist_imagepts = cv2.undistortPoints(centerpts, cm, d, None, cm)
#     undist_imagepts.shape = (-1, 2)
#     for i in range(0,len(imagepts)):
#         print(imagepts[i]," ",undist_imagepts[i], flush=True)
#     d2 = np.array([ 1.0398072487761672e+02, -3.7645367265747149e+02, 0., 0.,
#        1.0435612357041865e+02, 1.0452415481046461e+02,
#        -3.3781959832214437e+02, -2.9560062304725200e+01])
#     # d3 = np.array([ 1.1662428933547366e+01, 5.1191127220280123e+01,
#     #    1.9224191087474224e-03, -1.4065382557373269e-02,
#     #    -6.5086841566522358e-01, 1.2025393921430219e+01,
#     #    5.6131235009168797e+01, 1.7795259263801043e+01])
#     nfud = cv2.undistort(nf, cm, d)
#     nfud2 = cv2.undistort(nf, cm, d2)
#     # nfud3 = cv2.undistort(nf, cm, d3)
#     cv2.imshow("ud", nfud)
#     cv2.imshow("ud2", nfud2)
#     # cv2.imshow("ud3", nfud3)
#     cv2.waitKey(60)
#     return None

'''

LASTPROCESSED = 'srsdemdlastprocessed'
COMPANYHASHMAP = 'companyhashmap.json'

# we have a global s3cl for the multiprocessing.
s3cl = boto3.client('s3')


def process_bucket(loggrp, outbucket, dist, args):
    """
    Process SDE files found in a bucket.  The SDE files are found by processing the cloudwatch log file group.
    :param loggrp: The cloudwatch log group for the input bucket.
    :param outbucket:The destination bucket.
    :param dist:Overhead distance.
    :param args:Other args.
    :return: None
    """
    mpool = pool.Pool(os.cpu_count(), maxtasksperchild=10000)
    # Store temporary files in tmp
    with tempfile.TemporaryDirectory() as tmpdir:
        try:
            # read in the calibrations.  These are the only SRIDs we will look at.
            calibrations = {}
            try:
                response = s3cl.get_object(Bucket=outbucket, Key='calibrations.json')
                if 'Body' in response:
                    lastprefix = response['Body'].read()
                    caljson = lastprefix.decode("utf-8")
                    calibrations = json.loads(caljson)
            except Exception as ex:
                print("No Calibrations.json", flush=True)
                return None
            if len(calibrations) == 0:
                print("No Calibrations.json", flush=True)
                return None
            # get the ones we have already corrected.
            corrected_calibrations = {}
            corrected_cal_changed = False
            try:
                response = s3cl.get_object(Bucket=outbucket, Key='corrected_calibrations.json')
                if 'Body' in response:
                    lastprefix = response['Body'].read()
                    caljson = lastprefix.decode("utf-8")
                    corrected_calibrations = json.loads(caljson)
            except Exception as ex:
                print("No corrected_calibrations.json", flush=True)

            # get a list of SRID company hashs.  These are used if we don't have any yet.
            srid_to_hash = {}
            srid_to_hash_changed = False
            try:
                response = s3cl.get_object(Bucket=outbucket, Key=COMPANYHASHMAP)
                if 'Body' in response:
                    hashmap = response['Body'].read()
                    hashjson = hashmap.decode("utf-8")
                    srid_to_hash = json.loads(hashjson)
            except Exception as ex:
                print("No "+COMPANYHASHMAP, flush=True)

            # get the last processed data.
            lastdate = None
            try:
                response = s3cl.get_object(Bucket=outbucket, Key=LASTPROCESSED)
                if 'Body' in response:
                    lastprefix = response['Body'].read()
                    lastdate = lastprefix.decode("utf-8")
            except Exception as ex:
                print("Getting Last processed "+str(ex), flush=True)
                pass

            # get the new sde files to process.
            all_sde_list, newlastdate = get_sdemd_list(lastdate, loggrp, region_name='us-west-2', delete=False)
            if lastdate is None and len(srid_to_hash) > 0:
                all_sde_list = get_all_sde_from_hash(all_sde_list, srid_to_hash, s3cl)
            # group by SRID.
            srid_grps = {}
            for sdemd_name in all_sde_list:
                sp_name = sdemd_name.split('/')
                srid = sp_name[-1][:8]
                if srid in calibrations:
                    if srid in srid_grps:
                        srid_grps[srid].append(sdemd_name)
                    else:
                        srid_grps[srid] = [sdemd_name]
            # go through each SRID's sde files and accumulate lanes until we have a new calibration.
            for srid, sdelist in srid_grps.items():
                if srid not in srid_to_hash:
                    srid_to_hash[srid] = sp_name[1]
                    srid_to_hash_changed = True
                # this just sorts the sde by date.
                sdelist.sort()
                all_new_lanes = []
                calib = calibrations[srid]
                laneskey = 'lanes/' + srid + '.json'
                # get any lanes we already have.
                try:
                    response = s3cl.get_object(Bucket=outbucket, Key=laneskey)
                    if 'Body' in response:
                        rawlane = response['Body'].read()
                        lanjson = rawlane.decode("utf-8")
                        all_new_lanes = json.loads(lanjson)
                        s3cl.delete_object(Bucket=outbucket, Key=laneskey)
                except Exception as ex:
                    pass
                oldspeedsde = []
                newspeedsde = []
                # get any old speed sde lists.  These are used to eventually create teh videos.
                speedsdekey = 'speedsde/'+ srid + '.json'
                try:
                    response = s3cl.get_object(Bucket=outbucket, Key=speedsdekey)
                    if 'Body' in response:
                        rawsl = response['Body'].read()
                        sljson = rawlane.decode("utf-8")
                        oldspeedsde = json.loads(lanjson)
                except Exception as ex:
                    pass
                newcalibrationthissde = False
                # now process all the new lanes from the sde files.
                for sdemd_name in sdelist:
                    sp_name = sdemd_name.split('/')

                    print(sdemd_name, flush=True)
                    sdebucket = sp_name[0]
                    sdekey = "/".join(sp_name[1:])
                    tmpsde = os.path.join(tmpdir, sp_name[-1])
                    try:
                        s3cl.download_file(Bucket=sdebucket, Key=sdekey, Filename=tmpsde)
                    except Exception as ex:
                        print("Error processing"+sdemd_name+" "+str(ex), flush=True)
                    if os.path.exists(tmpsde):
                        sde = SdeTarSr4(tmpsde)
                        if not sde.have_avro():
                            sde.close()
                            os.remove(tmpsde)
                            continue
                        # get the mean speed and make sure we have speeds for most frames.
                        speed = np.array(sde.get_speed()[0])
                        meanspeed = speed[speed > 0.0]
                        if len(meanspeed) > int(len(speed)*.90):
                            meanspeed = np.mean(meanspeed)
                        else:
                            meanspeed = 0.0
                        try:
                            lane_data = sde.get_laneDetection()
                            if len(lane_data) == 0:
                                meanspeed = 0.0
                            all_new_lanes.extend(filter_lanes(lane_data, speed, (0,0), args.lanetol, args.speedmin))
                        except Exception as ex:
                            print("Error getting lanes" + sdemd_name + " " + str(ex), flush=True)
                            meanspeed = 0.0
                        sde.close()

                        # see if we have crossed the threshold on the number of lanes remove outliers.
                        if len(all_new_lanes) > args.numlanes:
                            all_new_lanes = remove_outliers(np.array(all_new_lanes)).tolist()
                        # do we still have enough lanes,
                        if len(all_new_lanes) > args.numlanes:
                            new_cal, success = min_cal(calib, np.array(all_new_lanes))
                            if success:
                                diff = calc_diff(new_cal, calib)
                                new_cal["diff"] = diff
                                new_cal["timestamp"] = int(time.time())
                                corrected_calibrations[srid] = new_cal
                                corrected_cal_changed = True
                                newcalibrationthissde = True
                                if args.debug:
                                    generate_debug_jpg(all_new_lanes, s3cl, outbucket, srid, sp_name[-1],
                                                       corrected_calibrations[srid])
                                all_new_lanes = []
                        if meanspeed > 0.0:
                            newspeedsde.append([sdemd_name,int(5*int(meanspeed/5))])
                        os.remove(tmpsde)
                    else:
                        print("Error Downloading SDE",tmpsde, flush=True)
                # save off any left over lanes.
                if len(all_new_lanes) > 0:
                    lanjson = json.dumps(np.array(all_new_lanes).tolist())
                    s3result = s3cl.put_object(
                        Body=io.BytesIO(lanjson.encode('utf-8')),
                        Bucket=outbucket, Key=laneskey, ContentType="application/json")
                    if not (s3result['ResponseMetadata'] is not None and
                            s3result['ResponseMetadata']['HTTPStatusCode'] is not None and
                            s3result['ResponseMetadata']['HTTPStatusCode'] == 200):
                        print("\nError writing to S3 " + laneskey + " ", s3result['ResponseMetadata']['HTTPStatusCode'], flush=True)
                # save off the 5 fast sde.
                newspeedsde.sort(key=lambda x:-x[1])
                newspeedsde = newspeedsde[:min(5, len(newspeedsde))]
                newspeedsde = [ns for ns in newspeedsde if ns[1] >= args.speedmin]
                if len(newspeedsde) < 5 and len(oldspeedsde) > 0:
                    newspeedsde.extend(oldspeedsde)
                    newspeedsde = newspeedsde[:min(5, len(newspeedsde))]
                if len(newspeedsde) > 0:
                    spjson = json.dumps(newspeedsde)
                    s3result = s3cl.put_object(
                        Body=io.BytesIO(spjson.encode('utf-8')),
                        Bucket=outbucket, Key=speedsdekey, ContentType="application/json")
                    if not (s3result['ResponseMetadata'] is not None and
                            s3result['ResponseMetadata']['HTTPStatusCode'] is not None and
                            s3result['ResponseMetadata']['HTTPStatusCode'] == 200):
                        print("\nError writing to S3 " + speedsdekey + " ", s3result['ResponseMetadata']['HTTPStatusCode'], flush=True)
                    # generate the overhead videos if all good.  Use a new process for each.
                    if not args.novideo and newcalibrationthissde and srid in corrected_calibrations:
                        mpool.apply_async(generate_videos_list, ([ns[0] for ns in newspeedsde], srid, tmpdir, calib,
                                                            corrected_calibrations[srid], outbucket,
                                                            args.offset, dist))
            # all done!!!
            # Save off what we worked on.
            if srid_to_hash_changed:
                try:
                    cmpnyjson = json.dumps(srid_to_hash)
                    s3result = s3cl.put_object(
                        Body=io.BytesIO(cmpnyjson.encode('utf-8')),
                        Bucket=outbucket, Key=COMPANYHASHMAP, ContentType="application/json")
                    if not (s3result['ResponseMetadata'] is not None and
                            s3result['ResponseMetadata']['HTTPStatusCode'] is not None and
                            s3result['ResponseMetadata']['HTTPStatusCode'] == 200):
                        print("\nError writing to S3 ", s3result['ResponseMetadata']['HTTPStatusCode'], flush=True)
                except Exception as ex:
                    print("Saving last "+COMPANYHASHMAP+": "+str(ex), flush=True)
                    pass
            if corrected_cal_changed:
                try:
                    cor_cal = json.dumps(corrected_calibrations)
                    s3result = s3cl.put_object(
                        Body=io.BytesIO(cor_cal.encode('utf-8')),
                        Bucket=outbucket, Key='corrected_calibrations.json', ContentType="application/json")
                    if not (s3result['ResponseMetadata'] is not None and
                            s3result['ResponseMetadata']['HTTPStatusCode'] is not None and
                            s3result['ResponseMetadata']['HTTPStatusCode'] == 200):
                        print("\nError writing to S3 ", s3result['ResponseMetadata']['HTTPStatusCode'], flush=True)
                except Exception as ex:
                    print("Saving last corrected_calibrations.json: "+str(ex), flush=True)
                    pass

            try:
                s3result = s3cl.put_object(
                    Body=io.BytesIO(newlastdate.encode('utf-8')),
                    Bucket=outbucket, Key=LASTPROCESSED)
                if not (s3result['ResponseMetadata'] is not None and
                        s3result['ResponseMetadata']['HTTPStatusCode'] is not None and
                        s3result['ResponseMetadata']['HTTPStatusCode'] == 200):
                    print("\nError writing to S3 ", s3result['ResponseMetadata']['HTTPStatusCode'], flush=True)
            except Exception as ex:
                print("Saving last processed: "+str(ex), flush=True)
                pass
        except  Exception as ex:
            print("Error from boto3 "+str(ex), flush=True)
        mpool.close()
        mpool.join()
    return None


def generate_videos_list(sdepathlist, srid, tmpdir, calib, corrected_calib, outbucket, offset, dist):
    """
    Generate videos from a list of SDE files.
    :param sdepathlist: List of SDE
    :param srid: The SRID for these sde.
    :param tmpdir: Where to put temp files.
    :param calib: The old calibration.
    :param corrected_calib: The corrected Claibration
    :param outbucket: Bucket the files go into
    :param offset: Video offset for drawing data.
    :param dist: Overhead distance to display
    :return: None
    """
    #Out with the old
    bucket = boto3.resource('s3').Bucket(outbucket)
    bucket.objects.filter(Prefix="videos/" + srid).delete()
    bucket.objects.filter(Prefix="corrected_videos/" + srid).delete()
    #In with the new.
    for sdepath in sdepathlist:
        generate_videos(sdepath, srid, tmpdir, calib, corrected_calib, outbucket, offset, dist)
    return None


def generate_videos(sdepath, srid, tmpdir, calib, corrected_calib, outbucket, offset, dist):
    """
    Generate overhead video for single SDE.
    :param sdepath: The SDE file in the bucket.
    :param srid: The SRID for these sde.
    :param tmpdir: Where to put temp files.
    :param calib: The old calibration.
    :param corrected_calib: The corrected Claibration
    :param outbucket: Bucket the files go into
    :param offset: Video offset for drawing data.
    :param dist: Overhead distance to display
    :return: None
    """
    sp_name = sdepath.split('/')
    sdebucket = sp_name[0]
    sdekey = "/".join(sp_name[1:])
    tmpsde = os.path.join(tmpdir, sp_name[-1])
    tmpout = os.path.join(tmpdir, sp_name[-1].split('.')[0] + ".webm")
    if not os.path.exists(tmpsde):
        try:
            s3cl.download_file(Bucket=sdebucket, Key=sdekey, Filename=tmpsde)
        except Exception as ex:
            print("Error processing" + sdekey + " " + str(ex), flush=True)

    if not os.path.exists(tmpsde):
        print("Missing SDE ",tmpsde, flush=True)
        if not os.path.exists(tmpdir):
            print("Missing tmpdir", flush=True)
        return None
    sde = SdeTarSr4(tmpsde)
    tmpvid = None
    if sde.have_avro():
        vidfn = sde.get_video_filename('Forward')
        tmpvid = os.path.join(tmpdir, vidfn)
        vidkey = "/".join(sp_name[1:-1]) + "/" + vidfn
        outvidkey = "videos/" + srid + "/" + "-".join(sp_name[2:-1]) + "/" + sp_name[-1].split('.')[
            0] + ".webm"
        try:
            s3cl.download_file(Bucket=sdebucket, Key=vidkey,
                               Filename=tmpvid)
        except Exception as ex:
            print("Error processing" + sdepath + " " + str(ex), flush=True)

        if os.path.exists(tmpvid):

            generate_dual_view(tmpvid, sde, calib, tmpout, offset, overhead_feet=dist, debug=False)
            with open(tmpout, mode='rb') as file:
                filebytes = file.read()
            s3cl.put_object(Bucket=outbucket, Key=outvidkey, Body=io.BytesIO(filebytes),
                            ContentType='video/webm')
            if os.path.exists(tmpout):
                os.remove(tmpout)
            outvidkey = "corrected_" + outvidkey
            generate_dual_view(tmpvid, sde, corrected_calib, tmpout, args.offset,
                               overhead_feet=dist, debug=False)
            with open(tmpout, mode='rb') as file:
                filebytes = file.read()
            s3cl.put_object(Bucket=outbucket, Key=outvidkey, Body=io.BytesIO(filebytes),
                            ContentType='video/webm')
    sde.close()
    if os.path.exists(tmpsde):
        os.remove(tmpsde)
    if os.path.exists(tmpout):
        os.remove(tmpout)
    if tmpvid is not None and os.path.exists(tmpvid):
        os.remove(tmpvid)

    return None


def get_all_sde_from_hash(all_sde_list, srid_to_hash, s3cl):
    """
    From the company hash list get all teh SDE for the last two months.  This is only used if we are starting from
    nothing.
    :param all_sde_list:
    :param srid_to_hash:
    :param s3cl:
    :return:
    """
    if len(all_sde_list) == 0:
        return all_sde_list
    sp_name = all_sde_list[0].split('/')
    sdebucket = sp_name[0]
    new_all_sde_list = []
    hashlist = set(srid_to_hash.values())
    nowtime = datetime.datetime.now(datetime.timezone.utc)
    datelist = ["/"+str(nowtime.year)+"/"+str(nowtime.month).zfill(2)]
    if nowtime.month == 1:
        datelist.append("/"+str(nowtime.year-1)+"/12")
    else:
        datelist.append("/"+str(nowtime.year)+"/"+str(nowtime.month-1).zfill(2))
    for ahash in hashlist:
        for dte in datelist:
            pref_hash = ahash+dte
            continuetok = None
            while True:
                if continuetok is None:
                    try:
                        resp = s3cl.list_objects_v2(Bucket=sdebucket,Prefix=pref_hash)
                    except Exception as ex:
                        print("Error listing bucket" + str(ex), flush=True)
                        break
                else:
                    try:
                        resp = s3cl.list_objects_v2(Bucket=sdebucket, Prefix=pref_hash, ContinuationToken=continuetok)
                    except Exception as ex:
                        print("Error listing bucket" + str(ex), flush=True)
                        break
                if 'Contents' in resp:
                    for ent in resp['Contents']:
                        key = ent['Key']
                        ksrid = key.split('/')[-1][:8]
                        if ksrid in srid_to_hash:
                            if key.endswith(('SDEMD', 'sdemd')):
                                new_all_sde_list.append(sdebucket+"/"+key)
                if 'IsTruncated' in resp:
                    if resp['IsTruncated']:
                        continuetok = resp['NextContinuationToken']
                    else:
                        break
                else:
                    break
    return new_all_sde_list


def generate_debug_jpg(all_lanes, s3cl, outbucket, srid, sdefilename, calib):
    """
    Generate the debug info.  This is a jpg showing the lane lines found and the vanishing point.
    :param all_lanes: All the lane line paris.
    :param s3cl: boto3 client
    :param outbucket: output bucket.
    :param srid: The SRID
    :param sdefilename: The name of the last SDE file we processed.  Just used to anme the debug output.
    :param calib: The calibration.
    :return:
    """
    key = "debug/"+srid+"/"+sdefilename.split('.')[0]
    frame = np.zeros((720,1280,3), dtype=np.uint8)
    
    cv2.line(frame, (int(calib["int_cx"]), 0), (int(calib["int_cx"]), 720), (0, 255, 0), 1)
    
    
    for lane in all_lanes:
        lnpoly = np.vstack((lane[0][0], lane[0][1])).astype(np.int32).T
        cv2.polylines(frame, [lnpoly], False, (555, 255, 255), 1)
        lnpoly = np.vstack((lane[1][0], lane[1][1])).astype(np.int32).T
        cv2.polylines(frame, [lnpoly], False, (555, 255, 255), 1)
        x, y, good = intersectLines([lane[0][0][0], lane[0][1][0]], [lane[0][0][-1], lane[0][1][-1]],
                                    [lane[1][0][0], lane[1][1][0]], [lane[1][0][-1], lane[1][1][-1]])
        if good == 1:
            cv2.circle(frame, (int(x),int(y)), 2 ,(255,255,0))
    retval, jpgbuf = cv2.imencode(".jpg", frame)
    s3result = s3cl.put_object(Body=io.BytesIO(jpgbuf), Bucket=outbucket, Key=key+".jpg", ContentType='image/jpeg')

    lanjson = json.dumps(np.array(all_lanes).tolist())
    s3result = s3cl.put_object(Body=io.BytesIO(lanjson.encode('utf-8')), Bucket=outbucket, Key=key+".json", ContentType="application/json")

    caljson = json.dumps(calib)
    s3result = s3cl.put_object(Body=io.BytesIO(caljson.encode('utf-8')), Bucket=outbucket, Key=key+"_cal.json", ContentType="application/json")

    return None

'''


if __name__ == "__main__":
    #freeze_support()

    def coords(s):
        try:
            x, y = map(int, s.split(','))
            return x, y
        except:
            raise argparse.ArgumentTypeError("Coordinates must be x,y")

    # parse the arguments.
    parse = argparse.ArgumentParser()
    parse.add_argument("--sde", help="SDE file or directory to SDE files")
    parse.add_argument("--cal", help="Calibration json file")
    parse.add_argument("--ini", help="Calibration ini file (linux.ini)")
    parse.add_argument("--debug", action='store_true', default=False, help="Debug mode.")
    parse.add_argument("--novideo", action='store_true', default=False, help="Debug mode.")
    parse.add_argument("--verbose", action='store_true', default=False, help="Verbose in video mode.")
    parse.add_argument("--divx", action='store_true', default=False, help="Used DIVX encoder")
    parse.add_argument("--mp4v", action='store_true', default=False, help="Used MP4V encoder")
    parse.add_argument("--out", help="Output file, without this the output will have the same name as the input plus .webm")
    parse.add_argument("--dist",help='Overhead distance to display in feet', type=int, default=180)
    parse.add_argument("--loggrp", help='AWS Logs to use for input from bucket')
    parse.add_argument("--outbucket", help='AWS output bucket.')
    parse.add_argument("--offset", help="offset to lane lines x,y pixels", dest="offset", type=coords, default=(26,0))
    parse.add_argument("--lanetol", help="lane quadratic parameter max", type=float, default=0.0001)
    parse.add_argument("--speedmin", help="Minimum speed to use in kph.", type=float, default=80.0)
    parse.add_argument("--numlanes", help="number of lanes needed to auto calibrate", type=int, default=2000)
    parse.add_argument("--shortfollowingS1", help="short following S1 thresholds", type=float, default=0.7)
    parse.add_argument("--shortfollowingS2", help="short following S2 thresholds", type=float, default=1.5)
    parse.add_argument("--lanedeparture", help="lane departure xdist threshold", type=float, default=25)
    parse.add_argument("--lanelineconfidence", help="minimum confidence for a lane line", type=float, default=80)
    parse.add_argument("--laneslopemax", help="maximum slope of lane line in overhead view", type=float, default=0.06)
    parse.add_argument("--lanelinedistsec", help="minimum time that lane line extends forward for new in-lane Boolean", type=float, default=1.1)
    parse.add_argument("--maxinlanetime", help="maximum time that the in-lane Boolean is defined", type=float, default=2.0)
    parse.add_argument("--numframesSF", help="number of frames (0.25s each) to require SF true-positive", type=float, default=1.0)
    parse.add_argument("--numframesLDW", help="number of frames (0.25s each) to require LDW true-positive", type=float, default=1.0)
    parse.add_argument("--TTCmin", help="minimum TTC", type=float, default=3.0)
    parse.add_argument("--vehicleconfidence", help="minimum confidence for a vehicle for FCW trigger", type=float, default=80)
    

    args = parse.parse_args()

    if args.ini is not None:
        cal = make_cal_json(args.sde, args.ini)
        if cal is not None:
            args.cal = cal
        else:
            print("Bad " + args.ini, flush=True)
            sys.exit(0)

    dist = 180 if args.dist is None else int(args.dist)

    if args.sde is not None:
        if args.cal is not None and os.path.exists(args.cal):

            # get the transform to the road.
            with open(args.cal, 'r') as jscontent:
                jsonstr = jscontent.read()
            cal = json.loads(jsonstr)
        else:
            cal = None

        fourccstg = None
        if args.divx:
            fourccstg = ['D','I','V','X']
        if args.mp4v:
            fourccstg = ['M','P','4','V']
        if os.path.isfile(args.sde):
            outfile = args.out
            if outfile is None:
                outfile = args.sde[:args.sde.rfind('.')]+(".mp4" if fourccstg is not None else ".webm")
            sde = SdeTarSr4(args.sde)
            if sde.have_avro():
                vidinf = sde.get_video_filename('Forward')
                if sde.is_video_in():
                    ovf,vf = tempfile.mkstemp()
                    os.close(ovf)
                    # open(vf, 'wb').write((sde.get_file_bytes(vidinf['@name']).read()))
                    avf = open(vf,'w+b')
                    avf.write(sde.get_file_bytes(vidinf).read())
                    avf.close()
                    generate_dual_view(vf, sde, cal, outfile, args.offset, overhead_feet=dist,debug=args.debug, verbose=args.verbose, fourccstg=fourccstg)
                    os.remove(vf)
                else:
                    vidfile = os.path.join(os.path.split(args.sde)[0], vidinf)
                    generate_dual_view(vidfile, sde, cal, outfile, args.offset, overhead_feet=dist,debug=args.debug, verbose=args.verbose, fourccstg=fourccstg)
            sde.close()
            sys.exit(0)
        elif os.path.isdir(args.sde):
            print(args.sde, flush=True)
            for root, dirs, files in os.walk(args.sde):
                for fname in files:
                    if not fname.endswith((".SDE",".sde",".SDEMD",".sdemd")):
                        continue
                    sedfname = os.path.join(root, fname)
                    print(sedfname, flush=True)
                    outfile = sedfname[:sedfname.rfind('.')] + (".mp4" if fourccstg is not None else ".webm")
                    sde = SdeTarSr4(sedfname)
                    if not sde.have_avro():
                        sde.close()
                        continue
                    vidinf = sde.get_video_filename('Forward')
                    if sde.is_video_in():
                        ovf, vf = tempfile.mkstemp()
                        os.close(ovf)
                        avf = open(vf, 'w+b')
                        avf.write(sde.get_file_bytes(vidinf).read())
                        avf.close()
                        generate_dual_view(vf, sde, cal, outfile, args.offset, overhead_feet=dist,debug=args.debug, verbose=args.verbose, fourccstg=fourccstg)
                        os.remove(vf)
                    else:
                        vidfile = os.path.join(os.path.split(sedfname)[0], vidinf)
                        generate_dual_view(vidfile, sde, cal, outfile, args.offset, overhead_feet=dist,debug=args.debug, verbose=args.verbose, fourccstg=fourccstg)
                    sde.close()
            sys.exit(0)
    if args.loggrp is not None and args.outbucket is not None:
        process_bucket(args.loggrp, args.outbucket, dist, args)
        sys.exit(0)
    parse.print_help()
    sys.exit(0)



