import os
import sys
import argparse
import cv2
import math
import numpy as np
from ADAS.ADAS_projective_transform import AdasProjectiveTransform
from ADAS.camera_info import CameraInfo
from ADAS.banner_calibrate_class import BannerCalibrate
from time import sleep
import copy


def map_to_rotated_image(yrad, xo, yo, cx, cy, fx):
    return [(np.tan((-yrad + np.arctan2(xo-cx, fx))) * fx) + cx, yo]

if __name__ == "__main__":
    parse = argparse.ArgumentParser()
    parse.add_argument("--vf",help="Video File")
    parse.add_argument("--ya", help="Y rotation in degrees")
    args = parse.parse_args()

    if args.vf is None or args.ya is None:
        print("No Arguments match")
        parse.print_help(sys.stderr)
        sys.exit(1)

    # get transformation
    int_fx = 1573.33
    int_cx = 675.5
    int_cy = 419.0

    yrads = np.radians(float(args.ya))

    # findhomography
    halfcx = int_cx/2
    halfcy = int_cy/2
    qcx = halfcx/2
    srcpts = []
    dstpts = []
    for icy in [int_cy-halfcy, int_cy+halfcy]:
        srcpts.append([int_cx-halfcx,icy])
        dstpts.append(map_to_rotated_image(yrads,int_cx-halfcx,icy,int_cx,int_cy,int_fx))
        srcpts.append([int_cx+halfcx,icy])
        dstpts.append(map_to_rotated_image(yrads,int_cx+halfcx,icy,int_cx,int_cy,int_fx))

    hrot, retval = cv2.findHomography(np.array(srcpts,np.float32),np.array(dstpts,np.float32))


    vcap = cv2.VideoCapture(args.vf)
    fourcc = cv2.VideoWriter_fourcc(*'MP4V')
    ret, frame = vcap.read()
    out = cv2.VideoWriter("out.mp4", fourcc, 5, (frame.shape[1]*2, frame.shape[0]))
    while True:
        ret, frame = vcap.read()
        if not ret:
            break
        warped = cv2.warpPerspective(frame,hrot,(frame.shape[1],frame.shape[0]))
        cv2.line(frame, (int(int_cx), 0), (int(int_cx), frame.shape[0]), (0, 255, 0), 1)
        cv2.line(warped, (int(int_cx), 0), (int(int_cx), frame.shape[0]), (0, 255, 0), 1)
        new_img = np.concatenate((frame, warped), axis=1)
        out.write(new_img)
        cv2.imshow("both",new_img)
        cv2.imshow("rotated",warped)
        cv2.imshow("orig",frame)
        cv2.waitKey(60)
    out.release()