import numpy as np
import json
import os


class CameraInfo:
    """
    Class to hold the camera calibration parameters
    """
    def __init__(self):
        self.int_fx = 0
        self.int_fy = 0
        self.int_cx = 0
        self.int_cy = 0
        self.ext_rx = 0
        self.ext_ry = 0
        self.ext_rz = 0
        self.ext_tx = 0
        self.ext_ty = 0
        self.ext_tz = 0

    def init(self,int_fx, int_fy, int_cx, int_cy, ext_rx, ext_ry, ext_rz, ext_tx, ext_ty, ext_tz):
        """
        Init with all the parameters.
        :param int_fx:
        :param int_fy:
        :param int_cx:
        :param int_cy:
        :param ext_rx:
        :param ext_ry:
        :param ext_rz:
        :param ext_tx:
        :param ext_ty:
        :param ext_tz:
        :return:
        """
        self.int_fx = int_fx
        self.int_fy = int_fy
        self.int_cx = int_cx
        self.int_cy = int_cy
        self.ext_rx = ext_rx
        self.ext_ry = ext_ry
        self.ext_rz = ext_rz
        self.ext_tx = ext_tx
        self.ext_ty = ext_ty
        self.ext_tz = ext_tz
        return self

    def toJson(self):
        """
        Return the json for this object.
        :return:
        """
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    def fromJson(self,js):
        """
        read this object from json.  Either a string or a file can be passed.
        :param js:
        :return:
        """
        jsonstr = js
        if os.path.isfile(js):
            with open(js,'r') as jscontent:
                jsonstr = jscontent.read()

        self.__dict__ = json.loads(jsonstr)
        return self
