import os
import sys
import argparse
import cv2
import numpy as np
from ADAS.ADAS_projective_transform import AdasProjectiveTransform
from ADAS.camera_info import CameraInfo
from ADAS.banner_calibrate_class import BannerCalibrate
from time import sleep
import copy

fivefeet = 1524.0
tenfeet = 3048.0
onefoot = 304.8
twofoot = 609.6
oneinch = 25.4
threefeet = 3*onefoot
twohalfft = 762.0


def get_banner_poly(ci):
    """
    Given the camera int and ext parameters creata a banner and map it to
    the image space.
    :param ci: Camera parameters
    :return: polygons black, white, red
    """
    bumpertobanner=(2*tenfeet+fivefeet)-ci.ext_tz

    pt = AdasProjectiveTransform(ci.int_fx, ci.int_fy, ci.int_cx, ci.int_cy, ci.ext_rx, ci.ext_ry, ci.ext_rz, ci.ext_tx, ci.ext_ty, ci.ext_tz)
    retinv, ptinvert = cv2.invert(pt.get_image_to_road(), flags=cv2.DECOMP_LU)
    print(pt.get_image_to_road())
    # first the black background.
    blk = np.array([[0-twohalfft,-bumpertobanner],[0-twohalfft,-bumpertobanner-tenfeet],[0+twohalfft,-bumpertobanner-tenfeet],[0+twohalfft,-bumpertobanner]])
    polylst = []

    polylst.append(map_poly(whitebloc(blk[0] - np.array([-onefoot, 2 * twofoot])), ptinvert))
    polylst.append(map_poly(whitebloc(blk[1] - np.array([-onefoot, -twofoot])), ptinvert))
    polylst.append(map_poly(whitebloc(blk[2] - np.array([onefoot + onefoot, -twofoot])), ptinvert))
    polylst.append(map_poly(whitebloc(blk[3] - np.array([onefoot + onefoot, 2 * twofoot])), ptinvert))
    red = []
    red.append(map_poly(np.array([[-12.7, -bumpertobanner], [-12.7, -bumpertobanner - tenfeet], [12.7, -bumpertobanner - tenfeet], [12.7, -bumpertobanner]]), ptinvert))
    red.append(map_poly(np.array([[0 - oneinch, -bumpertobanner - twofoot], [0 + oneinch, -bumpertobanner - twofoot], [0 + oneinch, -bumpertobanner - twofoot], [0 - oneinch, -bumpertobanner - twofoot]]), ptinvert))
    red.append(map_poly(np.array([[0 - oneinch, -bumpertobanner - tenfeet + twofoot], [0 + oneinch, -bumpertobanner - tenfeet + twofoot], [0 + oneinch, -bumpertobanner - tenfeet + twofoot], [0 - oneinch, -bumpertobanner - tenfeet + twofoot]]), ptinvert))
    return np.around(np.array(map_poly(blk, ptinvert))), np.around(np.array(polylst)), np.around(np.array(red)), bumpertobanner + twofoot + ci.ext_tz

def whitebloc(upperleft):
    return np.array([[upperleft[0],upperleft[1]],[upperleft[0]+onefoot,upperleft[1]],[upperleft[0]+onefoot,upperleft[1]+twofoot],[upperleft[0],upperleft[1]+twofoot]])

def map_poly(poly, ptinvert):
    ph = np.append(poly,np.full((poly.shape[0],1),1),axis=1).transpose()
    tp = np.dot(ptinvert,ph)
    mapped = (tp[0:2]/tp[2]).transpose()
    return mapped

def process_one(vid, blur, blk, wht, red, debug):
    """
    Process one frame from the video and draw the banner on it.
    :param vid:
    :param blk:
    :param wht:
    :param red:
    :param debug:
    :return:
    """
    frame = None
    if vid is not None:
        vcap = cv2.VideoCapture(vid)

        while True:
            ret,frame = vcap.read()
            if not ret:
                break
            # draw the background.
            cv2.fillConvexPoly(frame,blk.astype(np.int32),(0,0,0))
            for iban in range(0,len(wht)):
                cv2.fillConvexPoly(frame, wht[iban].astype(np.int32), (255, 255, 255))
            for iban in range(0,len(red)):
                cv2.fillConvexPoly(frame, red[iban].astype(np.int32), (0, 0, 255))
            # cv2.line(frame,(int(frame.shape[1]/2),0),(int(frame.shape[1]/2),frame.shape[0]),(0,255,0),thickness=1)
            if blur != 0:
                frame = cv2.blur(frame,(blur,blur))
            if debug:
                cv2.imshow("frame",frame)
                cv2.waitKey(60)

            break
        vcap.release()
    return frame


if __name__ == "__main__":
    parse = argparse.ArgumentParser()
    parse.add_argument("--out", help="Output File")
    parse.add_argument("--jpg", help="jpeg file")
    parse.add_argument("--png", help="png file")
    parse.add_argument("--bmp", help="png file")
    parse.add_argument("--int", help="Camera int param")
    parse.add_argument("--bump", help="dist to bumper and x offset")
    parse.add_argument("--white", help="White limit (220)")
    parse.add_argument("--red", help="red limit (50)")
    parse.add_argument("--pixtol", help="pixel tolerence (5)")
    parse.add_argument("--blur", help="debugging blur size")
    parse.add_argument("--debug", action='store_true' ,default=False, help="debugging if True")
    parse.add_argument("--mmdist", action='store_true' ,default=False, help="Show dist in meters not feet")
    parse.add_argument("--split", action='store_true', default=False, help="Ask for angle of split windshield")

    args = parse.parse_args()
    blur = 0
    if args.blur is not None:
        blur = int(args.blur)


    if args.jpg is not None or args.png is not None or args.bmp is not None:
        if args.int is not None:
            ints = args.int.split(',')
            fx = float(ints[0])
            cx = float(ints[1])
            cy = float(ints[2])
        else:
            fx = 1573.33
            cx = 675.5
            cy = 419.0
            print("int_fx: ",fx,"\nint_fy: ",fx,"\nint_cx: ",cx,"\nint_cy: ",cy)

        whitelimit = -1
        redlimit = -1
        pixtol = 5
        if args.white is not None:
            whitelimit=int(args.white)
        if args.red is not None:
            redlimit = int(args.red)
        if args.pixtol is not None:
            pixtol = int(args.pixtol)

        filepath = args.jpg
        fileext = ".jpg"
        if args.png is not None:
            filepath = args.png
            fileext = ".png"
        elif args.bmp is not None:
            filepath = args.bmp
            fileext = ".bmp"

        if os.path.exists(filepath):
            vframe = None
            last_h_error = 0
            last_error = 0
            while cv2.waitKeyEx(1000) != 99:
                if os.path.isdir(filepath):
                    files = os.listdir(filepath)
                    paths = [os.path.join(filepath, basename) for basename in files if basename.endswith(fileext)]
                    if len(paths) == 0:
                        print("No jpg found in: ",filepath)
                        sleep(5)
                        continue
                    newest = max(paths, key=os.path.getctime)
                    vframe = cv2.imread(newest)
                else:
                    vframe = cv2.imread(filepath)
                if blur != 0:
                    vframe = cv2.blur(vframe, (blur, blur))
                framegreen = vframe.copy()
                grey = cv2.cvtColor(framegreen, cv2.COLOR_BGR2GRAY)
                bc = BannerCalibrate(grey, 0, 0, fx, fx, cx, cy, whitelimit=whitelimit, redlimit=redlimit,
                                     pixtol=pixtol, debug=args.debug)
                if bc.imagepts is not None:
                    for xy in np.around(bc.imagepts).astype(np.int32):
                        cv2.circle(framegreen, (xy[0], xy[1]), 1, (0, 255, 0), thickness=2)
                cv2.line(framegreen,(int(cx),0),(int(cx),vframe.shape[0]),(0,255,0),1)
                cv2.imshow("banner",framegreen)
                if bc.mean_horizontal_error != last_h_error or bc.mean_error != last_error:
                    print("Mean Error ", bc.mean_error)
                    print("Horizontal Error ", bc.mean_horizontal_error)
                    print("Angle Error Degrees ", bc.mean_degrees_error)
                    last_error = bc.mean_error
                    last_h_error = bc.mean_horizontal_error
                # key = cv2.waitKey(0)
                # if key == 99:
                #     break
            if args.bump is not None:
                bumps = args.bump.split(',')
                bd = float(bumps[0])
                xo = float(bumps[1])
            else:
               try:
                   bd = float(input('Enter mm from bumper to horizontal line on banner: '))
                   xo = float(input('Enter mm offset from the center: '))
               except ValueError:
                   print("bad input")
                   sys.exit(-1)

            # grey = cv2.cvtColor(vframe, cv2.COLOR_BGR2YUV)[:, :, 0]
            grey = cv2.cvtColor(vframe,cv2.COLOR_BGR2GRAY)
            bc = BannerCalibrate(grey, bd, xo, fx, fx, cx, cy,whitelimit=whitelimit,redlimit=redlimit,pixtol=pixtol,debug=args.debug)
            if bc.imagepts is not None:
                framecpy = vframe.copy()
                for xy in np.around(bc.imagepts).astype(np.int32):
                    cv2.circle(framecpy,(xy[0],xy[1]),1,(0,255,0),thickness=2)
                cv2.imshow("banner", framecpy)
                cv2.waitKey(60)
            else:
                print("No Corners Found")

            print(bc)
            # see if we have a set of parameters to compare it to.
            cifile = os.path.join(filepath if os.path.isdir(filepath) else os.path.basename(filepath), "camerainfo.json")
            if os.path.exists(cifile):
                ci = CameraInfo().fromJson(cifile)
                pt = AdasProjectiveTransform(ci.int_fx, ci.int_fy, ci.int_cx, ci.int_cy, ci.ext_rx, ci.ext_ry,
                                             ci.ext_rz, ci.ext_tx, ci.ext_ty, ci.ext_tz)
                newrdpts = map_poly(bc.imagepts, pt.image_to_road)
                print("alternate error mm: ",np.mean(np.linalg.norm((bc.roadpts + np.array([0, bc.ext_tz])) - newrdpts, axis=1)))

            cv2.waitKey(0)
            # Now loop drawing lines 10 feet apart.
            if bc.status == 'OK':
                FORWARD_DIST = 3048
                dist_count = 10
                dist_mid = 8
                if args.mmdist:
                    FORWARD_DIST = 5000
                    dist_count = 6
                    dist_mid = 4
                SIDE_DIST = 1000
                starty = bc.ext_tz - FORWARD_DIST
                rdpts = []
                for i in range(0,dist_count):
                    rdpts.append([-SIDE_DIST, starty])
                    rdpts.append([SIDE_DIST, starty])
                    starty -= FORWARD_DIST
                rdpts.append([0,starty])
                rdpts.append([0,bc.ext_tz])
                bcpt = AdasProjectiveTransform(bc.int_fx, bc.int_fy, bc.int_cx, bc.int_cy, bc.ext_rx, bc.ext_ry, bc.ext_rz, bc.ext_tx, bc.ext_ty, bc.ext_tz)
                retinv,invbc = cv2.invert(bcpt.image_to_road,flags=cv2.DECOMP_LU)
                img10 = map_poly(np.array(rdpts),invbc).astype(np.int32)
                waittime=60
                while cv2.waitKey(waittime) == -1:
                    waittime=1000
                    if os.path.isdir(filepath):
                        files = os.listdir(filepath)
                        paths = [os.path.join(filepath, basename) for basename in files if basename.endswith(fileext)]
                        if len(paths) == 0:
                            print("No jpg found in: ", filepath)
                            sleep(5)
                            continue
                        newest = max(paths, key=os.path.getctime)
                        vframe = cv2.imread(newest)
                    else:
                        vframe = cv2.imread(filepath)
                    for i in range(0,len(img10),2):
                        cv2.line(vframe,(img10[i][0],img10[i][1]),(img10[i+1][0],img10[i+1][1]),(0,0,255) if i ==dist_mid else (0,255,0),1)
                    cv2.imshow("banner", vframe)

                while args.split:
                    z_rot_deg = float(input('Enter Z rotation in degrees, or 0 to quit: '))
                    if z_rot_deg == 0.0:
                        break
                    bccp = copy.deepcopy(bc)
                    bccp.add_camera_z_degrees(z_rot_deg)
                    print(bccp)
                    bcpt = AdasProjectiveTransform(bccp.int_fx, bccp.int_fy, bccp.int_cx, bccp.int_cy, bccp.ext_rx, bccp.ext_ry,
                                                   bccp.ext_rz, bccp.ext_tx, bccp.ext_ty, bccp.ext_tz)
                    retinv, invbc = cv2.invert(bcpt.image_to_road, flags=cv2.DECOMP_LU)
                    img10 = map_poly(np.array(rdpts), invbc).astype(np.int32)
                    if os.path.isdir(filepath):
                        files = os.listdir(filepath)
                        paths = [os.path.join(filepath, basename) for basename in files if basename.endswith(fileext)]
                        if len(paths) == 0:
                            print("No jpg found in: ", filepath)
                            sleep(5)
                            continue
                        newest = max(paths, key=os.path.getctime)
                        vframe = cv2.imread(newest)
                    else:
                        vframe = cv2.imread(filepath)
                    for i in range(0,len(img10),2):
                        cv2.line(vframe,(img10[i][0],img10[i][1]),(img10[i+1][0],img10[i+1][1]),(0,0,255) if i ==dist_mid else (0,255,0),1)
                    cv2.imshow("banner", vframe)
                    cv2.waitKey(60)

            sys.exit(0)
    else:
        print("No Arguments match")
        parse.print_help(sys.stderr)
        sys.exit(1)