import os
import sys
import numpy as np
import cv2
import glob
import argparse

# to capture the video for this on the SR4 run:
# wpcam dbgcap=rawyuv
# to stop the capture run
# wpcam dbgcap
# the file should appear in:
# /data/dump/cap_raw_yuv_?_?.yuv
# next run to break the video into png files
# ffmpeg -r 5 -pix_fmt yuv420p -video_size 1280x720 -i ..\cap_raw_yuv_1_4.yuv -r 1/1 frame%03d.png
# now go trhough and find 6-9 images where the checkerboard is not a translation or rotation in the same plane
# as any other image.
#
# do not run the video through here you will get bad results.

if __name__ == "__main__":
    parse = argparse.ArgumentParser()
    parse.add_argument("--dir",help="Directory of jpg images of checker board.")
    # parse.add_argument("--yuv",help="YUV Video images of checker board.")
    # parse.add_argument("--yuv_height", help="Height of yuv image",default=720, type=int)
    # parse.add_argument("--yuv_width", help="Width of yuv image", default=1280,type=int)
    parse.add_argument("--ccrow", help="Count inner corners in row")
    parse.add_argument("--cccol", help="Count inner corners in column")
    parse.add_argument("--display", action='store_true', default=False, help="Display if True")
    parse.add_argument("--max_good", help="Stop after this many good frames", default=200, type=int)
    parse.add_argument("--imgdiff", help="min difference in found corners between images", default=20, type=int)
    args = parse.parse_args()
    if (args.dir is None and args.yuv is None) or args.ccrow is None or args.cccol is None:
        parse.print_help(sys.stderr)
        sys.exit(-1)
    cbrow = int(args.ccrow)
    cbcol = int(args.cccol)
    # termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((cbrow*cbcol,3), np.float32)
    objp[:,:2] = np.mgrid[0:cbrow,0:cbcol].T.reshape(-1,2)
    # Arrays to store object points and image points from all the images.
    objpoints = [] # 3d point in real world space
    imgpoints = [] # 2d points in image plane.
    countgood = 0
    if args.dir is not None:
        imgpath = os.path.join(args.dir, '*.png')
        images = glob.glob(imgpath)
        for fname in images:
            img = cv2.imread(fname)
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            # Find the chess board corners
            ret, corners = cv2.findChessboardCorners(gray, (cbrow, cbcol), None)
            # If found, add object points, image points (after refining them)
            if ret:
                objpoints.append(objp)
                corners2 = cv2.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)
                imgpoints.append(corners2)
                # Draw and display the corners
                countgood += 1
                if args.display:
                    cv2.drawChessboardCorners(img, (13, 12), corners2, ret)
                    cv2.imshow('img', img)
                    cv2.waitKey(60)
            if countgood >= args.max_good:
                 break
    # else:
    #     yuvfile = open(args.yuv,mode='rb')
    #     sizeyuv = int(args.yuv_height*args.yuv_width*1.5)
    #     while True:
    #         bts = yuvfile.read(sizeyuv)
    #         if len(bts) <=0:
    #             break
    #         img = np.frombuffer(bts, dtype=np.uint8)
    #         gray = cv.cvtColor(img.reshape((-1, args.yuv_width)), cv.COLOR_YUV2GRAY_420,
    #                             dst=np.empty((args.yuv_height, args.yuv_width), dtype=np.uint8))
    #         img = cv.cvtColor(gray, cv.COLOR_GRAY2BGR)
    #         # Find the chess board corners
    #         ret, corners = cv.findChessboardCorners(gray, (cbrow,cbcol), None)
    #         # If found, add object points, image points (after refining them)
    #         if ret == True:
    #             corners2 = cv.cornerSubPix(gray,corners, (11,11), (-1,-1), criteria)
    #             isgood=True
    #             for i in range(0,len(imgpoints)):
    #                 diffs = imgpoints[i] - corners2
    #                 if np.mean(np.linalg.norm(diffs, axis=2)) < args.imgdiff:
    #                     isgood=False
    #                     break
    #                 if np.min(np.std(diffs,axis=0)) < args.imgdiff:
    #                     isgood=False
    #                     break
    #             if not isgood:
    #                 continue
    #             objpoints.append(objp)
    #             imgpoints.append(corners2)
    #             # Draw and display the corners
    #             countgood += 1
    #             if args.display:
    #                 cv.drawChessboardCorners(img, (13,12), corners2, ret)
    #                 cv.imshow('img', img)
    #                 cv.waitKey(60)
    #             if countgood >= args.max_good:
    #                 break
    #         else:
    #             if args.display:
    #                 cv.imshow('img', img)
    #                 cv.waitKey(60)

    if args.display:
        cv2.destroyAllWindows()

    print("Number good ", countgood)

    if countgood > 0:
        initguess = np.array([[1573.33, 0.0, 675.5],
                                [0.0, 1573.33, 419.0],
                                [0.0, 0.0, 1.0]])
        ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(np.array(objpoints), np.array(imgpoints), gray.shape[::-1],
                                                           initguess, None,
                                                           flags=cv2.CALIB_USE_INTRINSIC_GUESS | cv2.CALIB_FIX_ASPECT_RATIO | cv2.CALIB_ZERO_TANGENT_DIST |
                                                                 # cv.CALIB_FIX_K3|
                                                                 cv2.CALIB_FIX_K4 | cv2.CALIB_FIX_K5 | cv2.CALIB_FIX_K6,
                                                           criteria=(cv2.TERM_CRITERIA_COUNT | cv2.TERM_CRITERIA_EPS, 500, 0.001))
        print(mtx)
