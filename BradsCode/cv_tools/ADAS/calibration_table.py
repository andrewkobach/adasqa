import sys
import json
import boto3
import argparse
import io

if __name__ == "__main__":

    parse = argparse.ArgumentParser()
    parse.add_argument("--bucket", help="Output bucket")

    args = parse.parse_args()

    s3cl = boto3.client('s3')

    if args.bucket is None:
        parse.print_help()
        sys.exit()

    caldata = {}

    try:
        response = s3cl.get_object(Bucket=args.bucket, Key='corrected_calibrations.json')
        if 'Body' in response:
            lastprefix = response['Body'].read()
            caljson = lastprefix.decode("utf-8")
            caldata = json.loads(caljson)
    except Exception as ex:
        print("No corrected_calibrations.json", flush=True)

    data = []
    for key, val in caldata.items():
        vals = [key,val["diff"]/304.8,val["diff"],
                "<a href=\"videos/"+key+"/index.html\" target=\"_blank\">"+key+"</a>",
                "<a href=\"corrected_videos/"+key+"/index.html\" target=\"_blank\">"+key+"</a>",
                val["ext_rx"],val["ext_ry"],val["ext_rz"],val["ext_tx"],val["ext_ty"],val["ext_tz"],val["car_width"],val["int_fx"],val["int_fy"],val["int_cx"],val["int_cy"],val["timestamp"]]
        data.append(vals)

    data.sort(key=lambda x:x[1],reverse=True)

    table = "<table border=\"1\">\n"

    # Create the table's column headers
    header = ["SR ID","feet difference","mm difference","Original","Corrected","ext_rx","ext_ry","ext_rz","ext_tx","ext_ty","ext_tz","car_width","int_fx","int_fy","int_cx","int_cy","timestamp"]
    table += "  <tr bgcolor=cyan>\n"
    for column in header:
        table += "    <th>{0}</th>\n".format(column)
    table += "  </tr>\n"

    # Create the table's row data
    coloredrow = False
    for line in data[1:]:
        row = line
        if coloredrow:
            table += "  <tr bgcolor=yellow>\n"
            coloredrow = False
        else:
            table += "  <tr>\n"
            coloredrow = True
        for column in row:
            table += "    <td>{0}</td>\n".format(column)
        table += "  </tr>\n"

    table += "</table>"

    try:

        s3result = s3cl.put_object(
            Body=io.BytesIO(table.encode('utf-8')),
            Bucket=args.bucket, Key='corrected_calibrations.html', ContentType="text/html")
        if not (s3result['ResponseMetadata'] is not None and
                s3result['ResponseMetadata']['HTTPStatusCode'] is not None and
                s3result['ResponseMetadata']['HTTPStatusCode'] == 200):
            print("\nError writing to S3 ", s3result['ResponseMetadata']['HTTPStatusCode'], flush=True)
    except Exception as ex:
        print("Saving last corrected_calibrations.json: " + str(ex), flush=True)
        pass
