import math

import cv2
import numpy as np


class AdasProjectiveTransform:
    """
    This class takes the geometry of the camera and it's location in the vehicle and calculates a transform
    to the Road.  The resulting transform from pixel coordinates to the road has the origin directly below the
    camera pin hole.  The negative Y direction is forward of the vehicle and the right is to the negative X and
    left positive.
    """

    def __init__(self, int_fx, int_fy, int_cx, int_cy, ext_rx, ext_ry, ext_rz, ext_tx, ext_ty, ext_tz):
        """
        Calculate the projective transform from the input geometry
        :param int_fx: Camera Matrix fx in mm
        :param int_fy: Camera Matrix fy in mm
        :param int_cx: Camera Matrix cx in mm
        :param int_cy: Camera Matrix cy in mm
        :param ext_rx: Camera rotation about x axis in degrees
        :param ext_ry: Camera rotation about y axis in degrees
        :param ext_rz: Camera rotation about z axis in degrees
        :param ext_tx: Camera translation in x in mm
        :param ext_ty: Camera translation in y in mm
        :param ext_tz: Camera translation in z in mm
        """

        # Need to find 4 points on the road and the coresponding 4 on the image.
        # Initially ignore the ext_rz and ext_ry.  We will add those in later.
        # Pick a 2 points on the center line 10 meters and 20 meters ahead, find
        # the image points of these.  Then using these two planes find another point
        # on the road to the right 1 meter of the 10 meter center line point.  Then another
        # point 1 meter to the left of the 20 meter center line point.  Using similar triangles
        # find the image points (without rotations rz and ry)
        #
        img_pts = []
        dest_pts = []
        # 90 degrees rotated down.
        nintydeg = np.radians(-90)
        ten_metr = 10000
        cam_rot = np.radians(ext_rx)
        # find the angle from the center line to the 10 meter and 20 meter ahead points.
        angle_10_ahd = -math.atan2(abs(ext_tz) + ten_metr, abs(ext_ty))
        angle_20_ahd = -math.atan2(abs(ext_tz) + 2 * ten_metr, abs(ext_ty))
        # find the distance from the camera center to the image of the 10 meter ahead point.
        img_dst_10 = math.tan(-(nintydeg - (angle_10_ahd + cam_rot))) * int_fx
        # get the hypotenuse of this triangle for our offset point in pixel space
        img_dst_10_hyp = math.sqrt(int_fx * int_fx + img_dst_10 * img_dst_10)
        # get the hypotenuse of the triangle for our offset point in world coordinates.
        dest_10_hyp = math.sqrt(ext_ty * ext_ty + (abs(ext_tz) + ten_metr) ** 2)
        # find the distance from the camera center to the image of the 20 meter ahead point.
        img_dst_20 = math.tan(-(nintydeg - (angle_20_ahd + cam_rot))) * int_fx
        # get the hypotenuse of this triangle for our offset point in pixel space
        img_dst_20_hyp = math.sqrt(int_fx * int_fx + img_dst_20 * img_dst_20)
        # get the hypotenuse of the triangle for our offset point in world coordinates.
        dest_20_hyp = math.sqrt(ext_ty * ext_ty + (abs(ext_tz) + ten_metr + ten_metr) ** 2)
        # save off our 2 mapped points along the center plane.
        img_pts.append([int_cx, int_cy + img_dst_10])
        dest_pts.append([ext_tx, -(abs(ext_tz) + ten_metr)])
        img_pts.append([int_cx, int_cy + img_dst_20])
        dest_pts.append([ext_tx, -(abs(ext_tz) + 2 * ten_metr)])
        # now get the points offset to either side of the center plane
        # in pixel space
        off_x_10 = math.atan2(1000, dest_10_hyp) * img_dst_10_hyp
        off_x_20 = math.atan2(-1000, dest_20_hyp) * img_dst_20_hyp
        # ad these to our lists.
        img_pts.append([int_cx + off_x_10, int_cy + img_dst_10])
        dest_pts.append([ext_tx + 1000, -(abs(ext_tz) + ten_metr)])
        img_pts.append([int_cx + off_x_20, int_cy + img_dst_20])
        dest_pts.append([ext_tx - 1000, -(abs(ext_tz) + 2 * ten_metr)])
        # now get the transform from pixel space to teh road.
        itor = cv2.getPerspectiveTransform(np.array(img_pts, np.float32), np.array(dest_pts, np.float32))
        # need to add back in the rotations about z and y....
        # for y need to find homography to the Y rotation = 0 plane.
        # this is more complex.
        yrads = np.radians(ext_ry)

        def map_to_rotated_image(xo, yo):
            return [(np.tan((yrads + np.arctan2(xo - int_cx, int_fx))) * int_fx) + int_cx, yo]

        halfcx = int_cx / 2
        halfcy = int_cy / 2
        srcpts = []
        dstpts = []
        for icy in [int_cy - halfcy, int_cy + halfcy]:
            srcpts.append([int_cx - halfcx, icy])
            dstpts.append(map_to_rotated_image(int_cx - halfcx, icy))
            srcpts.append([int_cx + halfcx, icy])
            dstpts.append(map_to_rotated_image(int_cx + halfcx, icy))

        yrot, retval = cv2.findHomography(np.array(srcpts, np.float32), np.array(dstpts, np.float32))
        itor = np.dot(itor, yrot)
        # rotate about z using the right hand rule. translate to origin. No need to translate z
        zrads = np.radians(ext_rz)
        zcos = math.cos(zrads)
        zsin = math.sin(zrads)
        zrot = np.array([[zcos, -zsin, 0], [zsin, zcos, 0], [0, 0, 1]])
        zrot = np.dot(zrot, np.array([[1, 0, -int_cx], [0, 1, -int_cy], [0, 0, 1]]))
        zrot = np.dot(np.array([[1, 0, int_cx], [0, 1, int_cy], [0, 0, 1]]), zrot)
        itor = np.dot(itor, zrot)

        # Save everything off.
        # NOTE image_to_road HAS the ORIGIN DIRECTLY UNDER THE CAMERA!!!
        self.image_to_road = itor
        self.int_fx = int_fx
        self.int_fy = int_fy
        self.int_cx = int_cx
        self.int_cy = int_cy
        self.ext_rx = ext_rx
        self.ext_ry = ext_ry
        self.ext_rz = ext_rz
        self.ext_tx = ext_tx
        self.ext_ty = ext_ty
        self.ext_tz = ext_tz

    def get_image_to_road(self):
        """
        Return the projective transfer.  Note this
        transform is from pixel coordinates to mm on the road.  This has the origin
        at the bumper rather than under the camera.
        :return:
        """
        return np.dot(np.array([[1.0,0.0,0.0],[0.0,1.0,-self.ext_tz],[0.0,0.0,1.0]]),self.image_to_road)
