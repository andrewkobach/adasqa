#! /usr/bin/python
"""
Print out the homography from the image to the road using the calibration parameters
found in the linux.ini file.
"""

import argparse
import configparser
import sys
import os
from ADAS_projective_transform import AdasProjectiveTransform

# This is the default path.
DEFPATH = "/data/root/linux.ini"

if __name__ == "__main__":
    parse = argparse.ArgumentParser()
    parse.add_argument("--ini", help="Calibration ini file (linux.ini)")
    args = parse.parse_args()
    if args.ini is None:
        if os.path.isfile(DEFPATH):
            args.ini = DEFPATH
        else:
            parse.print_help()
            sys.exit(0)

    cfg = configparser.ConfigParser()
    cfg.read(args.ini)
    # this is the ini name that we look under for the claibration parameters.
    cal = dict(cfg['shengyang_1280x720_planar_camera_param'])
    cfgdctci = dict(cfg['CarInfo'])
    cal.update(cfgdctci)
    for keys in cal:
        cal[keys] = float(cal[keys])

    adast = AdasProjectiveTransform(cal["int_fx"], cal["int_fy"], cal["int_cx"], cal["int_cy"], cal["ext_rx"],
                                    cal["ext_ry"],
                                    cal["ext_rz"], cal["ext_tx"], cal["ext_ty"], cal["ext_tz"])
    t = adast.get_image_to_road()
    print(t)
    sys.exit(0)
