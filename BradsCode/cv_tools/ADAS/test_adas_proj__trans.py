import os
import argparse
from transform.PerspectiveTransInfo import PerspectiveTransInfo, pointhtrans
import numpy as np
import cv2
from ADAS.ADAS_projective_transform import AdasProjectiveTransform
from ADAS.camera_info import CameraInfo

"""
For the hard coded transform below run it on the video files found in the sub directories and
display the overhead view of the road.
"""

if __name__ == "__main__":
    parse = argparse.ArgumentParser()
    parse.add_argument("--dir",help="video files parent dir")
    args = parse.parse_args()

    #  ext_rx = -4.07
    # no affine -4.436002780914306
    # affine -4.445785156249998
    pt = None

    cinfo_file = os.path.join(args.dir,"camerainfo.json")
    if not os.path.exists(cinfo_file):
        pt = AdasProjectiveTransform(int_fx=1571, int_fy=1571, int_cx=680, int_cy=393, ext_rx=-4.07, ext_ry=0,
                                     ext_rz=0, ext_tx=0, ext_ty=-1464, ext_tz=-1630)
        print(pt.get_image_to_road())
        with open(cinfo_file, "w") as jf:
            jf.write(CameraInfo().init(int_fx = 1571, int_fy = 1571, int_cx = 680, int_cy = 393, ext_rx =-4.07, ext_ry = 0,
                ext_rz = 0, ext_tx = 0, ext_ty = -1464, ext_tz = -1630).toJson())
    else:
        ci = CameraInfo().fromJson(cinfo_file)
        pt = AdasProjectiveTransform(ci.int_fx, ci.int_fy, ci.int_cx, ci.int_cy, ci.ext_rx, ci.ext_ry,
                                        ci.ext_rz, ci.ext_tx, ci.ext_ty, ci.ext_tz)
        print(pt.get_image_to_road())

    if args.dir is not None:
        if args.dir and os.path.isdir(args.dir):
            basevd = args.dir
            onlydirs = [f for f in os.listdir(basevd) if os.path.isdir(os.path.join(basevd, f))]
            if len(onlydirs) > 0:
                for basedir in onlydirs:
                    basepath = os.path.join(basevd, basedir)
                    vidfile = os.path.join(basepath,"Video2.H264")
                    if os.path.exists(vidfile):

                        vcap = cv2.VideoCapture(vidfile)
                        ret,frame = vcap.read()
                        maxx = frame.shape[1]
                        maxy = frame.shape[0]
                        vcap.release()

                        newscale = maxy / 40000

                        scaletrans = np.array([[newscale, 0.0, 0.0],
                                               [0.0, newscale, 0.0], [0.0, 0.0, 1.0]])
                        # combine the scaling with the transform.
                        t = np.dot(scaletrans, pt.get_image_to_road())
                        # now translate to the center of the screen.
                        ptcentbot = pointhtrans([maxx / 2, maxy], t)
                        centbot = pointhtrans([maxx/2,maxy],pt.get_image_to_road())
                        centx = centbot[0]
                        bottom_offset = centbot[1]
                        twenty =  centbot[1] - 6096
                        justtrans = np.array([[1.0, 0.0, (maxx / 2) - ptcentbot[0]],
                                              [0.0, 1.0, (maxy) - ptcentbot[1]], [0.0, 0.0, 1.0]])
                        t = np.dot(justtrans, t)
                        totaltransscale = np.dot(justtrans, scaletrans)
                        sixfeet = 1828.8
                        tenfeet = 6096

                        vcap = cv2.VideoCapture(vidfile)
                        while True:
                            ret, frame = vcap.read()
                            if not ret:
                                break
                            # get the overhead view.
                            dst = cv2.warpPerspective(frame, t, (maxx, maxy))

                            # draw zigzag on overhead view for reference.
                            tshape = []
                            tshape.extend(
                                [[centx - sixfeet, twenty, 1], [centx - sixfeet, twenty - 10, 1], [centx + sixfeet, twenty, 1], [centx + sixfeet, twenty - 10, 1]])
                            extra_offset = 0
                            for k in range(0, 10):
                                tshape.append([centx - sixfeet, bottom_offset + extra_offset, 1])
                                extra_offset -= tenfeet
                                tshape.append([centx + sixfeet, bottom_offset + extra_offset, 1])
                            tview = np.dot(totaltransscale, np.array(tshape).transpose())
                            for k in range(0, len(tshape), 2):
                                cv2.line(dst, (int(tview[0, k]), int(tview[1, k])), (int(tview[0, k + 1]), int(tview[1, k + 1])),
                                         (0, 0, 255), 2)

                            cv2.imshow("overHead",dst)
                            cv2.waitKey(120)
                        vcap.release()


