import os
import fnmatch
import subprocess
import argparse

"""
convert the safety file to csv
"""

def bin2csv(tool_path, rootPath):
    pat1 = '*Safety.BIN'
    pat2 = '*Crash.BIN'
    pat3 = '*_halfhz.BIN'
    pat4 = '*_5hz.BIN'
    pat5 = '*_Combined.BIN'
    tool_name = 'bin2csv.exe'
    # schema_path = os.path.join(os.path.dirname(tool_path), 'Schemas')
    schema_path = os.path.join(tool_path, 'Schemas')
    for root, dirs, files in os.walk(rootPath):
        for filename in fnmatch.filter(files, pat1):
            filesize = os.path.getsize(os.path.join(root, filename))
            if filesize == 18400 or filesize == 27600:
                subprocess.check_call([os.path.join(tool_path, tool_name),
                                       os.path.join(schema_path, 'schema_safety_5.csv'),
                                       os.path.join(root, filename),
                                       os.path.join(root, filename[:-3]+'csv')])
            else:
                subprocess.check_call([os.path.join(tool_path, tool_name),
                                       os.path.join(schema_path, 'schema_safety_4.csv'),
                                       os.path.join(root, filename),
                                       os.path.join(root, filename[:-3]+'csv')])
            print('%scsv is created' % filename[:-3])
        for filename in fnmatch.filter(files, pat2):
            subprocess.check_call([os.path.join(tool_path, tool_name),
                                   os.path.join(schema_path, 'schema_crash.csv'),
                                   os.path.join(root, filename),
                                   os.path.join(root, filename[:-3]+'csv')])
            crash_data = pd.read_csv(os.path.join(root, filename[:-3]+'csv'), encoding="mbcs")
            del crash_data['z_crash']
            crash_data.to_csv(os.path.join(root, filename[:-3]+'csv'), index= False)
            print('%scsv is created with no z-axis' % filename[:-3])
        for filename in fnmatch.filter(files, pat3):
            subprocess.check_call([os.path.join(tool_path, tool_name),
                                   os.path.join(schema_path, 'schema_halfhz.csv'),
                                   os.path.join(root, filename),
                                   os.path.join(root, filename[:-3]+'csv')])
            print('%scsv is created' % filename[:-3])
        for filename in fnmatch.filter(files, pat4):
            subprocess.check_call([os.path.join(tool_path, tool_name),
                                   os.path.join(schema_path, 'schema_5hz.csv'),
                                   os.path.join(root, filename),
                                   os.path.join(root, filename[:-3]+'csv')])
            print('%scsv is created' % filename[:-3])
        for filename in fnmatch.filter(files, pat5):
            subprocess.check_call([os.path.join(tool_path, tool_name),
                                   os.path.join(schema_path, 'schema_velocity_combined.csv'),
                                   os.path.join(root, filename),
                                   os.path.join(root, filename[:-3]+'csv')])
            print('%scsv is created' % filename[:-3])
    return -1

def find_dirs_with_file(basedir):
    dirpaths = []
    for dirtpl in os.walk(basedir):
        if any("Safety.BIN" in s for s in dirtpl[2] ):
            dirpaths.append(dirtpl[0])
    return dirpaths


if __name__ == "__main__":
    parse = argparse.ArgumentParser()
    parse.add_argument("--dir",help="A directory")
    args = parse.parse_args()

    if args.dir and os.path.exists(args.dir):
        dirstocvt = find_dirs_with_file(args.dir)
        for adir in dirstocvt:
            bin2csv(r"C:\Users\bradr\Desktop\Workspace\bin2csv",adir)
