import argparse
from banner_calibrate_class import BannerCalibrate, parse_params, yuv_gray, bmp_gray, get_rys
# import cProfile, pstats, io


def main():
    parse = argparse.ArgumentParser()
    parse.add_argument("--out", help="Output File")
    parse.add_argument("--yuv", help="yuv image file")
    parse.add_argument("--yuv_height", help="Height of yuv image",default=720, type=int)
    parse.add_argument("--yuv_width", help="Width of yuv image", default=1280,type=int)
    parse.add_argument("--bmp", help="bmp image file")
    parse.add_argument("--params", help="string of json parameters")
    args = parse.parse_args()
    if (args.yuv is None and args.bmp is None) or args.params is None:
        print("Missing Arguments")
        parse.print_help()
        exit(-1)
    pd = parse_params(args.params)
    if 'ty' in pd:
        outstr = get_rys(pd, args.yuv_width, args.yuv_height)
    elif args.yuv is not None or args.bmp is not None:
        try:
            if args.yuv is not None:
                with open(args.yuv, mode='rb') as yuv:
                    grey = yuv_gray(yuv.read(), args.yuv_width, args.yuv_height)
            else:
                grey = bmp_gray(args.bmp)
            # pr = cProfile.Profile()
            # pr.enable()
            # for i in range(0,1000):
            bc = BannerCalibrate(grey, pd["bmm"], pd["co"], pd["fx"], pd["fy"] if "fy" in pd else pd["fx"], pd["cx"], pd["cy"], hint=pd["hint"] if "hint" in pd else None)
            # pr.disable()
            # pr.create_stats()
            # pr.print_stats()
            outstr = bc.to_json_str()
        except Exception as e:
            outstr = '{"status":"Exception"}'
    else:
        outstr = '{"status":"No File"}'
    if args.out is None:
        print(outstr)
    else:
        with open(args.out,"w") as fl:
            fl.write(outstr)
    exit(0)


if __name__ == "__main__":
    main()