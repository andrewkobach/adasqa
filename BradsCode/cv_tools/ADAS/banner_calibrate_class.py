import math
import cv2
import numpy as np
import json
from ADAS_projective_transform import AdasProjectiveTransform

distparm = np.array([-8.2944760069433592e+00, 6.9050435757954415e+02, 0., 0.,
              2.9883315472690225e+02, -7.8481132058431520e+00,
              6.8504116360425655e+02, 5.9495900344134816e+02])


class BannerCalibrate:
    """
    Class to get calibration parameters from a banner on the road.
    """

    def __init__(self, frame, bumpermm, center_offset, int_fx, int_fy, int_cx, int_cy, hint=None, whitelimit=-1,
                 pixtol=5, debug=False):
        """
        Find the calibration parameters from a Banner on the road.
        :param frame: An image of the banner on the road in grey scale.
        :param bumpermm: Distance in mm from the bumper to the banner mark.
        :param center_offset: Distance the camera is offset from the center of the vehicle
        :param int_fx: Camera intrinsic x focal length in pixels
        :param int_fy: Camera intrinsic y focal length in pixels
        :param int_cx: Camera intrinsic optical center x in pixels
        :param int_cy: Camera intrinsic optical center y in pixels
        :param hint: [minx, miny, maxx, maxy] hint on where to look.
        :param whitelimit: lower range for a white pixel
        :param pixtol: tolerence in pixels
        :param debug: True if debug is to be displayed.
        """
        orig_frame = frame.copy()
        self.bumpermm = -bumpermm
        self.center_offset = center_offset
        self.status = "No Corners Found"
        self.mean_error = -1
        self.mean_horizontal_error = 0
        self.mean_vertical_error = 0
        self.mean_degrees_error = 0
        self.imagepts = None
        self.int_fx = int_fx
        self.int_fy = int_fy
        self.int_cx = int_cx
        self.int_cy = int_cy
        self.ext_rx = 0
        self.ext_ry = 0
        self.ext_rz = 0
        self.ext_tx = 0
        self.ext_ty = 0
        self.ext_tz = 0
        self.pix_road_size = 0
        self.check_lines = None
        self.pixtol = pixtol

        # kernel for dilation.
        ker = np.ones((pixtol, pixtol), np.uint8)
        # Mask off center lower portion.
        pix_offset = int_cx - (frame.shape[1] / 2)
        xmin = int((frame.shape[1] / 3) + pix_offset)
        xmax = int((2 * frame.shape[1] / 3) + pix_offset)
        ymin = 0;
        ymax = frame.shape[0]
        if hint is not None:
            xmin = int(hint[0])
            xmax = int(hint[2])
            ymin = max(ymin,int(hint[1]))
            ymax = int(hint[3])
        gray = orig_frame.copy()
        gray[:, :xmin] = 0
        gray[:, xmax:] = 0
        gray[:ymin, :] = 0
        gray[ymax:, :] = 0
        # if no settings make up a threshold.
        th = whitelimit
        if whitelimit == -1:
            th, ret = cv2.threshold(gray[ymin:ymax, xmin:xmax], 0, 255,
                                    cv2.THRESH_BINARY + cv2.THRESH_OTSU)
            mxv = np.max(gray[ymin:ymax, xmin:xmax])
            whitelimit = int((th+th + mxv) / 3)
        # first filter on White
        _, frame_white = cv2.threshold(gray, whitelimit, 255, cv2.THRESH_BINARY)
        if debug:
            cv2.imshow("debug_calibrate", frame_white)
            cv2.waitKey(600)
        # Remove the erode and dialate.  Later on we are doing a least squares fit of a line
        # to the edges.  So no need to clean them up here and we get a better result.
        # erode and dilate to get rid of noise and find contours.
        # frame_white = cv2.dilate(cv2.erode(frame_white, ker), ker)
        # if debug:
        #     cv2.imshow("debug_calibrate_dilated", frame_white)
        #     cv2.waitKey(600)
        _, contours, _ = cv2.findContours(frame_white, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

        # go through and filter out obviously bad contours, and save the min rects.
        # make sure to keep both in order.
        minrects = []
        goodcontours = []
        for c in contours:
            if len(c) >= 4:
                rc, rwh, ra = cv2.minAreaRect(c)
                if rwh[0] > pixtol and rwh[1] > pixtol and rc[1] > ymin and xmin < rc[0] < xmax:
                    if abs(ra) < pixtol or abs(90 - abs(ra)) < pixtol:
                        minrects.append([rc, rwh, ra, rwh[0] * rwh[1], abs(rc[0] - self.int_cx)])
                        goodcontours.append(c.reshape((-1, 2)))
        # sort the results by the distance from the center.
        if len(minrects) == 0:
            if whitelimit != th:
                self.__init__(frame, bumpermm, center_offset, int_fx, int_fy, int_cx, int_cy, hint=hint, whitelimit=th,
                 pixtol=pixtol, debug=debug)
            else:
                self.status = "No Contours Found"
            return

        minrects = np.array(minrects)
        goodcontours = np.array(goodcontours)
        srt_by_ydist = np.argsort(-np.array(list(minrects[:, 0]))[:, 1])
        minrects = minrects[srt_by_ydist]
        goodcontours = goodcontours[srt_by_ydist]
        # find 4 rectangles about the same size and about the same distance from the center.
        # Start with the closest to the center and see if we can find a matcher.
        # must have a y center within pixtol and be with 5% the same size and within pixtol the same distance.
        indextbl = []
        idx = 0
        while idx < len(minrects):
            # go through and see if we can find matching pairs.
            curr = minrects[idx]
            for i in range(idx + 1, len(minrects)):
                if i in indextbl:
                    continue
                if abs(curr[0][1] - minrects[i, 0][1]) < 2*pixtol and \
                        abs(curr[3] - minrects[i, 3]) < (.2 * (curr[3] + minrects[i, 3])):
                    indextbl.append(idx)
                    indextbl.append(i)
                    break
            idx += 1
            while idx < len(minrects) and idx in indextbl:
                idx += 1
        # now we have pairs already sorted by closest to the center line.
        if len(indextbl) > 4:
            indextbl = indextbl[:4]
        minrects = minrects[indextbl]
        goodcontours = goodcontours[indextbl]
        # if len(minrects) > 4:
        #     # the first 4 are closest to the bottom.
        #     minrects = minrects[:4,:]
        #     goodcontours = goodcontours[:4,:]
        # # we have more than 4 so take the ones that are closest in size.
        # szdiff = np.argsort(np.abs(np.diff(minrects[:, 3])[::2]))
        # minrects = minrects[[2*szdiff[0], 1+2*szdiff[0], 2*szdiff[1], 1+2*szdiff[1]]]
        # goodcontours = goodcontours[[2*szdiff[0], 1+2*szdiff[0], 2*szdiff[1], 1+2*szdiff[1]]]

        # if we found enough set our bounds to look for corners.
        foundcontours = False
        if len(minrects) == 4:
            self.imagepts = self.__corners_from_all(goodcontours, minrects[:, :3])
            if self.__sanity_check_imagepts():
                foundcontours = True

        # if we did not find the contours the use the center line to set the limits.
        if not foundcontours:
            if whitelimit != th:
                self.__init__(frame, bumpermm, center_offset, int_fx, int_fy, int_cx, int_cy, hint=hint, whitelimit=th,
                              pixtol=pixtol, debug=debug)
            return

        if debug:
            framecpy = frame.copy()
            for xy in self.imagepts:
                cv2.circle(framecpy, (int(xy[0]), int(xy[1])), 1, (0, 0, 255), thickness=2)
            cv2.imshow("debug_calibrate", framecpy)
            cv2.waitKey(600)
            # cv2.imwrite("debugimage.bmp", framecpy)

        # Compensate for the red/green edge problem.
        # If this problem is ever fixed this next assignment should be removed.
        self.imagepts = self.imagepts+np.array(
            [[-1,-1],[+1,-1],[-1,-1],[+1,-1],
             [-1,+1],[+1,+1],[-1,+1],[+1,+1],
             [-1, -1], [+1, -1], [-1, -1], [+1, -1],
             [-1, +1], [+1, +1], [-1, +1], [+1, +1]])
        # for each corner point assign one from the banner.
        onefoot = 304.8
        twofoot = 609.6
        self.roadpts = np.array(
            [[-1.5 * onefoot, - 3 * twofoot], [-0.5 * onefoot, - 3 * twofoot],
             [0.5 * onefoot, - 3 * twofoot], [1.5 * onefoot, - 3 * twofoot],
             [-1.5 * onefoot, - 2 * twofoot], [-0.5 * onefoot, - 2 * twofoot],
             [0.5 * onefoot, - 2 * twofoot], [1.5 * onefoot, - 2 * twofoot],
             [-1.5 * onefoot, - 1 * twofoot], [-0.5 * onefoot, - 1 * twofoot],
             [0.5 * onefoot, - 1 * twofoot], [1.5 * onefoot, - 1 * twofoot],
             [-1.5 * onefoot, 0.0], [-0.5 * onefoot, 0.0], [0.5 * onefoot, 0.0],
             [1.5 * onefoot, 0.0], ])
        self.__get_geometry()

    def __corners_from_all(self, contours, bboxs):
        """
        Find the corners from all the rectangles.
        :param contours: Our contours.  Should be 4
        :param bboxs:  Our bounding boxe
        :return:
        """
        # first sort the bboxs so that we are ordered left to right, top to bottom.
        box_cents = np.array(list(bboxs[:, 0]))
        ordbyy = np.argsort(box_cents[:,1])
        ordtop = ordbyy[:2]
        ordbot = ordbyy[2:]
        bbsortord = []
        bbsortord.extend(ordtop[np.argsort(box_cents[ordtop,0])])
        bbsortord.extend(ordbot[np.argsort(box_cents[ordbot,0])])
        bbsrt = bboxs[bbsortord]
        consort = contours[bbsortord]
        # now for each bbox find the sides points in the contours.
        allsides = []
        for i in range(0, len(contours)):
            allsides.append(self.__get_ctur_sides(consort[i], bbsrt[i]))
        allsides = np.array(allsides)
        # fit the line for each horizontal line.
        horzlns = []
        pts = np.concatenate((consort[0][allsides[0] == 0], consort[1][allsides[1] == 0]))
        horzlns.append(np.polyfit(pts[:, 0], pts[:, 1], deg=1))
        pts = np.concatenate((consort[0][allsides[0] == 1], consort[1][allsides[1] == 1]))
        horzlns.append(np.polyfit(pts[:, 0], pts[:, 1], deg=1))
        pts = np.concatenate((consort[2][allsides[2] == 0], consort[3][allsides[3] == 0]))
        horzlns.append(np.polyfit(pts[:, 0], pts[:, 1], deg=1))
        pts = np.concatenate((consort[2][allsides[2] == 1], consort[3][allsides[3] == 1]))
        horzlns.append(np.polyfit(pts[:, 0], pts[:, 1], deg=1))
        # fit the line for each vertical line.
        vertlns = []
        pts = np.concatenate((consort[0][allsides[0] == 2], consort[2][allsides[2] == 2]))
        vertlns.append(np.polyfit(pts[:, 1], pts[:, 0], deg=1))
        pts = np.concatenate((consort[0][allsides[0] == 3], consort[2][allsides[2] == 3]))
        vertlns.append(np.polyfit(pts[:, 1], pts[:, 0], deg=1))
        pts = np.concatenate((consort[1][allsides[1] == 2], consort[3][allsides[3] == 2]))
        vertlns.append(np.polyfit(pts[:, 1], pts[:, 0], deg=1))
        pts = np.concatenate((consort[1][allsides[1] == 3], consort[3][allsides[3] == 3]))
        vertlns.append(np.polyfit(pts[:, 1], pts[:, 0], deg=1))

        # now find all 16 points in order.left to right and top to bottom.
        orderedpts = []
        for i in range(0, len(horzlns)):
            for j in range(0, len(vertlns)):
                orderedpts.append(self.__int_horz_vert(horzlns[i], vertlns[j]))

        return np.array(orderedpts)

    def __sanity_check_imagepts(self):
        """
        Do some sanity checks on the resulting image points.
        :return:
        """
        # check that we have 4 in each row with less than 5 degrees.
        rowavg = []
        for i in range(0, 16, 4):
            rowavg.append(np.mean(self.imagepts[i:i+4][:, 1]))
            if abs(math.atan2(np.ptp(self.imagepts[i:i+4][:, 1]),np.ptp(self.imagepts[i:i+4][:, 0]))) > 0.087266:
                self.status = "SA1"
                return False
                break
        # check that each row distance compresses from the bottom up in Y
        if (rowavg[3]-rowavg[2]+self.pixtol) < (rowavg[2]-rowavg[1]) or (rowavg[2]-rowavg[1]+self.pixtol) < (rowavg[1]-rowavg[0]):
            self.status = "SA2"
            return False
        # Check the horizontal distance decreases the further up we go.
        diffrow = np.diff(self.imagepts[:,0])
        for i in range(0,3):
            if diffrow[i] > diffrow[i+4]+self.pixtol or diffrow[i+4] > diffrow[i+8]+self.pixtol or diffrow[i+8] > diffrow[i+12]+self.pixtol:
                self.status = "SA3"
                return False
        return True

    def __get_ctur_sides(self, contour, bbox):
        """
        For a contour and bounding box find the side points
        :param contour:
        :param bbox:
        :return:
        """
        if abs(bbox[2]) < 10:
            topy = bbox[0][1] - (bbox[1][1] / 2)
            boty = bbox[0][1] + (bbox[1][1] / 2)
            leftx = bbox[0][0] - (bbox[1][0] / 2)
            rightx = bbox[0][0] + (bbox[1][0] / 2)
        else:
            topy = bbox[0][1] - (bbox[1][0] / 2)
            boty = bbox[0][1] + (bbox[1][0] / 2)
            leftx = bbox[0][0] - (bbox[1][1] / 2)
            rightx = bbox[0][0] + (bbox[1][1] / 2)
        # figure out which side each point is associated with.
        sides = np.argmin(np.column_stack((np.abs(contour[:, 1] - topy), np.abs(contour[:, 1] - boty),
                                   np.abs(contour[:, 0] - leftx), np.abs(contour[:, 0] - rightx))), axis=1)
        return sides

    def __int_horz_vert(self, ph, pv):
        """
        Calculate the intersection of a mostly horizontal line with a mostly vertical.
        :param ph: Horizontal line
        :param pv: Vertical line
        :return: the intersection point
        """
        y = (pv[1] * ph[0] + ph[1]) / (1 - ph[0] * pv[0])
        x = pv[0] * y + pv[1]
        return [x, y]

    def __get_geometry(self):
        """
        Now that we have the image points and the banner world coordinates find the transform parameters.
        :return:
        """
        # undistort the image
        cm = np.array([[self.int_fx, 0, self.int_cx], [0, self.int_fy, self.int_cy], [0.0, 0.0, 1.0]])
        centerpts = np.array(self.imagepts, dtype=np.float32)
        centerpts.shape = (-1, 1, 2)
        centerpts = np.ascontiguousarray(centerpts)
        undist_imagepts = cv2.undistortPoints(centerpts, cm, distparm, None, cm)
        undist_imagepts.shape = (-1,2)
        # undist_imagepts = self.imagepts
        self.__just_geom(self.roadpts, undist_imagepts)

        # get the resulting transform and calculate the mean error back to the road.
        maptrans = AdasProjectiveTransform(self.int_fx, self.int_fy, self.int_cx, self.int_cy, self.ext_rx, self.ext_ry,
                                           self.ext_rz, self.ext_tx, self.ext_ty, self.ext_tz)
        newrdpts = map_poly(undist_imagepts, maptrans.image_to_road)
        errorpts = (self.roadpts + np.array([0, self.ext_tz])) - newrdpts
        self.mean_error = np.mean(np.linalg.norm(errorpts, axis=1))
        self.mean_horizontal_error = np.mean(errorpts[:, 0])
        self.mean_vertical_error = np.mean(errorpts[:, 1])
        self.mean_degrees_error = np.degrees(np.arctan(np.polyfit(errorpts[:, 1], errorpts[:, 0], deg=1)[0]))
        self.status = "OK"
        # calculate pixel size
        centerpixel = np.mean(undist_imagepts, axis=0, dtype=np.int32)
        centpix_road = map_poly(np.array([centerpixel, centerpixel + np.array([1, 1])]), maptrans.image_to_road)
        self.pix_road_size = np.linalg.norm(np.diff(centpix_road, axis=0)[0])
        # calculate two horizontal lines for checking result.
        retinv, invbc = cv2.invert(maptrans.image_to_road, flags=cv2.DECOMP_LU)
        checkpts = []
        fourfoot = 1219.2
        halfwidth = 457.2
        twofoot = 609.6
        check_pt1 = self.ext_tz
        checkpts.append([-halfwidth+self.ext_tx,check_pt1])
        checkpts.append([halfwidth+self.ext_tx,check_pt1])
        checkpts.append([-halfwidth+self.ext_tx,check_pt1 - twofoot])
        checkpts.append([halfwidth+self.ext_tx,check_pt1 - twofoot])
        checkpts.append([-halfwidth+self.ext_tx,check_pt1 - fourfoot - twofoot])
        checkpts.append([halfwidth+self.ext_tx,check_pt1 - fourfoot - twofoot])
        di_pts = map_poly(np.array(checkpts), invbc)
        di_pts = distort(di_pts,cm,distparm)
        ci_pts = np.rint(di_pts).astype(int).tolist()
        self.check_lines = []
        self.check_lines.append([check_pt1,ci_pts[0][0],ci_pts[1][0],ci_pts[0][1],ci_pts[1][1]])
        self.check_lines.append([check_pt1 - twofoot, ci_pts[2][0], ci_pts[3][0], ci_pts[2][1],ci_pts[3][1]])
        self.check_lines.append([check_pt1 - fourfoot - twofoot, ci_pts[4][0], ci_pts[5][0], ci_pts[4][1], ci_pts[5][1]])
        return None

    def __just_geom(self, roadpts, imagepts):
        """
        Calculate the ext_ paramters from the road points and corresponding image points.
        :param roadpts:
        :param imagepts:
        :return: None
        """
        dst3d = np.ascontiguousarray(
            np.append(roadpts, np.zeros((roadpts.shape[0], 1), dtype=np.float32), axis=1).reshape(
                (roadpts.shape[0], 1, roadpts.shape[1] + 1)))

        img3d = np.ascontiguousarray(imagepts.astype(np.float32).reshape((imagepts.shape[0], 1, imagepts.shape[1])))

        retval, rotpnp, trpnp = cv2.solvePnP(dst3d, img3d, np.array(
            [[self.int_fx, 0, self.int_cx], [0, self.int_fx, self.int_cy], [0, 0, 1]]), distCoeffs=np.zeros((5, 1)),
                                             flags=cv2.SOLVEPNP_ITERATIVE)
        rotmatpnp, jacrot = cv2.Rodrigues(rotpnp)
        eulerpnp = self.__rotation_matrix_to_euler_degrees(rotmatpnp)
        cameraposition = np.dot((-rotmatpnp).transpose(), trpnp)
        # here are the parameters.  Note the X is flipped and rotated
        self.ext_rx = -90 - eulerpnp[0]
        self.ext_ry = eulerpnp[1]
        self.ext_rz = -eulerpnp[2]
        # assume the installer had the banner a little off and ignore the x translation.
        # self.ext_tx = self.center_offset - cameraposition.item(0)
        # self.ext_tx = self.center_offset
        self.ext_tx = cameraposition.item(0)
        self.ext_ty = cameraposition.item(2)
        self.ext_tz = -cameraposition.item(1)
        return None

    def __rotation_matrix_to_euler_degrees(self, r):
        """
        Calculates rotation matrix to euler angles in Degrees

        :param r:
        :return:
        """
        sy = math.sqrt(r[0, 0] * r[0, 0] + r[1, 0] * r[1, 0])
        s = math.sqrt(r[2, 1] * r[2, 1] + r[2, 2] * r[2, 2])
        singular = sy < 1e-6

        if not singular:
            x = math.atan2(r[2, 1], r[2, 2])
            y = math.atan2(-r[2, 0], s)
            z = math.atan2(r[1, 0], r[0, 0])
        else:
            x = math.atan2(-r[1, 2], r[1, 1])
            y = math.atan2(-r[2, 0], s)
            z = 0

        return np.degrees([x, y, z])

    def __str__(self):
        """
        Print out interesting stuff.
        :return:
        """
        return 'Status: {}\nerror mm = {}\nhorizontal error mm = {}\npixel size = {}\next_rx = {}' +\
               '\next_ry = {}\next_rz = {}\next_tx = {}\next_ty = {}\next_tz = {}\ncenter offset = {}\n'.format(
                   self.status, self.mean_error, self.mean_horizontal_error, self.pix_road_size,
                   self.ext_rx, self.ext_ry, self.ext_rz, self.ext_tx, self.ext_ty, self.ext_tz, self.center_offset)

    def add_camera_z_degrees(self, z_degrees):
        """
        Rotate the camera around the z-axis in world coordinates.
        :param z_degrees: Positive is a counter clockwise angle.
        :return: None, the ext_ parameters are modified.
        """
        # use negative degrees since we are rotating the points on the road
        # in the opposite direction.
        z_rads = np.radians(-z_degrees)
        z_cos = np.cos(z_rads)
        z_sin = np.sin(z_rads)
        # the new distance to the bumper from the camera is the cos time the old.
        new_ext_ty = self.ext_ty * z_cos
        rot_m = np.array([[z_cos, -z_sin], [z_sin, z_cos]])
        # calculate where the points should be in the new coordinate system of the road.
        # translate to the origin directly under the camera.  Rotate then translate back
        # to the new origin.
        newrdpts = np.dot(rot_m, (self.roadpts + np.array([0, self.ext_ty])).transpose()).transpose() - np.array(
            [0, new_ext_ty])
        self.__just_geom(newrdpts, self.imagepts)
        return None

    def to_json_str(self):
        """
        Get simplified json
        :return:
        """
        mydict = self.__dict__
        if "roadpts" in mydict:
            del mydict["roadpts"]
        if "imagepts" in mydict and mydict["imagepts"] is not None:
            mydict["imagepts"] = np.rint(self.imagepts).astype(int).tolist()
        if "pixtol" in mydict:
            del mydict["pixtol"]
        strjson = json.dumps(json.loads(json.dumps(mydict), parse_float=lambda x: round(float(x),2)))
        return strjson


def distort(points,cm, d):
    points.shape = (-1,1,2)
    normpts = cv2.undistortPoints(points, cm, None, None, None)
    normpts.shape = (-1,2)
    npts = []
    for pt in normpts:
        npts.append([[pt[0],pt[1],1.0]])
    npts = np.array(npts)
    imgpts, j = cv2.projectPoints(npts,(0,0,0),(0,0,0),cm,d)
    imgpts.shape = (-1,2)
    return imgpts


def map_poly(poly, ptinvert):
    """
    Map a polygon through a homogenous transform.
    :param poly: The polygon
    :param ptinvert: The transform
    :return: Mapped polygon.
    """
    ph = np.append(poly, np.full((poly.shape[0], 1), 1), axis=1).transpose()
    tp = np.dot(ptinvert, ph)
    mapped = (tp[0:2] / tp[2]).transpose()
    return mapped


def parse_params(params):
    """
    Turn json params into a dict.
    :param params:
    :return:
    """
    return json.loads(params)


def yuv_gray(yuvb, yuv_width, yuv_height):
    """
    Take a yuv image and turn it into an opencv gray scale image.
    :param yuvb:
    :param yuv_width:
    :param yuv_height:
    :return:
    """
    vframe = np.frombuffer(yuvb, dtype=np.uint8)
    grey = cv2.cvtColor(vframe.reshape((-1, yuv_width)), cv2.COLOR_YUV2GRAY_420,
                    dst=np.empty((yuv_height, yuv_width), dtype=np.uint8))
    # cframe = cv2.cvtColor(vframe.reshape((-1, yuv_width)), cv2.COLOR_YUV2BGR_I420,
    #                 dst=np.empty((yuv_height, yuv_width), dtype=np.uint8))
    # cv2.imshow("debug_yuv", cframe)
    # cv2.waitKey(600)
    # cv2.imwrite("image.bmp",cframe)
    # cv2.imwrite("image_grey.bmp",grey)
    return grey


def bmp_gray(bmpfile):
    """
    Turn a bmp file into an opencv gray scale image.
    :param bmpfile:
    :return:
    """
    vframe = cv2.imread(bmpfile)
    grey = cv2.cvtColor(vframe, cv2.COLOR_BGR2GRAY)
    return grey


def get_rys(pd, width, height):
    """
    Given the calibration parameters find the image lines for all ry
    between -5 and +5 degrees with a .05 increment.
    :param pd:
    :param width Image Width
    :param height Image Height
    :return: json list of [degree, x0,y0,x1,y1] where x and y are in image space
    """
    checkpts = []
    tenfoot = 3048.0
    onefoot = 304.8
    maptrans = AdasProjectiveTransform(pd["fx"], pd["fy"], pd["cx"], pd["cy"], pd["rx"], pd["ry"] ,
                                       pd["rz"], 0.0, pd["ty"], pd["tz"])
    check_pt = map_poly(np.array([[width/2.0,height]]), maptrans.image_to_road)
    check_pt1 = check_pt[0][1]
    checkpts.append([pd["tx"], check_pt1])
    checkpts.append([pd["tx"], check_pt1 - tenfoot])

    cm = np.array([[pd["fx"], 0, pd["cx"]], [0, pd["fy"], pd["cy"]], [0.0, 0.0, 1.0]])
    rotlist = []
    for ey in np.arange(-10.0,10.0,0.05,float):
        eyr = round(ey,2)
        maptrans = AdasProjectiveTransform(pd["fx"], pd["fy"], pd["cx"], pd["cy"], pd["rx"], pd["ry"]+eyr,
                                       pd["rz"], 0.0, pd["ty"], pd["tz"])
        retinv, invbc = cv2.invert(maptrans.image_to_road, flags=cv2.DECOMP_LU)
        ci_fpts = map_poly(np.array(checkpts), invbc)
        ci_fpts = distort(ci_fpts,cm,distparm)
        ci_pts = np.rint(ci_fpts).astype(int).flatten().tolist()
        ci_pts.insert(0,int(eyr*100))
        rotlist.append(ci_pts)
    rotations = {"rotate":rotlist}
    return json.dumps(json.loads(json.dumps(rotations), parse_float=lambda x: round(float(x),2)))
