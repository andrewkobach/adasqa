import numpy as np
from cv_utils.intersections import intersectLinefour
from cv_utils.MedianAbsDev import median_abs_dev
from .ADAS_projective_transform import AdasProjectiveTransform
from scipy.stats import ttest_ind
import cv2
import math


class ADAS_check_transform:

    def __init__(self, int_fx, int_fy, int_cx, int_cy, ext_rx, ext_ry, ext_rz, ext_tx, ext_ty, ext_tz, res_x, res_y):
        """
        Calculate the projective transform from the input geometry
        :param int_fx: Camera Matrix fx in mm
        :param int_fy: Camera Matrix fy in mm
        :param int_cx: Camera Matrix cx in mm
        :param int_cy: Camera Matrix cy in mm
        :param ext_rx: Camera rotation about x axis in degrees
        :param ext_ry: Camera rotation about y axis in degrees
        :param ext_rz: Camera rotation about z axis in degrees
        :param ext_tx: Camera translation in x in mm
        :param ext_ty: Camera translation in y in mm
        :param ext_tz: Camera translation in z in mm
        :param res_x: image resolution in the X direction
        :param res_y: image resolution in the Y direction
        """
        self.original_transform = AdasProjectiveTransform(int_fx, int_fy, int_cx, int_cy, ext_rx, ext_ry, ext_rz, ext_tx, ext_ty, ext_tz)
        self.new_tran = None
        self.res_x = res_x
        self.res_y = res_y
        self.all_van_pt = None

    def find_diff_transform(self, frame_segs):
        """
        Given a set of line segments find an affine mapping to
        improve the transform.
        :param frame_segs: List of tuples of left, right segments in 4 tuple format (x1, y1, x2, y2)
        :return: difference between the original and corrected transform.
        """
        # First need to get a list of potential vanishing points.
        self.van_pt_y = self.get_avg_vanish_pt_y(frame_segs)
        # calculate the rotation to get the new vanishing point.
        organg = math.atan2((-1/self.original_transform.image_to_road[2,1])-self.original_transform.int_cy,self.original_transform.int_fx)
        newang = math.atan2((self.van_pt_y) - self.original_transform.int_cy, self.original_transform.int_fx)
        angdiff = np.degrees(newang-organg)
        ot = self.original_transform
        self.new_tran = AdasProjectiveTransform(ot.int_fx, ot.int_fy, ot.int_cx, ot.int_cy, ot.ext_rx+angdiff, ot.ext_ry, ot.ext_rz, ot.ext_tx, ot.ext_ty, ot.ext_tz)
        return self.calc_diff()

    def get_rotation_correction(self):
        """
        Get the new rotation around the X axis.
        :return:
        """
        return self.new_tran.ext_rx

    def get_avg_vanish_pt_y(self,frame_segs):
        """
        From a list of frame segments return the mean
        vanishing point.
        :param frame_segs: Each frame has left and right segment lists.
        :return:
        """
        self.all_van_pt = []
        for frame_seg in frame_segs:
            # put the shorter side first.
            if len(frame_seg[0]) <= len(frame_seg[1]):
                self.all_van_pt.extend(self.get_frame_vpty(frame_seg[0],frame_seg[1]))
            else:
                self.all_van_pt.extend(self.get_frame_vpty(frame_seg[1], frame_seg[0]))
        self.all_van_pt = np.array(self.all_van_pt)
        vanpt_list_in = self.all_van_pt[~median_abs_dev(self.all_van_pt)]
        return np.mean(vanpt_list_in, axis=0)

    def get_frame_vpty(self,left_segs,right_segs):
        """
        For the single frame get the vanishing point Y
        :param left_segs: Left road lane lines
        :param right_segs: Right road lane lines
        :return:
        """
        yvals = []
        # only want ones where we have a right and left.
        if len(right_segs) > 0 and len(left_segs) > 0:
            # for the closest 1/2 of the left segments.
            for ls in left_segs[np.argsort(-left_segs[:,1])[0:int(len(left_segs)/2)],:]:
                # find the closest y valued right segment.
                rs = right_segs[np.argsort(np.abs(ls[1] - right_segs[:, 1]))[0]]
                x, y, v = intersectLinefour(ls, rs)
                if v == 1:
                    yvals.append(y)
        return [np.mean(yvals)] if len(yvals) > 0 else []

    def calc_diff(self):
        """
        Calculate the difference along the road between the
        new and old transforms.
        :return:
        """
        # now calculate the difference 10 meters out and 1 meter left from the bottom of the image.
        t_old = self.original_transform.get_image_to_road()
        retinv, t_old_invert = cv2.invert(t_old, flags=cv2.DECOMP_LU)
        ptcentbot = self.pointhtrans(np.array([ self.res_x/ 2, self.res_y]), t_old)
        # 10 meters ahead and 1 meter tot he left.
        ptcentbot[1] -= 10000
        ptcentbot[0] -= 1000
        ptimage = self.pointhtrans(ptcentbot, t_old_invert)
        pt_new = self.pointhtrans(ptimage, self.new_tran.get_image_to_road())
        diff = math.sqrt((pt_new[1] - ptcentbot[1]) ** 2 + (pt_new[0] - ptcentbot[0]) ** 2)
        return diff

    def get_p_value_halves(self):
        """
        For our Vanishing point Y data use the T Test to see if the mean of the first half is the
        same as the mean of the second half.
        :return: t statistic, probability
        """
        lenhalf = int(len(self.all_van_pt)/2)
        t, prob = ttest_ind(self.all_van_pt[0:lenhalf],self.all_van_pt[lenhalf:2*lenhalf],axis=0,equal_var=False)
        return t, prob, np.mean(self.all_van_pt[0:lenhalf]), np.mean(self.all_van_pt[lenhalf:2*lenhalf]), np.std(self.all_van_pt[0:lenhalf]), np.std(self.all_van_pt[lenhalf:2*lenhalf])

    def pointhtrans(self, pt, m):
        """
        Map a point though the perspective transform M
        :param pt:
        :param m:
        :return:
        """
        inpt = np.asarray([pt[0], pt[1], 1.0])
        outpt = np.dot(m, inpt)
        return np.asarray([outpt[0] / outpt[2], outpt[1] / outpt[2]])
