import os
import sys
import argparse
import cv2
import numpy as np
import math
import avro.schema
from avro.datafile import DataFileReader, DataFileWriter
from avro.io import DatumReader, DatumWriter
from ADAS.ADAS_check_transform import ADAS_check_transform
from ADAS.ADAS_projective_transform import AdasProjectiveTransform
from ADAS.camera_info import CameraInfo
from cv_utils.intersections import intersectLinefour
"""
Read in the lane lines and safety data from the avro files.  Call the code to analyse the data.
Display the videos with the found lane lines.  Finally print out the corrections to the transform.
"""

minspeed = 73

def process_one(avro, safetyfn, vid, tenscans, out, debug=True):
    reader = DataFileReader(open(avro, "rb"), DatumReader())
    allthings = list(reader)
    reader.close()
    # for thing in allthings:
    #     print(thing)
    #     laneres = thing['laneResult']
    #     if laneres is not None:
    #         lanes = laneres['lanes']
    #         if lanes is not None and len(lanes) > 0:
    #             print(lanes)
    #
    # print(len(allthings))
    # print("end")

    safety = read_safety_file(safetyfn)

    frame_segs = []

    for aidx in range(0, len(allthings)):
        safetyidx = int(aidx*(20/15))
        if safetyidx >= len(safety):
            safetyidx = len(safety)-1
        if safety[safetyidx,1] < minspeed:
            continue;
        lanes = allthings[aidx]['laneResult']['lanes']
        segs_left = []
        segs_right = []
        for lane in lanes:
            points = lane['points']
            if not (2 <= lane['position'] <=3):
                continue
            if lane['confidence'] < 75:
                continue
            isleft = lane['position'] <= 2
            if points is not None:
                xp = points['pointX']
                yp = points['pointY']
                if xp is not None and yp is not None:
                    for i in range(1, len(xp)):
                        if isleft:
                            segs_left.append([xp[i], yp[i], xp[i - 1], yp[i - 1]])
                        else:
                            segs_right.append([xp[i], yp[i], xp[i - 1], yp[i - 1]])
        if len(segs_left) > 0 and len(segs_right) > 0:
            frame_segs.append(np.array([np.array(segs_left),np.array(segs_right)]))

    if vid is not None:
        vcap = cv2.VideoCapture(vid)
        maxframeno= 0
        while True:
            ret, frame = vcap.read()
            if not ret:
                break
            maxframeno +=1
        print("Frames Video ",maxframeno)
        vcap.release()
        vcap = cv2.VideoCapture(vid)
        frameno=0

        while True:
            ret,frame = vcap.read()
            if not ret:
                break
            # find the first one after the next_time
            if tenscans is None:
                try:
                    idxlo = int(frameno * len(allthings) / maxframeno)
                    idxhi = min(int((frameno+1) * len(allthings) / maxframeno),len(allthings))

                    for idx in range(idxlo,idxhi):
                        lanes = allthings[idx]['laneResult']['lanes']
                        for lane in lanes:
                            points = lane['points']
                            if points is not None:
                                xp = points['pointX']
                                yp = points['pointY']
                                if xp is not None and yp is not None:
                                    for i in range(1,len(xp)):
                                        cv2.line(frame,(int(xp[i]),int(yp[i])),(int(xp[i-1]),int(yp[i-1])),(0,0,255),2)
                except:
                    pass
            if tenscans is not None:
                for sc in tenscans:
                    cv2.line(frame,tuple(sc[0]),tuple(sc[1]),(0,255,0),1)
            if debug:
                cv2.imshow("frame",frame)
                cv2.waitKey(60)
            if out is not None:
                out.write(frame)
            frameno +=1
        vcap.release()

    return frame_segs

def get_frame_vpty_lcl(left_segs,right_segs):
    """
    For the single frame get the vanishing point Y
    :param left_segs: Left road lane lines
    :param right_segs: Right road lane lines
    :return:
    """
    yvals = []
    # only want ones where we have a right and left.
    if len(right_segs) > 0 and len(left_segs) > 0:
        # for the closest 1/2 of the left segments.
        for ls in left_segs[np.argsort(-left_segs[:,1])[0:int(len(left_segs)/2)],:]:
            # find the closest y valued right segment.
            rs = right_segs[np.argsort(np.abs(ls[1] - right_segs[:, 1]))[0]]
            x, y, v = intersectLinefour(ls, rs)
            if v == 1:
                yvals.append(y)
    return np.mean(yvals) if len(yvals) > 0 else -1


def read_safety_file(fn):
    """
    Read the safety file into an np table,   Skip the headder.
    :param fn: file name
    :return:
    """
    # fid = open(fn, encoding='utf-8-sig')
    # safetyinfo = np.genfromtxt(io.BytesIO(fid.read().encode('ascii')),delimiter=',',usecols=(0,7),names=True)
    safetyinfo = np.genfromtxt(fn,delimiter=',',usecols=(0,7),skip_header=1, skip_footer=1)
    return safetyinfo


def get_ten_feet_scanlines(ci, maxy):
    pt = AdasProjectiveTransform(ci.int_fx, ci.int_fy, ci.int_cx, ci.int_cy, ci.ext_rx, ci.ext_ry, ci.ext_rz, ci.ext_tx, ci.ext_ty, ci.ext_tz)
    retinv, ptinvert = cv2.invert(pt.get_image_to_road(), flags=cv2.DECOMP_LU)
    scanlines = []
    mmtotenfeet = 3048
    offset = 0
    last = 0
    for i in range(0,12):
        yvl = np.around(pointhtrans([-mmtotenfeet/2, ci.ext_tz+(-mmtotenfeet * i)-offset], ptinvert)).astype(np.int)
        yvr = np.around(pointhtrans([mmtotenfeet/2,  ci.ext_tz+(-mmtotenfeet * i)-offset], ptinvert)).astype(np.int)
        if yvl[1] < maxy and yvl[1] > 0:
            if last == yvl[1]:
                break
            print(i,yvl[1])
            scanlines.append([yvl,yvr])
            last = yvl[1]
    return np.array(scanlines)


def pointhtrans(pt, m):
    """
    Map a point though the perspective transform M
    :param pt:
    :param m:
    :return:
    """
    inpt = np.asarray([pt[0], pt[1], 1.0])
    outpt = np.dot(m, inpt)
    return np.asarray([outpt[0] / outpt[2], outpt[1] / outpt[2]])



if __name__ == "__main__":
    parse = argparse.ArgumentParser()
    parse.add_argument("--af", help="avro file")
    parse.add_argument("--vf",help="video file")
    parse.add_argument("--dir",help="A directory")
    parse.add_argument("--tenfeet",help="Show 10 foot markers")
    args = parse.parse_args()

    fourcc = cv2.VideoWriter_fourcc(*'MP4V')
    if args.af is not None and args.vf is not None:
        process_one(args.af, args.vf)
    if args.dir is not None:
        if args.dir and os.path.isdir(args.dir):
            basevd = args.dir
            outvfile = os.path.join(basevd,"camout.mp4")
            if os.path.exists(outvfile):
                os.remove(outvfile)
            out = None
            cifile = os.path.join(basevd, "camerainfo.json")
            ci = None
            if os.path.exists(cifile):
                ci = CameraInfo().fromJson(cifile)
            else:
                ci = CameraInfo().init(int_fx=1571, int_fy=1571, int_cx=680, int_cy=393, ext_rx=-4.07, ext_ry=0,
                                       ext_rz=0, ext_tx=0, ext_ty=-1464, ext_tz=-1630)

            tenscans = None
            if args.tenfeet is not None:
                tenscans = get_ten_feet_scanlines(ci,720)

            onlydirs = [f for f in os.listdir(basevd) if os.path.isdir(os.path.join(basevd, f))]
            if len(onlydirs) > 0:
                allframe_segs = []
                for basedir in onlydirs:
                    basepath = os.path.join(basevd, basedir)
                    avrofile = os.path.join(basepath,"sde_laneDetection.avro")
                    safetyfile = os.path.join(basepath,"Safety.csv")
                    vidfile = os.path.join(basepath,"Video2.H264")
                    if os.path.exists(avrofile) and os.path.exists(vidfile) and os.path.exists(safetyfile):
                        print(avrofile,vidfile)
                        if out is None:
                            vcap = cv2.VideoCapture(vidfile)
                            ret, frame = vcap.read()
                            vcap.release()

                            out = cv2.VideoWriter(outvfile, fourcc, 20.0, (frame.shape[1], frame.shape[0]))
                        frame_segs = process_one(avrofile, safetyfile, vidfile, tenscans, out)
                        allframe_segs.extend(frame_segs)
                if out is not None:
                    out.release()

                if len(allframe_segs) > 0:
                    cktrans1 = ADAS_check_transform(ci.int_fx, ci.int_fy, ci.int_cx, ci.int_cy, ci.ext_rx, ci.ext_ry,
                                                                  ci.ext_rz, ci.ext_tx, ci.ext_ty, ci.ext_tz, res_x = 1280, res_y = 720)
                    print("diff exact dist ",cktrans1.find_diff_transform(allframe_segs))
                    print("correct ", cktrans1.get_rotation_correction())
                    print("T test P ",cktrans1.get_p_value_halves())

                    cktrans2 = ADAS_check_transform(ci.int_fx, ci.int_fy, ci.int_cx, ci.int_cy, ci.ext_rx-3, ci.ext_ry,
                                                                  ci.ext_rz, ci.ext_tx, ci.ext_ty, ci.ext_tz, res_x = 1280, res_y = 720)
                    print("diff down 3 deg dist ",cktrans2.find_diff_transform(allframe_segs))
                    print("correct ", cktrans2.get_rotation_correction())

                    cktrans = ADAS_check_transform(ci.int_fx, ci.int_fy, ci.int_cx, ci.int_cy, ci.ext_rx-6, ci.ext_ry,
                                                                  ci.ext_rz, ci.ext_tx, ci.ext_ty, ci.ext_tz, res_x = 1280, res_y = 720)
                    print("diff down 6 deg dist ",cktrans.find_diff_transform(allframe_segs))
                    print("correct ", cktrans.get_rotation_correction())

                    cktrans = ADAS_check_transform(ci.int_fx, ci.int_fy, ci.int_cx, ci.int_cy, ci.ext_rx+3, ci.ext_ry,
                                                                  ci.ext_rz, ci.ext_tx, ci.ext_ty, ci.ext_tz, res_x = 1280, res_y = 720)
                    print("diff up 3 deg dist ",cktrans.find_diff_transform(allframe_segs))
                    print("correct ", cktrans.get_rotation_correction())

                    cktrans = ADAS_check_transform(ci.int_fx, ci.int_fy, ci.int_cx, ci.int_cy, ci.ext_rx+6, ci.ext_ry,
                                                                  ci.ext_rz, ci.ext_tx, ci.ext_ty, ci.ext_tz, res_x = 1280, res_y = 720)
                    print("diff up 6 deg dist ",cktrans.find_diff_transform(allframe_segs))
                    print("correct ", cktrans.get_rotation_correction())

                    cktrans = ADAS_check_transform(ci.int_fx, ci.int_fy, ci.int_cx, ci.int_cy, ci.ext_rx, ci.ext_ry,
                                                                  ci.ext_rz, ci.ext_tx, ci.ext_ty, ci.ext_tz, res_x = 1280, res_y = 720)
                    print("corrected angle dist ",cktrans.find_diff_transform(allframe_segs))
                    print("correct ", cktrans.get_rotation_correction())
