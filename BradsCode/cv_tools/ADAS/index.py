import boto3
from boto3 import client
import bs4
import argparse

conn = client('s3')
s3 = boto3.resource('s3')


def getFilesAndFolderOfBucket(strBucket,strPrefix):

    sesFolder = conn.list_objects(Bucket=strBucket, Prefix=strPrefix, Delimiter='/')
    vecFiles = []
    vecFolders = []
    if (sesFolder.get('CommonPrefixes') != None):
        for key in sesFolder.get('CommonPrefixes'):
            if not key['Prefix'].endswith('//'):
                vecFolders.append(key['Prefix'])
    if (sesFolder.get('Contents')!=None):
        for key in sesFolder.get('Contents'):
            if not key['Key'].endswith('index.html'):
                vecFiles.append(key['Key'])

    return (vecFiles,vecFolders)

def uploadIndexFile(strBucket,strPrefix,strIndexFile):

    bucket = s3.Bucket(strBucket)
    keypre = strPrefix
    if not strPrefix.endswith('/'):
        keypre = ''
    bucket.upload_file(strIndexFile, keypre+strIndexFile,
                       ExtraArgs={'ContentType': 'text/html'})

def generateIndexFile(strBucket,strPrefix,strIndexFile,vecFiles,vecFolders,strTemplate):
    with open(strTemplate) as inf:
        txt = inf.read()
        soup = bs4.BeautifulSoup(txt,features="lxml")

    tagKeysList = soup.find("ul", {"id": "listkeys"})

    tagKeysList.append(generateHeader(soup, strBucket, strPrefix))

    for strFolder in vecFolders:
        strFolderLast = strFolder.split('/')[-2]
        tagKeysList.append(generateElement(soup, True, strFolderLast, '/' + strFolder + 'index.html'))

    for strFile in vecFiles:
        strFileLast = strFile.split('/')[-1]
        tagKeysList.append(generateElement(soup, False, strFileLast, '/' + strFile))

    with open(strIndexFile, "w") as outf:
        outf.write(str(soup))

def recPopulateIndexFiles(strBucket,strPrefix,strTemplate, toplevel):
    (vecFiles, vecFolders) = getFilesAndFolderOfBucket(strBucket, strPrefix)
    generateIndexFile(strBucket, strPrefix, strIndexFile, vecFiles, vecFolders,strTemplate)
    uploadIndexFile(strBucket, strPrefix, strIndexFile)
    for strFolder in vecFolders:
        recPopulateIndexFiles(strBucket, strFolder,strTemplate, False)

def generateElement(soup,flagIsFolder,strText,strURL):
    tagLi = soup.new_tag("li", **{'class': 'collection-item'})
    tagDiv = soup.new_tag("div", **{'class': 'valign-wrapper'})
    tagI = soup.new_tag("i", **{'class': 'material-icons iconitem'})
    if (flagIsFolder):
        tagI.string = 'folder_open'
    else:
        tagI.string = 'insert_drive_file'
    tagA = soup.new_tag("a", href=strURL)
    tagA.string = strText
    tagDiv.append(tagI)
    tagDiv.append(tagA)
    tagLi.append(tagDiv)
    return tagLi

def generateHeader(soup,strBucket,strPrefix):
    tagHeader = soup.new_tag("li", **{'class': 'collection-header'})
    tagH = soup.new_tag("h4")
    # tagH.string = 's3://' + strBucket + '/' + strPrefix
    lvls = strPrefix.split('/')
    for i in range(0,len(lvls)-1):
        atags = "/"+lvls[i]
        href = "/"+"/".join(lvls[0:i+1])+'/index.html'
        tagA = soup.new_tag("a", href=href)
        tagA.string = atags
        tagH.append(tagA)
    tagHeader.append(tagH)
    return tagHeader

# strBucket = 'sdsidev.int.adas.videos'
# strPrefix = 'videos'
strIndexFile = 'index.html'
# strTemplate = 'index_template.html'

if __name__ == "__main__":

    # parse the arguments.
    parse = argparse.ArgumentParser()
    parse.add_argument("--bucket", help="bucket")
    parse.add_argument("--prefix", help="prefix")
    parse.add_argument("--template", help="template file")
    args = parse.parse_args()

recPopulateIndexFiles(args.bucket,"",args.template, True)
