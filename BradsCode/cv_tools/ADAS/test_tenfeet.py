import os
import sys
import argparse
import cv2
import numpy as np
import math
import avro.schema
from avro.datafile import DataFileReader, DataFileWriter
from avro.io import DatumReader, DatumWriter
from ADAS.ADAS_check_transform import ADAS_check_transform
from ADAS.ADAS_projective_transform import AdasProjectiveTransform
from ADAS.camera_info import CameraInfo
from cv_utils.intersections import intersectLinefour


minspeed = 73

def process_one(vid, tenscans, out, debug=True):
    if vid is not None:
        vcap = cv2.VideoCapture(vid)

        while True:
            ret,frame = vcap.read()
            if not ret:
                break
            # find the first one after the next_time
            if tenscans is not None:
                for sc in tenscans:
                    cv2.line(frame,tuple(sc[0]),tuple(sc[1]),(0,255,0),1)
            if debug:
                cv2.imshow("frame",frame)
                cv2.waitKey(60)
            if out is not None:
                out.write(frame)
        vcap.release()

    return None


def get_ten_feet_scanlines(ci, maxy):
    pt = AdasProjectiveTransform(ci.int_fx, ci.int_fy, ci.int_cx, ci.int_cy, ci.ext_rx, ci.ext_ry, ci.ext_rz, ci.ext_tx, ci.ext_ty, ci.ext_tz)
    retinv, ptinvert = cv2.invert(pt.get_image_to_road(), flags=cv2.DECOMP_LU)
    scanlines = []
    mmtotenfeet = 3048
    offset = 0
    last = 0
    for i in range(0,12):
        yvl = np.around(pointhtrans([-mmtotenfeet/2, ci.ext_tz+(-mmtotenfeet * i)-offset], ptinvert)).astype(np.int)
        yvr = np.around(pointhtrans([mmtotenfeet/2,  ci.ext_tz+(-mmtotenfeet * i)-offset], ptinvert)).astype(np.int)
        if yvl[1] < maxy and yvl[1] > 0:
            if last == yvl[1]:
                break
            print(i,yvl[1])
            scanlines.append([yvl,yvr])
            last = yvl[1]
    return np.array(scanlines)



def pointhtrans(pt, m):
    """
    Map a point though the perspective transform M
    :param pt:
    :param m:
    :return:
    """
    inpt = np.asarray([pt[0], pt[1], 1.0])
    outpt = np.dot(m, inpt)
    return np.asarray([outpt[0] / outpt[2], outpt[1] / outpt[2]])




if __name__ == "__main__":
    parse = argparse.ArgumentParser()
    parse.add_argument("--dir",help="A directory")
    parse.add_argument("--out", help="Output File")

    args = parse.parse_args()

    fourcc = cv2.VideoWriter_fourcc(*'MP4V')
    if args.dir is not None:
        if args.dir and os.path.isdir(args.dir):
            basevd = args.dir
            outvfile = os.path.join(basevd,"camout.mp4" if args.out is None else args.out)
            if os.path.exists(outvfile):
                os.remove(outvfile)
            out = None
            cifile = os.path.join(basevd, "camerainfo.json")
            ci = None
            if os.path.exists(cifile):
                ci = CameraInfo().fromJson(cifile)
                tenscans = get_ten_feet_scanlines(ci,720)

                onlydirs = [f for f in os.listdir(basevd) if os.path.isdir(os.path.join(basevd, f))]
                if len(onlydirs) > 0:

                    for basedir in onlydirs:
                        basepath = os.path.join(basevd, basedir)
                        vidfile = os.path.join(basepath,"Video2.H264")
                        if os.path.exists(vidfile):
                            if out is None:
                                vcap = cv2.VideoCapture(vidfile)
                                ret, frame = vcap.read()
                                vcap.release()

                                out = cv2.VideoWriter(outvfile, fourcc, 20.0, (frame.shape[1], frame.shape[0]))
                            process_one(vidfile, tenscans, out, True)

                    if out is not None:
                        out.release()