import os
import io
import sys
import argparse
import cv2
import numpy as np
from ADAS_projective_transform import AdasProjectiveTransform
from sde_utils.sde_tar_sr4 import SdeTarSr4
from cv_utils.intersections import  intersectLines
from cv_utils.MedianAbsDev import MedianAbsDev
import tempfile
import json
import time
import configparser
import boto3
from sde_utils.sedsync_loging import get_sdemd_list
import tempfile
from scipy.optimize import minimize
import random

"""
Automatically recalcualte the calibratin, given an old calibration and a set of lane lines.
Some fields in the old calibration are reused without question: ext_tz, ext_tx.  These are
the distance fromt he bumber ot the camera and the camera offset in the x direction.
It is also assumed that ext_rz the camera roll is zero.

So we calcualte new ext_ty, ext_rx and ext_ry values.
"""


def filter_lanes(lane_data, speed_data, offset, lanetol, speedmin):
    """
    Given the ADAS avro lane data find lanes that are good.
    :param lane_data: Lane data from the SDE avro file.
    :param speed_data: Speed data from the avro file.
    :param offset: image offset to translate the lane data.
    :param lanetol: how big the squared parameter can be to check if theline is straight.
    :param speedmin: The Minimum speed needed to include a lane.
    :return:
    """
    new_lanes = []
    if len(lane_data) > 0:
        speedln_mul = len(speed_data)/len(lane_data)
        ln_idx = 0
        for lanes in lane_data:
            speedidx = int(ln_idx*speedln_mul)
            speed = speed_data[speedidx]
            ln_idx += 1
            # check if we are going fast enough.
            if speed < speedmin:
                continue
            good_lanes = []
            for lane in lanes['laneResult']['lanes']:
                # 2 and 3 are the left and right lanes.
                if 2 <= lane['position'] <= 3 and lane['valid'] == 1 and lane['points']['num'] > 0 and\
                        lane['XDistance'] > 0.0 and lane['confidence'] > 80:
                    # only take the closest half.  The lanes tend to twist at the end.
                    bottom = lane['points']['pointY'][0] > lane['points']['pointY'][-1]
                    half = int(len(lane['points']['pointY'])/2)
                    if bottom:
                        lane['points']['pointX'] = np.array(lane['points']['pointX'][:half])
                        lane['points']['pointY'] = np.array(lane['points']['pointY'][:half])
                    else:
                        lane['points']['pointX'] = np.array(lane['points']['pointX'][half:])
                        lane['points']['pointY'] = np.array(lane['points']['pointY'][half:])
                    if len(lane['points']['pointY']) < 2:
                        break
                    lane['hpoints'] = np.vstack((lane['points']['pointX'], lane['points']['pointY'],
                                                 np.full(len(lane['points']['pointY']), 1)))
                    if np.polyfit(lane['points']['pointY'], lane['points']['pointX'], 2)[0] < lanetol:
                        good_lanes.append(lane)
            # make sure we have a left and a right.
            if len(good_lanes) == 2:
                if good_lanes[0]['position'] != 2:
                    tplane = good_lanes[0]
                    good_lanes[0] = good_lanes[1]
                    good_lanes[1] = tplane
                new_lanes.append([good_lanes[0]['hpoints'], good_lanes[1]['hpoints']])
    return new_lanes


def remove_outliers(lanes):
    """
    Use Median Absolute Deviation to filter out bad lane line pairs.
    :param lanes:
    :return:
    """
    vanpt = []
    goodvp = []
    # dbg = True
    # frame = np.zeros((720, 1280, 3), dtype=np.uint8)
    for lane in lanes:
        x, y, good = intersectLines([lane[0][0][0],lane[0][1][0]], [lane[0][0][-1],lane[0][1][-1]], [lane[1][0][0],lane[1][1][0]], [lane[1][0][-1],lane[1][1][-1]])
        vanpt.append([x,y])
        goodvp.append(good == 1)

    # remove bad intersections.
    vanpt = np.array(vanpt)[goodvp]
    lanes = lanes[goodvp]
    mad = MedianAbsDev(vanpt)
    # vanpt = vanpt[mad.get_inliers()]
    lanes = lanes[mad.get_inliers()]

    return lanes


def min_cal(cal, lanes):
    """
    Find the new calibration parameters.
    :param cal: The old calibration.
    :param lanes: The filtered lane lines.
    :return: The new calibration and a succese boolean.
    """
    # our initial vector to find the minimum for.
    # alpha = np.array([cal["ext_ty"]/1000,cal["ext_rx"],cal["ext_ry"]])

    # minimization function
    def minfun(a, cal, lanes):
        # get the current transform to the road
        lena = len(a)
        if lena == 1:
            adast = AdasProjectiveTransform(cal["int_fx"], cal["int_fy"], cal["int_cx"], cal["int_cy"], cal["ext_rx"], cal["ext_ry"],
                                            cal["ext_rz"], cal["ext_tx"], a[0], cal["ext_tz"])
        else:
            adast = AdasProjectiveTransform(cal["int_fx"], cal["int_fy"], cal["int_cx"], cal["int_cy"], a[0], a[1],
                                            cal["ext_rz"], cal["ext_tx"], cal["ext_ty"], cal["ext_tz"])
        t = adast.get_image_to_road()
        s = 0.0
        for lane in lanes:
            hpts = np.dot(t, lane[0])
            rdpts0x = hpts[0]/hpts[2]
            hpts = np.dot(t, lane[1])
            rdpts1x = hpts[0]/hpts[2]
            # sum up the x differences along the segments. or the difference from 12 feet.
            if lena == 2:
                s += np.sum(np.square(np.diff(rdpts0x)))+np.sum(np.square(np.diff(rdpts1x)))
            else:
                s += np.square(np.abs(np.mean(rdpts0x)-np.mean(rdpts1x))-3657.6)
        # print(a," ",s)
        return s
    # initial search over the angles.  rx from 1 to -5 and ry from 0 to 3
    isvals = [[cal["ext_rx"], cal["ext_ry"]]]
    isres = [minfun(isvals[0], cal, lanes)]
    for rx in np.arange(1.0, -6.0, -1.0):
        for ry in np.arange(-1.0, 4.0, 1.0):
            isvals.append([rx, ry])
            isres.append(minfun([rx, ry], cal, lanes))
    minidx = np.argmin(isres)
    alpha = np.array([isvals[minidx][0], isvals[minidx][1]])

    # initial guess at alpha. and then minimize on the angles first.
    minres1 = minimize(minfun, alpha, args=(cal, lanes), method='Nelder-Mead', options={'xatol': 0.001})
    if not minres1.success:
        return cal, minres1.success

    # now minimize on the height.
    alpha = np.array([cal['ext_ty']])
    newcal = cal.copy()
    newcal['ext_rx'] = minres1.x[0]
    newcal['ext_ry'] = minres1.x[1]
    minres2 = minimize(minfun, alpha, args=(newcal, lanes), method='Nelder-Mead', options={'xatol': 0.01})
    newcal['ext_ty'] = minres2.x[0]

    return newcal, minres2.success


def calc_diff(new_cal, old_cal):
    """
    Estimate the difference between the two calibrations.  We calcualte the distance between a point at 100 feet and
    another poibt at 80 feet and then take the maximum.
    :param new_cal: The new calibration.
    :param old_cal: The old Claibration.
    :return: Error between the two calibrations.
    """
    new_adast = AdasProjectiveTransform(new_cal["int_fx"], new_cal["int_fy"], new_cal["int_cx"], new_cal["int_cy"],
                                        new_cal["ext_rx"], new_cal["ext_ry"],
                                        new_cal["ext_rz"], new_cal["ext_tx"], new_cal["ext_ty"], new_cal["ext_tz"])
    new_t = new_adast.get_image_to_road()
    old_adast = AdasProjectiveTransform(old_cal["int_fx"], old_cal["int_fy"], old_cal["int_cx"], old_cal["int_cy"],
                                        old_cal["ext_rx"], old_cal["ext_ry"],
                                        old_cal["ext_rz"], old_cal["ext_tx"], old_cal["ext_ty"], old_cal["ext_tz"])
    old_t = old_adast.get_image_to_road()
    retinv, invbc = cv2.invert(new_t, flags=cv2.DECOMP_LU)
    # point 100 feet ahead and 5 feet right.
    org_point = np.array([1524, -30480, 1.0])
    outpt = np.dot(invbc, org_point)
    img_pt = np.array([outpt[0]/outpt[2], outpt[1]/outpt[2], 1.0])
    h_old_pt = np.dot(old_t, img_pt)
    old_pt = np.array([h_old_pt[0]/h_old_pt[2], h_old_pt[1]/h_old_pt[2]])
    dist100 = np.sqrt(np.sum(np.square(org_point[:2]-old_pt)))
    org_point = np.array([1524, -24384,  1.0])
    outpt = np.dot(invbc, org_point)
    img_pt = np.array([outpt[0]/outpt[2], outpt[1]/outpt[2], 1.0])
    h_old_pt = np.dot(old_t, img_pt)
    old_pt = np.array([h_old_pt[0]/h_old_pt[2], h_old_pt[1]/h_old_pt[2]])
    dist80 = np.sqrt(np.sum(np.square(org_point[:2]-old_pt)))
    return max(dist100, dist80)


def main(args):
    """
    For Testing.
    :param args: The args from below.
    :return:
    """
    if args.sde is not None or args.lanes is not None:
        if args.cal is not None and os.path.exists(args.cal):

            # get the transform to the road.
            with open(args.cal, 'r') as jscontent:
                jsonstr = jscontent.read()
            cal = json.loads(jsonstr)
        else:
            cal = None

        new_lane = []
        if args.sde is not None and os.path.isfile(args.sde):
            sde = SdeTarSr4(args.sde)
            lane_data = sde.get_laneDetection()
            speed_data = sde.get_speed()
            sde.close()
            new_lane = filter_lanes(lane_data, speed_data[0], args.offset, args.lanetol, args.speedmin)
        elif args.sde is not None and os.path.isdir(args.sde):
            for rdir, fdir, fls in os.walk(args.sde):
                for fl in fls:
                    if fl.endswith(('sde', 'SDE', 'SDEMD', 'sdemd')):
                        sde = SdeTarSr4(os.path.join(rdir, fl))
                        lane_data = sde.get_laneDetection()
                        speed_data = sde.get_speed()
                        sde.close()
                        new_lane.extend(filter_lanes(lane_data, speed_data[0], args.offset, args.lanetol,
                                                     args.speedmin))
        elif os.path.isfile(args.lanes):
            with open(args.lanes) as lnfl:
                new_lane = json.loads(lnfl.read())
                new_lane = np.array(new_lane)
        if len(new_lane) > 100:
            print("Number of Lanes original ", len(new_lane))
            new_lane = remove_outliers(new_lane)
            print("Number of Lanes after outlier removal ", len(new_lane))
            # find the new calibration from the old and the lane lines.
            new_cal, success = min_cal(cal, new_lane)
            print("Initial result ", end='')
            print(new_cal)
            print("diff: ", str(calc_diff(new_cal, cal)))
            print("")
            randl = [(random.uniform(new_cal['ext_ty']-914, new_cal['ext_ty']+914),
                      random.uniform(max(new_cal['ext_rx']-3, -10.0), min(new_cal['ext_rx']+2, 2.0)),
                      random.uniform(max(new_cal['ext_ry']-1, -1), min(new_cal['ext_ry']+5, 10)),
                      random.uniform(max(new_cal['ext_rz']-1, -1), min(new_cal['ext_rz']+1, 1))) for i in range(10)]
            for rl in randl:
                starttimr = time.time()
                rcal = new_cal.copy()
                rcal['ext_ty'] = rl[0]
                rcal['ext_rx'] = rl[1]
                rcal['ext_ry'] = rl[2]
                rcal['ext_rz'] = 0.0
                print("Initial guess ", end='')
                print(rl)
                new_cal, success = min_cal(rcal, new_lane)
                print("Result "+str(success)+" ", end='')
                print([new_cal['ext_ty'], new_cal['ext_rx'], new_cal['ext_ry'], new_cal['ext_rz']])
                print("Seconds "+str(time.time()-starttimr))

            return False
    return True


if __name__ == "__main__":

    def coords(s):
        try:
            x, y = map(int, s.split(','))
            return x, y
        except:
            raise argparse.ArgumentTypeError("Coordinates must be x,y")


    # parse the arguments.
    parse = argparse.ArgumentParser()
    parse.add_argument("--sde", help="Video file")
    parse.add_argument("--caldiff", help="Other Calibration to compare.")
    parse.add_argument("--cal", help="Calibration json file")
    parse.add_argument("--offset", help="offset to lane lines x,y pixels", dest="offset", type=coords, default=(25, 0))
    parse.add_argument("--lanetol", help="lane quadratic parameter max", type=float, default=0.0001)
    parse.add_argument("--speedmin", help="Minimum speed to use.", type=float, default=80.0)
    parse.add_argument("--lanes", help="Lane lines instead of sed file.")

    args = parse.parse_args()

    if args.caldiff is not None:

        if os.path.isdir(args.caldiff):
            files_list = []
            for root, dirs, files in os.walk(args.caldiff):
                for file in files:
                    if file.endswith(".json"):
                        files_list.append(os.path.join(args.caldiff,file))
            if len(files_list) > 0:
                cal2 = None
                for fl in files_list:
                    with open(fl, 'r') as jscontent:
                        jsonstr = jscontent.read()
                    cal1 = json.loads(jsonstr)
                    if not isinstance(cal1,dict):
                        continue
                    if cal2 is not None:
                        print("Cal diff ", str(calc_diff(cal1, cal2))," ",fl)
                    cal2 = cal1

        else:
            with open(args.cal, 'r') as jscontent:
                jsonstr = jscontent.read()
            cal1 = json.loads(jsonstr)
            with open(args.caldiff, 'r') as jscontent:
                jsonstr = jscontent.read()
            cal2 = json.loads(jsonstr)
            print("Cal diff ",str(calc_diff(cal1,cal2)))
        sys.exit(0)

    if main(args):
        parse.print_help()
    sys.exit(0)
