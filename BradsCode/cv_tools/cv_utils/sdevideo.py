import cv2
import numpy as np
import os


class SdeVideo:
    """
    Class to wrap an SDE Video file.  This assumes that the video has a right and left half where the
    left side is the road video and the right side is the driver camera.
    """
    def __init__(self, filename, crop=None):
        """
        Create an SdeVideo
        :param filename: The file name for the video.
        """
        self.filename = filename
        self.frameno = -1
        self.vcap = cv2.VideoCapture(filename)
        ret, frame = self.vcap.read()
        self.rawmaxx = frame.shape[1]
        self.rawmaxy = frame.shape[0]
        self.vcap.release()
        self.vcap = cv2.VideoCapture(filename)
        self.vcap.set(cv2.CAP_PROP_POS_FRAMES, 0)

        self.maxy = self.rawmaxy
        self.maxx = self.rawmaxx
        if crop is None:
            self.docrop = False
            if self.rawmaxx > 752:
                # Assume it is a double video.
                # and we want the first half.
                self.minx = 0
                self.miny = 0
                self.maxx = int(self.rawmaxx/2)
                self.docrop = True
        else:
            self.docrop = True
            self.minx = int(crop[0,0])
            self.miny = int(crop[0,1])
            self.maxx = int(crop[1,0])
            self.maxy = int(crop[1,1])

    def get_shape(self):
        """
        Get the x and y shape for the video.
        :return: tuple of width and height of the video
        """
        return self.maxx, self.maxy

    def get_time(self):
        """
        Get the current frames time. 
        :return: time in milliseconds.
        """
        return self.vcap.get(cv2.CAP_PROP_POS_MSEC)

    def read(self, frameno=-1):
        """
        Read the next frame or a specific frame.  Note this will rewind the pointer.
        This only returns the forward road view.
        :param frameno: Which frame to get.
        :return: tuple of status and frame.  status is True if a good frame is returned.
        """
        if frameno != -1 and frameno != (self.frameno+1):
            self.vcap.set(cv2.CAP_PROP_POS_FRAMES, frameno)
            self.frameno = frameno-1
        ret, frame = self.vcap.read()
        if not ret:
            return ret, frame

        self.frameno += 1
        if self.docrop:
            frame = frame[self.miny:self.maxy, self.minx:self.maxx]
        return ret, frame

    def get_last_frame_no(self):
        """
        get the last returned Frame number.
        :return: 
        """
        return self.frameno

    def rewind(self):
        """
        Rewind all the way back to the beginning.
        :return: 
        """
        self.vcap.set(cv2.CAP_PROP_POS_FRAMES, 0)
        self.frameno = -1
        return None

    def is_good_video(self):
        """
        Simply check if the file exists.
        :return: 
        """
        return os.path.isfile(self.filename)

    def release(self):
        """
        Relase the resources for this video.
        :return: 
        """
        self.vcap.release()
        return None
