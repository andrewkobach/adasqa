from scipy.interpolate import PPoly
import numpy as np


class CubicInterp(PPoly):
    """ Simple Cubic Interpolation

    Interpolate a simple cubic between two points with the constraints:
    1. The second derivative at the first point is zero
    2. The first derivative at the last point is zero.

    The result is represented as a piecewise polynomial.

    """
    def __init__(self,xn,yn):
        if len(xn) != 2 or len(yn) !=2:
            raise ValueError("Only 2 points can be used.")
        x, y = map(np.asarray, (xn, yn))
        delx = x[1]-x[0]
        if delx == 0.0:
            raise ValueError("The 2 x points must be different")
        delxsq = delx*delx
        a = (y[1]-y[0])/(delx*delxsq-3.0*delxsq)
        c = np.empty((4,1), dtype=np.float32)
        c[0] = a
        c[1] = 0.0
        c[2] = -3.0*a*delxsq
        c[3] = y[1]
        super(CubicInterp, self).__init__(c, x, extrapolate=True)
