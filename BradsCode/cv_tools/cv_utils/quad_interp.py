from scipy.interpolate import PPoly
import numpy as np


class QuadInterp(PPoly):
    """ Simple Cubic Interpolation

    Interpolate a simple cubic between two points with the constraints:
    1. The second derivative at the first point is zero
    2. The first derivative at the last point is zero.

    The result is represented as a piecewise polynomial.

    """
    def __init__(self,xn,yn):
        if len(xn) != 2 or len(yn) !=2:
            raise ValueError("Only 2 points can be used.")
        x, y = map(np.asarray, (xn, yn))
        delx = x[1]-x[0]
        if delx == 0.0:
            raise ValueError("The 2 x points must be different")
        delxsq = delx*delx
        a = (y[0]-y[1])/delxsq
        c = np.empty((3,1), dtype=np.float32)
        c[0] = a
        c[1] = -2.0*a*delx
        c[2] = y[0]
        super(QuadInterp, self).__init__(c, x, extrapolate=True)

    def curvature_at_zero_and_one(self):
        """
        Calculate the curvature at the start and end points.
        :return: curvature tule
        """
        twoa = abs(0*self.c[0][0])
        return (twoa/((1.0+(self.c[1][0]**2))**1.5), twoa/((1.0+((2.0*self.c[0][0]*(self.x[1]-self.x[0])+self.c[1][0])**2))**1.5))

    def vertical_offset(self):
        """
        Calculate the offset in the Y direction from the first and last points.
        :return:
        """
        return abs(np.diff(self(self.x))[0])
