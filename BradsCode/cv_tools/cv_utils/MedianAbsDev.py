import numpy as np


class MedianAbsDev:
    """
    Class to calculate Median Absolute Deviation
    """

    def __init__(self, list):
        kfactor = 1.4826
        np.seterr(all='raise')
        self.median = np.median(list,axis=0)
        self.mad = kfactor * np.median(abs(list - self.median),axis=0)
        if len(list) <= 1:
            self.outliers = np.array([False])
        else:
            if len(list.shape) > 1:
                factor = 2.0
                while True:
                    self.outliers = np.any(np.greater(abs(list - self.median) , factor*self.mad),axis=1)
                    if np.sum(self.outliers) > 0:
                        break
                    factor = factor*.9
            else:
                self.outliers = np.greater(abs(list - self.median), 2 * self.mad)

    def get_outliers(self):
        return self.outliers

    def get_inliers(self):
        return ~self.get_outliers()

    def is_outlier(self,item):
        if min(abs(item - self.median)) == 0:
            return False
        return np.any(np.greater(abs(item - self.median) , self.mad* 2))

    def are_outlier(self,items):
        return np.greater(abs(items - self.median) , self.mad* 2)



def median_abs_dev(list,factor=2):
    """
    for a list mark the outliers with True using the
    Median Absolute Deviation Median.
    This uses the 1.4826 constant and checks for
    values twice the mad.
    :param list:
    :return: list of True/False for Outlier/inlier
    """
    mad = 1.4826 * np.median(abs(list - np.median(list)))
    return abs(list - np.median(list)) > factor*mad