from itertools import groupby
from operator import itemgetter
import datetime
import copy


def gps_interpolate(gps_list, groups, length):
    if groups is None:
        groups = []
        for k, g in groupby(sorted(gps_list, key=itemgetter(1)), key=itemgetter(1)):
            groups.append(list(g))
    # very first go through and add empty groups where a time is missing.
    lasttime = just_time_ms(groups[0])
    for ig in range(1, len(groups)):
        if len(groups[ig]) == 0:
            continue
        thistime = just_time_ms(groups[ig])
        if lasttime is not None and thistime is not None:
            if thistime-lasttime > 1.5:
                # add empty ones to fill in later.
                numadd = int(thistime-lasttime-1)
                for ia in range(0,numadd):
                    if ig+1 == len(groups)-1:
                        groups.append([])
                    else:
                        groups.insert(ig,[])
        lasttime = thistime
    while len(groups) < length:
        groups.append([])
    interpolated = [False for i in range(0,len(groups))]

    # next go through the groups and find the ones that have only one
    # for these we will take the missing one from the next or previous.
    # modify the time and location but keep the remaining data.

    for ig in range(0,len(groups)):
        # find one where we only have one type.
        if len(groups[ig]) > 1:
            headerset = set()
            for grp in groups[ig]:
                headerset.add(grp[0])
            if len(headerset) == 1:
                groups[ig] = [groups[ig][0]]
        if len(groups[ig]) == 1:
            itake_dir = 1 if ig+1 < len(groups) else -1
            itake = ig + itake_dir
            flipped = False
            isnotdone = True
            while isnotdone:
                if len(groups[itake]) >=2:
                    for take_grp in groups[itake]:
                        if groups[ig][0][0] != take_grp[0]:
                            groups[ig].append(copy.deepcopy(take_grp))
                            modify_time_gps(groups[ig])
                            interpolated[ig] = True
                            isnotdone = False
                            break
                        itake += itake_dir
                        if itake < 0 or itake >= len(groups):
                            itake =ig -itake_dir
                            itake_dir = - itake_dir
                            if flipped:
                                isnotdone = False
                                break
                            flipped = True
                else:
                    itake += itake_dir
                    if itake < 0 or itake >= len(groups):
                        itake = ig - itake_dir
                        itake_dir = - itake_dir
                        if flipped:
                            isnotdone = False
                            break
                        flipped = True
    # So now we have all the ones with one filled in if we can.  Now go through and remove duplicates
    # in a group.  Anyone with only one gets fully interpolated by removing it here

    for ig in range(0,len(groups)):
        if len(groups[ig]) == 1:
            groups[ig] = []
            interpolated[ig] = True
        elif len(groups[ig]) > 2:
            headerset = set()
            grp = groups[ig]
            grpidx = 0
            while len(grp) > 2:
                if grp[grpidx][0] in headerset:
                    del grp[grpidx]
                else:
                    headerset.add(grp[grpidx][0])
                    grpidx += 1

    # now we need to go through and interpolate the missing.
    # Special case if the first group is missing then copy the next one and decrement the time
    # there can only be one at this point.
    if len(groups[0]) == 0:
        groups[0] = copy.deepcopy(groups[1])
        backtime = just_time_ms(groups[0])+1
        groups[0][0][1] = time_ms_str(backtime)
        groups[0][1][1] = groups[0][0][1]
        interpolated[0] = True
    ig = 1
    istart = 0
    iend = 0
    while ig < len(groups):
        if len(groups[ig]) == 0:
            istart = ig
            iend = ig
            while len(groups[iend]) == 0:
                iend = iend+1
                if iend == len(groups):
                    break

            lasttime = just_time_ms(groups[ig-1])
            for iglast in range(istart,iend):
                groups[iglast] = copy.deepcopy(groups[ig-1])
                interpolated[iglast] = True
                lasttime += 1
                groups[iglast][0][1] = time_ms_str(lasttime)
                groups[iglast][1][1] = groups[iglast][0][1]
            if iend == len(groups):
                break
        ig += 1

    while len(groups) > length:
        del groups[len(groups)-1]
        del interpolated[len(interpolated)-1]

    for ig in range(0,len(groups)):
        swap_grp(groups[ig])

    same_loc = [False]
    last_loc = get_lat_lon_stg(groups[0])
    for ig in range(1,len(groups)):
        this_loc = get_lat_lon_stg(groups[ig])
        if this_loc == last_loc:
            same_loc.append(True)
        else:
            same_loc.append(False)
        last_loc = this_loc

    return groups, interpolated, same_loc


def get_lat_lon_stg(grp):
    if grp[0][0] == '$GPRMC':
        return ",".join(grp[0][3:7])
    elif grp[0][0] == '$GPGGA':
        return ",".join(grp[0][2:6])
    return ""


def swap_grp(grp):
    if grp[0][0] == '$GPRMC':
        grp.append(grp[0])
        del grp[0]
    return None


def modify_time_gps(grp):
    if grp[0][0] == '$GPRMC':
        grp[1][1] = grp[0][1]
        grp[1][2] = grp[0][3]
        grp[1][3] = grp[0][4]
        grp[1][4] = grp[0][5]
        grp[1][5] = grp[0][6]
        grp.append(grp[0])
        del grp[0]
    else:
        grp[1][1] = grp[0][1]
        grp[1][3] = grp[0][2]
        grp[1][4] = grp[0][3]
        grp[1][5] = grp[0][4]
        grp[1][6] = grp[0][5]
    return None


def just_time_ms(grp):
    if len(grp) == 0:
        return None
    tmstr = grp[0][1]
    dt = datetime.datetime.strptime(tmstr,"%H%M%S.%f")
    return dt.replace(year=1970, month=1, day=1, tzinfo=datetime.timezone.utc).timestamp()


def time_ms_str(tm):
    dt = datetime.datetime.fromtimestamp(tm,datetime.timezone.utc)
    return datetime.datetime.strftime(dt,"%H%M%S.%f")[:-3]
