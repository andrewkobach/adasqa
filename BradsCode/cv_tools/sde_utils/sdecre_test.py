#!/usr/bin/env python3
import argparse
import sys
import os
import io
import json
from sde_utils.sdemd_cre import SdemdCRE
import boto3
import tempfile
from itertools import groupby
from operator import itemgetter
import numpy as np
from sde_utils.gps_interpolate import gps_interpolate
import copy

"""
Script to test sdemd_cre.py.  Test both file and streaming SDEMD files.

"""


def parse_args():
    """
    Here are all our arguments.
    :return: parsed args and the parser.
    """
    parse = argparse.ArgumentParser()
    parse.add_argument("--sdebucket", help="Name of S3 bucket for SDE files and any prefix")
    parse.add_argument("--outdir", help="Directory for json results")
    parse.add_argument("--stream", action='store_true', default=False, help="Stream SDEMD files.")
    parse.add_argument("--gpstest", action='store_true', default=False, help="GSP statistics")
    args = parse.parse_args()
    return args, parse


def main():
    """
    The main program.  Here we just get the parsed arguments, do some checks then call the function that
    does all the work.
    :return: None
    """
    s3cl = boto3.client('s3')
    args, parse = parse_args()
    if args.sdebucket is None or (args.outdir is None and not args.gpstest):
        parse.print_help(sys.stderr)
        return
    if args.outdir is not None and not os.path.exists(args.outdir):
        os.makedirs(args.outdir)
    bucket, prefix = parse_out_bucket(args.sdebucket)

    istruncated = True
    next_token = None
    gps_test = None
    with tempfile.TemporaryDirectory() as tmpdir:
        while istruncated:
            all_sde_list = []
            if next_token is not None:
                files_list_res = s3cl.list_objects_v2(Bucket=bucket, ContinuationToken=next_token)
            else:
                files_list_res = s3cl.list_objects_v2(Bucket=bucket, Prefix=prefix, FetchOwner=False)
            if 'IsTruncated' not in files_list_res:
                error_happened = True
                print("\nError getting bucket list")
                break
            istruncated = files_list_res['IsTruncated']
            if istruncated and 'NextContinuationToken' in files_list_res:
                next_token = files_list_res['NextContinuationToken']
            else:
                next_token = None
            if 'Contents' not in files_list_res:
                break
            all_sde_list.extend([fl['Key'] for fl in files_list_res['Contents'] if fl['Key'].endswith(("SDEMD", "sdemd"))])
            last=""
            for sdepath in all_sde_list:
                sdefilesplit = sdepath.split('/')
                destfile = os.path.join(tmpdir, sdefilesplit[-1])
                thisone = sdefilesplit[-1][:8]
                if thisone == last:
                    continue
                last = thisone
                sde = None
                if args.outdir is not None:
                    outfile = os.path.join(args.outdir, sdefilesplit[-1].split('.')[0] + ".json")
                if args.stream:
                    resp = s3cl.get_object(Bucket=bucket, Key=sdepath)
                    if 'Body' in resp:
                        try:
                            sde = SdemdCRE(io.BytesIO(resp['Body'].read()))
                            if args.gpstest:
                                test_result = do_gps_test(sde)
                                if gps_test is None:
                                    gps_test = np.array(test_result)
                                else:
                                    gps_test = np.add(gps_test,np.array(test_result))
                                    print(gps_test)
                        except:
                            print("Error getting file")
                            continue
                    else:
                        print("Error getting file")
                        continue
                else:
                    s3cl.download_file(Bucket=bucket, Key=sdepath, Filename=destfile)
                    if os.path.exists(destfile):

                        sde = SdemdCRE(destfile)
                    else:
                        print("Error getting file")
                        continue
                    os.remove(destfile)
                if args.outdir is not None:
                    with open(outfile,mode="w") as of:
                        of.write(sde.to_json())
                if not is_good_json(sde):
                    print("Bad "+destfile)
                if args.outdir is not None:
                    print(sdepath)
        if args.gpstest:
            print(gps_test)
        print("Done")
    return


def is_good_json(sde):
    mdata = sde.get_metadata()
    if 'media' in mdata:
        for media in mdata['media']:
            if 'filename' not in media:
                return False
            if 'tracks' not in media:
                return False
            for track in media['tracks']:
                if 'id' not in track:
                    return False
                if 'type' not in track:
                    return False
                if 'frame_rate' not in track or 'duration' not in track or 'resolution' not in track:
                    return False
                if 'cameras' not in track:
                    return False
                for camera in track['cameras']:
                    if 'id' not in camera or 'name' not in camera or 'offsets' not in camera:
                        return False
    else:
        return False
    return True


def do_gps_test(sde):

    gps_list = sde.sde_metadata.get('sensors').get('gps')
    if gps_list is None:
        return [0,0,0,0,0,0,0,0,0,0,0,0,0,1]
    groups = []
    for k, g in groupby(sorted(gps_list, key=itemgetter(1)), key=itemgetter(1)):
        groups.append(list(g))
    gps_test = test_a_grouping(groups, sde.total_duration, sde.basename)

    # print(gps_test)
    if sum(gps_test) > 1:
        good_grps, interp, same_loc = gps_interpolate(gps_list, copy.deepcopy(groups), sde.total_duration )
        gps_fixed_test = test_a_grouping(good_grps, sde.total_duration, sde.basename)
        if sum(gps_fixed_test) > 1:
            print("orig"+str(groups))
            print("bad fixed"+str(good_grps))
        else:
            print("orig"+str(groups))
            print("fixed"+str(good_grps))

    return gps_test


def test_a_grouping(groups, total_duration, basename):
    gps_test = [0,0,0,0,0,0,0,0,0,0,0,0,0,1]
    if len(groups) < total_duration:
        gps_test[0] +=1
    if len(groups) > total_duration:
        gps_test[1] += 1
        if len(groups[0]) != 2 and len(groups[len(groups)-1]) != 2:
            gps_test[2]+=1
    # count number with only one.
    onlyonecount = 0
    countmissingdata = 0
    for group in groups:
        if len(group) < 2:
            onlyonecount+=1
        if gps_to_dict_str(basename, "", "", [0,0], 0, group) is None:
            countmissingdata +=1
    if onlyonecount == 1:
        gps_test[3]+=1
    elif onlyonecount == 2:
        gps_test[4]+=1
    elif onlyonecount == 3:
        gps_test[5]+=1
    elif onlyonecount == 4:
        gps_test[6]+=1
    elif onlyonecount >= 5:
        gps_test[7]+=1

    if onlyonecount == 0:
        if countmissingdata == 1:
            gps_test[8]+=1
        elif countmissingdata == 2:
            gps_test[9]+=1
        elif countmissingdata == 3:
            gps_test[10]+=1
        elif countmissingdata == 4:
            gps_test[11]+=1
        elif countmissingdata >= 5:
            gps_test[12]+=1
    return gps_test

def gps_to_dict_str(sr_id,jpg_bucket, jpg_key, jpg_wh,jpg_size, gps_data):
    jdict = {"source_id":""}
    # jpg_split = jpg_key.split("/")
    # jpg_split_len = len(jpg_split)
    # jdict["sde_year"] = jpg_split[jpg_split_len-4]
    # jdict["sde_month_num"] = jpg_split[jpg_split_len-3]
    # jdict["sde_day_of_month_num"] = jpg_split[jpg_split_len-2]
    # jdict["jpeg_file_id"] = jpg_split[jpg_split_len-1]
    jdict["jpeg_bucket"] = jpg_bucket
    jdict["jpeg_horiz_size_pixel"] = jpg_wh[1]
    jdict["jpeg_vert_size_pixels"] = jpg_wh[0]
    jdict["jpeg_image_size_bytes"] = jpg_size
    for gps in gps_data:
        if gps[0].startswith("$GPRMC"):
            if len(gps) < 13:
                return None
            jdict["gps_rmc_timefix"]=gps[1]
            jdict["gps_rmc_status_active_or_void"]=gps[2]
            jdict["gps_rmc_latitude_deg"]=gps[3]
            jdict["gps_rmc_latitude_directionNS"]=gps[4]
            jdict["gps_rmc_longitude_deg"]=gps[5]
            jdict["gps_rmc_longitude_directionEW"]=gps[6]
            jdict["gps_rmc_speed_in_knots"]=gps[7]
            jdict["gps_rmc_bearing_deg"]=gps[8]
            jdict["gps_rmc_date"]=gps[9]
            jdict["gps_rmc_magnetic_variation"]=gps[10]+","+gps[11]
            sidcsum = gps[12].split("*")
            if len(sidcsum) < 2:
                jdict["gps_rmc_checksum_tag"]=gps[12]
            else:
                jdict["gps_rmc_checksum_tag"] = "*" + sidcsum[1]
        else:
            if len(gps) < 15:
                return None
            jdict["gps_gga_timefix"]=gps[1]
            jdict["gps_gga_latitude_deg"]=gps[2]
            jdict["gps_gga_latitude_directionNS"]=gps[3]
            jdict["gps_gga_longitude_deg"]=gps[4]
            jdict["gps_gga_longitude_directionEW"]=gps[5]
            jdict["gps_gga_fix_quality"]=gps[6]
            jdict["gps_gga_num_satellites"]=gps[7]
            jdict["gps_gga_hdop"]=gps[8]
            jdict["gps_gga_altitude"]=gps[9]+","+gps[10]
            jdict["gps_gga_geoidheight_mean_sea_level"]=gps[11]+","+gps[12]
            jdict["gps_gga_dgps_time_since_last_fix"]=gps[13]
            sidcsum= gps[14].split("*")
            if len(sidcsum) < 2:
                jdict["gps_gga_dgps_station_id"]=""
                jdict["gps_gga_checksum_tag"]=gps[14]
            else:
                jdict["gps_gga_dgps_station_id"] = sidcsum[0]
                jdict["gps_gga_checksum_tag"] = "*" + sidcsum[1]
    if len(jdict) != 29:
        return None
    return json.dumps(jdict)



def parse_out_bucket(outbucketarg, extrapath=''):
    """
    Construct the outbucket and get the prefix.
    :param outbucketarg:
    :param extrapath: to append to the prefix.  Rememeber to add a /
    :return: bucket name, prefix to use
    """
    bucket_end = outbucketarg.find('/')
    if bucket_end < 0:
        bucket = outbucketarg
        prepre = ""
    else:
        bucket = outbucketarg[0:bucket_end]
        prepre = outbucketarg[bucket_end + 1:]
        # if not prepre.endswith("/"):
        #     prepre = prepre + "/"
    prepre = prepre + extrapath
    return bucket, prepre


if __name__ == "__main__":
    main()
    sys.exit(0)

