import os
import sys
import argparse
import numpy as np
import xml.etree.ElementTree as et
import tarfile
import tempfile


class SdeFile:
    """
    Class to handle SDE files with a minimum of temporary files.
    """
    def __init__(self, filename):
        """
        Constructor.
        :param filename: SDE file path.
        """
        # openit as a TAR file
        self.tfile = tarfile.TarFile(filename)
        # get the header file.
        with self.tfile.extractfile("Header.XML") as hfile:
            self.raw_header = hfile.read()
        # parse the Hader XML and get some interesting things.
        self.header_tree = et.fromstring(self.raw_header)
        self.total_duration = int(self.header_tree.findall("./EventInfo/TotalDuration")[0].text)
        self.location_el = self.header_tree.findall("./Files/File[@Type='Location']")[0]
        self.safety_el = self.header_tree.findall("./Files/File[@Type='Safety']")[0]
        self.videofiles = self.header_tree.findall("./VideoFiles//VideoFile")
        # we need the video file as a real on the disk file so track it here so we can delete it.
        self.videofile_name = None

    def __del__(self):
        self.close()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def close(self):
        """
        Close this file and clean up everything.
        :return:
        """
        try:
            self.tfile.close()
        except:
            pass
        if self.videofile_name is not None:
            if os.path.exists(self.videofile_name):
                os.remove(self.videofile_name)
                self.videofile_name = None
        return None

    def get_location(self):
        """
        Get the location information.  The header is returned as a dictionary and the GPS as just a table.
        :return:
        """
        with self.tfile.extractfile(self.location_el.text) as lfile:
            raw_location = lfile.read().splitlines()
            location_header = {}
            locations = []
            # The header is delimited by enpty lines.
            doing_header = True
            for rl in raw_location:
                if len(rl) > 0:
                    if doing_header:
                        strs = rl.decode("utf-8").split("=")
                        location_header[strs[0]] = strs[1]
                    else:
                        locations.append(rl.decode("utf-8").split(","))
                else:
                    doing_header = False
            return location_header, np.array(locations)
        return None, None

    def get_speed(self):
        """
        Get the speed data. And the sample rate.
        :return:
        """
        with self.tfile.extractfile(self.safety_el.text) as sfile:
            raw_safe = sfile.read()
            # get len of records.
            srate = int(self.safety_el.attrib['SampleRate'])
            # the block lenth is the total size divided by the number of samples.
            blen = int(len(raw_safe)/(self.total_duration*srate))
            # the speed is in all the schema.
            # 13 and 14 are the speed. 10 integer, 6 fraction. lsb
            speed = []
            for i in range(12, len(raw_safe), blen):
                spd = float(raw_safe[i])/(1 << 6)
                spd += float(raw_safe[i+1])*(1 << 2)
                speed.append(spd)
            return speed, srate
        return None, 0

    def get_temp_video(self, position='Forward'):
        """
        Get the video in a temporary file.  Note closing the SDE object will try and delete this file.
        :param position:
        :return: File name , Frames per second.
        """
        fl = tempfile.NamedTemporaryFile(delete=False)
        self.videofile_name = fl.name
        fps = 4
        for videofile in self.videofiles:
            cref = videofile.findall("./CameraReference")
            if len(cref) == 0 or cref[0].attrib['Position'] != position:
                continue
            fpslst = videofile.findall("./Fps")
            if len(fpslst) > 0:
                fps = int(fpslst[0].text)
            with self.tfile.extractfile(videofile.attrib['name']) as vfile:
                fl.write(vfile.read())
                fl.close()
                return self.videofile_name, fps
            break
        fl.close()
        return None, 0


# test code.
if __name__ == "__main__":
    parse = argparse.ArgumentParser()
    parse.add_argument("--sde", help="sde file")
    args = parse.parse_args()

    if args.sde is not None:
        sde = SdeFile(args.sde)
        loc_hdr, loc = sde.get_location()
        sp, sr = sde.get_speed()
        print(sp)
        vname, fpss = sde.get_temp_video()
        print(vname)
        print("debug stop")
        sde.close()
        sys.exit(0)
