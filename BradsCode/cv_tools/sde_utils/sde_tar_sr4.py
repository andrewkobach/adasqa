import os
import tarfile
import zipfile
import xmltodict
import avro.io
import avro.datafile
import io
import struct


"""
The only import that may need to be installed is xmltodict.  The others are standard python.
"""


class SdeTarSr4:
    """
    Class to handle SDEMD files with a minimum of temporary files.
    """
    def __init__(self, filename):
        self.sde_metadata = {}
        # openit as a Zip file
        self.filename = filename
        if isinstance(filename, str) and not os.path.exists(filename):
            return
        if tarfile.is_tarfile(filename):
            self.tarfile = tarfile.TarFile(filename)
            self.istar = True
            self.header = xmltodict.parse(self.tarfile.extractfile("Header.XML"))
        else:
            self.tarfile = zipfile.ZipFile(filename)
            self.istar = False
            self.header = xmltodict.parse(self.tarfile.read("header.xml"))

        # fllst = self.tarfile.getmembers()
        # print(fllst)

    def have_avro(self):
        try:
            havelanes = False
            haveveh = False
            for fldict in self.header['SdeHeader']['Files']['File']:
                if fldict['#text'] == 'sde_vehicleDetection.avro':
                    haveveh = True
                elif fldict['#text'] == 'sde_laneDetection.avro':
                    havelanes = True
            if haveveh and havelanes:
               return True
        except Exception as ex:
            print("Bad SED ", self.filename, str(ex), flush=True)
            pass
        return False


    def get_event_date_time(self):
        return self.header['SdeHeader']['EventInfo']['EventDateTime']
    
    def get_firmware_version(self):
        return self.header['SdeHeader']['FWVersion']
    
    def get_sr(self):
        return self.header['SdeHeader']['EventInfo']['SmartRecorderSN']
    
    def get_event_time(self):
        return self.header['SdeHeader']['EventInfo']['EventDateTime']

    def get_total_duration(self):
        return int(self.header['SdeHeader']['EventInfo']['TotalDuration'])

    def get_video_info(self,position):
        vflst = self.header['SdeHeader']['VideoFiles']['VideoFile']
        if isinstance(vflst,list):
            for vf in vflst:
                crlst = vf['CameraReference']
                if isinstance(crlst,list):
                    for cr in crlst:
                        if cr['@Position'] == position:
                            return vf, cr, True
                else:
                    if crlst['@Position'] == position:
                        return vf, crlst, False
        else:
            crlst = vflst['CameraReference']
            if isinstance(crlst, list):
                for cr in crlst:
                    if cr['@Position'] == position:
                        return vflst, cr, True
            else:
                if crlst['@Position'] == position:
                    return vflst, crlst, False
        return None, None

    def get_video_filename(self, position):
        vinf, cr, stiched = self.get_video_info(position)
        if vinf is None:
            return None
        return vinf['@name']

    def get_video_fps(self, position):
        vinf, cr, stiched = self.get_video_info(position)
        if vinf is None:
            return None
        return vinf['Fps']

    def get_laneDetection(self):
        try:
            if self.istar:
                return list(avro.datafile.DataFileReader(self.tarfile.extractfile("sde_laneDetection.avro"), avro.io.DatumReader()))
            return list(avro.datafile.DataFileReader(io.BytesIO(self.tarfile.read("sde_laneDetection.avro")), avro.io.DatumReader()))
        except:
            return []

    def get_vehicleDetection(self):
        try:
            if self.istar:
                return list(avro.datafile.DataFileReader(self.tarfile.extractfile("sde_vehicleDetection.avro"), avro.io.DatumReader()))
            return list(avro.datafile.DataFileReader(io.BytesIO(self.tarfile.read("sde_vehicleDetection.avro")), avro.io.DatumReader()))
        except:
            return []

    def get_file_bytes(self,filename):
        if self.istar:
            return self.tarfile.extractfile(filename)
        return io.BytesIO(self.tarfile.read(filename))

    def get_speed(self):
        """
        Get the speed data. And the sample rate.
        :return:
        """
        # find the file.
        files = self.header['SdeHeader']['Files']['File']
        raw_safe = None;
        safety_rate = 0
        for fl in files:
            if fl['@Type'] == 'Safety':
                raw_safe = self.get_file_bytes(fl['#text']).read()
                safety_rate = int(fl['@SampleRate'])
                break;
        if raw_safe is None:
            return None, 0

        # get len of records.
        # the block lenth is the total size divided by the number of samples.
        blen = int(len(raw_safe)/(self.get_total_duration()*safety_rate))
        # the speed is in all the schema.
        # 13 and 14 are the speed. 10 integer, 6 fraction. lsb
        speed = []
        for i in range(12, len(raw_safe), blen):
            spd = struct.unpack('<h', raw_safe[i:i + 2])[0] / (1 << 6)
            speed.append(spd)
        return speed, safety_rate

    def is_bad_video(self):
        vidinfo, cref, isstiched = self.get_video_info('Forward')
        if cref is None:
            return False
        if cref['@X2'] != "1012" or cref['@Y2'] != "566":
            return True
        return False

    def is_video_in(self):
        return self.istar

    def close(self):
        self.tarfile.close()
        return None