#!/usr/bin/env python3
# (c) 2019 SmartDrive Systems, Inc.
import argparse
import sys
import boto3
from time import sleep
import json


ONEDAY = (1000*60*60*24*7)
FILTERPAT = "BUCKET_KEY"


def get_sdemd_list(last_json, loggrp, region_name='us-west-2', delete=True, endswith=('.SDEMD', '.sdemd')):
    """
    Get a list of the new SDEMD files since the alst time this was called. Internal version.
    :param last_json: last json returned by this function or None.
    :param loggrp: The name of the log group.
    :param region_name: Region for logs
    :param delete: If true delete day old logs.
    :param endswith: tuple of file ending to return
    :return: tuple of: list of bucket and key, new start time, delaytime
    """

    clientlogs = boto3.client('logs', region_name=region_name)

    try:
        # Get all the log streams filter on LATEST and only ingested.
        all_streams = []
        next_token = None
        while True:
            if next_token is not None:
                resp = clientlogs.describe_log_streams(logGroupName=loggrp, nextToken=next_token)
            else:
                resp = clientlogs.describe_log_streams(logGroupName=loggrp)
            next_token = resp.get('nextToken', None)
            curr_streams = resp.get('logStreams', [])
            all_streams.extend([curr_streams[ix] for ix in range(0, len(curr_streams)) if
                                curr_streams[ix].get('lastEventTimestamp', None) is not None])
            if next_token is None:
                break
        if len(all_streams) == 0:
            return [], last_json
        last_dict = json.loads(last_json) if last_json is not None else {}
        new_dict = {}
        for stream in all_streams:
            new_dict[stream.get('logStreamName')] = stream.get('lastEventTimestamp')
        all_events = []
        new_end_time = 0
        # now go through each new one and compare to the old get the events.
        for sname, lastevent in new_dict.items():
            if lastevent > new_end_time:
                new_end_time = lastevent
            oldlastevent = last_dict.get(sname,0)
            if oldlastevent == lastevent:
                continue
            # get this logs events.
            next_token = None
            while True:
                if next_token is not None:
                    resp = clientlogs.get_log_events(logGroupName=loggrp, logStreamName=sname, startFromHead=False,
                                                     nextToken=next_token)
                else:
                    resp = clientlogs.get_log_events(logGroupName=loggrp, startFromHead=False, logStreamName=sname)
                next_token = resp.get('nextBackwardToken', None)
                respevt = resp.get('events', [])
                if len(respevt) == 0:
                    break
                filt_evts = [respevt[ix].get('message').split(":")[1].rstrip() for ix in range(0, len(respevt)) if
                             respevt[ix].get('message').startswith(FILTERPAT) and
                             oldlastevent < respevt[ix].get('timestamp') <= lastevent]
                if endswith is not None:
                    filt_evts = [filt_evts[ix] for ix in range(0, len(filt_evts)) if filt_evts[ix].endswith(endswith)]
                all_events.extend(filt_evts)
                if next_token is None:
                    break
    except:
        return [], last_json
    # finally if we are deleting do it here.
    if delete:
        back_day = new_end_time-ONEDAY
        for stream in all_streams:
            if stream.get('lastIngestionTime', 0) < back_day:
                clientlogs.delete_log_stream(logGroupName=loggrp, logStreamName=stream.get('logStreamName', ""))

    return list(set(all_events)), json.dumps(new_dict)


"""
Everything below here is for testing.  The test is to call the above routin and get the list of event.  Then wait a
minute and get all the events.  Then check if there are any missed events.
"""


def parse_args():
    """
    Here are all our arguments.
    :return: parsed args and the parser.
    """
    parse = argparse.ArgumentParser()
    parse.add_argument("--loggrp", help="Name of loging group")
    args = parse.parse_args()
    return args, parse


def main():
    """
    The main program.  Here we just get the parsed arguments, do some checks then call the function that
    does all the work.
    :return: None
    """
    args, parse = parse_args()
    if args.loggrp is None:
        parse.print_help(sys.stderr)
        return
    start_info = None
    generations = []
    for ig in range(0,1000):
        # get the list and new starting point.
        sdemdlist, end_time = get_sdemd_list(start_info, args.loggrp, region_name='us-west-2', delete=False, endswith=('.SDEMD', '.sdemd'))
        if len(sdemdlist) > 0:
            gen = [get_latest_time(start_info), get_latest_time(end_time), sdemdlist]
        else:
            sleep(60)
            continue
        start_info = end_time
        # get everything
        print(str(ig)+","+str(gen[0])+","+str(gen[1])+","+str(len(gen[2])))
        sleep(60)
    return None


def get_latest_time(json_info):
    if json_info is None:
        return 0
    info = json.loads(json_info)
    latest = 0
    for sname, time in info.items():
        if time > latest:
            latest = time
    return latest


if __name__ == "__main__":
    main()
    sys.exit(0)
