# (c) 2019 SmartDrive Systems, Inc.
import os
import sys
import json
import zipfile
import xmltodict
import struct
import datetime
"""
The only import that may need to be installed is xmltodict.  The others are standard python.
"""


class SdemdCRE:
    """
    Class to handle SDEMD files with a minimum of temporary files.
    """
    def __init__(self, filename):
        """
        Constructor.
        :param filename: SDE file path, or a file like object (when streaming).
        """
        self.sde_metadata = {}
        # openit as a Zip file
        self.filename = filename
        if isinstance(filename, str) and not os.path.exists(filename):
            return

        try:
            self.zfile = zipfile.ZipFile(filename)
            self.sde_metadata['sr_sn'] = ""
            self.sde_metadata['event_id'] = ""
            self.sde_metadata['event_number'] = ""
            self.sde_metadata['sr_type'] = ""
            self.sde_metadata['recorded_date_utc'] = ""
            self.sde_metadata['stored_date_utc'] = ""
            self.sde_metadata['sync_date_utc'] = ""
            self.sde_metadata['sync_target']=""
            self.sde_metadata['company'] = {}
            self.sde_metadata['trigger'] = {}
            self.sde_metadata['vehicle'] = {}
            self.sde_metadata['site'] ={}
            self.sde_metadata['review'] = {'reviewed': False, 'observations': []}
            self.sde_metadata['media'] = []
            self.sde_metadata['sensors'] = {}
            self.sde_metadata['smartsense'] = {}

            self.filenames = self.zfile.namelist()
            # get the header file.
            mfst_list = [ f for f in self.filenames if f.endswith(('.xml','.XML')) and not f.endswith(('ersion.xml',
                                       'ersion.XML','eader.xml','eader.XML','DFG.xml','DFG.XML','dfg.xml','dfg.XML'))]
            if len(mfst_list) > 0:
                self.sr_type="SR3"
                self.basename = mfst_list[0].split('.')[0]
                self.sde_metadata['sr_sn'] = self.basename[0:8]
                self.sde_metadata['event_number'] = self.basename
                self.sde_metadata['sr_type']="SR3"
                rawcr = xmltodict.parse(self.zfile.read(mfst_list[0]))
                if 'VideoManifest' in rawcr:
                    self.mfst = rawcr['VideoManifest']
                    if len(self.mfst) > 0:
                        self.sde_metadata['event_id'] = self.mfst.get('@EventID',self.basename[8:])
                        if 'Safety' in self.mfst:
                            safetydict = self.mfst['Safety']
                            self.safety_file = safetydict.get('#text')
                            if '@SampleRate' in safetydict:
                                self.safety_samplerate = int(safetydict['@SampleRate'])
                        if 'Velocity' in self.mfst:
                            velodict = self.mfst['Velocity']
                            if '#text' in velodict:
                                self.location_file = velodict['#text']
                        if 'Video' in self.mfst:
                            viddict = self.mfst['Video']
                            if '#text' in viddict:
                                self.video_file = viddict['#text']
                        if 'Header' in self.mfst:
                            rawhdr = xmltodict.parse(self.zfile.read(self.mfst['Header']))
                            if 'SdeHeader' in rawhdr:
                                self.header = rawhdr['SdeHeader']
                                if 'CameraSettings' in self.header:
                                    if 'TotalDuration' in self.header['CameraSettings']:
                                        self.total_duration = int(self.header['CameraSettings']['TotalDuration'])
                                self.sde_metadata['trigger']['id'] = self.header.get('TriggerTypeId',0)
                                self.sde_metadata['trigger']['latitude'] = self.header.get('Latitude',0)
                                self.sde_metadata['trigger']['longitude'] = self.header.get('Longitude', 0)
                                self.TriggerDateTime = self.header.get('TriggerDateTime', None)
                                self.EventDateTime = self.header.get('EventDateTime', None)
                                self.sde_metadata['vehicle']['vin'] = self.header.get('VinCode', "Unknown")
                                self.sde_metadata['media'] = self.__get_sr3_media()
            else:
                # No Manifest
                header_lst = [ f for f in self.filenames if f.endswith(('eader.xml', 'eader.XML'))]
                if len(header_lst) > 0:
                    raw_header = xmltodict.parse(self.zfile.read(header_lst[0]))
                    self.header = raw_header['SdeHeader']
                    self.sr_type = "SR4"
                    self.sde_metadata['sr_type'] = "SR4"
                    self.sde_metadata['media'] = self.__get_sr4_media()
                    if 'EventInfo' in self.header:
                        self.basename = self.header.get('OriginalSdeFilename',".").split(".")[0]
                        self.sde_metadata['sr_sn'] = self.header['EventInfo'].get('SmartRecorderSN',"")
                        self.sde_metadata['event_id'] = self.basename[8:]
                        self.sde_metadata['event_number'] = self.basename
                        if 'TotalDuration' in self.header['EventInfo']:
                            self.total_duration = int(self.header['EventInfo']['TotalDuration'])
                        self.sde_metadata['trigger']['id'] = self.header['EventInfo'].get('EventTypeID',0)
                        self.sde_metadata['trigger']['latitude'] = self.header['EventInfo'].get('Latitude',0)
                        self.sde_metadata['trigger']['longitude'] = self.header['EventInfo'].get('Longitude', 0)
                        self.TriggerDateTime = self.header['EventInfo'].get('TriggerDateTime', None)
                        self.EventDateTime = self.header['EventInfo'].get('EventDateTime', None)
                    if 'VehicleInfo' in self.header:
                        self.sde_metadata['vehicle']['vin'] = self.header['VehicleInfo'].get('VinCode', "Unknown")
                    if 'Files' in self.header and 'File' in self.header['Files']:
                        for fdict in self.header['Files']['File']:
                            if '@Type' in fdict:
                                ftype = fdict['@Type']
                                if ftype == "Safety":
                                    self.safety_file = fdict.get('#text')
                                    if '@SampleRate' in fdict:
                                        self.safety_samplerate = int(fdict.get('@SampleRate'))
                                elif ftype == 'Location':
                                    self.location_file = fdict.get('#text')
                                    if '@SampleRate' in fdict:
                                        self.location_samplerate = int(fdict.get('@SampleRate'))

            speed, rate = self.__get_speed()
            if speed is not None:
                self.sde_metadata['sensors']['speed_combined_kph'] = speed
                self.__get_event_speed(speed)
            location_header, locations = self.__get_location()
            if locations is not None:
                self.sde_metadata['sensors']['gps'] = locations
            accelerometer, rate = self.__get_accel()
            if accelerometer is not None:
                self.sde_metadata['sensors']['accelerometer'] = accelerometer
            self.__close()
        except Exception as ex:
            print("Exception ")
            raise ex
        return

    def to_json(self):
        """
        Return the metadata as a json string.
        :return:
        """
        return json.dumps(self.sde_metadata)

    def get_metadata(self):
        """
        Return the dictionary.
        :return:
        """
        return self.sde_metadata

    def __del__(self):
        self.__close()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__close()

    def __close(self):
        """
        Close this file and clean up everything.
        :return:
        """
        try:
            self.zfile.close()
        except:
            pass
        return None

    def __get_event_speed(self, speed):
        if hasattr(self,'EventDateTime') and hasattr(self,'TriggerDateTime') and self.EventDateTime is not None and self.TriggerDateTime is not None:
            event = datetime.datetime.strptime(self.EventDateTime, "%Y-%m-%dT%H:%M:%S")
            trig = datetime.datetime.strptime(self.TriggerDateTime, "%Y-%m-%dT%H:%M:%S")
            diff = (trig-event).total_seconds()
            offset = int(diff*self.safety_samplerate)
            cent = int(len(speed)/2)+offset
            if cent < 0:
                cent = 0
            if cent >= len(speed):
                cent = len(speed)-1
            self.sde_metadata['trigger']['speed_combined_kph'] = speed[cent]
        else:
            self.sde_metadata['trigger']['speed_combined_kph'] = speed[int(len(speed)/2)]
        return None

    def __get_sr4_media(self):
        """
        For an SR4 get the media data.
        :return:
        """
        media = list()
        total_dur = 20
        if self.header.get('EventInfo',None) is not None:
            total_dur = self.header.get('EventInfo').get('TotalDuration', 20)
        vidfiles = self.header.get('VideoFiles',None)
        if vidfiles is not None:
            if isinstance(vidfiles.get('VideoFile',[]), list):
                just_vidfile = vidfiles.get('VideoFile',[])
            else:
                just_vidfile = [vidfiles.get('VideoFile')]
            for vid in just_vidfile:
                vidmedia = {}
                vidmedia['filename'] = vid.get('@name', None)
                track = {}
                track['id']=0
                track['frame_rate'] = vid.get('Fps',4)
                track['duration'] = total_dur
                track['type'] = 'video'
                res = vid.get('Resolution',"0x0").split("x")
                track['resolution']={'width': res[0], 'height': res[1]}
                camref = vid.get('CameraReference',[])
                cameras = list()
                if isinstance(camref, list):
                    just_camref = camref
                else:
                    just_camref = [camref]
                for cr in just_camref:
                    cameras.append({'id': cr.get('@CameraID',0), 'name': cr.get('@Position',""),
                                    'offsets':[int(cr.get('@X1',0)), int(cr.get('@Y1',0)), int(cr.get('@X2',0)),
                                               int(cr.get('@Y2',0))]})
                track['cameras'] = cameras
                vidmedia['tracks']=[track]
                media.append(vidmedia)
        return media

    def __get_sr3_media(self):
        """
        For an SR3 get the media data.
        :return:
        """
        media = list()
        if isinstance(self.mfst.get('Video', []), list):
            just_vidfile = self.mfst.get('Video', [])
        else:
            just_vidfile = [self.mfst.get('Video')]
        v_filenames = []
        for vf in just_vidfile:
            if '@ID' in vf and '#text' in vf and '@View' in vf:
                v_filenames.append([vf.get('@ID'), vf.get('#text'), vf.get('@View').split(',')])
        camset = self.header.get('CameraSettings',None)
        if camset is not None:
            fps = camset.get('Fps',4)
            totaldur = camset.get('TotalDuration',20)
            result_cam ={}
            cameras = camset.get('Camera', None)
            if cameras is not None:
                if isinstance(cameras, list):
                    just_cameras = cameras
                else:
                    just_cameras = [cameras]
                for acam in just_cameras:
                    acamid = acam.get('ID',None)
                    name = acam.get('@name', None)
                    cr = acam.get('VideoArea')
                    enabled = acam.get('enable', '0')
                    if acamid is not None and name is not None and cr is not None and enabled == '1':
                        result_cam[acamid] = {'id': acamid, 'name': name,
                                              'offsets': [int(cr.get('@X1', 0)), int(cr.get('@Y1', 0)),
                                                          int(cr.get('@X2', 0)), int(cr.get('@Y2', 0))]}
        for vfile in v_filenames:
            rvf = {'filename':vfile[1]}
            track = {'id': vfile[0], 'type':'video', 'frame_rate': fps, 'duration': totaldur}
            tcams = []
            width = 0
            height=0
            for rcamid in vfile[2]:
                rcam = result_cam.get(rcamid, None)
                if rcam is not None:
                    rcam['offsets'][0] = width +rcam['offsets'][0]
                    tcams.append(rcam)
                    width += rcam['offsets'][2]
                    if rcam['offsets'][3] > height:
                        height = rcam['offsets'][3]
            if len(tcams) > 0:
                track['resolution'] = {'width': width, 'height': height}
                track['cameras'] = tcams
                rvf['tracks'] = [track]
                media.append(rvf)
        return media

    def __get_location(self):
        """
        Get the location information.  The header is returned as a dictionary and the GPS as just a table.
        The third results is the table grouped.
        :return:
        """
        try:
            with self.zfile.open(self.location_file) as lfile:
                raw_location = lfile.read().splitlines()
                location_header = {}
                locations = []
                # The header is delimited by enpty lines.
                doing_header = True
                for rl in raw_location:
                    if len(rl) > 0:
                        if doing_header:
                            strs = rl.decode("utf-8").split("=")
                            location_header[strs[0]] = strs[1]
                        else:
                            locations.append(rl.decode("utf-8").split(","))
                    else:
                        doing_header = False

                return location_header, locations
        except:
            pass
        return None, None

    def __get_speed(self):
        """
        Get the speed data. And the sample rate.
        :return:
        """
        try:
            with self.zfile.open(self.safety_file) as sfile:
                raw_safe = sfile.read()
                # get len of records.
                # the block lenth is the total size divided by the number of samples.
                blen = int(len(raw_safe)/(self.total_duration*self.safety_samplerate))
                # the speed is in all the schema.
                # 13 and 14 are the speed. 10 integer, 6 fraction. lsb
                speed = []
                for i in range(12, len(raw_safe), blen):
                    spd = struct.unpack('<h', raw_safe[i:i + 2])[0] / (1 << 6)
                    speed.append(spd)
                return speed, self.safety_samplerate
        except:
            pass
        return None, 0

    def __get_accel(self):
        """
        Get the accelerometer data. And the sample rate.
        :return:
        """
        try:
            with self.zfile.open(self.safety_file) as sfile:
                raw_safe = sfile.read()
                # get len of records.
                # the block lenth is the total size divided by the number of samples.
                blen = int(len(raw_safe)/(self.total_duration*self.safety_samplerate))
                accel = []
                for i in range(0, len(raw_safe), blen):
                    acf = list()
                    acf.append(self.__accel_to_fl(raw_safe, i))
                    acf.append(self.__accel_to_fl(raw_safe, i+2))
                    acf.append(self.__accel_to_fl(raw_safe, i+4))
                    acf.append(self.__accel_to_fl(raw_safe, i+6))
                    acf.append(self.__accel_to_fl(raw_safe, i+8))
                    acf.append(self.__accel_to_fl(raw_safe, i+10))
                    accel.append(acf)
                return accel, self.safety_samplerate
        except:
            pass
        return None, 0

    def __accel_to_fl(self,b,bidx):
        acc= struct.unpack('<h', b[bidx:bidx + 2])[0] / (1 << 12)
        return acc
