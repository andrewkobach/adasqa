import os
import sys
import argparse
import numpy as np
import xml.etree.ElementTree as et
import zipfile
from itertools import groupby
from operator import itemgetter
import struct

class SdemdFile:
    """
    Class to handle SDE files with a minimum of temporary files.
    """
    def __init__(self, filename):
        """
        Constructor.
        :param filename: SDE file path.
        """
        # openit as a Zip file
        self.filename = filename
        if not os.path.exists(filename):
            return
        try:
            self.zfile = zipfile.ZipFile(filename)
        except zipfile.BadZipFile:
            return
        self.basename = os.path.splitext(os.path.basename(filename))[0]
        self.SR_ID = self.basename[0:8]
        self.filenames = self.zfile.namelist()
        self.video_fps = 4
        # get the header file.
        mfst_list = [ f for f in self.filenames if f in (self.basename+'.xml', self.basename+'.XML')]
        if len(mfst_list) > 0:
            rawcr = self.zfile.read(mfst_list[0])
            mfsttree = et.fromstring(rawcr)

            # get what we need from the mfst
            safe_find = mfsttree.findall("./Safety")
            if len(safe_find) > 0:
                safety_el = safe_find[0]
                if hasattr(safety_el,'text'):
                    self.safety_file = safety_el.text
                if hasattr(safety_el,'attrib'):
                    self.safety_samplerate = int(safety_el.attrib['SampleRate'])

            velo_el = mfsttree.findall("./Velocity")
            if len(velo_el) > 0:
                velo_el = velo_el[0]
                if hasattr(velo_el,'text'):
                    self.location_file = velo_el.text

            video_el = mfsttree.findall("./Video")
            if len(video_el) > 0:
                video_el = video_el[0]
                if hasattr(video_el,'text'):
                    self.video_file = video_el.text

            hdrs = mfsttree.findall("./Header")
            if len(hdrs) > 0:
                hdrs = hdrs[0]
                if hasattr(hdrs,'text'):
                    self.raw_header = self.zfile.read(hdrs.text)
                    self.header_tree = et.fromstring(self.raw_header)
                    e_date_time_el =self.header_tree.find("./EventDateTime")
                    if e_date_time_el is not None:
                        self.event_date_time = e_date_time_el.text
                    camsettings = self.header_tree.findall("./CameraSettings/Fps")
                    if len(camsettings) > 0:
                        self.video_fps = int(camsettings[0].text)
                        ttldur = self.header_tree.findall("./CameraSettings/TotalDuration")
                        if len(ttldur) >0:
                            self.total_duration = int(ttldur[0].text)
                    cameras = self.header_tree.findall("./CameraSettings/Camera")
                    for camera in cameras:
                        camname = camera.attrib['name']
                        if camname is not None and "Forward" in camname:
                            vidarea = camera.find("./VideoArea")
                            if hasattr(vidarea,'attrib') and set(vidarea.attrib.keys()).issuperset(set(['X1','X2','Y1','Y2'])):
                                self.video_bounds = np.array([[vidarea.attrib['X1'],vidarea.attrib['Y1']],[vidarea.attrib['X2'],vidarea.attrib['Y2']]],dtype=np.int32)
                                break
                    if not hasattr(self,"video_bounds"):
                        res = self.header_tree.findall(".CameraSettings/Resolution")
                        if res is not None and len(res) > 0:
                            resolution = res[0].text.split("x")
                            self.video_bounds = np.array([[0,0],[int(int(resolution[0])/2),int(resolution[1])]],dtype=np.int32)

        else:
            # No Manifest
            header_lst = [ f for f in self.filenames if f.endswith(('eader.xml', 'eader.XML'))]
            if len(header_lst) > 0:
                self.raw_header = self.zfile.read(header_lst[0])
                self.header_tree = et.fromstring(self.raw_header)
                e_date_time_el =self.header_tree.find("./EventInfo/EventDateTime")
                if e_date_time_el is not None and hasattr(e_date_time_el,'text'):
                    self.event_date_time = e_date_time_el.text
                self.total_duration = int(self.header_tree.findall("./EventInfo/TotalDuration")[0].text)
                location_el = self.header_tree.findall("./Files/File[@Type='Location']")[0]
                self.location_file = location_el.text
                safety_el = self.header_tree.findall("./Files/File[@Type='Safety']")[0]
                self.safety_file = safety_el.text
                self.safety_samplerate = int(safety_el.attrib['SampleRate'])

                videofiles = self.header_tree.findall("./VideoFiles//VideoFile")

                self.video_fps = 4
                foundforward = False
                for videofile in videofiles:
                    crefs = videofile.findall("./CameraReference")
                    # Find the forward one.
                    for cref in crefs:
                        if cref.attrib['Position'] != "Forward":
                            continue
                        if set(cref.attrib.keys()).issuperset(set(['X1','X2','Y1','Y2'])):
                            self.video_bounds = np.array([[cref.attrib['X1'],cref.attrib['Y1']],[cref.attrib['X2'],cref.attrib['Y2']]], dtype=np.int32)
                            foundforward = True
                            break
                    fpslst = videofile.findall("./Fps")
                    if len(fpslst) > 0:
                        self.video_fps = int(fpslst[0].text)
                    self.video_file = videofile.attrib['name']
                    if foundforward:
                        break

    def __del__(self):
        self.close()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def close(self):
        """
        Close this file and clean up everything.
        :return:
        """
        try:
            self.zfile.close()
        except:
            pass
        return None

    def is_good(self):
        return len(self.missing_list()) == 0

    def missing_list(self):
        fullset = set(['zfile', 'filenames', 'safety_file', 'safety_samplerate', 'location_file',
                       'video_file', 'raw_header', 'header_tree', 'video_fps', 'total_duration', 'video_bounds',
                       'basename', 'filename', 'SR_ID', 'event_date_time'])
        missing = set(self.__dict__.keys()).symmetric_difference(fullset)
        return missing

    def get_location(self):
        """
        Get the location information.  The header is returned as a dictionary and the GPS as just a table.
        The third results is the table grouped.
        :return:
        """
        with self.zfile.open(self.location_file) as lfile:
            raw_location = lfile.read().splitlines()
            location_header = {}
            locations = []
            # The header is delimited by enpty lines.
            doing_header = True
            for rl in raw_location:
                if len(rl) > 0:
                    if doing_header:
                        strs = rl.decode("utf-8").split("=")
                        location_header[strs[0]] = strs[1]
                    else:
                        locations.append(rl.decode("utf-8").split(","))
                else:
                    doing_header = False
            locations = np.array(locations)
            # group the locations.
            groups = []
            for k, g in groupby(sorted(locations, key=itemgetter(1)), key=itemgetter(1)):
                groups.append(list(g))

            return location_header, locations, groups
        return None, None, None

    def get_speed(self):
        """
        Get the speed data. And the sample rate.
        :return:
        """
        with self.zfile.open(self.safety_file) as sfile:
            raw_safe = sfile.read()
            # get len of records.
            # the block lenth is the total size divided by the number of samples.
            blen = int(len(raw_safe)/(self.total_duration*self.safety_samplerate))
            # the speed is in all the schema.
            # 13 and 14 are the speed. 10 integer, 6 fraction. lsb
            speed = []
            for i in range(12, len(raw_safe), blen):
                spd = struct.unpack('<h', raw_safe[i:i + 2])[0] / (1 << 6)
                speed.append(spd)
            return speed, self.safety_samplerate
        return None, 0

    def get_video(self, position='Forward'):
        """
        Get the Video File Name
        :param position:
        :return: File name , Frames per second.
        """
        return os.path.join(os.path.dirname(self.filename),self.video_file), self.video_fps

    def get_video_filename(self):
        if hasattr(self,'video_file'):
            return self.video_file
        return None

    def get_YYYYMMDD(self):
        return "".join(self.event_date_time.split('T')[0].split('-'))

# test code.
if __name__ == "__main__":
    parse = argparse.ArgumentParser()
    parse.add_argument("--sde", help="sde file")
    parse.add_argument("--novideo", action='store_true', default=False, help="Look at SDEMD files without video.")
    args = parse.parse_args()

    if args.sde is not None and os.path.exists(args.sde) and os.path.isfile(args.sde):
        sde = SdemdFile(args.sde)
        loc_hdr, loc, loc_grp = sde.get_location()
        sp, sr = sde.get_speed()
        print(sp)
        vname, fpss = sde.get_temp_video()
        print(vname)
        print("debug stop")
        sde.close()
        sys.exit(0)
    if args.sde is not None and os.path.exists(args.sde) and os.path.isdir(args.sde):
        print("Walking Dir ",args.sde)
        count_good = 0
        for dirname, subdirlist, files in os.walk(args.sde,topdown=False):
            for f in files:
                if f.endswith((".SDEMD",".sdemd")):
                    sdemd_filename = os.path.join(dirname,f)
                    sde_no_ext = os.path.splitext(sdemd_filename)[0]
                    if args.novideo or os.path.exists(sde_no_ext+".SDV") or os.path.exists(sde_no_ext+".sdv"):
                        try:
                            sde = SdemdFile(os.path.join(dirname,f))
                            if not sde.is_good():
                                miss = sde.missing_list()
                                if not 'safety_file' in miss:
                                    print(os.path.join(dirname, f))
                                    print(sde.missing_list())
                                    print()
                            else:
                                count_good +=1
                        except:
                            print(os.path.join(dirname, f))
                            print(sys.exc_info()[1])
                            print()

                        sde.close()
        print("Number of good ones ",count_good)
