#!/bin/bash


if [[ $(pgrep -f ADAS_overhead.py) ]]; then
	echo Do not run ADAS_overhead
else
	DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
	export PYTHONPATH=$DIR
	cd $DIR
	LOGDIR=$DIR/../logs
	mkdir -p $LOGDIR
	DOM=$(date +%-d)
	LOGFILE=$LOGDIR/log$DOM.txt

	date >> $LOGFILE 2>&1
	/opt/conda/bin/python3 ADAS/ADAS_overhead.py  --loggrp "/aws/lambda/geoimage-sdesync-logging" --outbucket "sdsidev.int.adas.videos" --debug --numlanes 2000 >> $LOGFILE 2>&1
	/opt/conda/bin/python3 ADAS/calibration_table.py --bucket "sdsidev.int.adas.videos" >> $LOGFILE 2>&1
	/opt/conda/bin/python3 ADAS/index.py --bucket sdsidev.int.adas.videos --prefix "" --template ADAS/index_template.html >> $LOGFILE 2>&1
	date >> $LOGFILE 2>&1
fi
