import gc
import os
import argparse
import cv2
import numpy as np
from sklearn.cluster import KMeans
from scipy import stats
from cv_utils.sdevideo import SdeVideo
from cv_utils.intersections import my_b_ln, m_b_int, intersectLinefour
from cv_utils.MedianAbsDev import MedianAbsDev


def get_first_pass_vanishing_point_files_mean(dirname,vid_files):
    """
    Take a list of video files for detecting the vanishing point.
    For each file find a vanishing point and elimnate the bad ones.
    :param dirname:
    :param vid_files:
    :return:
    """
    vanish = []
    for vfile in vid_files:
        vcap = SdeVideo(os.path.join(dirname, vfile))
        maxx, maxy = vcap.get_shape()
        while True:
            ret, frame = vcap.read()
            if ret == False:
                break
            vanframe = get_segment_intersections(frame, maxx, maxy, 0,None,False)
            vanish.extend(vanframe)
    if len(vanish) == 0:
        return None, None, None
    vanish = np.array(vanish)
    vpstd = np.max(np.std(vanish,axis=0)*2)
    while vpstd > int(min(maxx,maxy)/10):
        mad = MedianAbsDev(vanish)
        vanish = vanish[mad.get_inliers()]
        vpstd = np.max(np.std(vanish,axis=0)*2)
    if vpstd < int(min(maxx,maxy)/10):
        vpstd = int(min(maxx,maxy)/10)
    vanpt = np.int32(np.mean(vanish, axis=0))
    return vanpt, int(vpstd),vid_files

def get_segment_intersections(frame, maxx, maxy, starty, out,debug):
    gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
    gray = cv2.blur(gray,(7,3))
    th, ret = cv2.threshold(gray.copy(), 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    canny = cv2.Canny(gray,th/2,th,apertureSize=3)
    lines = cv2.HoughLinesP(canny, 1, np.pi / 180.0, 5, minLineLength=3, maxLineGap=5)
    segsl = []
    segsr = []
    slopemax = 3*maxx/maxy
    xmid = maxx/2

    if debug:
        cv2.imshow("frame_gray",gray)
        cv2.imshow("frame_segs", canny)
        cv2.waitKey(60)


    if not lines is None :
        for l in lines:
            loff = l[0] + (0, starty, 0, starty)
            m,b,r = my_b_ln(loff[0:2],loff[2:4])
            if r == 1 and abs(m) < slopemax:
                # see if we intersect the center line correctly.
                xc,yc,r = m_b_int(m, b, 0.0, xmid)
                if r == 1 and yc < min(loff[1],loff[3]):
                    if m < 0:
                        segsl.append(loff)
                    else:
                        segsr.append(loff)

        segsl = filterlongesthalf(segsl)
        segsr = filterlongesthalf(segsr)
        # Now find the intersections as candidate vanishing points.
        # we only look in the middle third horizontally and in the op 3/4.
        minthirdx = maxx/3
        maxthirdx = minthirdx+minthirdx
        maxyquarter = maxy
        vanish = []

        for i in range(0, len(segsl)):
            for j in range(i+1, len(segsr)):
                xi, yi, ret = intersectLinefour(segsl[i], segsr[j])
                if ret == 1:
                    if yi >= 0 and yi <= maxyquarter and xi >= minthirdx and xi <= maxthirdx:
                        vanish.append([xi, yi])

        return vanish

    return []

def filterlongesthalf(segsin):
    if len(segsin) < 100:
        return segsin
    segs = np.array(segsin)
    lens = np.array([abs(segs[i][0]-segs[i][2])+abs(segs[i][1]-segs[i][3]) for i in range(0,len(segs))])
    mn = np.mean(lens)
    lensind = (-lens).argsort()
    many = int(len(lens)/2)
    if many > 200:
        many=200
    segs = segs[lensind[0:many]]
    lens = lens[lensind[0:many]]
    return segs[lens>mn]


# main to test with.

if __name__ == "__main__":
    # parse the arguments.
    parse = argparse.ArgumentParser()
    parse.add_argument("--vd", help="Video Directory")
    parse.add_argument("--sp",help="Speed in mph")
    args = parse.parse_args()

    if args.vd and os.path.isdir(args.vd):
        # we are looking for a directory that contains directories of video files.
        onlydirsall = [f for f in os.listdir(args.vd) if os.path.isdir(os.path.join(args.vd, f))]
        if len(onlydirsall) == 0:
            onlydirsall = [args.vd]
        # now see if each dir contains video files.
        onlydirs = []
        onlydirsfiles = []
        for sdir in onlydirsall:
            sdirpath = os.path.join(args.vd, sdir)
            onlyfiles = [f for f in os.listdir(sdirpath) if os.path.isfile(os.path.join(sdirpath, f)) and f.endswith("mp4")]
            if len(onlyfiles) > 0:
                onlydirs.append(sdirpath)
                onlydirsfiles.append(onlyfiles)

        for i in range(0,len(onlydirs)):
            vp, vr, ffiles = get_first_pass_vanishing_point_files_mean(onlydirs[i],onlydirsfiles[i])
            print(onlydirs[i])
            print(vp)
            print(vr)
            print(len(onlydirsfiles[i]))
            print(len(ffiles))
            print(ffiles)
