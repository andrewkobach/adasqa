from setuptools import setup

setup(
    name='cv_tools',
    version='1',
    packages=['ADAS', 'cv_utils', 'transform', 'hood_detect', 'vanishingpoint'],
    url='',
    license='',
    author='BradR',
    author_email='',
    description=''
)
