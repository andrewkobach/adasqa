#!/bin/bash


if [[ $(pgrep -f hood_hide.py) ]]; then
	echo Do not run hood hide
else
	DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
	export PYTHONPATH=$DIR
	cd $DIR
	LOGDIR=$DIR/../logs
	mkdir -p $LOGDIR
	DOM=$(date +%-d)
	LOGFILE=$LOGDIR/log$DOM.txt

#	/opt/conda/bin/python3 hood_detect/hood_hide.py  --sdebucket "sdsidev.int.event.syncsde.sde" --dbbucket "dev.int.syncsde.smartdrivesystems.com/db" --outbucket "sdsidev.ext.event.maps.jpeg" --bottompercent 30 --crop >> $LOGFILE 2>&1
	/opt/conda/bin/python3 hood_detect/hood_hide.py  --loggrp "/aws/lambda/geoimage-sdesync-logging" --dbbucket "dev.int.syncsde.smartdrivesystems.com/db" --outbucket "sdsidev.ext.event.maps.jpeg" --bottompercent 30 --crop >> $LOGFILE 2>&1
fi
