import os
import argparse
import cv2
import numpy as np
import skimage.morphology as morph
import itertools
from cv_utils.sdevideo import SdeVideo
from sklearn.linear_model import RANSACRegressor
from sklearn.base import BaseEstimator
from scipy.optimize import curve_fit
from scipy.interpolate import BSpline
from . import rcnn_model_wrap


"""
Detect hoods of vehicles.
"""


class SplEstimator(BaseEstimator):
    """
    Estimate using b-splines
    """
    def __init__(self, initval, nknot=3, max_x=720, anchors=None):
        """
        Create Spline Estimator using fixed knots
        :param initval: Initial y value for spline.
        :param nknot: Number of knots
        :param max_x: Maximum X value
        :param anchors: End points spline must go through
        """
        self.nknot = nknot
        self.max_x = max_x
        self.initval = initval
        self.anchors = anchors
        self.t = None
        self.c = None
        self.k = 2

    def splfunc(self, x, *parm):
        """
        Evaluate the spline
        :param x: X value to evaluate at.
        :param parm: Spline coefficients.
        :return: The f(x) for the spline.
        """
        return (BSpline(self.t, parm, self.k))(x)

    def fit(self, x, y, sample_weight=None):
        """
        Fit Spline to the data.
        :param x: X values
        :param y: Corresponding Ys
        :param sample_weight: Coresponding Weights.
        :return:
        """
        xe = None
        ye = None
        ws = None

        if self.anchors is None:
            try:
                self.getinit_param()
                xe = x.flatten()
                xsrt = np.argsort(xe)
                self.c, pcov = curve_fit(self.splfunc, xe[xsrt], y[xsrt], p0=self.c, sigma=(sample_weight if sample_weight is None else 1.0/sample_weight[xsrt]), absolute_sigma=False, method='trf')
            except Exception as e:
                print(e)
                raise e
        else:
            try:
                xe = np.concatenate([[self.anchors[0, 0]], x.flatten(), [self.anchors[1,0]]])
                ye = np.concatenate([[self.anchors[1, 1]], y, [self.anchors[1,1]]])
                if sample_weight is None:
                    ws = np.full(len(ye), 1.0)
                    ws[0] = ws[len(ws)-1] = ws.sum()
                else:
                    wsum = np.sum(sample_weight)
                    ws = np.concatenate([[wsum], sample_weight, [wsum]])
                self.getinit_param()
                xsrt = np.argsort(xe)
                self.c, pcov = curve_fit(self.splfunc, xe[xsrt], ye[xsrt], p0=self.c, sigma=1.0/ws[xsrt],absolute_sigma=False, method='trf')
            except Exception as e:
                print(e)
                raise e
        return None

    def score(self,x,y):
        """
        Return the mean squared error.
        :param x: X values
        :param y: Coresponding Ys
        :return: Mean Squared Errror
        """
        py = self.splfunc(x.flatten(),*self.c)
        mse = ((py-y)**2).mean()
        return mse

    def predict(self,x):
        """
        Predict the Ys given X
        :param x: X Values
        :return: f(X) returned
        """
        return self.splfunc(x.flatten(), *self.c)

    def getinit_param(self):
        """
        Find the initial spline guess.
        :return:
        """
        incsize = int(self.max_x / (self.nknot + self.k + 1))
        self.t = np.array([st for st in range(0,self.max_x+1,incsize)])
        self.c = np.full(len(self.t)-self.k-1,self.initval,dtype=np.float)
        return None


class PolyEstimator(BaseEstimator):
    """
    Class to provide polynomial estimator.
    """
    def __init__(self,anchors=None,degree=5):
        """
        Create Polynomial Estimator
        :param degree: Polniomial degree
        :param anchors: End points polynomial must go through
        """
        self.polycoef = None
        self.anchors = anchors
        self.degree = degree

    def fit(self,x,y,sample_weight=None):
        """
         Fit Spline to the data.
         :param x: X values
         :param y: Corresponding Ys
         :param sample_weight: Coresponding Weights.
         :return:
        """
        if self.anchors is None:
            if sample_weight is not None:
                self.polycoef = np.polyfit(x.flatten(), y, deg=4,w=sample_weight)
            else:
                self.polycoef = np.polyfit(x.flatten(),y,deg=4)
        else:
            xe = np.concatenate([[self.anchors[0,0]],x.flatten(),[self.anchors[1,0]]])
            ye = np.concatenate([[self.anchors[1,1]],y,[self.anchors[1,1]]])
            if sample_weight is None:
                ws = np.full(len(ye),1.0)
                ws[0] = ws[len(ws)-1] = ws.sum()
            else:
                wsum = np.sum(sample_weight)
                ws = np.concatenate([[wsum], sample_weight, [wsum]])
            self.polycoef = np.polyfit(xe, ye, deg=self.degree, w=ws)
        return None

    def score(self,x,y):
        """
        Return the mean squared error.
        :param x: X values
        :param y: Coresponding Ys
        :return: Mean Squared Errror
        """
        py = np.polyval(self.polycoef, x.flatten())
        mse = ((py-y)**2).mean()
        return mse

    def predict(self,x):
        """
        Predict the Ys given X
        :param x: X Values
        :return: f(X) returned
        """
        return np.polyval(self.polycoef, x.flatten())


class HoodDetect:
    """
    Hood detector Class
    """

    def __init__(self, height_width=None, threshold=80, emptythresh=0.1, dilate_size=3, debug=False):
        """
        Create the hood detector
        :param height_width: Height and width tuple for the frames.
        :param threshold: Hight threshold for determining menaingfull data is in 95th percentila
        :param emptythresh: When determining if an area is empty this is the threshold.
        :param dilate_size: When dialating images this is how much
        :param debug: Debug is output when True
        """
        self.threshold = threshold
        self.emptythresh = emptythresh
        self.debug = debug
        self.map = None
        self.height_width = None
        self.poly_line = None
        self.hood_scanline = None
        if height_width is not None:
            self.set_height_width(height_width)
        self.selem = morph.square(dilate_size)
        self.dilate_size = dilate_size
        self.framecount = 0
        self.weightsum = 0
        self.confidence = 0
        self.segments = np.array([])
        self.maxbright_file = None
        self.maxbright_frame = 0

    def set_height_width(self,height_width):
        """
        Allow setting the height and width after creating the object
        :param height_width: Height and width of the image
        :return:
        """
        self.hood_scanline=height_width[0]-1
        self.height_width = height_width
        self.map = np.zeros(self.height_width,dtype=np.uint32)
        self.poly_line = (np.array([[0, self.height_width[0] - 1], [self.height_width[1] - 1, self.height_width[0] - 1]])).reshape((-1, 1, 2))
        return None

    def add_pts(self, xs, ys, weight):
        """
        Add a list of segments to our map.
        :param xs , ys: x and y lists for points.
        :param weight: Weight to apply to the segments.
        :return:
        """
        weight = 1
        if len(ys) > 0:
            tmp = np.zeros(self.map.shape, dtype=np.uint32)
            tmp[ys, xs] = 1
            tmp = morph.binary_dilation(tmp, self.selem) * np.uint32(weight)
            if self.debug:
                tmpd = (tmp*255).astype(np.uint8)
                cv2.imshow("add",tmpd)
                cv2.waitKey(60)
                if outdebug is not None:
                    outdebug.write(cv2.cvtColor(tmpd,cv2.COLOR_GRAY2BGR))

            self.map += tmp
        self.framecount +=1
        self.weightsum += weight

        return None

    def find_hood(self):
        """
        Finally find the hood line and scanline.
        :return: hood scan line
        """
        # This version just tries to find the curve.
        if self.framecount == 0:
            self.confidence = 1.0
            return

        self.calc_poly()
        return self.hood_scanline


    def get_hood_mask(self):
        """
        Return a map of the hood edge points.  This is realy only
        usefull for debugging.
        :return:
        """
        mapth = self.map.copy()
        maxval = np.percentile(mapth[mapth != 0], self.threshold)
        mapth[mapth <= maxval] = 0
        mapth[mapth > maxval] = 255
        return mapth.astype(np.uint8)

    def calc_poly(self):
        """
        Get a poly line of the hood edge.
        :return: unreduced poly line.
        """
        mapth = self.map.copy()
        # take only the top percentil points.
        maxval = np.percentile(mapth[mapth !=0], self.threshold)
        yx = np.where(mapth > maxval)
        ys = []
        xs = []
        for xx,yl in itertools.groupby(np.sort(np.array(list(zip(yx[0],yx[1])),dtype=[('ys',int),('xs',int)]),order=['xs','ys']),key=lambda x:x[1]):
            ysor = np.array(np.array(list(yl)).tolist())
            # we want to get the highest weight points of the dilate size
            # for each X column.
            yweig = mapth[ysor[:,0],ysor[:,1]]
            # sort the weights and get the sorted index from highest to lowest
            weisort = np.argsort(-yweig)
            # now take the top dilatesize of these
            weisort = weisort[0:min(len(weisort),self.dilate_size)]
            # make sure we have unique points.
            weisort = np.unique(np.append(weisort,range(0,min(self.dilate_size*2,len(ysor)))))
            # save the points.
            for i in weisort:
                ys.append(ysor[i][0])
                xs.append(xx)
        ys = np.array(ys)
        xs = np.array(xs)
        if len(xs) == 0:
            self.hood_scanline = self.height_width[0] - 1
            self.poly_line = (np.array([[0, self.height_width[0] - 1], [self.height_width[1] - 1, self.height_width[0] - 1]])).reshape((-1, 1, 2))
            return self.poly_line
        # get the weights for each point. and normalize.
        ws = mapth[ys,xs]
        ws = ws / np.max(ws)
        if self.debug:
            mapthd = mapth.copy()
            mapthd[mapthd<=maxval]=0
            mapmax = np.max(mapthd)
            mapthd = mapthd * 255.0 / mapmax
            mapthd = mapthd.astype(np.uint8)
            cv2.imshow("map_fit", cv2.cvtColor(mapthd, cv2.COLOR_GRAY2BGR))
            mapthd = mapth.copy()
            mapthd[:,:] = 0
            mapthd[ys,xs] = 255.0
            mapthd = mapthd.astype(np.uint8)
            cv2.imshow("map_select", cv2.cvtColor(mapthd, cv2.COLOR_GRAY2BGR))
            cv2.waitKey(60)

        # fit 2 3 and 4 without anchors. Then with.
        resultsfit = []
        xp = np.array(range(0, self.height_width[1]))
        resultsfit.append(self.ransack_and_fit(SplEstimator(nknot=2,initval=self.height_width[0],max_x=self.height_width[1]), xs, ys, ws, xp))
        resultsfit.append(self.ransack_and_fit(PolyEstimator(degree=3), xs, ys, ws, xp))
        resultsfit.append(self.ransack_and_fit(SplEstimator(nknot=3,initval=self.height_width[0],max_x=self.height_width[1]), xs, ys, ws, xp))
        resultsfit = np.array(resultsfit)
        # find the best fit.
        minres = np.argmin(resultsfit[:, 1])
        self.poly_line = resultsfit[minres,0]
        self.hood_scanline = np.min(self.poly_line[:,:,1])
        # calculate confidence
        avg_abs_error = resultsfit[minres,1]
        if avg_abs_error < self.dilate_size and (np.max(self.map)/self.weightsum) > self.emptythresh:
            self.confidence = 1.0
        else:
            # for the curve we chose see how much hits the points.
            yfilt = np.concatenate((self.poly_line[:,:,1]+1,self.poly_line[:,:,1],self.poly_line[:,:,1]-1))
            yfilt[yfilt > self.height_width[0]-1]=self.height_width[0]-1
            xfilt = np.concatenate((self.poly_line[:,:,0], self.poly_line[:,:,0], self.poly_line[:,:,0]))
            ineements = self.map[yfilt,xfilt]
            ineements = ineements[ineements>0]
            if len(ineements) > 0:
                self.confidence = np.mean(ineements)/self.weightsum
            else:
                self.confidence = 0
        if self.debug:
            mrp = self.map.copy()
            mapmax = np.max(mrp)
            mrp = cv2.cvtColor((mrp * 255.0 / mapmax).astype(np.uint8), cv2.COLOR_GRAY2BGR)
            cv2.polylines(mrp, [resultsfit[0][0]], False, (255,255,255) if minres ==0 else (0, 0, 255), 2)
            cv2.polylines(mrp, [resultsfit[1][0]], False, (255,255,255) if minres ==1 else (0, 255, 0), 2)
            cv2.polylines(mrp, [resultsfit[2][0]], False, (255,255,255) if minres ==2 else (255, 0, 0), 2)
            cv2.imshow("mrnoa",mrp)
            cv2.waitKey(60)
        #
        if self.confidence < self.emptythresh:
            self.hood_scanline = self.height_width[0] - 1
            self.poly_line = (np.array([[0, self.height_width[0] - 1], [self.height_width[1] - 1, self.height_width[0] - 1]])).reshape((-1, 1, 2))

        return self.poly_line

    def reduced_to_polygon(self):
        """
        Take the poly line and convert it to a polygon along the bottom
        edge of the frame.
        :return: Polygon
        """
        rpoly = [self.poly_line[0,0]]
        curx = self.poly_line[0,0,0]
        cury = self.poly_line[0,0,1]
        i = 1
        while i < len(self.poly_line):
            if self.poly_line[i,0,0] == curx:
                i=i+1
                while i < len(self.poly_line):
                    if self.poly_line[i,0,0] != curx:
                        break
                    i = i+1
                rpoly.append(self.poly_line[i-1,0])
            elif self.poly_line[i,0,1] == cury:
                i=i+1
                while i < len(self.poly_line):
                    if self.poly_line[i,0,1] != cury:
                        break
                    i = i+1
                rpoly.append(self.poly_line[i-1,0])
            if i < len(self.poly_line):
                rpoly.append(self.poly_line[i,0])
                curx = self.poly_line[i,0,0]
                cury = self.poly_line[i,0,1]
                i = i+1
        # fill in start to end polygon.
        rpoly.append([self.height_width[1]-1,self.height_width[0]-1])
        rpoly.append([0,self.height_width[0]-1])
        rpoly.append(rpoly[0])
        return (np.array(rpoly)).reshape((-1, 1, 2))

    def ransack_and_fit(self, estimator, xs , ys, ws, xp):
        """
        Wrapper to handle RANSAC errors.
        :param estimator: Estimator Function.
        :param xs: Xs to fit
        :param ys: Ys to fit
        :param ws: Weights for each point.
        :param xp: Prediction X values to return
        :return: F(xp), MSE
        """
        min_samples = int(.1*len(xs))
        while True:
            ransac = RANSACRegressor(estimator ,min_samples=min_samples)
            try:
                ransac.fit(np.array(xs).reshape((-1, 1)), ys, ws)
            except FloatingPointError:
                # sample sizr too big error.
                min_samples = int(min_samples/2)
                continue
            except Exception as e:
                print(e)
                yp = np.empty(len(xp))
                yp.fill(self.height_width[0])
                return [(np.array(np.column_stack((xp, yp)), dtype=np.int32)).reshape((-1, 1, 2)),self.height_width[0]*self.height_width[1]]
            break
        ymin = np.min(ys)
        yp = ransac.predict(xp)
        # Truncate to hood scan line
        yp[yp > (self.height_width[0]-1)] = self.height_width[0]-1
        yp[yp < ymin] = ymin
        totalerror = 0
        for i in range(0,len(xp)):
            totalerror += np.sum(np.abs(ys[xs==xp[i]] -yp[i])*ws[xs==xp[i]])
        return [(np.array(np.column_stack((xp, yp)), dtype=np.int32)).reshape((-1, 1, 2)),totalerror/len(ys)]

    def detect_hood_from_files_model(self,dir,files, model):
        """
        Find the hood from a number of video files.
        :param dir: Directory to find the files in
        :param files: List of file names.
        :return: Hood scan line found.
        """
        vcap = SdeVideo(os.path.join(dir, files[0]))
        maxx, maxy = vcap.get_shape()
        vcap.release()
        self.set_height_width((maxy,maxx))
        # keep track of the brightest frame.
        brightlst = []
        brightvals = []
        hoodid = 6
        for f in files:
            vcap = SdeVideo(os.path.join(dir, f))
            while True:
                ret, frame = vcap.read()
                if not ret:
                    break

                res = model.detect([frame], verbose=0)
                hoodids = np.where(res[0]['class_ids'] == hoodid)
                px = []
                py = []
                weightv = int(np.mean(cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)))
                if len(hoodids[0])  > 0:
                    for j in hoodids[0]:
                        if self.debug:
                            cv2.imshow("hoodmask",(res[0]['masks'][:,:,j])*255)
                            cv2.waitKey(60)
                        msk = np.argmax(res[0]['masks'][:,:,j],axis=0).flatten()
                        if len(msk) == 0:
                            continue
                        for i in range(0,len(msk)):
                            if msk[i] != 0:
                                px.append(i)
                                py.append(msk[i])
                self.add_pts(px,py, weightv)
                if self.debug:
                    cv2.imshow("true",frame)
                    cv2.waitKey(60)
                brightlst.append([f,vcap.get_last_frame_no()])
                brightvals.append(weightv)
            vcap.release()

        # find a bright frame.
        val80 = np.argsort(brightvals)
        maxbr = val80[int(len(val80)*.80)]
        self.maxbright_file = brightlst[maxbr][0]
        self.maxbright_frame = brightlst[maxbr][1]
        self.find_hood()
        return self.hood_scanline



if __name__ == "__main__":

    np.seterr(all='raise')
    # parse the arguments.
    parse = argparse.ArgumentParser()
    parse.add_argument("--vd",help="Videos directory")
    parse.add_argument("--d",help="Debug", default=False, type=bool)
    parse.add_argument("--m", help="min number of files", default=-1, type=int)
    parse.add_argument("--x", help="max number of files", default=-1, type=int)
    parse.add_argument("--vo",help="Video output directory to evaluate")
    args = parse.parse_args()

    dooutput = args.d is not True

    """
    If vd is set then we are processing videos and finding the hood.  The directory 
    structure has a root and for each truck a directory of videos.
    """
    if args.vd and os.path.isdir(args.vd):

        # Setup the output paths.
        onlydirsall = [f for f in os.listdir(args.vd) if os.path.isdir(os.path.join(args.vd, f))]
        if len(onlydirsall) == 0:
            # only one directory.
            onlydirnames = [os.path.basename(args.vd)]
            outdir = os.path.dirname(args.vd) + "_outputhood"
            onlydirs =[args.vd]
            onlydirsfiles=[[f for f in os.listdir(args.vd) if os.path.isfile(os.path.join(args.vd, f)) and f.endswith("mp4")]]
        else:
            # multiple directories.
            outdir = args.vd+"_outputhood"
            onlydirs = []
            onlydirnames = []
            onlydirsfiles = []
            for sdir in onlydirsall:
                sdirpath = os.path.join(args.vd, sdir)
                onlyfiles = [f for f in os.listdir(sdirpath) if os.path.isfile(os.path.join(sdirpath, f)) and f.endswith("mp4")]
                if len(onlyfiles) > 0:
                    onlydirnames.append(sdir)
                    onlydirs.append(sdirpath)
                    onlydirsfiles.append(onlyfiles)
        if not os.path.exists(outdir):
            os.mkdir(outdir)
        fourcc = cv2.VideoWriter_fourcc(*'MP4V')

        # we are looking for a directory that contains directories of video files.
        # now see if each dir contains video files.
        confid = []
        training = []
        sampleframes =  []
        overallmaxx = 0
        overallmaxy = 0
        stop = False

        model_model = rcnn_model_wrap.load_model()

        for i in range(0,len(onlydirs)):
            if stop:
                break
            outpathfile = os.path.join(outdir , onlydirnames[i]+".mp4")
            outpathfiledebug = os.path.join(outdir , onlydirnames[i]+"_debug.mp4")
            if os.path.exists(outpathfile):
                continue
            if args.m >= 0 and len(onlydirsfiles[i]) < args.m:
                continue
            if args.x >= 0 and len(onlydirsfiles[i]) > args.x:
                continue

            # create the video writer for the results.
            vcap = SdeVideo(os.path.join(onlydirs[i],onlydirsfiles[i][0]))
            maxx, maxy = vcap.get_shape()
            if maxx > overallmaxx:
                overallmaxx = maxx
            if maxy > overallmaxy:
                overallmaxy = maxy
            vcap.release()
            outdebug = None
            # if args.d :
            #     outdebug = cv2.VideoWriter(outpathfiledebug, fourcc, 20.0, (maxx, maxy))

            hd = HoodDetect(debug=args.d)
            hline = hd.detect_hood_from_files_model(onlydirs[i],onlydirsfiles[i], model_model)
            hmask = hd.get_hood_mask()
            hpoly = hd.reduced_to_polygon()
            confidence = hd.confidence
            if len(hd.segments) > 0:
                # noinspection PyTypeChecker
                trn = np.concatenate((np.full((len(hd.segments),1), onlydirnames[i]),hd.segments,np.full((len(hd.segments),1),0)),axis=1)
                try:
                    idxtrn = list(hd.segments[:,3]).index(hd.hood_scanline)
                    trn[idxtrn, -1] = 1
                except:
                    pass

                training.extend(list(trn))
            confid.append([onlydirnames[i],confidence])
            bmask = cv2.bitwise_not(hmask)
            bmask = cv2.cvtColor(bmask,cv2.COLOR_GRAY2BGR)
            zz=np.zeros((hmask.shape[0], hmask.shape[1]), dtype=np.uint8)
            cmask=np.stack((hmask, zz, zz),
                     axis=-1)
            # noinspection PyArgumentList
            out = cv2.VideoWriter(outpathfile, fourcc, 20.0, (maxx, maxy))
            for f in onlydirsfiles[i]:
                fvcap = SdeVideo(os.path.join(onlydirs[i],f))
                if stop:
                    break
                while True:
                    ret, frame = fvcap.read()
                    if not ret:
                        break
                    if dooutput and hd.maxbright_file == f and hd.maxbright_frame == fvcap.get_last_frame_no():
                        bframe = frame.copy()
                        cv2.line(bframe, (0, hline), (maxx, hline), (0, 0, 255), 2)
                        cv2.polylines(bframe, [hpoly], False, (0, 255, 0), 2)
                        sampleframes.append(bframe)
                        # cv2.imshow("bframe",bframe)
                        # cv2.waitKey(60)
                        # print("bframe "+f)

                    cv2.line(frame,(0,hline),(maxx,hline),(0,0,255),2)
                    if frame.shape[1] > hmask.shape[1]:
                        frame = frame[:,0:hmask.shape[1]]
                    elif frame.shape[1] < hmask.shape[1]:
                        frame = np.pad(frame,((0,0),(0,hmask.shape[1]-frame.shape[1]),(0,0)),mode='constant',constant_values=0)
                    frame = cv2.bitwise_and(frame,bmask)
                    frame = cv2.bitwise_or(frame,cmask)
                    cv2.polylines(frame,[hpoly],False,(0,255,0),2)
                    out.write(frame)
                    # cv2.imshow('frame',frame)
                    # cv2.waitKey(60)

            out.release()
            if outdebug is not None:
                outdebug.release()

        if dooutput:
            if os.path.exists(os.path.join(outdir ,"confidence.csv")):
                with open(os.path.join(outdir ,"confidence.csv"),'a') as cf:
                    # noinspection PyTypeChecker
                    np.savetxt(cf, np.array(confid), fmt='%s', delimiter=',')
            else:
                np.savetxt(os.path.join(outdir ,"confidence.csv"), np.array(confid),fmt='%s',delimiter=',')
            np.savetxt(os.path.join(outdir, "segments.csv"), np.array(training), fmt='%s', delimiter=',')
            out = cv2.VideoWriter(os.path.join(outdir , "overall.mp4"), fourcc, 20.0, (overallmaxx, overallmaxy))
            for frame in sampleframes:
                if frame.shape[0] != overallmaxy or frame.shape[1] != overallmaxx:
                    frame = np.pad(frame,((0,overallmaxy-frame.shape[0]),(0,overallmaxx-frame.shape[1]),(0,0)),mode='constant')
                out.write(frame)
            out.release()
    elif args.vo and os.path.isdir(args.vo):
        """
        If vo is set then we are evaluatiing the results.  For this we need to find the confidence files
        and overall file.  Each line in the confidence file also has a related frame in the overall file.
        """
        conffile = os.path.join(args.vo,"confidence.csv")
        overallfile = os.path.join(args.vo,"overall.mp4")
        if os.path.exists(conffile) and os.path.exists(overallfile):
            # Read in the file we are going to add a new column to.
            overalltable = np.genfromtxt(conffile,delimiter=',',dtype=None)
            outputtable =[]
            vcap = SdeVideo(overallfile)
            frameno=0
            while True:
                ret, frame = vcap.read(frameno)
                if not ret:
                    print("end reading")
                    break
                frameno = vcap.get_last_frame_no()
                cv2.imshow("hood",frame)
                key = cv2.waitKey(0)
                if key == 0:
                    frameno -=1
                    if frameno < 0:
                        frameno = 0
                    continue
                if frameno >= len(outputtable):
                    outputtable.append((overalltable[frameno][0],overalltable[frameno][1],chr(key)))
                else:
                    outputtable[frameno] = (overalltable[frameno][0],overalltable[frameno][1],chr(key))
                frameno +=1
            # write the results.
            np.savetxt(os.path.join(args.vo, "confidence_eval.csv"), outputtable, fmt='%s', delimiter=',')
        else:
            print("check if these 2 files exist:")
            print(conffile)
            print(overallfile)