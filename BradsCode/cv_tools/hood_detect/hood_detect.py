import argparse
import itertools
import os
import json

import cv2
import numpy as np
import skimage.draw as draw
import skimage.morphology as morph
from scipy.interpolate import BSpline
from scipy.optimize import curve_fit
from sklearn.base import BaseEstimator
from sklearn.linear_model import RANSACRegressor

from cv_utils.intersections import m_b_int, my_b_ln
from cv_utils.sdevideo import SdeVideo
from sde_utils.sdemd_file import SdemdFile

"""
Detect hoods of vehicles.
"""


class SplEstimator(BaseEstimator):
    """
    Estimate using b-splines
    """

    def __init__(self, initval, nknot=3, max_x=720, anchors=None):
        """
        Create Spline Estimator using fixed knots
        :param initval: Initial y value for spline.
        :param nknot: Number of knots
        :param max_x: Maximum X value
        :param anchors: End points spline must go through
        """
        self.nknot = nknot
        self.max_x = max_x
        self.initval = initval
        self.anchors = anchors
        self.t = None
        self.c = None
        self.k = 2

    def splfunc(self, x, *parm):
        """
        Evaluate the spline
        :param x: X value to evaluate at.
        :param parm: Spline coefficients.
        :return: The f(x) for the spline.
        """
        return (BSpline(self.t, parm, self.k))(x)

    def fit(self, x, y, sample_weight=None):
        """
        Fit Spline to the data.
        :param x: X values
        :param y: Corresponding Ys
        :param sample_weight: Coresponding Weights.
        :return:
        """
        if self.anchors is None:
            try:
                self.getinit_param()
                xe = x.flatten()
                xsrt = np.argsort(xe)
                self.c, pcov = curve_fit(self.splfunc, xe[xsrt], y[xsrt], p0=self.c,
                                         sigma=(sample_weight if sample_weight is None else 1.0 / sample_weight[xsrt]),
                                         absolute_sigma=False, method='trf')
            except Exception as e:
                print(e)
                raise e
        else:
            try:
                xe = np.concatenate([[self.anchors[0, 0]], x.flatten(), [self.anchors[1, 0]]])
                ye = np.concatenate([[self.anchors[1, 1]], y, [self.anchors[1, 1]]])
                if sample_weight is None:
                    ws = np.full(len(ye), 1.0)
                    ws[0] = ws[len(ws) - 1] = ws.sum()
                else:
                    wsum = np.sum(sample_weight)
                    ws = np.concatenate([[wsum], sample_weight, [wsum]])
                self.getinit_param()
                xsrt = np.argsort(xe)
                self.c, pcov = curve_fit(self.splfunc, xe[xsrt], ye[xsrt], p0=self.c, sigma=1.0 / ws[xsrt],
                                         absolute_sigma=False, method='trf')
            except Exception as e:
                print(e)
                raise e
        return None

    def score(self, x, y):
        """
        Return the mean squared error.
        :param x: X values
        :param y: Coresponding Ys
        :return: Mean Squared Errror
        """
        py = self.splfunc(x.flatten(), *self.c)
        mse = ((py - y) ** 2).mean()
        return mse

    def predict(self, x):
        """
        Predict the Ys given X
        :param x: X Values
        :return: f(X) returned
        """
        return self.splfunc(x.flatten(), *self.c)

    def getinit_param(self):
        """
        Find the initial spline guess.
        :return:
        """
        incsize = int(self.max_x / (self.nknot + self.k + 1))
        self.t = np.array([st for st in range(0, self.max_x + 1, incsize)])
        self.c = np.full(len(self.t) - self.k - 1, self.initval, dtype=np.float)
        return None


class PolyEstimator(BaseEstimator):
    """
    Class to provide polynomial estimator.
    """

    def __init__(self, anchors=None, degree=5):
        """
        Create Polynomial Estimator
        :param degree: Polniomial degree
        :param anchors: End points polynomial must go through
        """
        self.polycoef = None
        self.anchors = anchors
        self.degree = degree

    def fit(self, x, y, sample_weight=None):
        """
         Fit Spline to the data.
         :param x: X values
         :param y: Corresponding Ys
         :param sample_weight: Coresponding Weights.
         :return:
        """
        if self.anchors is None:
            if sample_weight is not None:
                self.polycoef = np.polyfit(x.flatten(), y, deg=4, w=sample_weight)
            else:
                self.polycoef = np.polyfit(x.flatten(), y, deg=4)
        else:
            xe = np.concatenate([[self.anchors[0, 0]], x.flatten(), [self.anchors[1, 0]]])
            ye = np.concatenate([[self.anchors[1, 1]], y, [self.anchors[1, 1]]])
            if sample_weight is None:
                ws = np.full(len(ye), 1.0)
                ws[0] = ws[len(ws) - 1] = ws.sum()
            else:
                wsum = np.sum(sample_weight)
                ws = np.concatenate([[wsum], sample_weight, [wsum]])
            self.polycoef = np.polyfit(xe, ye, deg=self.degree, w=ws)
        return None

    def score(self, x, y):
        """
        Return the mean squared error.
        :param x: X values
        :param y: Coresponding Ys
        :return: Mean Squared Errror
        """
        py = np.polyval(self.polycoef, x.flatten())
        mse = ((py - y) ** 2).mean()
        return mse

    def predict(self, x):
        """
        Predict the Ys given X
        :param x: X Values
        :return: f(X) returned
        """
        return np.polyval(self.polycoef, x.flatten())


class HoodDetect:
    """
    Hood detector Class
    """

    def __init__(self, height_width=None, threshold=95, cmbthresh=5, emptythresh=0.25, dilate_size=3, debug=False):
        """
        Create the hood detector
        :param height_width: Height and width tuple for the frames.
        :param threshold: Hight threshold for determining menaingfull data is in 95th percentila
        :param cmbthresh: When combining segments or ares this is the threshold.
        :param emptythresh: When determining if an area is empty this is the threshold.
        :param dilate_size: When dialating images this is how much
        :param debug: Debug is output when True
        """
        self.threshold = threshold
        self.cmbthresh = cmbthresh
        self.emptythresh = emptythresh
        self.debug = debug
        self.hoodline = []
        self.hood_scanline = 0
        self.height_width = []
        self.map = None
        self.maphorz = None
        self.vpmap = None
        if height_width is not None:
            self.set_height_width(height_width)
        self.selem = morph.square(dilate_size)
        self.dilate_size = dilate_size
        self.framecount = 0
        self.weightsum = 0
        self.confidence = 0
        self.segments = np.array([])
        self.vpy = 0
        self.vpy_count = 0
        self.maxbright_file = None
        self.maxbright_frame = 0

    def set_height_width(self, height_width):
        """
        Allow setting the height and width after creating the object
        :param height_width: Height and width of the image
        :return:
        """
        self.hood_scanline = height_width[0] - 1
        self.height_width = height_width
        self.map = np.zeros(self.height_width, dtype=np.uint32)
        self.maphorz = np.zeros(self.height_width, dtype=np.uint32)
        self.vpmap = np.zeros(self.height_width, dtype=np.uint32)
        return None

    def add_segs(self, segs, weight):
        """
        Add a list of segments to our map.
        :param segs: segments from HoughLineP
        :param weight: Weight to apply to the segments.
        :return:
        """
        mb_left = []
        len_left = []
        mb_right = []
        len_right = []
        # go trhough and seperate out horizontal and verticle.
        # then apply to the maps.
        halfway = self.height_width[1] / 2
        slopemax = 3 * self.height_width[1] / self.height_width[0]
        tmphorz = np.zeros(self.map.shape, dtype=np.uint32)
        tmp = np.zeros(self.map.shape, dtype=np.uint32)

        for x1, y1, x2, y2 in segs:
            rr, cc = draw.line(y1, x1, y2, x2)
            tmp[rr, cc] += 1
            m, b, r = my_b_ln([x1, y1], [x2, y2])
            if r == 0 or abs(m) >= slopemax:
                tmphorz[rr, cc] += 1
            else:
                if m < 0:
                    if x1 < halfway and x2 < halfway:
                        mb_left.append([m, b])
                        len_left.append(abs(x1 - x2) + abs(y1 - y2))
                elif m > 0:
                    if x1 > halfway and x2 > halfway:
                        mb_right.append([m, b])
                        len_right.append(abs(x1 - x2) + abs(y1 - y2))

        # Now apply the dilation and weight.
        tmp = morph.binary_dilation(tmp, self.selem) * np.uint32(weight)
        tmphorz = morph.binary_dilation(tmphorz, self.selem) * np.uint32(weight)
        # sum up with our existing maps
        self.map += tmp
        self.maphorz += tmphorz
        self.framecount += 1
        self.weightsum += weight

        # now summ up the vanishing point map.
        if len(len_left) > 0 and len(len_right) > 0:
            mb_left = np.array(mb_left)
            mb_right = np.array(mb_right)
            len_left = np.array(len_left)
            len_right = np.array(len_right)
            mean_left = np.percentile(len_left, 90)
            mean_right = np.percentile(len_right, 90)
            for mbl in mb_left[len_left > mean_left]:
                for mbr in mb_right[len_right > mean_right]:
                    x, y, r = m_b_int(mbl[0], mbl[1], mbr[0], mbr[1])
                    if r == 1:
                        if 0 < y < self.height_width[0] and self.height_width[1] / 3 < x < self.height_width[1] * 2 / 3:
                            self.vpy += y
                            self.vpy_count += 1
                            ys = []
                            xs = []
                            for yi in range(int(y - (self.dilate_size / 2)), int(y + (self.dilate_size / 2))):
                                for xi in range(int(x - (self.dilate_size / 2)), int(x + (self.dilate_size / 2))):
                                    ys.append(yi)
                                    xs.append(xi)
                            self.vpmap[ys, xs] += 1

        if self.debug:
            tmp = (tmp * 255).astype(np.uint8)
            cv2.imshow("add", tmp)
            tmphorz = (tmphorz * 255).astype(np.uint8)
            cv2.imshow("addhorz", tmphorz)
            vpmax = np.max(self.vpmap)
            if vpmax > 0:
                cv2.imshow("vpmap", (255 * (self.vpmap / vpmax)).astype(np.uint8))
                cv2.waitKey(60)
        return None

    def find_hood(self):
        """
        Finally find the hood line and scanline.
        :return:
        """
        if self.framecount == 0:
            self.confidence = 100
            return
        maxval = np.percentile(self.maphorz, self.threshold)
        if self.debug:
            mapmax = np.max(self.maphorz)
            mapth = self.maphorz * 255.0 / mapmax
            mapth = mapth.astype(np.uint8)
            cv2.imshow("mapshaded", cv2.cvtColor(mapth, cv2.COLOR_GRAY2BGR))
            cv2.waitKey(60)
        # now set anything below the threshold to zero.
        mapcpy = self.maphorz.copy()
        mapcpy[self.maphorz < maxval] = 0
        if self.debug:
            mapmax = np.max(mapcpy)
            mapth = mapcpy * 255.0 / mapmax
            mapth = mapth.astype(np.uint8)
            cv2.imshow("maphorzshadedtrunc", cv2.cvtColor(mapth, cv2.COLOR_GRAY2BGR))
            mapth = self.map - self.maphorz
            mapmax = np.max(mapth)
            mapth = mapth * 255.0 / mapmax
            mapth = mapth.astype(np.uint8)
            th, ret = cv2.threshold(mapth, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
            canny = cv2.Canny(mapth, th / 2, th, apertureSize=3)
            cv2.imshow("mapshadedtrunc", cv2.cvtColor(canny, cv2.COLOR_GRAY2BGR))
            cv2.waitKey(60)
        # So now we need to find the far end of the hood.  To do this if we have a hint
        # then use that.  Otherwise we need to find the emptyish areas
        rowsum = np.sum(mapcpy, axis=1)
        rmean = np.mean(rowsum)
        # now from the bottom up find the first largest empty area.
        tfsegs = []
        for tf, b in itertools.groupby(zip(rowsum < rmean, range(0, len(rowsum))), key=lambda x: x[0]):
            lb = np.array(list(b))
            tfsegs.append([tf, len(lb), lb[len(lb) - 1][1], lb[0][1]])
        if len(tfsegs) == 0:
            self.confidence = 100
            return
        tfsegs = np.array(tfsegs)
        # add 95th percential column
        tfsegs = np.append(tfsegs, np.zeros((tfsegs.shape[0], 2)), axis=1)
        for segidx in range(0, len(tfsegs)):
            tfsegs[segidx][5] = np.percentile(mapcpy[int(tfsegs[segidx][3]):int(tfsegs[segidx][2]) + 1, :], 95)

        # combine False regions into True using threshold
        segidx = 0
        if len(tfsegs) > 1:
            while segidx < len(tfsegs):
                idxtest = segidx + 1
                if idxtest >= len(tfsegs):
                    break
                if tfsegs[segidx][1] <= self.cmbthresh or (
                        tfsegs[segidx][5] == tfsegs[idxtest][5] and tfsegs[segidx][0] == tfsegs[idxtest][0]):
                    # combine.
                    if segidx < len(tfsegs) - 1:
                        # combine next.
                        tfsegs[segidx + 1][1] += tfsegs[segidx][1]
                        tfsegs[segidx + 1][3] = tfsegs[segidx][3]
                    else:
                        # combine last with previous
                        tfsegs[segidx - 1][1] += tfsegs[segidx][1]
                        tfsegs[segidx - 1][2] = tfsegs[segidx][2]
                    tfsegs = np.delete(tfsegs, segidx, 0)
                    if segidx >= len(tfsegs):
                        segidx -= 1
                    tfsegs[segidx][5] = np.percentile(mapcpy[int(tfsegs[segidx][3]):int(tfsegs[segidx][2]) + 1, :], 95)
                    if len(tfsegs) <= 1:
                        break
                else:
                    segidx += 1
        # now combine adjacent Trues
        segidx = 0
        while segidx < len(tfsegs) - 1:
            if tfsegs[segidx][0] == tfsegs[segidx + 1][0]:
                tfsegs[segidx + 1][1] += tfsegs[segidx][1]
                tfsegs[segidx + 1][3] = tfsegs[segidx][3]
                tfsegs = np.delete(tfsegs, segidx, 0)
                tfsegs[segidx][5] = np.percentile(mapcpy[int(tfsegs[segidx][3]):int(tfsegs[segidx][2]) + 1, :], 95)
            else:
                segidx += 1

        tfsegs = np.append(tfsegs, np.zeros((tfsegs.shape[0], 6)), axis=1)
        mapmax =  np.max(self.map)
        if mapmax == 0:
            mapnorm = self.map
        else:
            mapnorm = (255 * (self.map / np.max(self.map))).astype(np.uint8)
        all_lap = cv2.Laplacian(mapnorm, cv2.CV_32F).var()
        for segidx in range(0, len(tfsegs)):
            # get the nonzero elements.
            nonz = mapcpy[int(tfsegs[segidx][3]):(int(tfsegs[segidx][2]) + 1), :][
                mapcpy[int(tfsegs[segidx][3]):(int(tfsegs[segidx][2]) + 1), :] > 0]
            tfsegs[segidx][4] = np.mean(mapcpy[int(tfsegs[segidx][3]):(int(tfsegs[segidx][2]) + 1), :])
            tfsegs[segidx][6] = 100.0 * tfsegs[segidx][5] / self.weightsum
            if all_lap > 0:
                tfsegs[segidx][7] = cv2.Laplacian(
                    mapnorm[int(tfsegs[segidx][3]):(int(tfsegs[segidx][2]) + 1), :].astype(np.uint8),
                    cv2.CV_32F).var() / all_lap
            if len(nonz) > 0:
                tfsegs[segidx][8] = np.median(nonz)
                tfsegs[segidx][9] = 1.0 - (
                            len(nonz) / (len(rowsum) * ((int(tfsegs[segidx][2]) + 1) - tfsegs[segidx][3])))
            tfsegs[segidx][10] = tfsegs[segidx - 1][1] if segidx > 0 else 0
            tfsegs[segidx][11] = len(tfsegs) - segidx

        self.segments = tfsegs
        # check for just horizon.  This is the case where we have 2 trues and one false and the
        # false is a small percentage of the framecount.
        if len(tfsegs) == 3 and tfsegs[0][0] == 1 and tfsegs[1][0] == 0 and tfsegs[2][0] == 1 \
                and tfsegs[1][3] < self.height_width[0] / 2:
            self.confidence = 100
            return

        # # The first one is in the clouds.
        # now need to find largest last true.  Get average of
        # True and then find the last one larger than the average.
        tfsegst = tfsegs[tfsegs[:, 0] == True]
        if len(tfsegst) < 1:
            self.confidence = 100
            return
        # # Now look for most dense.
        y_vanish = self.get_vanish_y() + self.cmbthresh
        tfsegsf = tfsegs[np.logical_and(tfsegs[:, 0] == False,
                                        np.logical_and(tfsegs[:, 6] > self.emptythresh * max(tfsegs[:, 6]),
                                                       tfsegs[:, 3] > y_vanish))]
        if len(tfsegsf) < 1:
            self.confidence = 100
            return

        fmaxidx = 0
        fmaxsort = None
        normsum = np.array([])
        if len(tfsegsf) > 1:
            norcols = tfsegsf[:, 4:]
            norcols = norcols / np.max(norcols, axis=0)
            normsum = np.sum(norcols, axis=1)
            fmaxsort = np.argsort(-normsum)
            fmaxidx = fmaxsort[0]
        # Never take the near the top one.
        if fmaxidx == 0 and (tfsegsf[fmaxidx, 3] - tfsegsf[fmaxidx, 10]) < self.cmbthresh:
            if len(normsum) <= 1:
                self.confidence = 100
                return
            fmaxidx = fmaxsort[1]

        self.hood_scanline = int(tfsegsf[fmaxidx, 3])
        self.confidence = 100
        if fmaxidx + 1 < len(normsum):
            self.confidence = abs(normsum[fmaxidx] - normsum[fmaxidx + 1]) / normsum[fmaxidx]

        if self.debug:
            mapth = mapcpy.astype(np.uint8)
            mapth[0:self.hood_scanline, :] = 0
            mapth[mapth > 0] = 255
            mapth[self.hood_scanline, :] = 255
            cv2.imshow("threshold", mapth)
            cv2.waitKey(60)
        return 0

    def get_vanish_y(self):
        """
        Calcilate the vanishing point.
        :return:
        """
        bmy = np.argmax(self.vpmap) / self.height_width[1]
        if self.vpy_count > 0:
            calcy = self.vpy / self.vpy_count
            if calcy > bmy:
                return calcy
        return bmy

    def get_hood_mask(self):
        """
        Return a map of the hood edge points.
        :return:
        """
        mapth = self.map.copy()
        mapth[0:self.hood_scanline - 1, :] = 0
        maxval = np.percentile(mapth, self.threshold)
        mapth[mapth <= maxval] = 0
        mapth[mapth > maxval] = 255
        return mapth.astype(np.uint8)

    def get_poly(self):
        """
        Get a poly line of the hood edge.
        :return:
        """
        if self.hood_scanline == (self.height_width[0] - 1):
            return (np.array(
                [[0, self.height_width[0] - 1], [self.height_width[1] - 1, self.height_width[0] - 1]])).reshape(
                (-1, 1, 2))
        mapth = self.map.copy()
        abovehood = int(self.hood_scanline) - 1

        # From the map get X y points.
        mapth[0:abovehood, :] = 0
        maxval = np.percentile(mapth[abovehood + 1:, :], 75)
        yx = np.where(mapth > maxval)
        ys = []
        xs = []
        for xx, yl in itertools.groupby(
                np.sort(np.array(list(zip(yx[0], yx[1])), dtype=[('ys', int), ('xs', int)]), order=['xs', 'ys']),
                key=lambda x: x[1]):
            ysor = np.array(np.array(list(yl)).tolist())
            yweig = mapth[ysor[:, 0], ysor[:, 1]]
            weisort = np.argsort(-yweig)
            weisort = weisort[0:min(len(weisort), self.dilate_size)]
            weisort = np.unique(np.append(weisort, range(0, min(self.dilate_size * 2, len(ysor)))))
            for i in weisort:
                ys.append(ysor[i][0])
                xs.append(xx)
        ys = np.array(ys)
        xs = np.array(xs)
        if len(xs) == 0:
            # No hood
            return (np.array(
                [[0, self.height_width[0] - 1], [self.height_width[1] - 1, self.height_width[0] - 1]])).reshape(
                (-1, 1, 2))

        # The weights
        ws = mapth[ys, xs]
        ws = ws / np.max(ws)
        if self.debug:
            mapthd = mapth
            mapthd[mapthd <= maxval] = 0
            mapmax = np.max(mapthd)
            mapthd = mapthd * 255.0 / mapmax
            mapthd = mapthd.astype(np.uint8)
            cv2.imshow("map_fit", cv2.cvtColor(mapthd, cv2.COLOR_GRAY2BGR))
            mapthd = mapth.copy()
            mapthd[:, :] = 0
            mapthd[ys, xs] = 255.0
            mapthd = mapthd.astype(np.uint8)
            cv2.imshow("map_select", cv2.cvtColor(mapthd, cv2.COLOR_GRAY2BGR))
            cv2.waitKey(60)

        # fit several.  And pick the best
        resultsfit = []
        xp = np.array(range(0, self.height_width[1]))
        resultsfit.append(
            self.ransack_and_fit(SplEstimator(nknot=2, initval=self.height_width[0], max_x=self.height_width[1]), xs,
                                 ys, ws, xp))
        resultsfit.append(self.ransack_and_fit(PolyEstimator(degree=3), xs, ys, ws, xp))
        resultsfit.append(
            self.ransack_and_fit(SplEstimator(nknot=3, initval=self.height_width[0], max_x=self.height_width[1]), xs,
                                 ys, ws, xp))
        resultsfit = np.array(resultsfit)
        minres = np.argmin(resultsfit[:, 1])
        self.confidence = minres
        if self.debug:
            mrp = self.map.copy()
            mapmax = np.max(mrp)
            mrp = cv2.cvtColor((mrp * 255.0 / mapmax).astype(np.uint8), cv2.COLOR_GRAY2BGR)
            cv2.polylines(mrp, [resultsfit[0][0]], False, (255, 255, 255) if minres == 0 else (0, 0, 255), 2)
            cv2.polylines(mrp, [resultsfit[1][0]], False, (255, 255, 255) if minres == 1 else (0, 255, 0), 2)
            cv2.polylines(mrp, [resultsfit[2][0]], False, (255, 255, 255) if minres == 2 else (255, 0, 0), 2)
            cv2.imshow("mrnoa", mrp)
            cv2.waitKey(60)
        #
        return resultsfit[minres, 0]

    def get_reduced_to_polygon(self):
        """
        Take the poly line and convert it to a polygon along the bottom
        edge of the frame.
        :return: Polygon
        """
        poly_line = self.get_poly()
        rpoly = [poly_line[0,0]]
        curx = poly_line[0,0,0]
        cury = poly_line[0,0,1]
        i = 1
        while i < len(poly_line):
            if poly_line[i,0,0] == curx:
                i=i+1
                while i < len(poly_line):
                    if poly_line[i,0,0] != curx:
                        break
                    i = i+1
                rpoly.append(poly_line[i-1,0])
            elif poly_line[i,0,1] == cury:
                i=i+1
                while i < len(poly_line):
                    if poly_line[i,0,1] != cury:
                        break
                    i = i+1
                rpoly.append(poly_line[i-1,0])
            if i < len(poly_line):
                rpoly.append(poly_line[i,0])
                curx = poly_line[i,0,0]
                cury = poly_line[i,0,1]
                i = i+1
        # fill in start to end polygon.
        rpoly.append([self.height_width[1]-1,self.height_width[0]-1])
        rpoly.append([0,self.height_width[0]-1])
        if not all(rpoly[0] == rpoly[len(rpoly)-1]):
            rpoly.append(rpoly[0])
        return (np.array(rpoly)).reshape((-1, 1, 2))


    def ransack_and_fit(self, estimator, xs, ys, ws, xp):
        """
        Wrapper to handle RANSAC errors.
        :param estimator: Estimator Function.
        :param xs: Xs to fit
        :param ys: Ys to fit
        :param ws: Weights for each point.
        :param xp: Prediction X values to return
        :return: F(xp), MSE
        """
        min_samples = int(.1 * len(xs))
        while True:
            ransac = RANSACRegressor(estimator, min_samples=min_samples)
            try:
                ransac.fit(np.array(xs).reshape((-1, 1)), ys, ws)
            except FloatingPointError:
                # sample sizr too big error.
                min_samples = int(min_samples / 2)
                continue
            except Exception as e:
                print(e)
                yp = np.empty(len(xp))
                yp.fill(self.height_width[0])
                return [(np.array(np.column_stack((xp, yp)), dtype=np.int32)).reshape((-1, 1, 2)),
                        self.height_width[0] * self.height_width[1]]
            break
        yp = ransac.predict(xp)
        # Truncate to hood scan line
        yp[yp < self.hood_scanline] = self.hood_scanline
        totalerror = 0
        for i in range(0, len(xp)):
            totalerror += np.sum(np.abs(ys[xs == xp[i]] - yp[i]))
        return [(np.array(np.column_stack((xp, yp)), dtype=np.int32)).reshape((-1, 1, 2)), totalerror]

    def detect_hood_from_files(self, dirnm, files, minspeed=40, minbright=50, dofindhood=True):
        """
        Find the hood from a number of video files.
        :param dirnm: Directory to find the files in
        :param files: List of file names.
        :return: Hood scan line found.
        """
        is_sde = False
        # see if we have SDE or other files.
        if files[0].endswith((".SDEMD",".sdemd")):
            is_sde = True
            foundone = False
            for idx in range(0,len(files)):
                try:
                    sde = SdemdFile(os.path.join(dirnm, files[0]))
                    sdefn, sdefps = sde.get_video()
                    sde.close()
                    vcap = SdeVideo(sdefn, crop=sde.video_bounds)
                    maxx, maxy = vcap.get_shape()
                    vcap.release()
                    foundone = True
                    break
                except:
                    continue
            if not foundone:
                return self.hood_scanline
        else:
            vcap = SdeVideo(os.path.join(dirnm, files[0]))
            maxx, maxy = vcap.get_shape()
            vcap.release()
        self.set_height_width((maxy, maxx))
        # keep track of the brightest frame.
        brightlst = []
        brightvals = []
        for f in files:
            if is_sde:
                sde = SdemdFile(os.path.join(dirnm, files[0]))
                sdevfn, sdevfps = sde.get_video()
                sde_speed, sde_speed_sr = sde.get_speed()
                if sdevfn is None or sde_speed is None or np.array(sde_speed).max() < minspeed:
                    sde.close()
                    continue
                try:
                    vcap = SdeVideo(sdevfn, crop=sde.video_bounds)
                except:
                    vcap.release()
                    continue
            else:
                vcap = SdeVideo(os.path.join(dirnm, f))
            while True:
                ret, frame = vcap.read()
                if not ret:
                    break
                if is_sde:
                    frameno = vcap.get_last_frame_no()
                    speed_idx = min(len(sde_speed)-1, int(sde_speed_sr*frameno/sdefps))
                    if sde_speed[speed_idx] < minspeed:
                        continue
                fgray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                fhlssp = fgray
                weightv = np.mean(fhlssp)
                if weightv < minbright:
                    continue
                th, ret = cv2.threshold(fhlssp, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
                # Run canny and filter results on any contours that
                canny = cv2.Canny(fhlssp, th / 2, th, apertureSize=3)
                cmean = np.mean(canny)
                if cmean > th:
                    # try again with a different threshold.
                    fhlssp = cv2.blur(fgray, (7, 3))
                    weightv = np.mean(fhlssp)
                    th, ret = cv2.threshold(fhlssp, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
                    canny = cv2.Canny(fhlssp, th / 2, th, apertureSize=3)
                    cmean = np.mean(canny)
                    if cmean > th:
                        continue
                if self.debug:
                    cv2.imshow("canny", canny)
                    cv2.imshow("fgray", fgray)
                    cv2.imshow("fhlssp", fhlssp)
                    cv2.waitKey(60)

                # get the lines and save them off.
                lines = cv2.HoughLinesP(canny, 1, np.pi / 180.0, 10, minLineLength=10, maxLineGap=3)
                if lines is not None:
                    self.add_segs(lines[:, 0], weightv)
                brightlst.append([f, vcap.get_last_frame_no()])
                brightvals.append(weightv)
            vcap.release()
            if is_sde:
                sde.close()

        # find a bright frame.
        if self.framecount == 0:
            return self.hood_scanline
        val80 = np.argsort(brightvals)
        maxbr = val80[int(len(val80) * .80)]
        self.maxbright_file = brightlst[maxbr][0]
        self.maxbright_frame = brightlst[maxbr][1]
        if dofindhood:
            self.find_hood()
        return self.hood_scanline


def main():
    np.seterr(all='raise')
    # parse the arguments.
    parse = argparse.ArgumentParser()
    parse.add_argument("--vd", help="Vidoes directory")
    parse.add_argument("--d", help="Debug", default=False, type=bool)
    parse.add_argument("--m", help="min number of files", default=-1, type=int)
    parse.add_argument("--x", help="max number of files", default=-1, type=int)
    parse.add_argument("--vo", help="Video output directory")
    args = parse.parse_args()

    dooutput = args.d is not True

    if args.vd and os.path.isdir(args.vd):

        # Setup the output paths.
        onlydirsall = [f for f in os.listdir(args.vd) if os.path.isdir(os.path.join(args.vd, f))]
        if len(onlydirsall) == 0:
            onlydirnames = [os.path.basename(args.vd)]
            outdir = os.path.dirname(args.vd) + "_outputhood"
            onlydirs = [args.vd]
            onlydirsfiles = [
                [f for f in os.listdir(args.vd) if os.path.isfile(os.path.join(args.vd, f)) and f.endswith(("mp4","SDE"))]]
        else:
            outdir = args.vd + "_outputhood"
            onlydirs = []
            onlydirnames = []
            onlydirsfiles = []
            for sdir in onlydirsall:
                sdirpath = os.path.join(args.vd, sdir)
                onlyfiles = [f for f in os.listdir(sdirpath) if
                             os.path.isfile(os.path.join(sdirpath, f)) and f.endswith(("mp4","SDE"))]
                if len(onlyfiles) > 0:
                    onlydirnames.append(sdir)
                    onlydirs.append(sdirpath)
                    onlydirsfiles.append(onlyfiles)
        if not os.path.exists(outdir):
            os.mkdir(outdir)
        fourcc = cv2.VideoWriter_fourcc(*'MP4V')

        # we are looking for a directory that contains directories of video files.
        # now see if each dir contains video files.
        confid = []
        training = []
        sampleframes = []
        overallmaxx = 0
        overallmaxy = 0
        stop = False
        for i in range(0, len(onlydirs)):
            if stop:
                break
            outpathfile = os.path.join(outdir, onlydirnames[i] + ".mp4")
            if os.path.exists(outpathfile):
                continue
            if args.m >= 0 and len(onlydirsfiles[i]) < args.m:
                continue
            if len(onlydirsfiles[i]) > args.x >= 0:
                continue

            # hear is where we do the real work.
            hd = HoodDetect(debug=args.d)
            hline = hd.detect_hood_from_files(onlydirs[i], onlydirsfiles[i])
            hmask = hd.get_hood_mask()
            hpoly = hd.get_poly()
            confidence = hd.confidence

            print("")
            print(json.dumps({"hline":hline,"hpoly":hpoly.tolist()}, default=lambda o: o.__dict__, sort_keys=False, indent=4))
            print("")

            # save of the statistics.
            if len(hd.segments) > 0:
                trn = np.concatenate(
                    (np.full((len(hd.segments), 1), onlydirnames[i]), hd.segments, np.full((len(hd.segments), 1), 0)),
                    axis=1)
                try:
                    idxtrn = list(hd.segments[:, 3]).index(hd.hood_scanline)
                    trn[idxtrn, -1] = 1
                except:
                    pass

                training.extend(list(trn))

            confid.append([onlydirnames[i], confidence])

            # create the output video showing what was found.
            bmask = cv2.bitwise_not(hmask)
            bmask = cv2.cvtColor(bmask, cv2.COLOR_GRAY2BGR)
            zz = np.zeros((hmask.shape[0], hmask.shape[1]), dtype=np.uint8)
            cmask = np.stack((hmask, zz, zz),
                             axis=-1)
            # create the video writer for the results.
            vcap = SdeVideo(os.path.join(onlydirs[i], onlydirsfiles[i][0]))
            maxx, maxy = vcap.get_shape()
            if maxx > overallmaxx:
                overallmaxx = maxx
            if maxy > overallmaxy:
                overallmaxy = maxy
            vcap.release()
            out = cv2.VideoWriter(outpathfile, fourcc, 20.0, (maxx, maxy))
            for f in onlydirsfiles[i]:
                fvcap = SdeVideo(os.path.join(onlydirs[i], f))
                if stop:
                    break
                while True:
                    ret, frame = fvcap.read()
                    if not ret:
                        break
                    if dooutput and hd.maxbright_file == f and hd.maxbright_frame == fvcap.get_last_frame_no():
                        bframe = frame.copy()
                        cv2.line(bframe, (0, hline), (maxx, hline), (0, 0, 255), 2)
                        cv2.polylines(bframe, [hpoly], False, (0, 255, 0), 2)
                        sampleframes.append(bframe)

                    cv2.line(frame, (0, hline), (maxx, hline), (0, 0, 255), 2)
                    if frame.shape[1] > hmask.shape[1]:
                        frame = frame[:, 0:hmask.shape[1]]
                    elif frame.shape[1] < hmask.shape[1]:
                        frame = np.pad(frame, ((0, 0), (0, hmask.shape[1] - frame.shape[1]), (0, 0)), mode='constant',
                                       constant_values=0)
                    frame = cv2.bitwise_and(frame, bmask)
                    frame = cv2.bitwise_or(frame, cmask)
                    cv2.polylines(frame, [hpoly], False, (0, 255, 0), 2)
                    out.write(frame)

            out.release()

        if dooutput:
            # Save everything
            np.savetxt(os.path.join(outdir, "confidence.csv"), np.array(confid), fmt='%s', delimiter=',')
            np.savetxt(os.path.join(outdir, "segments.csv"), np.array(training), fmt='%s', delimiter=',')
            out = cv2.VideoWriter(os.path.join(outdir, "overall.mp4"), fourcc, 20.0, (overallmaxx, overallmaxy))
            for frame in sampleframes:
                if frame.shape[0] != overallmaxy or frame.shape[1] != overallmaxx:
                    frame = np.pad(frame,
                                   ((0, overallmaxy - frame.shape[0]), (0, overallmaxx - frame.shape[1]), (0, 0)),
                                   mode='constant')
                out.write(frame)
            out.release
    elif args.vo and os.path.isdir(args.vo):

        # this is the review program.
        conffile = os.path.join(args.vo, "confidence.csv")
        overallfile = os.path.join(args.vo, "overall.mp4")
        if os.path.exists(conffile) and os.path.exists(overallfile):
            overalltable = np.genfromtxt(conffile, delimiter=',', dtype=None)
            outputtable = []
            vcap = SdeVideo(overallfile)
            frameno = 0
            while True:
                ret, frame = vcap.read(frameno)
                if not ret:
                    print("end reading")
                    break
                frameno = vcap.get_last_frame_no()
                cv2.imshow("hood", frame)
                key = cv2.waitKey(0)
                if key == 0:
                    frameno -= 1
                    if frameno < 0:
                        frameno = 0
                    continue
                if frameno >= len(outputtable):
                    outputtable.append((overalltable[frameno][0], overalltable[frameno][1], chr(key)))
                else:
                    outputtable[frameno] = (overalltable[frameno][0], overalltable[frameno][1], chr(key))
                frameno += 1
            np.savetxt(os.path.join(args.vo, "confidence_eval.csv"), outputtable, fmt='%s', delimiter=',')
        else:
            print("check if these 2 files exist:")
            print(conffile)
            print(overallfile)


if __name__ == "__main__":
    main()
