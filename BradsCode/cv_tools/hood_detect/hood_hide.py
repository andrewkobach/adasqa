#!/usr/bin/env python3
import argparse
import sys
import os
import io
import json
import cv2
from sde_utils.sdemd_file import SdemdFile
from sde_utils.sedsync_loging import get_sdemd_list
from sde_utils.gps_interpolate import gps_interpolate
from cv_utils.sdevideo import SdeVideo
from hood_detect import HoodDetect
import numpy as np
import traceback
import hashlib
import boto3

from datetime import datetime, timedelta
import glob
from multiprocessing import pool, freeze_support, cpu_count
import time
import tempfile

"""
Script to process SDEMD files and produce jpg and gps files from the Forward camera video.
"""


def parse_args():
    """
    Here are all our arguments.
    :return: parsed args and the parser.
    """
    parse = argparse.ArgumentParser()
    parse.add_argument("--sdewalk", help="Directorys to walk looking for SDE files")
    parse.add_argument("--sdebucket", help="Name of S3 bucket for SDE files")
    parse.add_argument("--loggrp", help="Cloud watch log group")
    parse.add_argument("--outbucket", help="Name of output pucket and any prefix.")
    parse.add_argument("--tmpdir", help="Temporary directory for storing files copied from the bucket.")
    parse.add_argument("--statefilter", help="Filter Bucket on state")
    parse.add_argument("--dbbucket", help="Name of database bucket and any prefix.")
    # These parameters have to do with how we find the hood outline.
    parse.add_argument("--bottompercent",type=int, default=30, help="Bottom percent to mask out")
    parse.add_argument("--bottompercentwidth",type=int, default=80, help="When bottompercent is not 0 this is the width of the mask")
    parse.add_argument("--crop", action='store_true', default=False, help="Crop the image rather than mask.")
    parse.add_argument("--minfiles", type=int, default=10, help="Minimum number of files need to search for hood.")
    parse.add_argument("--minframes", type=int, default=600, help="Minimum number of frames to search for a hood.")
    parse.add_argument("--minkph", type=int, default=40, help="minimum KPH when selecting frames")
    parse.add_argument("--minbright", type=int, default=50, help="Minimum average brightness of a frame.")
    parse.add_argument("--fixed", nargs=2, type=int, help="For fixed rectangle percent from bottom and percent width. --fixed 20 80 20 percent from bottom and 80 percent of width")
    # These options control multiprocessing.
    parse.add_argument("--nprocess", help="Number of processes to use.  Default is number of cpus", type=int, default=0)
    parse.add_argument("--backlog", help="Number of tasks that can get backed up before pausing", type=int, default=500)
    # There are for debugging purposes.
    parse.add_argument("--verbose", action='store_true', default=False, help="Verbose logging.")
    parse.add_argument("--nohash", action='store_true', default=False, help="Do not hash the file names.")
    parse.add_argument("--nopool", action='store_true', default=False, help="Do not use multiple processes.")
    parse.add_argument("--auditdir", help="Directory where audit results stored.")
    parse.add_argument("--debug", action='store_true', default=False, help="Debug mode.")
    args = parse.parse_args()
    return args, parse


def main():
    """
    The main program.  Here we just get the parsed arguments, do some checks then call the function that
    does all the work.
    :return: None
    """
    start_time = datetime.now()
    args, parse = parse_args()

    if (args.sdewalk is None and args.sdebucket is None and args.loggrp is None) or (args.sdewalk is not None and args.sdebucket is not None and args.loggrp is not None):
        print("One and only one of sdewalk or sdebucket can be specified")
        parse.print_help(sys.stderr)
        return
    if args.loggrp is None and (args.outbucket is None or args.dbbucket is None):
        print("Both of outbucket and dbbucket must be specified")
        parse.print_help(sys.stderr)
        return
    if args.auditdir is not None and not os.path.exists(args.auditdir):
        os.makedirs(args.auditdir)

    if cpu_count() == 1:
        args.nopool = True

    # either walk a directory or walk a bucket.
    if args.sdewalk is not None:
        sde_paths = args.sdewalk.split(',')
        process_dirs(sde_paths, args)
    elif args.sdebucket is not None:
        if args.tmpdir is None:
            with tempfile.TemporaryDirectory() as tmpdir:
                args.tmpdir = tmpdir
                process_bucket(args.sdebucket, args)
        else:
            if not os.path.exists(args.tmpdir):
                os.makedirs(args.tmpdir)
            process_bucket(args.sdebucket, args)
    elif args.loggrp is not None:
        if args.tmpdir is None:
            with tempfile.TemporaryDirectory() as tmpdir:
                args.tmpdir = tmpdir
                process_from_log(args.loggrp, args)
        else:
            if not os.path.exists(args.tmpdir):
                os.makedirs(args.tmpdir)
            process_from_log(args.loggrp, args)
    else:
        parse.print_help(sys.stderr)

    print("\n\nTotal Run time: ", datetime.now() - start_time)
    print(datetime.now())
    return None


def get_sdemd_hash(basename):
    """
    Given the files basename with out extension get a hash.
    :param basename: File basename with no extension.
    :return: hash
    """
    # SHA 256 always gives you the sme hash for a given base name.
    return hashlib.sha256(basename.encode('utf-8')).hexdigest()[:32]


# Create the s3cl here.  This is done once for every process started.  The boto3 client takes a long time to create.


s3cl = boto3.client('s3')


# The hood polygons are stored in S3 in the dbbucket plus hooddir and srid, srhoodslist are list of sde files for
# srs we have not found a hood for.  The srsdemdhash goes fromt he hash to the original file.
HOODDIR = 'srhoods/'
HOODLIST = 'srhoodslist/'
SRSDEMDHASH = 'srsdemdhash/'
LASTPROCESSED = 'srsdemdlastprocessed'


def sr_fetch_hood_s3(srid, args):
    """
    Get the hood outline.
    :param srid: The SRID
    :param args: Program arguments.
    :return: The hood outline or None.
    """
    outbucket, outprefix = parse_out_bucket(args.dbbucket, HOODDIR)
    fullkey = outprefix+srid+'.json'
    try:
        response = s3cl.get_object(Bucket=outbucket, Key=fullkey)
        if 'Body' in response:
            hood = json.loads(response['Body'].read())
            return hood
    except Exception as ex:
        pass
    return {'sdes':[]}


def sr_fetch_hoodlist_s3(srid, args):
    """
    Get the list of SDEMD files for an SR where we don't have the hood yet.
    :param srid: SRID
    :param args: program args.
    :return:
    """
    outbucket, outprefix = parse_out_bucket(args.dbbucket, HOODLIST)
    fullkey = outprefix+srid+'.json'
    try:
        response = s3cl.get_object(Bucket=outbucket, Key=fullkey)
        if 'Body' in response:
            hood = json.loads(response['Body'].read())
            hoods= hood['hoodlist'] if 'hoodlist' in hood else []
            for ho in hoods:
                destfile = os.path.join(args.tmpdir, ho[1].split('/')[-1])
                ho[2] = destfile
            return hoods
    except Exception as ex:
        pass
    return []


def sr_put_hoodlist_s3(srid, hoodlist, args):
    """
    Put the list of SDEMD files for an SR we don't have a hood for.
    :param srid: SRID
    :param hoodlist: list of SDEMD files
    :param args: program args.
    :return:
    """
    outbucket, outprefix = parse_out_bucket(args.dbbucket, HOODLIST)
    fullkey = outprefix+srid+'.json'
    # if the hoodlist is empty then try and delete otherwise write
    if len(hoodlist) == 0:
        s3result = s3cl.delete_object(Bucket=outbucket, Key=fullkey)
        if 'ResponseMetadata' not in s3result or 'HTTPStatusCode' not in s3result['ResponseMetadata'] or s3result['ResponseMetadata']['HTTPStatusCode'] != 204:
            print("\nError deleteing from S3 ", srid)
    else:
        hoodlistjson = json.dumps({'hoodlist':hoodlist})
        s3result = s3cl.put_object(
            Body=io.BytesIO(hoodlistjson.encode('utf-8')),
            Bucket=outbucket, Key=fullkey)
        if not (s3result['ResponseMetadata'] is not None and
                s3result['ResponseMetadata']['HTTPStatusCode'] is not None and
                s3result['ResponseMetadata']['HTTPStatusCode'] == 200):
            print("\nError writing to S3 ", s3result['ResponseMetadata']['HTTPStatusCode'])
        return None


def sr_write_hood_s3(srid,  hoodjson, args):
    """
    Write the hood json for the SR
    :param srid: SRID
    :param hoodjson: json of the hoos polygon.
    :param args: program args
    :return:
    """
    outbucket, outprefix = parse_out_bucket(args.dbbucket, HOODDIR)
    fullkey = outprefix+srid+'.json'
    s3result = s3cl.put_object(
        Body=io.BytesIO(hoodjson.encode('utf-8')),
        Bucket=outbucket, Key=fullkey)
    if not (s3result['ResponseMetadata'] is not None and
            s3result['ResponseMetadata']['HTTPStatusCode'] is not None and
            s3result['ResponseMetadata']['HTTPStatusCode'] == 200):
        print("\nError writing to S3 ", s3result['ResponseMetadata']['HTTPStatusCode'])
    return None


def sr_write_last_prefix(last_prefix, args):
    """
    Write out the last processed prefix.
    :param last_prefix:
    :param args:
    :return:
    """
    outbucket, outprefix = parse_out_bucket(args.dbbucket, LASTPROCESSED)
    fullkey = outprefix
    s3result = s3cl.put_object(
        Body=io.BytesIO(last_prefix.encode('utf-8')),
        Bucket=outbucket, Key=fullkey)
    if not (s3result['ResponseMetadata'] is not None and
            s3result['ResponseMetadata']['HTTPStatusCode'] is not None and
            s3result['ResponseMetadata']['HTTPStatusCode'] == 200):
        print("\nError writing to S3 ", s3result['ResponseMetadata']['HTTPStatusCode'])
    return None


def sr_read_last_prefix(args):
    """
    Read back in the last processed prefix.
    :param args:
    :return:
    """
    outbucket, outprefix = parse_out_bucket(args.dbbucket, LASTPROCESSED)
    fullkey = outprefix
    try:
        response = s3cl.get_object(Bucket=outbucket, Key=fullkey)
        if 'Body' in response:
            lastprefix =response['Body'].read()
            return lastprefix.decode("utf-8")
    except Exception as ex:
        pass
    return None


LASTPROCESSEDDATE = 'lastlogprocessedtimes'


def sr_read_last_log_date(args):
    """
    Read back in the last log processed date.
    :param args:
    :return:
    """
    outbucket, outprefix = parse_out_bucket(args.outbucket, LASTPROCESSEDDATE)
    fullkey = outprefix
    try:
        response = s3cl.get_object(Bucket=outbucket, Key=fullkey)
        if 'Body' in response:
            lastprefix =response['Body'].read()
            return lastprefix.decode("utf-8")
    except Exception as ex:
        pass
    return None


def sr_write_last_log_date(utime, args):
    """
    Write out the last processed prefix.
    :param last_prefix:
    :param args:
    :return:
    """
    outbucket, outprefix = parse_out_bucket(args.outbucket, LASTPROCESSEDDATE)
    fullkey = outprefix
    s3result = s3cl.put_object(
        Body=io.BytesIO(utime.encode('utf-8')),
        Bucket=outbucket, Key=fullkey)
    if not (s3result['ResponseMetadata'] is not None and
            s3result['ResponseMetadata']['HTTPStatusCode'] is not None and
            s3result['ResponseMetadata']['HTTPStatusCode'] == 200):
        print("\nError writing to S3 ", s3result['ResponseMetadata']['HTTPStatusCode'])
    return None



def process_dirs(sde_paths, args):
    """
    This is the main routine when we are proceesing a list of directories of SDEMD files in shared directories.
    Just walk the directories processing SDEMD files.
    :param sde_paths: List of directories.
    :param sqlconn: SQL connection
    :param args: program arguments.
    :return:
    """
    mpool = pool.Pool(os.cpu_count() if args.nprocess == 0 else args.nprocess)
    all_results = []
    numprocessed = 0
    for sdepath in sde_paths:
        for dirname, subdirlist, files in os.walk(sdepath, topdown=False):
            for f in files:
                process_one_sdemd(('', os.path.join(dirname, f)), os.path.join(dirname, f), args, lambda n: None, mpool, all_results)
                numprocessed += 1
                while check_for_done(all_results, args, mpool, lambda n: None) > args.backlog:
                    print("\nBacked up, sleep for 15.")
                    time.sleep(15)
                print("\r processed {}, backlog {}".format(numprocessed, len(all_results)), end='', flush=True)
    while check_for_done(all_results, args, mpool, lambda n: None) > 0:
        time.sleep(5)
    mpool.close()
    mpool.join()
    return None


def process_bucket(bucket, args):
    """
    This is the main routine when processing from a s3 bucket.  Here we assume the bucket keys are delimited by
    dates in the YYYY/MM/DD format.
    :param bucket: bucket name
    :param tmpdir: temporary directory used to store SDME files across seperate runs.
    :param sqlconn: SQL connection.
    :param args: program arguments.
    :return:
    """
    mpool = pool.Pool(os.cpu_count() if args.nprocess == 0 else args.nprocess, maxtasksperchild=100000)

    rem_fun = lambda n: remove_sdemds(args.tmpdir, n)

    # when doing async calls keep the results here.
    lastdate = sr_read_last_prefix(args)
    newlastdate = "00000000"
    docheckdate = True
    if lastdate is None:
        lastdate = "00000000"
        docheckdate = False
    all_results = []
    # start with the year and loop on month then day
    error_happened = False
    numprocessed = 0
    istruncated = True
    next_token = None
    all_sde_list = []
    try:
        while istruncated:
            if error_happened:
                break
            if next_token is not None:
                files_list_res = s3cl.list_objects_v2(Bucket=bucket, ContinuationToken=next_token)
            else:
                files_list_res = s3cl.list_objects_v2(Bucket=bucket, FetchOwner=False)
            if 'IsTruncated' not in files_list_res:
                error_happened = True
                print("\nError getting bucket list")
                break
            istruncated = files_list_res['IsTruncated']
            if istruncated and 'NextContinuationToken' in files_list_res:
                next_token = files_list_res['NextContinuationToken']
            else:
                next_token = None
            if 'Contents' not in files_list_res:
                break
            mxdate = sorted([str(fl['LastModified']) for fl in files_list_res['Contents']])[len(files_list_res)-1]
            if mxdate > newlastdate:
                newlastdate = mxdate
            if docheckdate:
                if mxdate < lastdate:
                    continue
                all_sde_list.extend([fl['Key'] for fl in files_list_res['Contents'] if
                                    fl['Key'].endswith(("SDEMD", "sdemd")) and str(fl['LastModified']) >= lastdate])
            else:
                all_sde_list.extend([fl['Key'] for fl in files_list_res['Contents'] if fl['Key'].endswith(("SDEMD", "sdemd"))])
        if len(all_sde_list) > 0 and not error_happened:
            # step through each SDEMD file.
            for sdefile in all_sde_list:
                sdefilesplit = sdefile.split('/')
                if args.statefilter is not None:
                    metaresponse = s3cl.head_object(Bucket=bucket, Key=sdefile)
                    if not ('Metadata' in metaresponse and 'state' in metaresponse['Metadata'] and
                            metaresponse['Metadata']['state'] == args.statefilter):
                        continue
                destfile = os.path.join(args.tmpdir, sdefilesplit[-1])
                process_one_sdemd((bucket, sdefile), destfile, args,
                                  rem_fun, mpool, all_results)
                numprocessed += 1
                while check_for_done(all_results, args, mpool, rem_fun) > args.backlog:
                    time.sleep(15)
                print("\r processed {}, backlog {}  ".format(numprocessed, len(all_results)), end='', flush=True)
    except:
        error_happened = True

    if not error_happened:
        sr_write_last_prefix(newlastdate, args)
    # wait for everything to finish.  Then close out the pool
    while check_for_done(all_results, args, mpool,rem_fun) > 0:
        print("\r processed {}, backlog {}   ".format(numprocessed, len(all_results)), end='', flush=True)
        time.sleep(5)
    mpool.close()
    mpool.join()
    return None


def process_from_log(loggrp, args):
    """
    Use the logs from the s3 copy lambda to find new sedmd files.
    :param loggrp: Log file group
    :param args:
    :return:
    """
    mpool = pool.Pool(os.cpu_count() if args.nprocess == 0 else args.nprocess, maxtasksperchild=100000)

    rem_fun = lambda n: remove_sdemds(args.tmpdir, n)

    # get the last date.
    lastdate = sr_read_last_log_date(args)
    all_results = []
    # start with the year and loop on month then day
    error_happened = False
    numprocessed = 0
    try:
        all_sde_list, newlastdate = get_sdemd_list(lastdate,args.loggrp)
        if len(all_sde_list) > 0 and not error_happened:
            print("Number to process ",len(all_sde_list))
            # step through each SDEMD file.
            for sdeelem in all_sde_list:
                elmsp = sdeelem.split('/',1)
                sdefile = elmsp[1]
                bucket = elmsp[0]
                sdefilesplit = sdefile.split('/')
                if args.statefilter is not None:
                    metaresponse = s3cl.head_object(Bucket=bucket, Key=sdefile)
                    if not ('Metadata' in metaresponse and 'state' in metaresponse['Metadata'] and
                            metaresponse['Metadata']['state'] == args.statefilter):
                        continue
                destfile = os.path.join(args.tmpdir, sdefilesplit[-1])
                process_one_sdemd((bucket, sdefile), destfile, args,
                                  rem_fun, mpool, all_results)
                numprocessed += 1
                while check_for_done(all_results, args, mpool, rem_fun) > args.backlog:
                    time.sleep(15)
                print("\r processed {}, backlog {}  ".format(numprocessed, len(all_results)), end='', flush=True)
        else:
            print("Nothing new to process")
    except:
        error_happened = True

    if not error_happened and newlastdate is not None:
        sr_write_last_log_date(newlastdate, args)
    # wait for everything to finish.  Then close out the pool
    while check_for_done(all_results, args, mpool,rem_fun) > 0:
        print("\r processed {}, backlog {}   ".format(numprocessed, len(all_results)), end='', flush=True)
        time.sleep(5)
    mpool.close()
    mpool.join()
    return None


def check_for_done(all_results, args, mpool, removefunc):
    """
    When multiprocessing this method checks if the queued tasks are done and finishes the work that needs to be done
    by the main process.
    :param all_results: List of our results.  Each result tuple are: (multiprocessing results, flag of 'mask' or 'hood'
    key sent to remove function to clean up.
    :param args: progam args
    :param mpool: multiprocessing pool for starting new tasks.
    :param removefunc: Function to do clean up.
    :return:
    """
    i = len(all_results) - 1
    while i >= 0:
        if all_results[i][1] == 'mask':
            if all_results[i][0].ready():
                try:
                    res = all_results[i][0].get()
                    mask_and_gps_finish(removefunc, res)
                except:
                    removefunc(all_results[i][2])
                del all_results[i]
        else:
            # hood
            if all_results[i][0].ready():
                try:
                    res = all_results[i][0].get()
                    if res[1] is not None:
                        all_results.extend(
                            found_hood(res[0], res[1], args, removefunc, mpool))
                        del all_results[i]
                        i = len(all_results)
                    else:
                        all_results.extend(
                            hood_not_found(res[0], res[1], args, removefunc, mpool))
                        del all_results[i]
                        i = len(all_results)
                except:
                    del all_results[i]
        i = i - 1
    return len(all_results)


def remove_sdemds(basedir, sdeid):
    """
    Clean up function for when processing from a bucket.  This deletes files from the local directory
    that we no longer need.
    :param basedir: Where to delete them from.
    :param sdeid: The SDEid to delete.  This is just the base of the SDEMD file name.
    :return:
    """
    try:
        for fr in glob.iglob(os.path.join(basedir, "*" + sdeid + "*")):
            if os.path.exists(fr):
                os.remove(fr)
    except:
        pass
    return None


def process_one_sdemd(original_path, sdemd_filename, args, removefunc, mpool, resultlist):
    """
    This processes one SDEMD file. The real work is done
    in a seperate process.
    :param original_path: The original path or bucket key pair for the file
    :param sdemd_filename: The name of the SDEMD file to process locally
    :param sqlconn: The SQL connection
    :param args: program args
    :param removefunc: Clean up function.
    :param mpool: multiprocessing pool
    :param resultlist: in commong of current results we are waiting on.
    :return: list of multiprocessing pool results.
    """

    if sdemd_filename.endswith((".SDEMD", ".sdemd")):

        # have an sdemd file.  Open it and get the srid and see if we have a hood.
        try:
            if not os.path.exists(sdemd_filename):
                if len(original_path) > 0:
                    s3cl.download_file(Bucket=original_path[0], Key=original_path[1],
                                       Filename=sdemd_filename)
                if not os.path.exists(sdemd_filename):
                    if args.verbose:
                        print("\nNot Found ", sdemd_filename)
                    return resultlist

            sde = SdemdFile(sdemd_filename)

            if not sde.is_good():
                sde.close()
                removefunc(sde.basename)
                if args.verbose:
                    print("\nDropped ", original_path[0]," ", original_path[1]," ", sde.missing_list(),"\n")
                return resultlist
            if args.bottompercent > 0:
                sr_hood_ent = get_percent_mask(sde, args)
            else:
                sr_hood_ent = sr_fetch_hood_s3(sde.SR_ID,args)
            if 'hpoly' in sr_hood_ent:
                # have a hood already.  So go mask it
                if args.verbose:
                    print("\nProcess hood", sde.filename)

                sde.close()
                queue_up_masking(sr_hood_ent, original_path, sdemd_filename, sde.basename, removefunc, mpool, args, resultlist)
                # if args.nopool:
                #     resulttpl = mask_and_gps(sr_hood_ent, [original_path[0],original_path[1],sdemd_filename], args)
                #     mask_and_gps_finish(removefunc, resulttpl)
                # else:
                #     asres = mpool.apply_async(mask_and_gps,
                #                               (sr_hood_ent, [original_path[0],original_path[1],sdemd_filename], args,))
                #     resultlist.append((asres, "mask", sde.basename))
            else:
                # No hood yet.
                # check the maximum speed to see if it is above our minimum.  If not then drop this one.
                # sde_speed, sde_speed_sr = sde.get_speed()
                sde.close()
                # if sde_speed is None or np.array(sde_speed).max() < args.minkph:
                #     # mark this as processed since it is useless to us.
                #     removefunc(sde.basename)
                #     if args.verbose:
                #         print("\nSpeed drop ", sde.filename)
                #     return resultlist
                # if we are masking do it here but don't clean up
                # if args.bottompercent > 0:
                #     queue_up_masking(sr_hood_ent, original_path, sdemd_filename, sde.basename, lambda n: None, mpool,
                #                      args, resultlist)
                # no hood yet.  So save to sde list if not already there.
                sr_hood_list = sr_fetch_hoodlist_s3(sde.SR_ID, args)
                finddups = [i for i in range(0,len(sr_hood_list)) if sr_hood_list[i][1] == original_path[1]]
                if len(finddups) == 0:
                    sr_hood_list.append([original_path[0],original_path[1],sdemd_filename])
                    sr_put_hoodlist_s3(sde.SR_ID, sr_hood_list, args)
                # see if we have enough to find the hood
                all_sde = sr_hood_list
                if len(all_sde) >= args.minfiles:
                    # go find the hood.
                    if args.nopool:
                        if args.verbose:
                            print("\nFind hood ", all_sde)
                        sr_id, hood_json = find_hood(sde.SR_ID, all_sde, args)
                        if args.verbose:
                            print("\nFound ", hood_json)
                        if hood_json is not None:
                            found_hood(sr_id, hood_json, args, removefunc, mpool)
                        else:
                            hood_not_found(sr_id, hood_json, args, removefunc, mpool)
                    else:
                        if sde.SR_ID not in [ x[2] for x in resultlist if x[1] == 'hood']:
                            asres = mpool.apply_async(find_hood, (sde.SR_ID, all_sde, args,))
                            resultlist.append((asres, "hood", sde.SR_ID))
                else:
                    if args.bottompercent > 0:
                        queue_up_masking(None, original_path, sdemd_filename, sde.basename, removefunc, mpool,
                                         args, resultlist)
                    else:
                        removefunc(sde.basename)
        except Exception as ex:
            # something bad about this file.
            print(ex)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_tb(exc_traceback)
            removefunc(sde.basename)
    return resultlist


def get_percent_mask(sde,args):
    height = sde.video_bounds[1,1]-sde.video_bounds[0,1]
    width = sde.video_bounds[1, 0] - sde.video_bounds[0, 0]
    hline = int(height-(height*args.bottompercent/100))
    if args.bottompercentwidth != 100:
        arcrad = max(int(width*.03),4)
        yarc = np.array(range(0, arcrad + 1))
        arcrad2 = arcrad * arcrad
        xarc = np.around(np.sqrt(arcrad2 - np.square(yarc))).astype(int)
        xwidth = int(width*args.bottompercentwidth/100)
        xmin = int((width-xwidth)/2)
        xmax = xmin+xwidth
        hpoly = [[xmin, height]]
        for i in range(0,len(yarc)):
            hpoly.append([xmin-xarc[i]+arcrad,hline+arcrad-yarc[i]])
        for i in range(len(yarc)-1,-1,-1):
            hpoly.append([xmax -arcrad + xarc[i] , hline - yarc[i]+arcrad])
        hpoly.extend([[xmax, height],[xmin, height]])
    else:
        hpoly = [[0,hline],[width,hline],[width,height],[0,height],[0,hline]]
    return {'hline':hline, 'hpoly':hpoly}


def queue_up_masking(sr_hood_ent, original_path, sdemd_filename, sde_basename, removefunc, mpool, args, resultlist):
    if args.nopool:
        resulttpl = mask_and_gps(sr_hood_ent, [original_path[0], original_path[1], sdemd_filename], args)
        mask_and_gps_finish(removefunc, resulttpl)
    else:
        asres = mpool.apply_async(mask_and_gps,
                                  (sr_hood_ent, [original_path[0], original_path[1], sdemd_filename], args,))
        resultlist.append((asres, "mask", sde_basename))
    return None


def find_hood(sr_id, all_sde, args):
    """
    Find a hood if we can.
    :param sr_id: the SRID
    :param all_sde: List of SDEMD files.
    :param args: Program Arguments
    :return: SRID, hood json
    """
    # First see if we have all the files we need local.
    sde_files =[]
    for fltpl in all_sde:
        if not os.path.exists(fltpl[2]):
            if len(fltpl[0]) > 0:
                s3cl.download_file(Bucket=fltpl[0], Key=fltpl[1],
                                   Filename=fltpl[2])
            if not os.path.exists(fltpl[2]):
                continue
        sde = SdemdFile(fltpl[2])
        sde.close()
        sdevideo = sde.get_video_filename()
        sdevideo_key = fltpl[1][0:fltpl[1].rfind('/')+1]+sdevideo
        sdevid_dest = os.path.join(os.path.dirname(fltpl[2]),sdevideo)
        if not os.path.exists(sdevid_dest):
            s3cl.download_file(Bucket=fltpl[0], Key=sdevideo_key,
                               Filename=sdevid_dest)
            if not os.path.exists(sdevid_dest):
                continue
        sde_files.append(fltpl[2])

    hd = HoodDetect()
    hline = hd.detect_hood_from_files("", sde_files, minspeed=args.minkph, minbright=args.minbright)
    if hd.framecount < args.minframes:
        return sr_id, None
    hpoly = hd.get_reduced_to_polygon().reshape((-1, 2))

    if args.auditdir is not None and hd.maxbright_file is not None:
        sde = SdemdFile(hd.maxbright_file)
        sdefn, sdefps = sde.get_video()
        sde.close()
        vid = SdeVideo(sdefn, crop=sde.video_bounds)
        while True:
            ret, frame = vid.read()
            if not ret:
                break
            if vid.get_last_frame_no() < hd.maxbright_frame:
                continue
            cv2.fillPoly(frame, [hpoly], (0, 0, 0))
            newfilename = os.path.join(args.auditdir, "{}_{}.jpg".format(sde.SR_ID, int(hd.weightsum / hd.framecount)))
            cv2.imwrite(newfilename, frame)
            vid.release()
            break

    # all_vp = []
    # for fltpl in all_sde:
    #     hd = HoodDetect()
    #     hd.detect_hood_from_files("", [fltpl], minspeed=args.minkph, minbright=args.minbright, dofindhood=False)
    #     all_vp.append(hd.get_vanish_y())
    # print("\n"+all_vp+"\n")

    return sr_id, json.dumps({'hline': hline, 'hpoly': hpoly.tolist()})


def found_hood(sr_id, hood_json, args, removefunc, mpool):
    """
    Handle a found hood on the main process.
    :param sr_id: SRID
    :param hood_json: Hood json
    :param args: program args
    :param removefunc: function to remove files we are done with.
    :param mpool: multiprocessing pool.
    :return: list of additional results to check later.
    """
    resultlist = []
    # get the old hood data.
    sr_hood_list = sr_fetch_hoodlist_s3(sr_id, args)
    # delete the hood list.
    # sr_put_hoodlist_s3(sr_id,[],args)
    # put the results in the database.
    sr_write_hood_s3(sr_id, hood_json, args)
    # Get all the SDEMD files for this SRID
    all_sde = sr_hood_list
    if len(all_sde) > 0:
        # go mask the hood.
        hood_hp = json.loads(hood_json)
        for an_sde in all_sde:
            try:
                queue_up_masking(hood_hp, an_sde, an_sde[2], os.path.splitext(os.path.basename(an_sde[2]))[0], removefunc, mpool, args, resultlist)
                # if args.nopool:
                #     sdesrid, sdefilename, sdebasename, origfile, isgood = mask_and_gps(hood_hp, an_sde, args)
                #     mask_and_gps_finish(removefunc, (sdesrid, sdefilename, sdebasename, origfile, isgood))
                # else:
                #     asres = mpool.apply_async(mask_and_gps, (hood_hp, an_sde, args,))
                #     resultlist.append((asres, "mask", an_sde[2]))

            except Exception as exx:
                print(exx)
                exc_type, exc_value, exc_traceback = sys.exc_info()
                traceback.print_tb(exc_traceback)
                removefunc(os.path.splitext(os.path.basename(an_sde))[0])
                continue
    return resultlist


def hood_not_found(sr_id, hood_json, args, removefunc, mpool):
    """
    Handle a found hood on the main process.
    :param sr_id: SRID
    :param hood_json: Hood json
    :param args: program args
    :param removefunc: function to remove files we are done with.
    :param mpool: multiprocessing pool.
    :return: list of additional results to check later.
    """
    resultlist = []
    # get the old hood data.
    sr_hood_list = sr_fetch_hoodlist_s3(sr_id, args)
    all_sde = sr_hood_list
    if len(all_sde) > 0:
        # go mask the hood.
        for an_sde in all_sde:
            try:
                queue_up_masking(None, an_sde, an_sde[2], os.path.splitext(os.path.basename(an_sde[2]))[0], removefunc, mpool, args, resultlist)
            except Exception as exx:
                print(exx)
                exc_type, exc_value, exc_traceback = sys.exc_info()
                traceback.print_tb(exc_traceback)
                removefunc(os.path.splitext(os.path.basename(an_sde))[0])
                continue
    return resultlist


def mask_and_gps(sr_hood, sdefiletpl, args):
    """
    create the masked jpg and gps files for this SDEMD file
    :param sr_hood: SR hood data.
    :param sdefiletpl: The sdefile name locally
    :param args: program args.
    :return: SR_ID, sde filename, sde basename, orig_path, good(1 or 0)
    """
    if not os.path.exists(sdefiletpl[2]):
        if len(sdefiletpl[0]) > 0:
            s3cl.download_file(Bucket=sdefiletpl[0], Key=sdefiletpl[1],
                               Filename=sdefiletpl[2])
        if not os.path.exists(sdefiletpl[2]):
            return "", "", "", sdefiletpl[2], 1
    sde = SdemdFile(sdefiletpl[2])
    if not sde.is_good():
        sde.close()
        return sde.SR_ID, sde.filename, sde.basename, sdefiletpl[2], 1

    path_date = sde.get_YYYYMMDD()
    outbucket, outprefix = parse_out_bucket(args.outbucket, path_date[0:4] + "/" + path_date[4:6] + "/" + path_date[6:8] + "/")
    hashname = get_sdemd_hash(sde.basename)
    try:
        jpgkey = get_key(outprefix, hashname, "", "")
        files_list_res = s3cl.list_objects_v2(Bucket=outbucket, Prefix=jpgkey, FetchOwner=False)
        if 'Contents' in files_list_res and len(files_list_res['Contents']) > 0:
            sde.close()
            return sde.SR_ID, sde.filename, sde.basename, sdefiletpl[2], 1
    except Exception as e:
        pass
    if args.bottompercent > 0:
        sr_hood = get_percent_mask(sde, args)
    if sr_hood is None or sr_hood["hpoly"] is None:
        return sde.SR_ID, sde.filename, sde.basename, sdefiletpl[2], 0
    sdevideo = sde.get_video_filename()
    sdevideo_key = sdefiletpl[1][0:sdefiletpl[1].rfind('/') + 1] + sdevideo
    sdevid_dest = os.path.join(os.path.dirname(sdefiletpl[2]), sdevideo)
    if not os.path.exists(sdevid_dest):
        s3cl.download_file(Bucket=sdefiletpl[0], Key=sdevideo_key,
                           Filename=sdevid_dest)
        if not os.path.exists(sdevid_dest):
            sde.close()
            return sde.SR_ID, sde.filename, sde.basename, sdefiletpl[2], 1

    hood_poly = np.array(sr_hood['hpoly'])
    loc_hdr, loc, locgrps = sde.get_location()
    if loc is None:
        sde.close()
        return sde.SR_ID, sde.filename, sde.basename, sdefiletpl[2], 1
    try:
        locgrps, grp_interp, same_loc = gps_interpolate(loc,None,sde.total_duration)
    except:
        pass
    # get the number of GPS records per secong.
    gps_ps = int(len(locgrps) / sde.total_duration)
    vfile, fps = sde.get_video()
    if not os.path.exists(vfile):
        return sde.SR_ID, sde.filename, sde.basename, sdefiletpl[2], 0
    # check if we are writing to a bucket.
    vid = SdeVideo(vfile, crop=sde.video_bounds)
    vframe = 0
    vframe_idx = 0
    gps_idx = 0
    error_happened = False
    while True:
        ret, frame = vid.read()
        if not ret:
            break
        if vid.get_last_frame_no() < vframe:
            continue
        if len(locgrps) > gps_idx and len(locgrps[gps_idx]) >= 2:
            if args.crop:
                frame = frame[0:int(sr_hood['hline']),:]
            else:
                cv2.fillPoly(frame, [hood_poly], (45, 45, 45))
            # to get no loss use  params=(cv2.IMWRITE_JPEG_QUALITY, 100)
            retval, jpgbuf = cv2.imencode(".jpg", frame)
            jpgkey = get_key(outprefix, hashname, vframe_idx, ".jpg")
            jsons = gps_to_dict_str(sde.SR_ID, outbucket, jpgkey, frame.shape[:2],len(jpgbuf),locgrps[gps_idx], grp_interp[gps_idx], same_loc[gps_idx], vframe_idx)
            if jsons is None:
                continue
            s3result = s3cl.put_object(Body=io.BytesIO(jpgbuf), Bucket=outbucket,
                                       Key=jpgkey)
            if not (s3result['ResponseMetadata'] is not None and
                    s3result['ResponseMetadata']['HTTPStatusCode'] is not None and
                    s3result['ResponseMetadata']['HTTPStatusCode'] == 200):
                error_happened = True
                print("\nError writing to S3 ", s3result['ResponseMetadata']['HTTPStatusCode'])
                break

            # convert gps data to text and write to file.
            s3result = s3cl.put_object(
                Body=io.BytesIO(jsons.encode('utf-8')),
                Bucket=outbucket, Key=get_key(outprefix, hashname, vframe_idx, ".json"))
            if not (s3result['ResponseMetadata'] is not None and
                    s3result['ResponseMetadata']['HTTPStatusCode'] is not None and
                    s3result['ResponseMetadata']['HTTPStatusCode'] == 200):
                error_happened = True
                print("\nError writing to S3 ", s3result['ResponseMetadata']['HTTPStatusCode'])
                break

            # convert gps data to text and write to file.

            # s3result = s3cl.put_object(
            #     Body=io.BytesIO("{}\n{}\n".format(",".join(locgrps[gps_idx][0]),
            #                                       ",".join(locgrps[gps_idx][1])).encode('utf-8')),
            #     Bucket=outbucket, Key=get_key(outprefix, hashname, vframe_idx, ".gps"))
            # if not (s3result['ResponseMetadata'] is not None and
            #         s3result['ResponseMetadata']['HTTPStatusCode'] is not None and
            #         s3result['ResponseMetadata']['HTTPStatusCode'] == 200):
            #     error_happened = True
            #     print("\nError writing to S3 ", s3result['ResponseMetadata']['HTTPStatusCode'])
            #     break

        vframe += fps
        vframe_idx += 1
        gps_idx += gps_ps

    vid.release()
    sde.close()
    if args.debug:
        # save the hash name to SDEMD file name mapping for debugging.
        outbucket, outprefix = parse_out_bucket(args.dbbucket,SRSDEMDHASH)
        # just blindly write this.
        s3cl.put_object(Body=io.BytesIO("{}, {}".format(sdefiletpl[0], sdefiletpl[1]).encode('utf-8')), Bucket=outbucket,
                        Key=outprefix+hashname+'.txt')
    return sde.SR_ID, sde.filename, sde.basename, sdefiletpl[2], 0 if error_happened else 1


def parse_out_bucket(outbucketarg, extrapath=''):
    """
    Construct the outbucket and get the prefix.
    :param outbucketarg:
    :param extrapath: to append to the prefix.  Rememeber to add a /
    :return: bucket name, prefix to use
    """
    bucket_end = outbucketarg.find('/')
    if bucket_end < 0:
        bucket = outbucketarg
        prepre = ""
    else:
        bucket = outbucketarg[0:bucket_end]
        prepre = outbucketarg[bucket_end + 1:]
        if not prepre.endswith("/"):
            prepre = prepre + "/"
    prepre = prepre + extrapath
    return bucket, prepre


def get_key(outprefix, hashname, vframe_idx, ext):
    """
    Form the key for the destination bucket
    :param outprefix: output prefix
    :param hashname: hash name to use
    :param vframe_idx: index sequence number
    :param ext: file extension
    :return: output key for S3
    """
    return "{}{}_{}{}".format(outprefix, hashname, vframe_idx, ext)


def mask_and_gps_finish(removefunc, resulttpl):
    """
    Finish processing on the main process.
    :param removefunc: clean up function
    :param resulttpl: results tuple
    :return:
    """
    removefunc(resulttpl[2])
    return None


def gps_to_dict_str(sr_id,jpg_bucket, jpg_key, jpg_wh,jpg_size, gps_data, gps_interp, gps_same_previous, video_seq):
    jdict = {"source_id":hashlib.sha512(sr_id.encode('utf-8')).hexdigest()[:16]}
    jpg_split = jpg_key.split("/")
    jpg_split_len = len(jpg_split)
    jdict["sde_year"] = jpg_split[jpg_split_len-4]
    jdict["sde_month_num"] = jpg_split[jpg_split_len-3]
    jdict["sde_day_of_month_num"] = jpg_split[jpg_split_len-2]
    jdict["jpeg_file_id"] = jpg_split[jpg_split_len-1]
    jdict["video_id"] = jpg_split[jpg_split_len-1].split("_")[0]
    jdict["jpeg_bucket"] = jpg_bucket
    jdict["jpeg_horiz_size_pixel"] = jpg_wh[1]
    jdict["jpeg_vert_size_pixels"] = jpg_wh[0]
    jdict["jpeg_image_size_bytes"] = jpg_size
    jdict["gps_interpolated"] = gps_interp
    jdict["gps_same_loc"] = gps_same_previous
    jdict["video_sequence"] = video_seq
    for gps in gps_data:
        if gps[0].startswith("$GPRMC"):
            if len(gps) < 13:
                return None
            jdict["gps_rmc_timefix"]=gps[1]
            jdict["gps_rmc_status_active_or_void"]=gps[2]
            jdict["gps_rmc_latitude_deg"]=gps[3]
            jdict["gps_rmc_latitude_directionNS"]=gps[4]
            jdict["gps_rmc_longitude_deg"]=gps[5]
            jdict["gps_rmc_longitude_directionEW"]=gps[6]
            jdict["gps_rmc_speed_in_knots"]=gps[7]
            jdict["gps_rmc_bearing_deg"]=gps[8]
            jdict["gps_rmc_date"]=gps[9]
            jdict["gps_rmc_magnetic_variation"]=gps[10]+","+gps[11]
            sidcsum = gps[12].split("*")
            if len(sidcsum) < 2:
                jdict["gps_rmc_checksum_tag"]=gps[12]
            else:
                jdict["gps_rmc_checksum_tag"] = "*" + sidcsum[1]
        else:
            if len(gps) < 15:
                return None
            jdict["gps_gga_timefix"]=gps[1]
            jdict["gps_gga_latitude_deg"]=gps[2]
            jdict["gps_gga_latitude_directionNS"]=gps[3]
            jdict["gps_gga_longitude_deg"]=gps[4]
            jdict["gps_gga_longitude_directionEW"]=gps[5]
            jdict["gps_gga_fix_quality"]=gps[6]
            jdict["gps_gga_num_satellites"]=gps[7]
            jdict["gps_gga_hdop"]=gps[8]
            jdict["gps_gga_altitude"]=gps[9]+","+gps[10]
            jdict["gps_gga_geoidheight_mean_sea_level"]=gps[11]+","+gps[12]
            jdict["gps_gga_dgps_time_since_last_fix"]=gps[13]
            sidcsum= gps[14].split("*")
            if len(sidcsum) < 2:
                jdict["gps_gga_dgps_station_id"]=""
                jdict["gps_gga_checksum_tag"]=gps[14]
            else:
                jdict["gps_gga_dgps_station_id"] = sidcsum[0]
                jdict["gps_gga_checksum_tag"] = "*" + sidcsum[1]
    if len(jdict) != 37:
        return None
    return json.dumps(jdict)

if __name__ == "__main__":
    freeze_support()
    main()
    sys.exit(0)
