import os
#os.environ["CUDA_VISIBLE_DEVICES"]="-1"
import tensorflow as tf

from tensorflow.python.client import device_lib
print(device_lib.list_local_devices())

import sys
import model as modellib
from config import Config

def load_model():
    class SDConfig(Config):
        """Configuration for training on the SD dataset.
        Derives from the base Config class and overrides values specific
        to the SD dataset.
        """
        # Give the configuration a recognizable name
        NAME = "SmartDrive"

        # Train on 1 GPU and 8 images per GPU. We can put multiple images on each
        # GPU because the images are small. Batch size is 8 (GPUs * images/GPU).
        GPU_COUNT = 1
        IMAGES_PER_GPU = 1

        # Number of classes (including background)
        NUM_CLASSES = 1 + 11  # background + 3 shapes

        # Use small images for faster training. Set the limits of the small side
        # the large side, and that determines the image shape.
        IMAGE_MIN_DIM = 512
        IMAGE_MAX_DIM = 768  # 1280

        RPN_ANCHOR_RATIOS = [0.5, 1, 2]
        #    MASK_SHAPE = [56,56]

        # Use smaller anchors because our image and objects are small
        RPN_ANCHOR_SCALES = (32, 64, 128, 256, 512)  # (8, 16, 32, 64, 128)  # anchor side in pixels

        # Reduce training ROIs per image because the images are small and have
        # few objects. Aim to allow ROI sampling to pick 33% positive ROIs.
        TRAIN_ROIS_PER_IMAGE = 16

        # Use a small epoch since the data is simple
        STEPS_PER_EPOCH = 100

        # use small validation steps since the epoch is small
        VALIDATION_STEPS = 5

        DETECTION_MAX_INSTANCES = 50

        # Minimum probability value to accept a detected instance
        # ROIs below this threshold are skipped
        DETECTION_MIN_CONFIDENCE = 0.85

    # set up to use NN
    MODEL_PATH = "C:\\Users\\BradR\\Desktop\\mask_rcnn_hood"
    sys.path.append(MODEL_PATH)
    MODEL_DIR = os.path.join(MODEL_PATH, "logs")
    model_config = SDConfig()
    model_config.display()
    # Recreate the model in inference mode
    model_model = modellib.MaskRCNN(mode="inference",
                              config=model_config,
                              model_dir=MODEL_DIR)

    # Get path to saved weights
    # Either set a specific path or find last trained weights
    # model_path = os.path.join(ROOT_DIR, ".h5 file name here")
    model_path = model_model.find_last()[1]

    # Load trained weights (fill in path to trained weights here)
    assert model_path != "", "Provide path to trained weights"
    print("Loading weights from ", model_path)
    model_model.load_weights(model_path, by_name=True)
    return model_model
