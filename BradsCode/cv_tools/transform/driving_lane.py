import os
import sys
import argparse
import cv2
import numpy as np
import math
from cv_utils.sdevideo import SdeVideo
from cv_utils.cubic_interp import CubicInterp
from cv_utils.quad_interp import QuadInterp
from transform.PerspectiveTransInfo import PerspectiveTransInfo, pointhtrans
from sklearn.linear_model import RANSACRegressor
from sklearn.base import BaseEstimator
from scipy.optimize import curve_fit, minimize
from scipy.interpolate import BSpline, CubicSpline
from cv_utils.MedianAbsDev import median_abs_dev, MedianAbsDev
from shapely.geometry import box, Polygon
import json
import gc



class PolyEstimator(BaseEstimator):
    """
    Class to provide polynomial estimator.
    """
    def __init__(self,anchors=None,degree=5):
        """
        Create Polynomial Estimator
        :param degree: Polniomial degree
        :param anchors: End points polynomial must go through
        """
        self.polycoef = None
        self.anchors = anchors
        self.degree = degree

    def fit(self,x,y,sample_weight=None):
        """
         Fit Spline to the data.
         :param x: X values
         :param y: Corresponding Ys
         :param sample_weight: Coresponding Weights.
         :return:
        """
        if self.anchors is None:
            if sample_weight is not None:
                self.polycoef = np.polyfit(x.flatten(), y, deg=self.degree,w=sample_weight)
            else:
                self.polycoef = np.polyfit(x.flatten(),y,deg=self.degree)
        else:
            xe = np.concatenate([[self.anchors[0,0]],x.flatten(),[self.anchors[1,0]]])
            ye = np.concatenate([[self.anchors[1,1]],y,[self.anchors[1,1]]])
            if sample_weight is None:
                ws = np.full(len(ye),1.0)
                ws[0] = ws[len(ws)-1] = ws.sum()
            else:
                wsum = np.sum(sample_weight)
                ws = np.concatenate([[wsum], sample_weight, [wsum]])
            self.polycoef = np.polyfit(xe, ye, deg=self.degree, w=ws)
        return None

    def score(self,x,y):
        """
        Return the mean squared error.
        :param x: X values
        :param y: Coresponding Ys
        :return: Mean Squared Errror
        """
        py = np.polyval(self.polycoef, x.flatten())
        mse = ((py-y)**2).mean()
        return mse

    def predict(self,x):
        """
        Predict the Ys given X
        :param x: X Values
        :return: f(X) returned
        """
        return np.polyval(self.polycoef, x.flatten())


class CubicSplEstimator(BaseEstimator):
    def __init__(self, initval, nknot=3, minmax_x=(0,480)):
        self.nknot = nknot
        self.minmax_x = minmax_x
        self.initval = initval
        self.xn = np.concatenate((np.arange(minmax_x[0], minmax_x[1], minmax_x[1] / (nknot )),[minmax_x[1]]))
        self.yn = np.full(len(self.xn),initval)
        self.bc_type = ((2,0.0),(1,0.0))
        self.cs = CubicSpline(self.xn,self.yn,bc_type = self.bc_type)

    def cubicfunc(self, x, *parm):
        self.yn = parm
        self.cs = CubicSpline(self.xn, self.yn, bc_type=self.bc_type)
        return self.cs(x)

    def fit(self, x, y, sample_weight=None):
        """
        Fit Spline to the data.
        :param x: X values
        :param y: Corresponding Ys
        :param sample_weight: Coresponding Weights.
        :return:
        """
        xe = None
        ye = None
        ws = None

        try:
            xe = x.flatten()
            xsrt = np.argsort(xe)
            self.yn, pcov = curve_fit(self.cubicfunc, xe[xsrt], y[xsrt], p0=self.yn, sigma=(sample_weight if sample_weight is None else 1.0/sample_weight[xsrt]), absolute_sigma=False, method='trf')
        except Exception as e:
            print(e)
            raise e
        return None

    def score(self,x,y):
        """
        Return the mean squared error.
        :param x: X values
        :param y: Coresponding Ys
        :return: Mean Squared Errror
        """
        py = self.cs(x.flatten())
        mse = ((py-y)**2).mean()
        return mse

    def predict(self,x):
        """
        Predict the Ys given X
        :param x: X Values
        :return: f(X) returned
        """
        return self.cs(x.flatten())


class SplEstimator(BaseEstimator):
    """
    Estimate using b-splines
    """
    def __init__(self, initval, nknot=3, max_x=720, anchors=None):
        """
        Create Spline Estimator using fixed knots
        :param initval: Initial y value for spline.
        :param nknot: Number of knots
        :param max_x: Maximum X value
        :param anchors: End points spline must go through
        """
        self.nknot = nknot
        self.max_x = max_x
        self.initval = initval
        self.anchors = anchors
        self.t = None
        self.c = None
        self.k = 2

    def splfunc(self, x, *parm):
        """
        Evaluate the spline
        :param x: X value to evaluate at.
        :param parm: Spline coefficients.
        :return: The f(x) for the spline.
        """
        return (BSpline(self.t, parm, self.k))(x)

    def fit(self, x, y, sample_weight=None):
        """
        Fit Spline to the data.
        :param x: X values
        :param y: Corresponding Ys
        :param sample_weight: Coresponding Weights.
        :return:
        """
        xe = None
        ye = None
        ws = None

        if self.anchors is None:
            try:
                self.getinit_param()
                xe = x.flatten()
                xsrt = np.argsort(xe)
                self.c, pcov = curve_fit(self.splfunc, xe[xsrt], y[xsrt], p0=self.c, sigma=(sample_weight if sample_weight is None else 1.0/sample_weight[xsrt]), absolute_sigma=False, method='trf')
            except Exception as e:
                print(e)
                raise e
        else:
            try:
                xe = np.concatenate([[self.anchors[1,0],self.anchors[0, 0]], x.flatten()])
                ye = np.concatenate([[self.anchors[1,1], self.anchors[0, 1]], y])
                if sample_weight is None:
                    ws = np.full(len(ye), 1.0)
                    ws[0] = ws[len(ws)-1] = ws.sum()
                else:
                    wsum = np.sum(sample_weight)
                    ws = np.concatenate([[wsum, wsum], sample_weight])
                self.getinit_param()
                xsrt = np.argsort(xe)
                self.c, pcov = curve_fit(self.splfunc, xe[xsrt], ye[xsrt], p0=self.c, sigma=1.0/ws[xsrt],absolute_sigma=False, method='trf')
            except Exception as e:
                print(e)
                raise e
        return None

    def score(self,x,y):
        """
        Return the mean squared error.
        :param x: X values
        :param y: Coresponding Ys
        :return: Mean Squared Errror
        """
        py = self.splfunc(x.flatten(),*self.c)
        mse = ((py-y)**2).mean()
        return mse

    def predict(self,x):
        """
        Predict the Ys given X
        :param x: X Values
        :return: f(X) returned
        """
        return self.splfunc(x.flatten(), *self.c)

    def getinit_param(self):
        """
        Find the initial spline guess.
        :return:
        """
        incsize = int(self.max_x / (self.nknot + self.k + 1))
        self.t = np.array([st for st in range(0,self.max_x+1,incsize)])
        self.c = np.full(len(self.t)-self.k-1,self.initval,dtype=np.float)
        return None


def drive_lane(vidfile, speedfile, ppt, seconds_ahead=1, out=None, debug=True):
    """
    Given a SDE file and the safety file.  Return the list of polygons for up to 3 seconds away. Also
    return lane departure info per frame.
    :param vidfile:
    :param speedfile:
    :param seconds_ahead:
    :return: per frame array of polygons paths, and per frame departure
    """
    vcap = SdeVideo(vidfile)
    maxx, maxy = vcap.get_shape()
    safety = read_safety_file(speedfile)
    # along the bottom find 12 feet in the image plane.
    org = ppt.getInvPerTrans()
    _, orginv = cv2.invert(org, flags=cv2.DECOMP_LU)
    pt_cent_onroad = pointhtrans([ppt.van_pt[0],ppt.f_hood_scanline], org)
    left_6 = pointhtrans([pt_cent_onroad[0]-6,pt_cent_onroad[1]],orginv)
    right_6 = pointhtrans([pt_cent_onroad[0]+6,pt_cent_onroad[1]],orginv)
    #
    # get the mapping to the road image and back.
    # fps = 0.911344 * np.max(safety[:,1])
    # newscale = ppt.f_height/(fps*seconds_ahead)
    newscale = ppt.f_height/60.0
    five_feet_in_ahead = int(np.round(5.0*newscale))
    scaletrans = np.array([[newscale, 0.0, 0.0],
                           [0.0, newscale, 0.0], [0.0, 0.0, 1.0]])
    # combine the scaling with the transform.
    toroad = np.dot(scaletrans, ppt.getInvPerTrans())
    # now translate to the center of the screen.
    leftside = pointhtrans([0,ppt.f_height],toroad)
    rightside = pointhtrans([ppt.f_width,ppt.f_height],toroad)
    #  stretch = .80*ppt.f_width/abs(leftside[0]-rightside[0])
    stretch = 1.0
    # half_foot_horz = int(np.ceil(stretch*abs(leftside[0]-rightside[0])/24))

    toroad = np.dot(np.array([[stretch,0.0,0.0],[0.0,1.0,0.0],[0.0,0.0,1.0]]), toroad)
    ptcentbot = pointhtrans([ppt.f_width / 2, ppt.f_hood_scanline], toroad)
    justtrans = np.array([[1.0, 0.0, ((ppt.f_width / 2) - ptcentbot[0])],
                          [0.0, 1.0, (ppt.f_height) - ptcentbot[1]], [0.0, 0.0, 1.0]])
    toroad = np.dot(justtrans, toroad)
    left_init = pointhtrans(left_6,toroad)[0]
    right_init = pointhtrans(right_6,toroad)[0]
    _, toroadinv = cv2.invert(toroad, flags=cv2.DECOMP_LU)
    half_foot_horz = max(int(np.rint((right_init-left_init)/24.0)),1)

    nframes = 0
    while True:
        ret, frame = vcap.read(frameno=nframes)
        if not ret:
            break
        # if model_model is not None:
        #     mcnn_frame(frame.copy(),toroad, maxx,maxy)
        lane_sides_frame(frame, toroad, toroadinv, maxx, maxy, five_feet_in_ahead,half_foot_horz,left_init,right_init,out, debug)
        nframes += 1
    vcap.release()
    print(vidfile, speedfile,ppt)
    return None

def lane_sides_frame(frame, toroad, toroadinv, maxx, maxy, five_feet_ahead, half_foot_horz, left_init, right_init, out, debug):
    """
    For a frame find the left and right of the lane.
    :param frame:
    :param toroad mapping to the road
    :param toroadinv back to the image.
    :return:
    """

    # find a number that evenly splits the forward distance
    splitto = find_exact_divisor(maxy, int(five_feet_ahead/2))

    # warp the iamge to the road. And gray scale it.
    dst = cv2.warpPerspective(cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY),toroad,(maxx,maxy))
    dstcol = cv2.cvtColor(dst, cv2.COLOR_GRAY2BGR)
    dst_debug = dstcol.copy()
    if not debug:
        dst_debug = None
        dstcol = None
    # cmax = cv2.blur(np.amax(frame,axis=2),(7,3))
    # dst = cv2.blur(cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY),(7,3))

    # try a morphology to remove wide bright things.
    # bwt = cv2.adaptiveThreshold(dst,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,3*half_foot_horz,0)
    # th, bwt = cv2.threshold(dst.copy(), 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    # th = np.percentile(dst,95)
    # thmax = np.amax(dst)
    # th, bwt = cv2.threshold(dst.copy(), th, 255, cv2.THRESH_BINARY)
    # vfilt = np.ones((1,3*half_foot_horz),np.uint8)
    # bwt = cv2.erode(bwt,vfilt,iterations=1)
    # bwt = cv2.dilate(bwt,vfilt,iterations=1)
    # bwt[bwt==255] = (thmax-th)
    # dst = np.clip(dst-(bwt.astype(np.int32)),0,255).astype(np.uint8)

    # cv2.imshow("result",dst)
    # cv2.waitKey(60)

    # Split the image into horizontal segments.  Then histogram on brightness across each band.
    dst_segd = dst.copy().reshape((splitto,int(maxy/splitto),maxx))
    histos = np.sum(dst_segd, axis=1,dtype=np.int32)


    # threshs = np.percentile(dst_segd,95,axis=(1,2))
    last_left = int(left_init)
    last_right = int(right_init)
    last_center = int((right_init+left_init)/2)
    win_size = int((last_right-last_left)*1.5)
    yhgt = int(maxy/splitto)

    xfit_right = []
    yfit_right = []
    xfit_left = []
    yfit_left = []
    last_bright_percent = 0

    for i in range(splitto-1,-1,-1):
        leftpeak = [last_left]
        rightpeak = [last_right]
        left_lowest = int(last_center - win_size)
        right_highest = int(last_center+win_size)
        mid_high_low = int(last_center)
        leftmean = np.mean(histos[i,left_lowest:mid_high_low])+np.std(histos[i,left_lowest:mid_high_low])
        rightmean = np.mean(histos[i, mid_high_low:right_highest])+np.std(histos[i, mid_high_low:right_highest])
        leftpeaks = (np.argsort(-histos[i,left_lowest:mid_high_low])+left_lowest)
        leftpeaks = leftpeaks[histos[i,leftpeaks] > leftmean]
        rightpeaks = (np.argsort(-histos[i,mid_high_low:right_highest])+mid_high_low)
        rightpeaks = rightpeaks[histos[i,rightpeaks] > rightmean]
        # filter out ones near the peaks.
        iteridx = 0
        while iteridx < 8 and iteridx < len(leftpeaks) :
            keepers = ~np.logical_and(np.greater(leftpeaks, leftpeaks[iteridx] - half_foot_horz), np.less(leftpeaks, leftpeaks[iteridx] + half_foot_horz))
            keepers[iteridx] =True
            leftpeaks = leftpeaks[keepers]
            iteridx +=1
        leftpeaks = leftpeaks[:8]
        iteridx = 0
        while iteridx < 8 and iteridx < len(rightpeaks):
            keepers = ~np.logical_and(np.greater(rightpeaks, rightpeaks[iteridx] - half_foot_horz), np.less(rightpeaks, rightpeaks[iteridx] + half_foot_horz))
            keepers[iteridx] =True
            rightpeaks = rightpeaks[keepers]
            iteridx +=1
        rightpeaks = rightpeaks[:8]
        # leftpeaks = (leftpeaks[leftpeaks > left_lowest])[:8]
        # rightpeaks = (rightpeaks[rightpeaks < right_highest])[:8]

        if dstcol is not None:
            for ll in leftpeaks:
                cv2.line(dstcol, (ll, int(yhgt * (i + 1))), (ll, int(yhgt * i)), (255, 0, 0), thickness=1)
            for ll in rightpeaks:
                cv2.line(dstcol, (ll, int(yhgt * (i + 1))), (ll, int(yhgt * i)), (255, 0, 0), thickness=1)
            cv2.imshow("road +",dstcol)
            # cv2.imshow("frame",frame)
            cv2.waitKey(60)
        # if len(leftpeaks) > 0 and len(rightpeaks) > 0:
        #     midxdist = np.argmin(np.abs(np.abs(leftpeaks[:,np.newaxis]-rightpeaks)-abs(last_left-last_right))+np.abs(leftpeaks-last_left)[:,np.newaxis]+np.abs(rightpeaks-last_right))
        #     leftpeak = leftpeaks[int(midxdist/len(rightpeaks))]
        #     rightpeak = rightpeaks[int(midxdist%len(rightpeaks))]
        #     cv2.line(dstcol, (leftpeak, int(yhgt * (i + 1))), (leftpeak, int(yhgt * i)), (128, 0, 255), thickness=2)
        #     cv2.line(dstcol, (rightpeak, int(yhgt * (i + 1))), (rightpeak, int(yhgt * i)), (128, 255, 0), thickness=2)
        dropleft = True
        dropright = True
        if len(leftpeaks) > 0:
            ldistlast = np.abs(leftpeaks - left_init)
            ldistlast = ldistlast-np.min(ldistlast)
            if np.max(ldistlast) > 0.0:
                ldistlast = ldistlast/(np.max(ldistlast))
            lhist = 1.0/histos[i,leftpeaks]
            lhist = lhist-np.min(lhist)
            if np.max(lhist) > 0.0:
                lhist = lhist/np.max(lhist)
            leftpeak = leftpeaks[np.argsort(ldistlast+lhist)[0:min(3,len(lhist))]]
            dropleft = False
        if len(rightpeaks) > 0:
            rdistlast = np.abs(rightpeaks - right_init)
            rdistlast = rdistlast-np.min(rdistlast)
            if np.max(rdistlast) > 0.0:
                rdistlast = rdistlast/(np.max(rdistlast))
            rhist = 1.0/histos[i,rightpeaks]
            rhist = rhist-np.min(rhist)
            if np.max(rhist) > 0.0:
                rhist = rhist/np.max(rhist)
            rightpeak = rightpeaks[np.argsort(rdistlast+rhist)[0:min(3,len(rhist))]]
            dropright = False

        # left_x_extend = np.full(yhgt, leftpeak)
        left_x_extend = np.repeat([leftpeak],yhgt,axis=0).flatten()
        # left_y_extend = np.arange((i * yhgt), (i + 1) * yhgt, 1)
        left_y_extend = np.repeat(np.arange((i * yhgt), (i + 1) * yhgt, 1),len(leftpeak))
        xfit_left.extend(left_x_extend)
        yfit_left.extend(left_y_extend)
        right_x_extend = np.repeat([rightpeak],yhgt,axis=0).flatten()
        right_y_extend = np.repeat(np.arange((i * yhgt), (i + 1) * yhgt, 1),len(rightpeak))
        xfit_right.extend(right_x_extend)
        yfit_right.extend(right_y_extend)


        horzdist = abs(rightpeak[0]-leftpeak[0])
        if horzdist > 30*half_foot_horz or horzdist < 12*half_foot_horz:
            if abs(last_right-rightpeak[0]) < abs(last_left-leftpeak[0]):
                leftpeak[0] = int(rightpeak[0] - 24*half_foot_horz)
                dropleft = True
            else:
                rightpeak[0] = int(leftpeak[0] + 24*half_foot_horz)
                dropright = True

        if not dropleft:
            last_left = leftpeak[0]
            # xfit_left.append([leftpeak, histos[i, leftpeak]])
            # yfit_left.append((yhgt * (i + .5)))
            left_bright = np.percentile(dst[(left_y_extend,left_x_extend)],95)
        if not dropright:
            last_right = rightpeak[0]
            # xfit_right.append([rightpeak,histos[i,rightpeak]])
            # yfit_right.append((yhgt*(i+.5)))
            right_bright = np.percentile(dst[(right_y_extend, right_x_extend)], 95)
        if not dropright and not dropleft:
            if last_bright_percent != 0:
                if left_bright > last_bright_percent and right_bright > last_bright_percent:
                    last_center = int((last_center + ((last_left+last_right)/2.0))/2.0)
            last_bright_percent = np.percentile(np.concatenate((dst[(yfit_left,xfit_left)],dst[(yfit_right,xfit_right)])),80)
        # if not dropleft:
        #     last_left = leftpeak
        # if not dropright:
        #     last_right = rightpeak
        if dstcol is not None:
            cv2.line(dstcol, (last_center, int(yhgt * (i + 1))), (last_center, int(yhgt * i)), (255, 255, 255), thickness=1)
            cv2.line(dstcol, (int(left_init), int(yhgt * (i + 1))), (int(left_init), int(yhgt * i)), (0, 128, 128), thickness=1)
            cv2.line(dstcol, (int(right_init), int(yhgt * (i + 1))), (int(right_init), int(yhgt * i)), (0, 128, 128), thickness=1)
            # if not dropleft:
            #     cv2.line(dstcol, (leftpeak, int(yhgt * (i + 1))), (leftpeak, int(yhgt * i)), (0, 0, 255), thickness=1)
            #     cv2.line(frame, tuple(np.int32(pointhtrans((leftpeak, int(yhgt * (i + 1))),toroadinv))), tuple(np.int32(pointhtrans((leftpeak, int(yhgt * i)),toroadinv))), (0, 0, 255), thickness=2)
            # if not dropright:
            #     cv2.line(dstcol, (rightpeak, int(yhgt * (i + 1))), (rightpeak, int(yhgt * i)), (0, 255, 0), thickness=1)
            #     cv2.line(frame,  tuple(np.int32(pointhtrans((rightpeak, int(yhgt * (i + 1))),toroadinv))), tuple(np.int32(pointhtrans((rightpeak, int(yhgt * i)),toroadinv))), (0, 255, 0), thickness=2)
            cv2.imshow("road +",dstcol)
            # cv2.imshow("frame",frame)
            cv2.waitKey(60)
            print("wait")

    if len(xfit_left) > 0 and len(xfit_right) > 0:
        ysi = np.array(list(range(int(np.amin([np.amin(yfit_right),np.amin(yfit_left)])), int(np.amax([np.amax(yfit_left), np.amax(yfit_right)])))))
        left_dark = dst[(yfit_left, xfit_left)]
        darkmin = np.amin(left_dark)
        darkmax = np.amax(left_dark)
        if darkmax == darkmin:
            darkmin =0
        left_dark = (left_dark-darkmin)/(darkmax-darkmin)
        # left_dark = (left_dark)/(np.amax(left_dark))
        right_dark = dst[(yfit_right, xfit_right)]
        darkmin = np.amin(right_dark)
        darkmax = np.amax(right_dark)
        if darkmax == darkmin:
            darkmin =0
        right_dark = (right_dark-darkmin)/(darkmax-darkmin)
        # right_dark = (right_dark)/(np.amax(right_dark))
        lxs ,rxs = min_and_fit((half_foot_horz*15,half_foot_horz*27, half_foot_horz),xfit_left,yfit_left,left_dark,xfit_right,yfit_right,right_dark, dst ,ysi,0,maxy, dst_debug)

        for i in range(1, len(lxs)):
            if dstcol is not None:
                cv2.line(dstcol, (int(lxs[i]), ysi[i]), (int(lxs[i - 1]), ysi[i - 1]), (0, 128, 128), thickness=2)
            im1 = pointhtrans([lxs[i], ysi[i]], toroadinv)
            im2 = pointhtrans([lxs[i - 1], ysi[i - 1]], toroadinv)
            cv2.line(frame, tuple(np.int32(im1)), tuple(np.int32(im2)), (0, 128, 128), thickness=2)
            if dstcol is not None:
                cv2.line(dstcol, (int(rxs[i]), ysi[i]), (int(rxs[i - 1]), ysi[i - 1]), (128, 128, 0), thickness=2)
            im1 = pointhtrans([rxs[i], ysi[i]], toroadinv)
            im2 = pointhtrans([rxs[i - 1], ysi[i - 1]], toroadinv)
            cv2.line(frame, tuple(np.int32(im1)), tuple(np.int32(im2)), (128, 128, 0), thickness=2)

    # if len(xfit_left) > 0:
    #     left_dark = dst[(yfit_left,xfit_left)]
    #     # xfit_left = np.array(xfit_left,dtype=np.float32)
    #     # filtdark = ~np.logical_and(left_dark < np.percentile(left_dark,50),median_abs_dev(left_dark))
    #     # xfit_left = xfit_left[filtdark]
    #     # yfit_left = np.array(yfit_left)[filtdark]
    #     # left_dark = left_dark[filtdark]
    #     if len(xfit_left) > 0:
    #         # xfit_left[:,1] = xfit_left[:,1]/np.max(xfit_left[:,1])
    #         # poly = np.polyfit(x=yfit_left,y=xfit_left[:,0],w=xfit_left[:,1],deg=2)
    #         ysi = np.array(list(range(int(min(min(yfit_left),min(yfit_right))),int(max(max(yfit_left),max(yfit_right))))))
    #         # xs = np.polyval(poly,ysi)
    #         # xs = ransack_and_fit(SplEstimator(initval=left_init, anchors=np.array([[maxy,left_init],[maxy+five_feet_ahead, left_init]])), yfit_left, xfit_left, left_dark, ysi)
    #         xs = ransack_and_fit(CubicSplEstimator(initval=left_init, minmax_x=(min(yfit_left),max(yfit_left))), yfit_left, xfit_left, left_dark, ysi)
    #         for i in range(1,len(ysi)):
    #             cv2.line(dstcol, (int(xs[i]), ysi[i]), (int(xs[i-1]),ysi[i-1]), (0, 128, 128), thickness=2)
    #             im1 = pointhtrans([xs[i], ysi[i]],toroadinv)
    #             im2 = pointhtrans([xs[i-1], ysi[i-1]], toroadinv)
    #             cv2.line(frame,tuple(np.int32(im1)),tuple(np.int32(im2)),(0, 128, 128),thickness=2)
    # if len(xfit_right) > 0:
    #     right_dark = dst[(yfit_right, xfit_right)]
    #     # xfit_right = np.array(xfit_right,dtype=np.float32)
    #     # filtdark = ~np.logical_and(right_dark < np.percentile(right_dark,50),median_abs_dev(right_dark))
    #     # xfit_right = xfit_right[filtdark]
    #     # yfit_right = np.array(yfit_right)[filtdark]
    #     # right_dark = right_dark[filtdark]
    #     if len(xfit_right) > 0:
    #         # xfit_right[:, 1] = xfit_right[:, 1] / np.max(xfit_right[:, 1])
    #         # poly = np.polyfit(x=yfit_right,y=xfit_right[:,0],w=xfit_right[:,1],deg=2)
    #         ysi = np.array(list(range(int(min(min(yfit_right),min(yfit_left))), int(max(max(yfit_right),max(yfit_left))))))
    #         # xs = np.polyval(poly,ysi)
    #         # xs = ransack_and_fit(SplEstimator(initval=right_init, anchors=np.array([[maxy, right_init],[maxy+five_feet_ahead, right_init,]])), yfit_right, xfit_right, right_dark, ysi)
    #         xs = ransack_and_fit(CubicSplEstimator(initval=right_init, minmax_x=(min(yfit_right),max(yfit_right))), yfit_right, xfit_right, right_dark, ysi)
    #         for i in range(1,len(ysi)):
    #             cv2.line(dstcol, (int(xs[i]), ysi[i]), (int(xs[i-1]),ysi[i-1]), (128, 128, 0), thickness=2)
    #             im1 = pointhtrans([xs[i], ysi[i]],toroadinv)
    #             im2 = pointhtrans([xs[i-1], ysi[i-1]], toroadinv)
    #             cv2.line(frame,tuple(np.int32(im1)),tuple(np.int32(im2)),(128, 128, 0),thickness=2)
    if dstcol is not None:
        cv2.imshow("road +",dstcol)
        cv2.imshow("frame",frame)
        cv2.waitKey(60)
    if out is not None:
        out.write(frame)
    pass
        # if histos[i,leftpeak] > leftmean:
        # if histos[i, rightpeak] > rightmean:
        # zmap = dst_segd[i] < threshs[i]
        # dst_segd[i][zmap]=0
        # dst_segd[i][~zmap]=255
    # dst_segd = dst_segd.reshape((maxy,maxx))
    # th, dst2 = cv2.threshold(cmax[maxy-120:,:], 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    # _, dst2 = cv2.threshold(cmax[maxy-120:,:], th*1.5, 255, cv2.THRESH_BINARY)
    # canny = cv2.Canny(cmax, th / 3, th, apertureSize=3)
    # th, dst2 = cv2.threshold(dst[maxy-120:,:], 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    # _, dst2 = cv2.threshold(dst[maxy-120:,:], th*1.5, 255, cv2.THRESH_BINARY)
    # canny2 = cv2.Canny(cmax, th / 3, th, apertureSize=3)

    # ret, dst2 = cv2.threshold(dst,th,255,cv2.THRESH_BINARY)
    #ret, dst2 = cv2.threshold(dst, th, 255, cv2.THRESH_BINARY)
    # cv2.imshow("road",dst)
    # cv2.imshow("road +",dstcol)
    # cv2.imshow("cannymax",canny)
    # cv2.imshow("canny", canny2)
    # cv2.imshow("a road th",dst_segd)
    # cv2.imshow("frame",frame)
    # cv2.imshow("cmax",cmax)
    # cv2.waitKey(60)
    return None


def find_exact_divisor(numerator,init_div):
    div = float(int(numerator/(math.ceil(init_div/2.)*2)))
    num = float(int(numerator))
    while True:
        rem = num%div
        if rem == 0.0:
            break
        div +=1
    return int(div)

def read_safety_file(fn):
    """
    Read the safety file into an np table,   Skip the headder.
    :param fn: file name
    :return:
    """
    safetyinfo = np.genfromtxt(fn,delimiter=',',usecols=(0,7),skip_header=1)
    return safetyinfo

def ransack_and_fit(estimator, xs , ys, ws, xp):
    """
    Wrapper to handle RANSAC errors.
    :param estimator: Estimator Function.
    :param xs: Xs to fit
    :param ys: Ys to fit
    :param ws: Weights for each point.
    :param xp: Prediction X values to return
    :return: F(xp), MSE
    """
    min_samples = int(.9*len(xs))
    while True:
        ransac = RANSACRegressor(estimator ,min_samples=min_samples)
        try:
            ransac.fit(np.array(xs).reshape((-1, 1)), ys, ws)
        except FloatingPointError:
            # sample sizr too big error.
            min_samples = int(min_samples/2)
            continue
        except Exception as e:
            print(e)
            yp = np.empty(len(xp))
            yp.fill(np.mean(ys))
            return yp
        break
    ymin = np.min(ys)
    yp = ransac.predict(xp)
    # totalerror = 0
    # for i in range(0,len(xp)):
    #     totalerror += np.sum(np.abs(ys[xs==xp[i]] -yp[i])*ws[xs==xp[i]])
    return yp


def ransac_min_and_fit(lane_width_bounds, lxs , lys, lws, rxs, rys, rws, dst_image, debug_draw):

    class MinEstimator(BaseEstimator):
        def __init__(self, yn, ppinit, leftoffsetdep,leftoffsetind, dst_image, nknot=3):
            self.nknot = nknot
            self.ppinit = ppinit
            self.yn = yn
            self.leftoffsetdep = leftoffsetdep
            self.leftoffsetind = leftoffsetind
            self.bc_type = ((2, 0.0), (1, 0.0))
            self.cs = CubicSpline(self.yn, self.ppinit[1:], bc_type=self.bc_type)
            self.pp = ppinit
            maxdst = np.max(dst_image)
            self.dst_image = dst_image.astype(np.float32)/maxdst

        def cubicmin(self, p, y, x, ws, isleft):
            self.cs = CubicSpline(self.yn, p[1:], bc_type=((2, 0.0), (1, 0.0)))
            xreal = np.subtract(x,(isleft*self.leftoffsetind))
            yreal = np.subtract(y,(isleft*(self.leftoffsetdep + p[0])))
            lxd = self.cs(xreal) - (yreal)
            if ws is None:
                ws = self.dst_image[(xreal.astype(np.int32),yreal.astype(np.int32))]
                return np.sum(lxd * lxd * ws)
            return np.sum(lxd * lxd * ws)

        def fit(self, x, y, sample_weight=None):
            """
            Fit Spline to the data.
            :param x: X values
            :param y: Corresponding Ys
            :param sample_weight: Coresponding Weights.
            :return:
            """
            xe = None
            ye = None
            ws = None

            try:
                xe = x.flatten()
                isleft = y >= self.leftoffsetdep
                minres = minimize(self.cubicmin, x0=self.pp, args=(y, xe, sample_weight, isleft), method='Nelder-Mead',
                                  options={'xatol': 0.1})
                self.pp = minres.x
            except Exception as e:
                print(e)
                raise e
            return None

        def score(self, x, y):
            """
            Return the mean squared error.
            :param x: X values
            :param y: Coresponding Ys
            :return: Mean Squared Errror
            """
            isleft = y >= self.leftoffsetdep
            return self.cubicmin(self.pp,x.flatten(),y,None,isleft)

        def predict(self, x):
            """
            Predict the Ys given X
            :param x: X Values
            :return: f(X) returned
            """
            xe = x.flatten()
            isleft = xe >= self.leftoffsetind
            xe = xe-(isleft*self.leftoffsetind)
            return self.cs(xe) +(isleft*(self.leftoffsetdep+self.pp[0]))

    # Need to fake out ransac.  Translate the left and include both in
    # one list.
    leftoffsetdep = 2.0* np.amax(rxs)
    leftoffsetind = 2.0* np.amax(rys)
    xsransac = np.concatenate((rxs,(lxs+leftoffsetdep)))
    ysransac = np.concatenate((rys,(lys+leftoffsetind)))
    wsransac = np.concatenate((rws,lws))
    ynmax = np.amax([np.amax(lys),np.amax(rys)])
    ynmin = np.amin([np.amin(lys),np.amin(rys)])
    nknot = 1
    yn = np.concatenate((np.arange(ynmin, ynmax, ynmax / (nknot )),[ynmax]))
    lane_width = (lane_width_bounds[0]+lane_width_bounds[1])/2.0
    ppinit = np.concatenate(([-lane_width],np.full(len(yn),np.mean(rxs))))
    min_samples = int(min(len(rxs),len(lxs)))
    while True:
        ransac = RANSACRegressor(MinEstimator(yn, ppinit, leftoffsetdep,leftoffsetind, dst_image, nknot), min_samples=min_samples)
        try:
            ransac.fit(np.array(ysransac).reshape((-1, 1)), xsransac, wsransac)
        except FloatingPointError:
            # sample sizr too big error.
            min_samples = int(min_samples/2)
            continue
        except Exception as e:
            print(e)
            return None, None, None
        break
    ransac.estimator_.pp
    csright = CubicSpline(yn, ransac.estimator_.pp[1:], bc_type=((2, 0.0), (1, 0.0)))
    csleft = CubicSpline(yn, ransac.estimator_.pp[1:]+ransac.estimator_.pp[0], bc_type=((2, 0.0), (1, 0.0)))
    return csleft, csright, ransac.estimator_.pp[0]


def min_fit_cs(lane_width, lxs , lys, lws, rxs, rys, rws, yknots= None):
    # define the function.
    def cubicmin(p, yn, lxs, lys, lws, rxs, rys, rws):
        # cs = CubicSpline(yn, p[1:], bc_type=((2, 0.0), (1, 0.0)))
        cs = QuadInterp(yn, p[1:])
        lxd = cs(lys)-(lxs+p[0])
        rxd = cs(rys)-rxs
        return np.sum(lxd*lxd*lws)+np.sum(rxd*rxd*rws)

    # create the starting guess w plus cubic spline.
    nknot = 1
    if yknots is None:
        ynmax = np.amax([np.amax(lys),np.amax(rys)])
        ynmin = np.amin([np.amin(lys),np.amin(rys)])
    else:
        ynmin = yknots[0]
        ynmax = yknots[1]
    yn = np.concatenate((np.arange(ynmin, ynmax, ynmax / (nknot )),[ynmax]))
    pp = np.concatenate(([lane_width],np.full(len(yn),np.mean(rxs))))

    minres = minimize(cubicmin, x0=pp, args=(yn, lxs , lys, lws, rxs, rys, rws), method='Nelder-Mead', options={'fatol':0.001})

    # csright = CubicSpline(yn, minres.x[1:], bc_type=((2, 0.0), (1, 0.0)))
    # csleft = CubicSpline(yn, minres.x[1:]-minres.x[0], bc_type=((2, 0.0), (1, 0.0)))
    csright = QuadInterp(yn, minres.x[1:])
    csleft = QuadInterp(yn, minres.x[1:]-minres.x[0])
    return csleft, csright, minres.x[0]


def min_fit_one_cs(xs , ys, ws, yknots= None):
    # define the function.
    def cubicmin(p, yn, xs, ys, ws):
        # cs = CubicSpline(yn, p[1:], bc_type=((2, 0.0), (1, 0.0)))
        cs = QuadInterp(yn, p)
        xd = cs(ys)-xs
        return np.sum(xd*xd*ws)

    # create the starting guess w plus cubic spline.
    nknot = 1
    if yknots is None:
        ynmax = np.amax(ys)
        ynmin = np.amin(ys)
    else:
        ynmin = yknots[0]
        ynmax = yknots[1]
    yn = np.concatenate((np.arange(ynmin, ynmax, ynmax / (nknot )),[ynmax]))
    pp = np.full(len(yn),np.mean(xs))

    minres = minimize(cubicmin, x0=pp, args=(yn, xs, ys, ws), method='Nelder-Mead', options={'fatol':0.001})

    # csright = CubicSpline(yn, minres.x[1:], bc_type=((2, 0.0), (1, 0.0)))
    # csleft = CubicSpline(yn, minres.x[1:]-minres.x[0], bc_type=((2, 0.0), (1, 0.0)))
    cs = QuadInterp(yn, minres.x)
    return cs


def optimize_min_fit_cs(lane_width_bounds, lxs, lys, lws, rxs, rys, rws, debug_draw):
    """
    Optimize the lane fit.
    :param lane_width:
    :param lxs:
    :param lys:
    :param lws:
    :param rxs:
    :param rys:
    :param rws:
    :return:
    """
    # We iterate over the input finding the best data.  The filtering is done
    # from two aspects.  The weights are an indication of the brightness.  So
    # removing outliers on the dark side is good.  The other is to remove outliers
    # in distance from the fit.  Then iterate.  When the change in the result is small
    # then we are done.
    lane_width = (lane_width_bounds[0]+lane_width_bounds[1])/2
    lxs_r = np.array(lxs)
    lys_r = np.array(lys)
    lws_r = np.array(lws)
    rxs_r = np.array(rxs)
    rys_r = np.array(rys)
    rws_r = np.array(rws)
    # first remove outlier brightness on the low end.
    lblow = np.percentile(lws_r,10)
    lbout = ~np.bitwise_and(median_abs_dev(lws_r), lws_r < lblow)
    if 0 < np.sum(lbout) <len(lxs_r):
        lxs_r = lxs_r[lbout]
        lys_r = lys_r[lbout]
        lws_r = lws_r[lbout]
    rblow = np.percentile(rws_r,10)
    rbout = ~np.bitwise_and(median_abs_dev(rws_r), rws_r < rblow)
    if 0 < np.sum(rbout) <len(rxs_r):
        rxs_r = rxs_r[rbout]
        rys_r = rys_r[rbout]
        rws_r = rws_r[rbout]

    # now iterate removing out liers by distance.
    csleft = csright = None
    last_left = last_right = None
    tol = lane_width_bounds[2]/2.0
    while True:
        if len(lys_r) == 0 or len(rys_r) == 0:
            csleft = csright = None
            break

        if debug_draw is not None:
            debug = debug_draw.copy()
            for i in range(0,len(lxs_r)):
                cv2.circle(debug,(int(lxs_r[i]), int(lys_r[i])),1,(0,0,255),-1)
            for i in range(0,len(rxs_r)):
                cv2.circle(debug, (int(rxs_r[i]), int(rys_r[i])),1, (0, 255, 0), -1)
            cv2.imshow("debugmin",debug)
            cv2.waitKey(60)
            print("debugmin")
        csleft, csright, lane_width = min_fit_cs(lane_width,lxs_r,lys_r,lws_r,rxs_r,rys_r,rws_r)
        if lane_width < lane_width_bounds[0] or lane_width > lane_width_bounds[1]:
            # too wide or narrow.  Figure out which side has the largest variance.
            leftdiff = csright(lys_r)-lxs_r
            rightdiff = np.abs(rxs_r -csleft(rys_r))
            lvar = np.var(leftdiff)
            rvar = np.var(rightdiff)
            if lvar > tol or rvar > tol:
                last_left = csleft(csleft.x)
                last_right = csright(csright.x)
                if lane_width > lane_width_bounds[1]:
                    if lvar > rvar:
                        lin = leftdiff < np.percentile(leftdiff,90)
                        lxs_r = lxs_r[lin]
                        lys_r = lys_r[lin]
                        lws_r = lws_r[lin]
                        continue
                    else:
                        rin = rightdiff < np.percentile(rightdiff, 90)
                        rxs_r = rxs_r[rin]
                        rys_r = rys_r[rin]
                        rws_r = rws_r[rin]
                        continue
                else:
                    if lvar > rvar:
                        lin = np.bitwise_and(median_abs_dev(leftdiff),leftdiff < np.percentile(leftdiff,10))
                        lxs_r = lxs_r[lin]
                        lys_r = lys_r[lin]
                        lws_r = lws_r[lin]
                        continue
                    else:
                        rin = np.bitwise_and(median_abs_dev(rightdiff), rightdiff < np.percentile(rightdiff, 10))
                        rxs_r = rxs_r[rin]
                        rys_r = rys_r[rin]
                        rws_r = rws_r[rin]
                        continue

        this_left = csleft(csleft.x)
        this_right = csright(csright.x)
        if last_left is not None:
            if np.sum(np.concatenate((np.abs(last_left-this_left),np.abs(last_right-this_right))))/(2*len(last_left)) < tol:
                break
        lin = ~median_abs_dev(((csleft(lys_r)-lxs_r)**2)*lws_r)
        rin = ~median_abs_dev(((csright(rys_r)-rxs_r)**2)*rws_r)
        # if we have no more in liers for either then we are done.
        lsumin = np.sum(lin)
        rsumin = np.sum(rin)
        if lsumin == 0 or rsumin == 0:
            csleft = csright = None
            break
        if lsumin == len(lin) and rsumin == len(rin):
            break
        lxs_r = lxs_r[lin]
        lys_r = lys_r[lin]
        lws_r = lws_r[lin]
        rxs_r = rxs_r[rin]
        rys_r = rys_r[rin]
        rws_r = rws_r[rin]
        last_left = this_left
        last_right = this_right

    # done.
    return csleft, csright, lane_width

def  min_and_fit(lane_width_bounds, lxs , lys, lws, rxs, rys, rws, dst_image, ryp, ymin, ymax, debug_draw):
    # csleft, csright, lwidth = ransac_min_and_fit(lane_width_bounds, lxs , lys, lws, rxs, rys, rws, dst_image, debug_draw)
    csleft, csright, lwidth, err = optimize_min_fit_cs_bitmap(lane_width_bounds, lxs , lys, lws, rxs, rys, rws, ymin, ymax, debug_draw)
    # if csleft is not None and csright is not None and err < (lane_width_bounds[2]**2):
    lxp = rxp = np.array([])
    if csleft is not None and csright is not None:
        if csleft.vertical_offset() < lwidth*0.25:
            rxp = csright(ryp)
            lxp = csleft(ryp)

    return lxp,rxp

def filter_inlier_brightness(weights):
    return weights > 0.5
    # stdw = np.std(weights)
    # if stdw == 0.0:
    #     bins = 4
    # else:
    #     bins = int(1.0 / np.std(weights))
    # if bins < 4:
    #     bins=4
    # hist, bin_edges = np.histogram(weights, bins)
    # hsrt = np.argsort(-hist)
    # if abs(hsrt[0]-hsrt[1]) > 1:
    #     if bin_edges[hsrt[0]+1] > bin_edges[hsrt[1]+1]:
    #         return weights > bin_edges[hsrt[1] + 1]
    #     return weights > bin_edges[hsrt[0]+1]
    # else:
    #     return np.full(len(weights),True)


def optimize_min_fit_cs_bitmap(lane_width_bounds, lxs, lys, lws, rxs, rys, rws, ymin, ymax, debug_draw):
    """
    Optimize the lane fit.
    :param lane_width:
    :param lxs:
    :param lys:
    :param lws:
    :param rxs:
    :param rys:
    :param rws:
    :return:
    """
    # We iterate over the input finding the best data.  The filtering is done
    # from two aspects.  The weights are an indication of the brightness.  So
    # removing outliers on the dark side is good.  The other is to remove outliers
    # in distance from the fit.  Then iterate.  When the change in the result is small
    # then we are done.
    lane_width = (lane_width_bounds[0]+lane_width_bounds[1])/2
    lxs_r = np.array(lxs)
    lys_r = np.array(lys)
    lws_r = np.array(lws)
    rxs_r = np.array(rxs)
    rys_r = np.array(rys)
    rws_r = np.array(rws)
    # left_good = np.full(len(lxs_r), True)
    # right_good = np.full(len(rxs_r), True)
    # first remove outlier brightness on the low end.
    left_good = filter_inlier_brightness(lws_r)
    right_good = filter_inlier_brightness(rws_r)
    # lblow = np.percentile(lws_r,10)
    # lbout = ~np.bitwise_and(median_abs_dev(lws_r), lws_r < lblow)
    # if 0 < np.sum(lbout) <len(lxs_r):
    #     left_good = np.bitwise_and(left_good,lbout)
    # rblow = np.percentile(rws_r,10)
    # rbout = ~np.bitwise_and(median_abs_dev(rws_r), rws_r < rblow)
    # if 0 < np.sum(rbout) <len(rxs_r):
    #     right_good = np.bitwise_and(right_good,rbout)

    # now iterate removing out liers by distance.
    csleft = csright = None
    last_left = last_right = None
    tol = lane_width_bounds[2]/2.0
    fulliters = 0
    while True:
        if np.sum(left_good) == 0 or np.sum(right_good) == 0:
            csleft = csright = None
            break

        if debug_draw is not None:
            debug = debug_draw.copy()
            for i in range(0,len(lxs_r)):
                if left_good[i]:
                    cv2.circle(debug,(int(lxs_r[i]), int(lys_r[i])),1,(0,0,255),-1)
            for i in range(0,len(rxs_r)):
                if right_good[i]:
                    cv2.circle(debug, (int(rxs_r[i]), int(rys_r[i])),1, (0, 255, 0), -1)
            cv2.imshow("debugmin",debug)
            cv2.waitKey(60)
            pass
        csleft, csright, lane_width = min_fit_cs(lane_width,lxs_r[left_good],lys_r[left_good],lws_r[left_good],rxs_r[right_good],rys_r[right_good],rws_r[right_good], yknots=[ymin,ymax])
        if lane_width < lane_width_bounds[0] or lane_width > lane_width_bounds[1]:
            # too wide or narrow.  Figure out which side has the largest variance.
            # refit with just the left and right data.  Avoids sways.
            csleft = min_fit_one_cs(lxs_r[left_good],lys_r[left_good],lws_r[left_good], yknots=[ymin,ymax])
            csright = min_fit_one_cs(rxs_r[right_good],rys_r[right_good],rws_r[right_good], yknots=[ymin,ymax])
            leftdiff = np.abs(csright(lys_r)-lxs_r)
            rightdiff = np.abs(rxs_r -csleft(rys_r))
            lvar = np.var(leftdiff[left_good])
            rvar = np.var(rightdiff[right_good])
            if lvar > tol or rvar > tol:
                last_left = csleft(csleft.x)
                last_right = csright(csright.x)
                if lane_width > lane_width_bounds[1]:
                    if lvar > rvar:
                        #left_good = np.bitwise_and(left_good,leftdiff < np.percentile(leftdiff[left_good],90))
                        left_good = np.bitwise_and(left_good, leftdiff < np.mean(leftdiff[left_good]))
                        continue
                    else:
                        #right_good = np.bitwise_and(right_good,rightdiff < np.percentile(rightdiff[right_good], 90))
                        right_good = np.bitwise_and(right_good, rightdiff < np.mean(rightdiff[right_good]))
                        continue
                else:
                    if lvar > rvar:
                        left_good = np.bitwise_and(left_good, leftdiff > np.mean(leftdiff[left_good]))
                        # left_good = np.bitwise_and(left_good, leftdiff < np.percentile(leftdiff[left_good], 10))
                        continue
                    else:
                        # right_good = np.bitwise_and(right_good, rightdiff < np.percentile(rightdiff[right_good], 10))
                        right_good = np.bitwise_and(right_good, rightdiff > np.mean(rightdiff[right_good]))
                        continue

        this_left = csleft(csleft.x)
        this_right = csright(csright.x)
        if last_left is not None:
            fulliters += 1
            if np.sum(np.concatenate((np.abs(last_left-this_left),np.abs(last_right-this_right))))/(2*len(last_left)) < tol:
                break
        oldlsumin = np.sum(left_good)
        oldrsumin = np.sum(right_good)
        l_errs = ((csleft(lys_r)-lxs_r)**2)*lws_r
        tmad = MedianAbsDev(l_errs[left_good])
        left_good = np.bitwise_and(left_good, ~tmad.are_outlier(l_errs))
        left_good = np.bitwise_or(left_good,l_errs < np.percentile(l_errs,10))

        r_errs = ((csright(rys_r)-rxs_r)**2)*rws_r
        tmad = MedianAbsDev(r_errs[right_good])
        right_good = np.bitwise_and(right_good, ~tmad.are_outlier(r_errs))
        right_good = np.bitwise_or(right_good, r_errs < np.percentile(r_errs, 10))

        # if we have no more in liers for either then we are done.
        lsumin = np.sum(left_good)
        rsumin = np.sum(right_good)
        if lsumin == 0 or rsumin == 0:
            csleft = csright = None
            break
        if lsumin == oldlsumin and rsumin == oldrsumin:
            break
        last_left = this_left
        last_right = this_right
        if fulliters > 10:
            csleft = csright = None
            break
    # done. calculate the mean squared error.
    mse = sys.float_info.max
    if csright is not None and csleft is not None:
        mse = np.amax((np.mean(((csleft(lys_r[left_good])-lxs_r[left_good])**2)*lws_r[left_good]),np.mean(((csright(rys_r[right_good])-rxs_r[right_good])**2)*rws_r[right_good])))
    return csleft, csright, lane_width, mse


# def mcnn_frame(frame,toroad, maxx,maxy):
#     rect = cv2.warpPerspective(frame,toroad,(maxx,maxy))
#     res = model_model.detect([rect], verbose=0)
#     ids = np.searchsorted([7],res[0]['class_ids'])
#     rois = res[0]['rois']
#     for i in range(0,len(res[0]['class_ids'])):
#         if res[0]['class_ids'][i] == 7:
#             rec= rois[i]
#             cv2.line(rect,(rec[1],rec[0]),(rec[3],rec[2]),(0,0,255),thickness=2)
#
#     cv2.imshow("mrcnn",rect)
#     cv2.waitKey(60)

if __name__ == "__main__":

    # parse the arguments.
    parse = argparse.ArgumentParser()
    parse.add_argument("--vd", help="Directory containing directories of video files and speed data")
    parse.add_argument("--safety",help="Directory of safety files")
    parse.add_argument("--clip",help='Clip Overhead View')
    parse.add_argument("--dist",help='Overhead distance to display')
    parse.add_argument("--guide",help='Show Guides')
    parse.add_argument("--stats",help='Show Statistics.')
    parse.add_argument("--fps", help="Frames per second output.")
    parse.add_argument("--polys", help="Create polygon files")
    parse.add_argument("--close", help="close following seconds")
    parse.add_argument("--debug", help="Debug")

    args = parse.parse_args()

    safety_file_dir = args.vd if args.safety is None else args.safety
    do_clip = False if args.clip is None else True if args.clip == "True" else False
    dist = 180 if args.dist is None else int(args.dist)
    guide = False if args.guide is None else True if args.guide == "True" else False
    stats = False if args.stats is None else True if args.stats == "True" else False
    fps = 4.0 if args.fps is None else float(args.fps)
    polys = False if args.polys is None else True if args.polys == "True" else False
    debug = False if args.debug is None else True if args.debug == "True" else False
    # close_follow = 1 if args.close is None else int(args.close)

    # model_model = None
    # if args.close is not None:
    #     from hood_detect import rcnn_model_wrap
    #     model_model = rcnn_model_wrap.load_model()

    if args.vd and os.path.isdir(args.vd):
        basevd = args.vd
        onlydirs = [f for f in os.listdir(basevd) if os.path.isdir(os.path.join(basevd, f))]
        if len(onlydirs) == 0:
            onlydirs = [os.path.basename(basevd)]
            basevd = os.path.dirname(basevd)
        outbasedir = basevd+"_output"
        overheaddir = os.path.join(outbasedir,"overhead")
        lanedir = os.path.join(outbasedir,"lanes")
        if not os.path.exists(overheaddir):
             os.mkdir(overheaddir)
        if not os.path.exists(lanedir):
             os.mkdir(lanedir)
        if polys:
            polysdir = os.path.join(outbasedir,"polys")
            if not os.path.exists(polysdir):
                os.mkdir(polysdir)
        else:
            polysdir = None

        jsontrans = os.path.join(outbasedir,"jsontrans")

        dirsidx = 0
        while dirsidx < len(onlydirs):
            d = onlydirs[dirsidx]
            dirsidx +=1
            if os.path.exists(os.path.join(lanedir, d + ".mp4")):
                continue
            vid_file_dir = os.path.join(basevd,d)
            transfile = os.path.join(jsontrans,d+".json")
            if not os.path.exists(transfile):
                continue
            pertrans = PerspectiveTransInfo()
            pertrans.fromJson(transfile)
            if pertrans.status_message != 'SUCCESS' or pertrans.error_feet >= 2.0:
                continue
            onlyfile = [f for f in os.listdir(vid_file_dir) if os.path.isfile(os.path.join(vid_file_dir, f)) and f.endswith(".mp4")]
            if len(onlyfile) == 0:
                continue
            vcap = SdeVideo(os.path.join(vid_file_dir,onlyfile[0]))
            fourcc = cv2.VideoWriter_fourcc(*'MP4V')
            out = cv2.VideoWriter(os.path.join(lanedir, d + ".mp4"), fourcc, fps, vcap.get_shape())
            vcap.release()
            flidx = 0
            while flidx < len(onlyfile):
                fl = onlyfile[flidx]
                flidx +=1
                drive_lane(os.path.join(vid_file_dir,fl),os.path.join(safety_file_dir,fl.replace(".mp4","_safety.csv")) ,pertrans,out=out,debug=debug)
            out.release()
            gc.collect()