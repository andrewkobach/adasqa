import numpy as np
from scipy.optimize import minimize
from cv_utils.MedianAbsDev import MedianAbsDev


"""
This function is used to find the alpha value in the affine mapping.  Segments
thought to be parallel pointing to the vanishing line are passed in.  In addition
the projective part of the transforr is passed in.
The finction finds the alpha that minimizes the sum of the weighted squared slopes.
For lines pointing to the vanishing line the slope should be zero when transformed.  

"""


def affine_alpha_min(segs, ppt, alpha=2.0):
    """
    Find the alpha that minimizes the sum of the weighted squared slopes.
    :param segs: list of segments in 4 tuple form.
    :param ppt: the projective transform.
    :param alpha: initial guess at alpha
    :return: the best alpha
    """
    # First transform all the segments by ppt.  To do this we need to
    # turn them into homogeneous points.  Each segment has a start and end.
    # then transpose so in the loop we can quickly do the dot.
    transseg = np.dot(ppt, (np.concatenate((np.array(segs).reshape((-1,2)),np.full((len(segs)*2,1),1)),axis=1)).transpose())
    lenweight = np.abs(segs[:, 1] - segs[:, 3])

    def minfun(a,tpts,weight):
        # form the affine alpha only treaform
        # transform all the points by this.
        npts = np.dot(np.array([[1.0,-a,0.0],[0.0,1.0,0.0],[0.0,0.0,1.0]]),tpts).transpose().reshape((-1, 6))
        zdivs = np.logical_or(npts[:,2] == 0,npts[:,5] == 0)
        if np.sum(zdivs) > 0:
            npts = npts[~zdivs,:]
            ms = my_b_ln_v(npts)
            return np.sum(ms * ms * weight[~zdivs])
        #
        # now find the weighted sum of squares of the
        # slopes of the tranformed segments.
        ms = my_b_ln_v(npts)
        return np.sum(ms*ms*weight)

    # initial guess at alpha. and then minimize.
    minres = minimize(minfun, alpha, args=(transseg,lenweight), method='Nelder-Mead',options={'xatol':0.000001})
    # here is the minimized alpha.
    return minres.x[0], ((minres.final_simplex[1][0]+minres.final_simplex[1][1])/2.0)/float(len(segs))


def my_b_ln_v(ph):
    """
    Calculate the slope of the line in the y direction.
    :param ph: First points of segment followed by the end points in homogenious coordinates.
    :return: slope, any that failed are returnd as 0 so they don't include in the error.
    """
    p1x = ph[:,0] / ph[:,2]
    p2x = ph[:,3] / ph[:,5]
    md = (ph[:,1] / ph[:,2])-(ph[:,4] / ph[:,5])
    mdzero = md == 0.0
    p1x[mdzero] = 0.0
    p2x[mdzero] = 0.0
    md[mdzero] = 1.0
    m= (p1x-p2x)/md
    return m


def get_affine_mad(segs,afpp):
    zdiv = filter_zero_devisors(segs,afpp)
    nsegs = segs[~zdiv]
    transseg = np.dot(afpp, (np.concatenate((np.array(nsegs).reshape((-1,2)),np.full((len(nsegs)*2,1),1)),axis=1)).transpose()).transpose().reshape((-1, 6))
    ms = my_b_ln_v(transseg)
    return MedianAbsDev(ms)


def filter_zero_devisors(segs,afpp):
    ph = np.dot(afpp, (np.concatenate((np.array(segs).reshape((-1,2)),np.full((len(segs)*2,1),1)),axis=1)).transpose()).transpose().reshape((-1, 6))
    zdivs = np.logical_or(ph[:, 2] == 0, ph[:, 5] == 0)
    ph[zdivs,2] = 1.0
    ph[zdivs,5] = 1.0
    md = (ph[:, 1] / ph[:, 2]) - (ph[:, 4] / ph[:, 5])
    return np.logical_or(md == 0.0,zdivs)


def filter_on_mad(segs,afpp,mad):
    ph = np.dot(afpp, (np.concatenate((np.array(segs).reshape((-1,2)),np.full((len(segs)*2,1),1)),axis=1)).transpose()).transpose().reshape((-1, 6))
    zdivs = np.logical_or(ph[:, 2] == 0, ph[:, 5] == 0)
    ph[zdivs,2] = 1.0
    ph[zdivs,5] = 1.0
    ms = my_b_ln_v(ph)
    return np.logical_or(mad.are_outlier(ms),zdivs)
