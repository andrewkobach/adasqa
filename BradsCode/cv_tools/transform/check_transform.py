import argparse
import os
import sys
import copy
import numpy as np
import cv2
import math
from transform.find_transform import get_vid_lanes
from cv_utils.MedianAbsDev import median_abs_dev
from transform.PerspectiveTransInfo import pointhtrans, PerspectiveTransInfo
from transform.affine_alpha_min import affine_alpha_min, get_affine_mad, filter_zero_devisors, filter_on_mad
from hood_detect import hood_detect
import cProfile

def check_transform_change(dirname, vid_files, old_pti, feet_ahead=20, debug=False):
    """
    Calculate the number of feet difference between the current transform and
    what these video files predict 10 feet out.
    :param dirname: Direcotry of video files
    :param vid_files: List of video files.
    :param pti: Existing transform
    :return: -1 if we could not determine the difference other wise the difference in feet 10 feet out.
    """
    laneframes= []
    vrad = int(min(old_pti.f_width,old_pti.f_height)/10)
    vanpt = np.int32(old_pti.van_pt)
    for vf in vid_files:
        lanefs = get_vid_lanes(dirname,vf,vanpt,vrad, old_pti.f_hood_scanline,debug)
        laneframes.extend(lanefs)

    # Start the perspective transform
    pti = copy.deepcopy(old_pti)
    pti.lrn_tot_frame = len(laneframes)
    pti.lrn_vid_names = np.array(vid_files).tolist()

    # Finally get the final vanishing point as the mean of all the
    # frames vanishing points.
    for lanf in laneframes:
        lanf.set_vanpt(vanpt, vrad)
        lanf.order_into_seg_groups_mean_shift_to_centroid()

    for iter in range(0,2):
        vanpt_list = []
        for lanf in laneframes:
            vpts = lanf.get_vanishing_pts()
            vanpt_list.extend(vpts)
        if len(vanpt_list) < 5:
            return -1

        # eliminate outliers in the y direction.  Then take the mean vanishing point.
        vanpt_list = np.array(vanpt_list)
        vanpt_list_in = vanpt_list[~median_abs_dev(vanpt_list[:,1])]
        pti.set_vanpt(np.mean(vanpt_list_in,axis=0).tolist())
        pti.lrn_vanpt_hist = [len(vanpt_list_in),len(vanpt_list)-len(vanpt_list_in)]
        #get a new radius that elimnates 5% outliers. (2 times the STD)
        newvrad = np.max(np.std(vanpt_list_in,axis=0)*2)
        # set this in the lane frames and then filter.
        if iter == 0:
            for lanf in laneframes:
                lanf.set_vanpt(pti.van_pt,newvrad)
                lanf.filter_to_vanishing_pt()
        # Now get the afine parameters.
        # First get all the segments from our detected lanes.
        ppt = pti.getPureProjective()
        afinelist = []
        for lanf in laneframes:
            afinelist.extend(lanf.get_affine_segments_to_minimize())
        if len(afinelist) < 5:
            return -1

        #Now find the affine alpha that tries to make all the lanes parallel.
        pti.t_alpha,pti.lrn_alpha_err = affine_alpha_min(np.array(afinelist),ppt)
        if iter >=1:
            break
        pti.t_beta_inv = 1.0
        # reorder into seg groups using this affine mapping.
        # makes it easier to find the lanes.
        afpp = pti.getAfine_PureProjective()
        # Now filter on the affine.
        afine_mad = get_affine_mad(np.array(afinelist),afpp)
        for lanf in laneframes:
            lanf.set_vanpt(pti.van_pt, newvrad)
            lanf.filter_affine(afine_mad,afpp)
            lanf.order_into_seg_groups_partial_affine(afpp)

    # just temporary set the beta to 1.
    pti.t_beta_inv = old_pti.t_beta_inv
    pti.t_scale = old_pti.t_scale

    # now calculate the difference 10 feet out from the bottom of the image.
    t_new = pti.getInvPerTrans()
    t_old = old_pti.getInvPerTrans()
    retinv, t_old_invert = cv2.invert(t_old, flags=cv2.DECOMP_LU)
    ptcentbot = pointhtrans([old_pti.f_width / 2, old_pti.f_height], t_old)
    ptcentbot[1] -= feet_ahead
    ptcentbot[0] -= feet_ahead
    ptimage = pointhtrans(ptcentbot,t_old_invert)
    pt_new = pointhtrans(ptimage,t_new)
    diff = math.sqrt((pt_new[1]-ptcentbot[1])**2 + (pt_new[0]-ptcentbot[0])**2)
    return diff


if __name__ == "__main__":
    # parse the arguments.
    parse = argparse.ArgumentParser()
    parse.add_argument("--vd", help="Video directorys")
    parse.add_argument("--tweak",help='Amount to tweak the transform by.')
    args = parse.parse_args()

    tweak = float(args.tweak)

    if args.vd and os.path.isdir(args.vd):
        # Setup the output paths.
        onlydirsall = [f for f in os.listdir(args.vd) if os.path.isdir(os.path.join(args.vd, f))]
        if len(onlydirsall) == 0:
            onlydirnames = [os.path.basename(args.vd)]
            outdir = os.path.dirname(args.vd) + "_output"
            onlydirs =[args.vd]
            onlydirsfiles=[[f for f in os.listdir(args.vd) if os.path.isfile(os.path.join(args.vd, f)) and f.endswith("mp4")]]
        else:
            outdir = args.vd+"_output"
            onlydirs = []
            onlydirnames = []
            onlydirsfiles = []
            for sdir in onlydirsall:
                sdirpath = os.path.join(args.vd, sdir)
                onlyfiles = [f for f in os.listdir(sdirpath) if os.path.isfile(os.path.join(sdirpath, f)) and f.endswith("mp4")]
                if len(onlyfiles) > 0:
                    onlydirnames.append(sdir)
                    onlydirs.append(sdirpath)
                    onlydirsfiles.append(onlyfiles)
        json_path = os.path.join(outdir, "jsontrans")
        if not os.path.exists(outdir):
            sys.exit("No jsontrans dir")
        if not os.path.exists(json_path):
            sys.exit("No jsontrans dir")
        # we are looking for a directory that contains directories of video files.
        # now see if each dir contains video files.

        for i in range(0,len(onlydirs)):
            jsonfile = os.path.join(json_path, onlydirnames[i] + ".json")
            if not os.path.exists(jsonfile):
                continue

            old_trans = PerspectiveTransInfo()
            old_trans.fromJson(jsonfile)
            if not hasattr(old_trans,'f_hood_scanline'):
                hdetect = hood_detect.HoodDetect([old_trans.f_height,old_trans.f_width])
                hood_scanline = hdetect.detect_hood_from_files(onlydirs[i],onlydirsfiles[i])
                if hood_scanline < old_trans.van_pt[1]:
                    hood_scanline = old_trans.f_height
                old_trans.f_hood_scanline = hood_scanline

            # pr = cProfile.Profile()
            # pr.enable()
            old_trans.van_pt[1] += tweak

            halflen = int(len(onlydirsfiles[i])/2)
            firsthalf = onlydirsfiles[i][:halflen]
            secondhalf = onlydirsfiles[i][halflen:]
            if len(firsthalf) > 0:
                pr = cProfile.Profile()
                pr.enable()
                dist_changed1 = check_transform_change(onlydirs[i], firsthalf, old_trans,feet_ahead=20, debug=False)
                pr.disable()
                print("The distance changed first half ",dist_changed1, " from a tweak ",tweak)
                pr.print_stats(sort='cumtime')
            if len(secondhalf) > 0:
                pr = cProfile.Profile()
                pr.enable()
                dist_changed2 = check_transform_change(onlydirs[i], secondhalf, old_trans,feet_ahead=20, debug=False)
                pr.disable()
                print("The distance changed second half ",dist_changed2, " from a tweak ",tweak)
                pr.print_stats(sort='cumtime')

            print("Avg ",(dist_changed1+dist_changed2)/2.0)

            # pr.disable()
            # pr.print_stats(sort='cumtime')

