import cv2
import numpy as np
from cv_utils import intersections
from sklearn.cluster import mean_shift, estimate_bandwidth
import itertools
from . import PerspectiveTransInfo
from cv_utils.MedianAbsDev import median_abs_dev
from . import affine_alpha_min

maxangleline = 100.0*np.pi/180.0
minangleline = 80*np.pi/180.0


class LaneFrame:
    """
    Class to hold what we think are lane lines from a video frame.
    This class also holds methonds that operaate on the lanes.
    """
    def __init__(self,frameidx,frame):
        self.frameidx = frameidx
        self.all_lines = []
        self.dropped_seg = []
        self.left_seg_groups = []
        self.right_seg_groups = []
        self.dash_candidates = []
        self.width_candidates = []
        self.dashscale_candidates = []
        self.vanpt = None
        self.frame = frame.copy()
        self.maxx = frame.shape[1]
        self.maxy = frame.shape[0]
        self.debug = False
        self.vrad = 0

    def set_all_lines(self,lines):
        """
        Set all the lines found.
        :param lines:
        :return:
        """
        self.all_lines = lines
        if self.debug:
            dframe = self.frame.copy()
            for s in self.all_lines:
                cv2.line(dframe, (int(s[0]), int(s[1])), (int(s[2]), int(s[3])), (0, 0, 255), 2)
            cv2.imshow("all_lines", dframe)
            cv2.waitKey(60)

    def set_vanpt(self,vp,vr):
        """
        Set the vanishing point and radius
        :param vp:
        :param vr:
        :return:
        """
        self.vanpt = vp
        self.vrad = vr

    def get_mid_lane_width(self,rightidx=0,leftidx=0):
        """
        Once we have the seg groups.  Use this to find the widths.
        :param rightidx:
        :param leftidx:
        :return:
        """
        if len(self.right_seg_groups) <= rightidx or len(self.left_seg_groups) <= leftidx:
            return -1
        ymidway = int((self.vanpt[1]+self.maxy)/2.0)
        ln_mid = [0, ymidway, self.maxx, ymidway]
        right_avg = 0.0
        count = 0
        for s in self.right_seg_groups[rightidx]:
            x, y, r = intersections.intersectLinefour(s, ln_mid)
            if r == 1:
                right_avg += x
                count +=1
        if count == 0:
            return -1
        right_avg = right_avg/count
        left_avg = 0.0
        count = 0
        for s in self.left_seg_groups[leftidx]:
            x, y, r = intersections.intersectLinefour(s, ln_mid)
            if r == 1:
                left_avg += x
                count +=1
        if count == 0:
            return -1
        left_avg = left_avg/count
        return abs(right_avg-left_avg)

    def filter_affine(self, afine_mad, afpp):
        zerod = affine_alpha_min.filter_zero_devisors(self.all_lines,afpp)
        if np.sum(zerod) > 0:
            self.dropped_seg.extend(self.all_lines[zerod])
            self.all_lines = self.all_lines[~zerod]
        out_aff =  affine_alpha_min.filter_on_mad(self.all_lines,afpp,afine_mad)
        if np.sum(out_aff) > 0:
            self.dropped_seg.extend(self.all_lines[out_aff])
            self.all_lines = self.all_lines[~out_aff]
        return

    def filter_on_width(self,lane_width_min,lane_width_max):
        """
        filter out any segment groups that would put the lane width
        less than the min width.  WE do this by checking the rightish
        and leftish lanes for the best one to drop.
        If the lane width is greater than the max or one side is missing.
        Make all the lanes dropped lanes in this frame.
        :param lane_width_min:
        :param lane_width_max:
        :return:
        """
        initwidth = self.get_mid_lane_width(0,0)
        if initwidth <= 0 or initwidth >= lane_width_max:
            self.drop_all()
            return None
        if lane_width_min < initwidth < lane_width_max:
            return None
        # have to do something...
        rightright = self.get_mid_lane_width(1,0)
        leftleft = self.get_mid_lane_width(0,1)
        if rightright < 0 or leftleft < 0:
            self.drop_all()
            return None
        if rightright < leftleft:
            self.dropped_seg.extend(self.right_seg_groups[0])
            del self.right_seg_groups[0]
        else:
            self.dropped_seg.extend(self.left_seg_groups[0])
            del self.left_seg_groups[0]

        return self.filter_on_width(lane_width_min,lane_width_max)

    def has_lanes(self):
        return 1 if len(self.left_seg_groups) > 0 and len(self.right_seg_groups) > 0 else 0

    def get_affine_segments_to_minimize(self):
        """
        Use al the lines we found.  These have
        been filtered to point to the vanishing point.
        :return:
        """
        return self.all_lines

    def drop_all(self):
        for s_g in self.right_seg_groups:
            self.dropped_seg.extend(s_g)
        self.right_seg_groups = []
        for s_g in self.left_seg_groups:
            self.dropped_seg.extend(s_g)
        self.left_seg_groups =[]
        return None

    def get_vanishing_pts(self, out = None):
        """
        Intersect all the left and right segments.
        USe this to find the vanishing point.

        :param out: Draw debugging output to out.
        :return: list of points found
        """
        if len(self.right_seg_groups) == 0 or len(self.left_seg_groups) == 0:
            return []
        # First ransac all seg groups.
        # Now intersect pairwise
        vpts = []
        for ll in self.left_seg_groups[0]:
            for rl in self.right_seg_groups[0]:
                x,y,r = intersections.intersectLinefour(ll,rl)
                if r == 1:
                    vpts.append([x,y])

        # framed = self.frame.copy()
        # for vp in vpts:
        #     cv2.circle(framed, (int(vp[0]), int(vp[1])), 4, (0, 255, 0), -1)
        # cv2.imshow("vln",framed)
        # cv2.waitKey(60)
        return vpts

    def filter_to_vanishing_pt(self):
        """
        Do an initial filter on the lines pointing
        toward the vanishing point.
        :return:
        """
        if self.debug:
            dframe = self.frame.copy()
        reduced_lines = []
        if self.all_lines is not None:
            for loff in self.all_lines:
                if self.line_points_to_vp(loff, self.vanpt, self.vrad):
                        reduced_lines.append(loff)
                        if self.debug:
                            cv2.line(dframe, tuple(loff[0:2]), tuple(loff[2:4]), (0, 0, 255), 2)
                else:
                    self.dropped_seg.append(loff)
                    if self.debug:
                        cv2.line(dframe, tuple(loff[0:2]), tuple(loff[2:4]), (255, 0, 0), 2)
            if self.debug:
                cv2.imshow("dframe", dframe)
                cv2.waitKey(60)
            self.all_lines = np.array(reduced_lines)

    def find_min_max_lane_y(self):
        justy = []
        if len(self.left_seg_groups) > 0:
            justy = np.array([self.left_seg_groups[0][:,1],self.left_seg_groups[0][:,3]]).flatten()
        if len(self.right_seg_groups) > 0:
            justy = np.append(justy,self.right_seg_groups[0][:, 1])
            justy = np.append(justy, self.right_seg_groups[0][:, 3])
        if len(justy) < 1:
            return 0,0
        return np.min(justy), np.max(justy)

    def find_potential_dash(self, afpp,minyln,maxyln):
        """
        Search for potential dashes.
        :param afpp: The current afine and projective transform.
        :param minyln:
        :param maxyln:
        :return:
        """
        all_dash = []
        all_dscale = []
        all_width = []
        if len(self.left_seg_groups) > 0:
            dashes, dscales, lxmeant = self.__find_potential_dash(self.left_seg_groups[0], afpp,minyln,maxyln)
            all_dash.extend(dashes)
            all_dscale.extend(dscales)
        if len(self.right_seg_groups) > 0:
            dashes, dscales, rxmeant = self.__find_potential_dash(self.right_seg_groups[0], afpp,minyln,maxyln)
            all_dash.extend(dashes)
            all_dscale.extend(dscales)
            if len(self.left_seg_groups) > 0:
                all_width.append(abs(lxmeant-rxmeant))
        self.dash_candidates= all_dash
        self.width_candidates = all_width
        self.dashscale_candidates = all_dscale
        return all_dscale, all_width

    def __find_potential_dash(self, seg_grp, afpp,minyln,maxyln):
        """
        For a given segment group find potential
        dashes
        :param seg_grp:
        :return: dash segment list
        """
        if len(seg_grp) == 0:
            return [],[],0
        # make a list of all the points in the y direction
        pts_list = []
        for s in seg_grp:
            pts = intersections.line_seg_y_points(s)
            pts_list.extend(pts)
        # sort all the points so we can use group by y
        pts_list = np.array(sorted(pts_list, key=lambda xy: xy[1]))

        # Calculate the average transformed X
        ptsh = np.full((len(pts_list),3),1)
        ptsh[:,:-1]=pts_list
        ptsh = ptsh.transpose()
        ptstr = np.dot(afpp,ptsh)
        xmeantrans = np.mean(np.divide(ptstr[0],ptstr[2]))

        # group so for each y we have a number of X to
        # take the average of.
        avgx = []
        ys = []
        for y,xg in itertools.groupby(pts_list,key=lambda xy: xy[1]):
            xxs = [r[0] for r in list(xg)]
            avgx.append((np.mean(xxs),np.max(xxs)-np.min(xxs)))
            ys.append(y)
        # save off the max y for later.
        ymin = ys[0]
        ymax = ys[len(ys)-1]

        # now find the start and end of each sequential set of y
        # turn these into dash segs
        dashes = []
        dscales = []
        for k,yg in itertools.groupby(enumerate(ys),key=lambda iy:iy[0]-iy[1]):
            ist=iend = next(yg)[0]
            for iend,_ in yg:
                pass
            if ist != iend:
                # if the dash on the screen is less than 10 pixels lets drop it here.
                if abs(ys[ist]-ys[iend]) > 10:
                    dscale, adx, ady = PerspectiveTransInfo.scale_for_ten_foot_seg([avgx[ist][0],ys[ist]], [avgx[iend][0],ys[iend]], afpp)
                    if adx < (ady/4.0):
                        # adx and ady are in the transformed space and the dash should be mostly in the y direction.
                        dashes.append([avgx[ist][0],ys[ist],avgx[iend][0],ys[iend]])
                        dscales.append(dscale)
        # now go through and see if there is no other dash below this dash and closer than 25%
        if len(dashes) > 1:
            curr_dash = dashes[0]
            curr_dash_idx = 0
            drop_current = False
            i=1
            while i < len(dashes):
                cur_dash_len = abs(curr_dash[1]-curr_dash[3])*.25
                next_dash = dashes[i]
                dist_to_next = abs(next_dash[1]-curr_dash[3])
                if dist_to_next < cur_dash_len:
                    # Drop these two
                    del dashes[curr_dash_idx]
                    del dscales[curr_dash_idx]
                    drop_current = True
                    curr_dash_idx = i-1
                    curr_dash = next_dash
                elif drop_current:
                    del dashes[curr_dash_idx]
                    del dscales[curr_dash_idx]
                    drop_current = False
                    curr_dash_idx = i-1
                    curr_dash = next_dash
                else:
                    curr_dash_idx = i
                    curr_dash = next_dash
                    i = i+1
            if drop_current:
                del dashes[curr_dash_idx]
                del dscales[curr_dash_idx]

        # NOTE:  I dropped this test and depend on
        # mean shift to find the bad dashes.
        # drop the last one if it has the max y value
        # or the first one if it has the min
        # if len(dashes) > 0:
        #     if abs(dashes[len(dashes)-1][3] - ymax) < 2:
        #         del dashes[len(dashes)-1]
        #         del dscales[len(dscales)-1]
        # if len(dashes) > 0:
        #     if abs(dashes[0][1] - ymin) < 2:
        #         del dashes[0]
        #         del dscales[0]

        # if the length of the dash is more than 1/2 the min and max then
        # assume this is a solid one and drop it.
        toolong = (maxyln - minyln)/2
        i = 0
        while i < len(dashes):
            lend = abs(dashes[i][1]-dashes[i][3])
            if lend > toolong:
                del dashes[i]
                del dscales[i]
            else:
                i = i+1

        return dashes,dscales,xmeantrans

    def filter_potential_dash(self, afpp, minscale, maxscale):
        i = 0
        while i < len(self.dash_candidates):
            dash = self.dash_candidates[i]
            dscale, _, _ = PerspectiveTransInfo.scale_for_ten_foot_seg(dash[0:2], dash[2:4], afpp)
            if dscale > maxscale or dscale < minscale:
                del self.dash_candidates[i]
            else:
                i = i+1
        return len(self.dash_candidates)

    def line_points_to_vp(self,ll, vanpt, vrad):
        """
        See if the line points to the vanishing point and
        is not too horizontal
        :param ll: the line segment.
        :param vanpt: the vanishing point
        :param vrad: the vanishing radius
        :return:
        """
        if not self.seg_points_to_vp(ll[0:2],ll[2:4],vanpt,vrad):
            return False
        if minangleline <= abs(intersections.line_to_theta(ll)) <= maxangleline:
            return False
        return True

    def seg_points_to_vp(self,pt1, pt2, vanpt, vrad):
        """
        Find the distance from the line to the
        vanishing point.  If it is less than our radius
        then we have a good one.
        :param pt1: first point of line
        :param pt2: second point
        :param vanpt: The vanishing center
        :param vrad: The vanishing radius
        :return:
        """
        return intersections.line_point_dist(pt1,pt2,vanpt) <= vrad

    def order_into_seg_groups_partial_affine(self, afpp):
        """
        Use the affine to seperate into seg groups.  The afine
        maps parallel lines to be parallel.
        :param afpp:
        :return:
        """
        if len(self.all_lines) == 0:
            return
        self.left_seg_groups = []
        self.right_seg_groups = []

        if self.debug:
            dframe = self.frame.copy()
        vanpttrans = PerspectiveTransInfo.pointhtrans([self.vanpt[0],(self.maxy+self.vanpt[1])/2.0],afpp)
        # Now have the partial affine.  Transform the segments to slope mid point form for clustering.

        def slope_mid_pt(p1, p2):
            md = p1[1] - p2[1]
            if md == 0.0:
                return 0, 0, 0
            m = 10*(p1[0] - p2[0]) / md
            b = (p1[0]+p2[0])/2.0
            return m, b, 1

        mblst = np.array([slope_mid_pt(PerspectiveTransInfo.pointhtrans([self.all_lines[l][0],self.all_lines[l][1]],afpp),
                                       PerspectiveTransInfo.pointhtrans([self.all_lines[l][2],self.all_lines[l][3]],afpp)) for l in range(0,len(self.all_lines))])
        # any where the third part of the tuple is 0 should be dropped.
        self.dropped_seg.extend(self.all_lines[mblst[:,2]== 0])
        self.all_lines = self.all_lines[mblst[:,2]== 1]
        mblst = mblst[mblst[:,2]== 1]
        # all the m's should be near zero.  So eliminate outliers.
        moutliers = median_abs_dev(mblst[:,0],factor=1)
        self.dropped_seg.extend(self.all_lines[moutliers])
        self.all_lines = self.all_lines[~moutliers]
        mblst = mblst[~moutliers]
        # Finally we have the good segments in slope mid point form.
        # So cluter them.
        #bandwidth = (np.max(mblst[:,1])-np.min(mblst[:,1]))/10
        try:
            bandwidth = np.abs((PerspectiveTransInfo.pointhtrans([self.maxx,self.maxy],afpp)-PerspectiveTransInfo.pointhtrans([0,self.maxy],afpp))[0])/10.0
            centers, labels = mean_shift(mblst[:,[1,2]],bandwidth=bandwidth)
        except:
            # no seg groups.
            return

        # Now group original segments by the lables.
        # Order the groups by the centers rho distance left or right.
        ord_cent = np.argsort(-centers[:,0] ,axis=0)

        for lb in ord_cent:
            grp = np.array(self.all_lines[labels == lb])
            if self.get_groupLen(grp) < self.maxy/48:
                self.dropped_seg.extend(grp)
                continue

            if centers[lb,0] > vanpttrans[0]:
                self.left_seg_groups[:0] = [grp]
                if self.debug:
                    for s in grp:
                        cv2.line(dframe, tuple(s[0:2]), tuple(s[2:4]), (0, 0, 255), 2)
                    cv2.imshow("regroup", dframe)
                    cv2.waitKey(60)

            else:
                self.right_seg_groups.append(grp)
                if self.debug:
                    for s in grp:
                        cv2.line(dframe, tuple(s[0:2]), tuple(s[2:4]), (0, 255, 0), 2)
                    cv2.imshow("regroup", dframe)
                    cv2.waitKey(60)

        # now drop any right on the left and left on the right.
        while len(self.right_seg_groups) > 0 and len(self.left_seg_groups) > 0:
            if self.minx_of_grp(self.right_seg_groups[0]) <= self.maxx_of_grp(self.left_seg_groups[0]):
                del self.right_seg_groups[0]
                del self.left_seg_groups[0]
            else:
                break
        # finally see if any are close and should be combined.
        # only check the neighboring pairs right and left.
        self.check_overlap(self.right_seg_groups)
        self.check_overlap(self.left_seg_groups)

        return

    def order_into_seg_groups_mean_shift_to_centroid(self):
        """
        Before we have the affine we still need approximate lane lines.
        So this one does that.
        :return:
        """
        self.filter_to_vanishing_pt()
        self.left_seg_groups = []
        self.right_seg_groups = []
        if self.debug:
            dframe = self.frame.copy()
        rt_seg = []
        # if a line does not intersect we remove it from the list.
        si =0
        all_good_lns = []
        for s in self.all_lines:
            mc,ret = self.get_x_by_y_slope([(s[0]+s[2])/2.0,(s[1]+s[3])/2.0,self.vanpt[0],self.vanpt[1]])
            if ret == 1 and abs(mc) < 10.0:
                # dframe = self.frame.copy()
                if self.debug:
                    cv2.line(dframe, (int(s[0]), int(s[1])), (int(s[2]), int(s[3])), (0, 0, 255), 2)
                    cv2.imshow("fd", dframe)
                    cv2.waitKey(60)
                rt_seg.append([mc,0])
                all_good_lns.append(s)
            else:
                # dframe = self.frame.copy()
                if self.debug:
                    cv2.line(dframe, (int(s[0]), int(s[1])), (int(s[2]), int(s[3])), (0, 0, 255), 2)
                    cv2.imshow("fd", dframe)
                    cv2.waitKey(60)
                self.dropped_seg.append(s)

        if len(rt_seg) < 1:
            return
        self.all_lines = np.array(all_good_lns)
        # now mean shift cluster the bottom representations.
        ms_tol = 4/40
        centers,labels = mean_shift(np.asarray(rt_seg),ms_tol)

        # Now group original segments by the lables.
        # Order the groups by the centers rho distance left or right.
        ord_cent = np.argsort(centers[:,0] ,axis=0)

        for lb in ord_cent:
            grp = np.array(self.all_lines[labels == lb])
            # fd = self.frame.copy()
            # for s in grp:
            #     cv2.line(fd,(int(s[0]),int(s[1])),(int(s[2]),int(s[3])),(0,0,255),2)
            # cv2.imshow("fd",fd)
            # cv2.waitKey(60)
            if self.get_groupLen(grp) < self.maxx/40:
                self.dropped_seg.extend(grp)
                continue

            if centers[lb,0] < 0:
                self.left_seg_groups[:0] = [grp]
            else:
                self.right_seg_groups.append(grp)

        # now drop any right on the left and left on the right.
        while len(self.right_seg_groups) > 0 and len(self.left_seg_groups) > 0:
            if self.minx_of_grp(self.right_seg_groups[0]) <= self.maxx_of_grp(self.left_seg_groups[0]):
                del self.right_seg_groups[0]
                del self.left_seg_groups[0]
            else:
                break
        # finally see if any are close and should be combined.
        # only check the neighboring pairs right and left.
        self.check_overlap(self.right_seg_groups)
        self.check_overlap(self.left_seg_groups)

        return

    def check_overlap(self,seg):
        """
        Check for overlap of the first two.  Do this
        by finding the min and max slope for the line
        that goes through the vanishing point.
        :param seg:
        :return:
        """
        def getminmaxslope(s):
            minm = +float('inf')
            maxm = -float('inf')
            for ll in s:
                m1,r1 = self.get_x_by_y_slope_pts(ll[0],ll[1],self.vanpt[0],self.vanpt[1])
                if r1 == 0:
                    m1 = minm
                m2,r2 = self.get_x_by_y_slope_pts(ll[2], ll[3], self.vanpt[0], self.vanpt[1])
                if r2 == 0:
                    m2 = minm
                if m1 < minm:
                    minm = m1
                if m2 < minm:
                    minm = m2
                if m1 > maxm:
                    maxm = m1
                if m2 > maxm:
                    maxm = m2
            return minm,maxm
        if len(seg) < 2:
            return
        min0,max0 = getminmaxslope(seg[0])
        min1,max1 = getminmaxslope(seg[1])
        if min0 < min1 < max0 or min0 < max1 < max0 or min1 < min0 < max1 or min1 < max0 < max1:
            # overlap so combine
            seg[1] = np.append(seg[0],seg[1],axis=0)
            del seg[0]
            self.check_overlap(seg)
        return

    def minx_of_grp(self,grp):
        minx = grp[0][0]
        for s in grp:
            if s[0] < minx:
                minx = s[0]
            if s[2] < minx:
                minx = s[2]
        return minx

    def maxx_of_grp(self,grp):
        maxx = grp[0][0]
        for s in grp:
            if s[0] > maxx:
                maxx = s[0]
            if s[2] > maxx:
                maxx = s[2]
        return maxx

    def get_x_by_y_slope(self,ll):
        return self.get_x_by_y_slope_pts(ll[0],ll[1],ll[2],ll[3])

    def get_x_by_y_slope_pts(self,x1,y1,x2,y2):
        denom = y2-y1
        if denom == 0.0:
            return 0.0, 0
        return (x2-x1)/denom, 1

    def get_groupLen(self,s_g):
        totallen = 0.0
        for s in s_g:
            totallen += intersections.line_len(s)
        return totallen

    def debug_display(self):
        dframe = self.frame.copy()
        color = 255
        linewidth = 2
        for sg in self.right_seg_groups:
            for s in sg:
                cv2.line(dframe, tuple(s[0:2]), tuple(s[2:4]), (0, color, 0), linewidth)
            # cv2.imshow("laneframedebug", dframe)
            # cv2.waitKey(60)
            color = 150
            linewidth = 1

        color = 255
        linewidth = 2
        for sg in self.left_seg_groups:
            for s in sg:
                cv2.line(dframe, tuple(s[0:2]), tuple(s[2:4]), (0, 0, color), linewidth)
            # cv2.imshow("laneframedebug", dframe)
            # cv2.waitKey(60)
            color = 150
            linewidth = 1

        for s in self.dropped_seg:
            cv2.line(dframe, tuple(s[0:2]), tuple(s[2:4]), (255, 0, 0), 1)

        for s in self.dash_candidates:
            cv2.line(dframe, (int(s[0]),int(s[1])), (int(s[2]),int(s[3])), (0, 255, 255), 4)

        if self.debug:
            cv2.imshow("laneframedebug",dframe)
            cv2.waitKey(60)

        return dframe
