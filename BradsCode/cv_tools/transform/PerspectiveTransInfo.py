import numpy as np
import json
import os
from math import *


class PerspectiveTransInfo:
    """
    Class to hold our projective transformation.
    """
    def __init__(self, name=""):
        self.van_pt=[0, 0] # Vanishing point.
        self.mid_width=0 # Average Lane width at halfway between the vanising point and bottom.
        self.t_alpha=0 # Affine Alpha
        self.t_beta_inv=0 # Affine one over beta
        self.t_scale = 0
        self.f_width=0
        self.f_height=0
        self.f_hood_scanline=0

        self.lrn_tot_frame=0
        self.lrn_frames_with_lanes=0
        self.lrn_frames_dashs=0
        self.lrn_total_dash=0
        self.lrn_vid_names=[]
        self.lrn_dash_hist=None
        self.lrn_width_hist=None
        self.lrn_vanpt_hist=None
        self.lrn_alpha_err=0
        self.name= name
        self.error_feet = 100000

        self.status_message = "SUCCESS"

    def getPureProjective(self):
        """
        Return just the projective transform.  This is formed from the horizontal vanishing line.
        :return:
        """
        return np.asarray([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 1.0/float(-self.van_pt[1]), 1.0]])

    def getAfineOnly(self):
        """
        Return just the affine.  The transfomr that turns parallel lines back to being
        parallel and also has the relative horizontal and vertical scale.
        :return:
        """
        return np.asarray([[self.t_beta_inv,-self.t_alpha*self.t_beta_inv,0.0],[0.0,1.0,0.0],[0.0,0.0,1.0]])

    def getScaleOnly(self):
        """
        Return just the scaling transform
        :return:
        """
        return np.asarray([[self.t_scale,0.0,0.0],[0.0,self.t_scale, 0.0],[0.0,0.0,1.0]])

    def getAfine_PureProjective(self):
        """
        return the combined affine adn projective.
        :return:
        """
        return np.dot(self.getAfineOnly(),self.getPureProjective())

    def getInvPerTrans(self):
        """
        Get the inverse perspective transform.
        :return:
        """
        return np.dot(self.getScaleOnly(),self.getAfine_PureProjective())

    def set_scale(self,scale):
        self.t_scale = scale

    def set_vanpt(self, vanpt):
        self.van_pt = [int(vanpt[0]),int(vanpt[1])]
        if self.van_pt[1] == 0:
            self.van_pt[1] = 1

    def rescale_by(self,newscale):
        self.t_scale = newscale * self.t_scale
        return

    def toJson(self):
        """
        Return the json for this object.
        :return:
        """
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    def fromJson(self,js):
        """
        read this object from json.  Either a string or a file can be passed.
        :param js:
        :return:
        """
        jsonstr = js
        if os.path.isfile(js):
            with open(js,'r') as jscontent:
                jsonstr = jscontent.read()

        self.__dict__ = json.loads(jsonstr)

def scale_for_ten_foot_seg(pt1,pt2,ampm):
    """
    Return the scale to make the segment between the two points
    10 feet long.
    :param pt1: First point
    :param pt2: Second Point
    :param ampm: Current afine and projective transform with 1/beta set to 1.0
    :return:
    """
    pt1ap = pointhtrans(pt1,ampm)
    pt2ap = pointhtrans(pt2,ampm)
    dx = pt1ap[0]-pt2ap[0]
    dy = pt1ap[1]-pt2ap[1]
    dist = sqrt((dx)**2+(dy)**2)
    # 10 is for 10 feet.
    return 10.0/dist, abs(dx), abs(dy)

# transform a cartesian point through a homogenious transform back to cartesian
def pointhtrans(pt,m):
    """
    MAp a point trhough the perspective transform M
    :param pt:
    :param m:
    :return:
    """
    inpt = np.asarray([pt[0],pt[1],1.0])
    outpt = np.dot(m,inpt)
    return np.asarray([outpt[0]/outpt[2],outpt[1]/outpt[2]])

if __name__ == "__main__":
    ob = PerspectiveTransInfo()
    print(ob.toJson())

    ob2 = PerspectiveTransInfo()
    ob2.fromJson('{"lrn_dashs": 1,"lrn_good_frame": 1,"lrn_tot_frame": 1, "mid_width": 300,"t_scale": 5,"t_alpha": 2.1,"t_beta_inv": 1e-15,"van_pt": [5.2, 3.6], "f_height": 0, "f_width": 0}')
    print(ob2.toJson())

