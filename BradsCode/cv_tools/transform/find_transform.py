import gc
import os
import argparse
import cv2
import numpy as np
from vanishingpoint import vanishing_point
from hood_detect import hood_detect
from cv_utils.sdevideo import SdeVideo
from transform.LaneFrame import LaneFrame
from cv_utils.MedianAbsDev import median_abs_dev
from transform.PerspectiveTransInfo import PerspectiveTransInfo, pointhtrans
from transform.affine_alpha_min import affine_alpha_min, get_affine_mad, filter_zero_devisors, filter_on_mad
from sklearn.cluster import mean_shift, estimate_bandwidth

import cProfile

def get_transform_multiple(dirname, vid_files, vanpt, vrad, hood_scanline, maxx, maxy, out, debug):
    """
    Find he perspective transform for a single vehicle from multiple vidoes.

    :param dirname: Name of the direcctory containing the videos.
    :param vid_files: List of video file names.
    :param vanpt: Guess at the vanishing point.
    :param vrad: Radius of the vanishing point area.
    :param hood_scanline: scan line just beyond hood.
    :param maxx: maximum x value for video.
    :param maxy: maximum y value.
    :param out: A Video stream to save debugging output to.
    :param debug: If True output debug info.
    :return: json of the transform.
    """

    # Check for bad hood scan line.
    if hood_scanline < vanpt[1]+vrad:
        hood_scanline = maxy
    # gather up all the lane frames across all the videos.
    laneframes= []
    for vf in vid_files:
        lanefs = get_vid_lanes(dirname,vf,vanpt,vrad, hood_scanline,debug)
        laneframes.extend(lanefs)

    # Start the perspective transform
    pti = PerspectiveTransInfo(os.path.basename(dirname))
    pti.lrn_tot_frame = len(laneframes)
    pti.f_width = maxx
    pti.f_height = maxy
    pti.lrn_vid_names = np.array(vid_files).tolist()
    pti.f_hood_scanline = hood_scanline

    # Finally get the final vanishing point as the mean of all the
    # frames vanishing points.
    vanpt_list = []
    for lanf in laneframes:
        lanf.set_vanpt(vanpt, vrad)
        lanf.order_into_seg_groups_mean_shift_to_centroid()
        vpts = lanf.get_vanishing_pts()
        vanpt_list.extend(vpts)
    if len(vanpt_list) < 5:
        pti.status_message="Too few vanishing points"
        return pti.toJson()

    # eliminate outliers in the y direction.  Then take the mean vanishing point.
    vanpt_list = np.array(vanpt_list)
    vanpt_list_in = vanpt_list[~median_abs_dev(vanpt_list[:,1])]
    pti.set_vanpt(np.mean(vanpt_list_in,axis=0).tolist())
    pti.lrn_vanpt_hist = [len(vanpt_list_in),len(vanpt_list)-len(vanpt_list_in)]
    #get a new radius that elimnates 5% outliers. (2 times the STD)
    newvrad = np.max(np.std(vanpt_list_in,axis=0)*2)
    # set this in the lane frames and then filter.
    for lanf in laneframes:
        lanf.set_vanpt(pti.van_pt,newvrad)
        lanf.filter_to_vanishing_pt()
    # Now get the afine parameters.
    # First get all the segments from our detected lanes.
    ppt = pti.getPureProjective()
    afinelist = []
    for lanf in laneframes:
        afinelist.extend(lanf.get_affine_segments_to_minimize())
    if len(afinelist) < 5:
        pti.status_message = "too few affine segments"
        return pti.toJson()

    #Now find the affine alpha that tries to make all the lanes parallel.
    pti.t_alpha,pti.lrn_alpha_err = affine_alpha_min(np.array(afinelist),ppt)
    # just temporary set the beta to 1.
    pti.t_beta_inv = 1.0
    # reorder into seg groups using this affine mapping.
    # makes it easier to find the lanes.
    afpp = pti.getAfine_PureProjective()
    # Now filter on the affine.
    afine_mad = get_affine_mad(np.array(afinelist),afpp)
    for lanf in laneframes:
        lanf.filter_affine(afine_mad,afpp)
        lanf.order_into_seg_groups_partial_affine(afpp)

    # refine the vanishing point one last time.
    vanpt_list = []
    for lanf in laneframes:
        vpts = lanf.get_vanishing_pts()
        vanpt_list.extend(vpts)
    if len(vanpt_list) < 5:
        pti.status_message="Too few vanishing points"
        return pti.toJson()

    # eliminate outliers in the y direction.  Then take the mean vanishing point.
    vanpt_list = np.array(vanpt_list)
    vanpt_list_in = vanpt_list[~median_abs_dev(vanpt_list[:,1])]
    pti.set_vanpt(np.mean(vanpt_list_in,axis=0).tolist())
    pti.lrn_vanpt_hist = [len(vanpt_list_in),len(vanpt_list)-len(vanpt_list_in)]
    #get a new radius that elimnates 5% outliers. (2 times the STD)
    newvrad = np.max(np.std(vanpt_list_in,axis=0)*2)
    # set this in the lane frames and then filter.
    for lanf in laneframes:
        lanf.set_vanpt(pti.van_pt,newvrad)
    afinelist = []
    for lanf in laneframes:
        afinelist.extend(lanf.get_affine_segments_to_minimize())
    if len(afinelist) < 5:
        pti.status_message = "too few affine segments"
        return pti.toJson()

    #Now find the affine alpha that tries to make all the lanes parallel.
    ppt = pti.getPureProjective()
    pti.t_alpha,pti.lrn_alpha_err = affine_alpha_min(np.array(afinelist), ppt, alpha=pti.t_alpha)
    afpp = pti.getAfine_PureProjective()

    # now find a min and max for the lane lines.
    minyln = maxy
    maxyln = 0
    for lanf in laneframes:
        mn,mx = lanf.find_min_max_lane_y()
        if mx == 0 or mn == 0:
            continue
        if minyln > mn:
            minyln = mn
        if maxyln < mx:
            maxyln = mx

    # find the dash scale and widths.
    all_dash_scale = []
    all_width = []
    for lanf in laneframes:
        dashs,widths = lanf.find_potential_dash(afpp,minyln,maxyln)
        all_dash_scale.extend(dashs)
        all_width.extend(widths)

    if len(all_width) < 3:
        pti.status_message = "too few widths"
        return pti.toJson()

    # now use mean shift to find the best width.
    midlen, _, _, widthph = best_mean(np.array(all_width))
    pti.mid_width = midlen
    pti.lrn_width_hist = widthph
    if len(widthph) <= 1:
        pti.status_message = "lane widths histogram"

    # now find the best dash scale.
    if len(all_dash_scale) < 3:
        pti.status_message = "too few dashes"
        return pti.toJson()
    all_dash_scale = np.array(all_dash_scale)
    bestdscale, minscale, maxscale, scalehist = best_mean(all_dash_scale)
    pti.lrn_dash_hist = scalehist
    if len(scalehist) <=1:
        pti.status_message = "dashes histogram"

    # finally gather up some statistics.
    lanes_with_dash = 0
    total_dash = 0
    frames_with_lanes = 0
    for lanf in laneframes:
        num_dash = lanf.filter_potential_dash(afpp, minscale, maxscale)
        lanes_with_dash += 1 if num_dash > 0 else 0
        frames_with_lanes += lanf.has_lanes()
        total_dash += num_dash

    pti.lrn_frames_dashs = lanes_with_dash
    pti.lrn_frames_with_lanes = frames_with_lanes
    pti.lrn_total_dash = total_dash

    # Assume the lane width is 12 feet.
    pti.t_beta_inv = - 12.0/(bestdscale*midlen)
    pti.set_scale(bestdscale)

    pti.error_feet = calc_dest_error(pti, laneframes)

    if out is not None:
        for lanf in laneframes:
            oframe = lanf.debug_display()
            cv2.circle(oframe,(int(pti.van_pt[0]),int(pti.van_pt[1])),4,(0,0,0),1,-1)
            out.write(oframe)

    return pti.toJson()


def calc_dest_error(pti,laneframes):
    """
    Estimate the error in the transform by looking at 2 halves of the frames.  Then
    compare these to the transform from all the frames.
    :param pti: The transform found from all the frames
    :param laneframes: The frames data.
    :return: error in feet along the road.
    """
    # get the transform for each half of the data.
    halfpoint = int(len(laneframes)/2)
    fhtrans = get_transform_from_laneframe(laneframes[:halfpoint], pti.f_width, pti.f_height)
    if fhtrans is None:
        return 100000
    shtrans = get_transform_from_laneframe(laneframes[halfpoint:], pti.f_width, pti.f_height)
    if shtrans is None:
        return 100000

    t_full = pti.getInvPerTrans()
    retinv, t_full_invert = cv2.invert(t_full, flags=cv2.DECOMP_LU)
    # Find the point on the road at the bottom of the image.  Then
    # add 10 feet forward and 5 off to the side.
    ptcentbot = pointhtrans([pti.f_width / 2, pti.f_height], t_full)
    ptcentbot[1] -= 20
    ptcentbot[0] -= 5
    # map this back to the iamge and pixelize it.
    ptimage = pointhtrans(ptcentbot,t_full_invert)
    ptimage = np.int32(ptimage)
    # now find this pixel along the road.
    ptcentbot = pointhtrans(ptimage,t_full)
    # for each half find the same pixel location on the road.
    fh_point = pointhtrans(ptimage,fhtrans.getInvPerTrans())
    sh_point = pointhtrans(ptimage,shtrans.getInvPerTrans())
    fhdiff = ((fh_point[1] - ptcentbot[1]) ** 2 + (fh_point[0] - ptcentbot[0]) ** 2)**.5
    shdiff = ((sh_point[1] - ptcentbot[1]) ** 2 + (sh_point[0] - ptcentbot[0]) ** 2)**.5
    # since the algorithm depends on most common and elimnates outliers.  Choose the
    # smaller error as the error.
    return min(shdiff,fhdiff)

def get_transform_from_laneframe(laneframes, maxx, maxy):
    """
    Given the already optimized lane frames find the transform.
    :param laneframes:  The lane frames already optimized
    :param maxx: max x
    :param maxy: max y
    :return: Transform
    """
    pti = PerspectiveTransInfo()
    pti.lrn_tot_frame = len(laneframes)
    pti.f_width = maxx
    pti.f_height = maxy
    pti.lrn_vid_names = []
    pti.f_hood_scanline = maxy

    vanpt_list = []
    for lanf in laneframes:
        vpts = lanf.get_vanishing_pts()
        vanpt_list.extend(vpts)
    if len(vanpt_list) < 5:
        return None
    vanpt_list = np.array(vanpt_list)
    vanpt_list_in = vanpt_list[~median_abs_dev(vanpt_list[:,1])]
    pti.set_vanpt(np.mean(vanpt_list_in,axis=0).tolist())
    afinelist = []
    for lanf in laneframes:
        afinelist.extend(lanf.get_affine_segments_to_minimize())
    if len(afinelist) < 5:
        return None
    ppt =pti.getPureProjective()
    pti.t_alpha,pti.lrn_alpha_err = affine_alpha_min(np.array(afinelist), ppt, alpha=pti.t_alpha)

    all_dash_scale = []
    all_width = []
    for lanf in laneframes:
        all_dash_scale.extend(lanf.dashscale_candidates)
        all_width.extend(lanf.width_candidates)

    if len(all_width) < 3 or len(all_dash_scale) < 3:
        return None

    midlen, _, _, widthph = best_mean(np.array(all_width))
    pti.mid_width = midlen
    pti.lrn_width_hist = widthph
    if len(widthph) <= 1:
        return None
    all_dash_scale = np.array(all_dash_scale)
    bestdscale, minscale, maxscale, scalehist = best_mean(all_dash_scale)
    pti.lrn_dash_hist = scalehist
    if len(scalehist) <=1:
        return None
    pti.t_beta_inv = - 12.0/(bestdscale*midlen)
    pti.set_scale(bestdscale)
    return pti


def get_vid_lanes(dirname, vf, vanpt, vrad, hood_scanline, debug):
    """
    Simply loop on this video and return the lane frames.
    :param dirname: Directory to find the videos in.
    :param vf: Video file name
    :param vanpt: Initial Vanishing point
    :param vrad: Initial vanishing radius
    :param hood_scanline: Hood scan line
    :param debug: print debug if true
    :return:
    """
    flname = os.path.join(dirname,vf)
    vcap = SdeVideo(flname)
    laneframes = []

    while True:
        ret, frame = vcap.read()
        if ret == False:
            break
        laneframes.append(process_frame(frame,vcap.get_last_frame_no(),vanpt,vrad, hood_scanline,debug))
    vcap.release()

    return laneframes


# use mean shift to find the best dashed line length
def best_mean(sample_data):
    """
    wrapper for mean shift
    :param sample_data:
    :return:
    """
    if len(sample_data.shape) == 1:
        if len(sample_data) < 5:
            smean = np.mean(sample_data)
            return smean, min(sample_data),max(sample_data),\
                   [[len(sample_data), float(np.mean(sample_data)), float(np.mean(sample_data)),
                    float(np.median(sample_data)), float(min(sample_data)), float(max(sample_data)), float(np.std(sample_data))]]
        dscales = np.array([[sample_data[i], 0] for i in range(0, len(sample_data))])
        bandwidth = estimate_bandwidth(dscales)
        if bandwidth == 0.0:
            bandwidth = None
        centers, labels = mean_shift(dscales,bandwidth=bandwidth)
    else:
        raise IndexError("best_mean only handles 1 dimensional arrays")
    # unique returns the sorted unique labels.
    unlbl = np.unique(labels)
    # bincount returns the count of each index  so lbcnt[2] is the number of 2s
    lbcnt = np.bincount(labels)
    # Since the labels are contigous argmax gives us the index in unlbl of the max label.
    indxsort = np.argsort(-lbcnt)
    maxidx = indxsort[0]
    histogram = []
    for idx in indxsort:
        goodones = sample_data[labels == unlbl[idx]]
        histogram.append([int(lbcnt[idx]),float(centers[unlbl[idx]][0]), float(np.mean(goodones)),
                          float(np.median(goodones)), float(min(goodones)), float(max(goodones)), float(np.std(goodones))])
    maxent = histogram[maxidx]
    return maxent[1], maxent[4], maxent[5],histogram

# For each frame we need to find:
# anything that looks like a dash.
# right and left lane lines.
# list of potential vanishing points from lane lines.


def process_frame(frame, frameno, vanpt, vrad, hood_scanline, debug):
    """
    Take a single vidoe frame and turn it into one of our lane frames with
    found potential lane lines.
    :param frame: The video frame
    :param frameno: the index of this video in the frame.
    :param vanpt: Approximate vanishing point
    :param vrad: Radius of vanishing point region.
    :param debug: if True then output debug.
    :return:
    """
    goodframe = frame.copy()
    # turn the frame into gray
    maxy = frame.shape[0]
    maxx = frame.shape[1]
    if hood_scanline < maxy:
        maxy = hood_scanline
        gray = cv2.cvtColor(frame[0:maxy,:], cv2.COLOR_RGB2GRAY)
    else:
        gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
    # we do a 7x3 blur to some what match the aspect ratio.
    gray = cv2.blur(gray,(7,3))
    # to get a great threshold we take the middle third from the
    # vanishing point on down to calculate the threshold for canny.
    athird = int((maxy-vanpt[1])/3)
    bottommidthird = vanpt[1]+2*athird
    topmidthird = vanpt[1]+athird
    canny_offset = vanpt[1]+vrad
    xmidthird = int(maxx/3)
    xmidthirdend = xmidthird+xmidthird

    # lineswy = get_white_yellow(frame[canny_offset:maxy,:], debug)
    lineswy = None

    th, ret = cv2.threshold(gray[topmidthird:bottommidthird,xmidthird:xmidthirdend].copy(), 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    # Run canny and filter results on any contours that
    canny = cv2.Canny(gray[canny_offset:maxy,:],th/3,th,apertureSize=3)
    if lineswy is not None:
        canny = cv2.bitwise_and(canny,lineswy)

    lines = cv2.HoughLinesP(canny, rho=1, theta=np.pi / 180.0, threshold=5, minLineLength=3, maxLineGap=5)
    if lines is None:
        th, ret = cv2.threshold(gray[topmidthird:bottommidthird, :].copy(), 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        canny = cv2.Canny(gray[canny_offset:maxy, :], th / 3, th, apertureSize=3)
        lines = cv2.HoughLinesP(canny, rho=1, theta=np.pi / 180.0, threshold=5, minLineLength=3, maxLineGap=5)

    # debug = True
    # if debug:
    #     canny = cv2.cvtColor(canny, cv2.COLOR_GRAY2BGR)
    #     cv2.imshow("canny",canny)
    #     if not lines is None :
    #         dframe = goodframe.copy()
    #         for l in lines:
    #             loff = l[0]+(0,canny_offset,0,canny_offset)
    #             cv2.line(dframe, tuple(loff[0:2]), tuple(loff[2:4]), (255, 0, 0), 2)
    #         cv2.imshow("Hough",dframe)
    #     cv2.waitKey(60)
    # debug = False

    lanef = LaneFrame(frameno, goodframe)

    if debug:
        lanef.debug = True
    if lines is not None:
        lanef.set_all_lines(np.reshape((lines + (0, canny_offset, 0, canny_offset)),(-1,4)))
    # if debug:
    #     cv2.imshow("frame", frame)
    #     cv2.waitKey(60)

    return lanef

# main to test with.


if __name__ == "__main__":
    # parse the arguments.
    parse = argparse.ArgumentParser()
    parse.add_argument("--vd", help="Video directorys")
    parse.add_argument("--debug")
    parse.add_argument("--m",help='Minimum number of files.')
    parse.add_argument("--x", help="maximum number of files")
    args = parse.parse_args()

    if args.vd and os.path.isdir(args.vd):
        # Setup the output paths.
        onlydirsall = [f for f in os.listdir(args.vd) if os.path.isdir(os.path.join(args.vd, f))]
        if len(onlydirsall) == 0:
            onlydirnames = [os.path.basename(args.vd)]
            outdir = os.path.dirname(args.vd) + "_output"
            onlydirs =[args.vd]
            onlydirsfiles=[[f for f in os.listdir(args.vd) if os.path.isfile(os.path.join(args.vd, f)) and f.endswith("mp4")]]
        else:
            outdir = args.vd+"_output"
            onlydirs = []
            onlydirnames = []
            onlydirsfiles = []
            for sdir in onlydirsall:
                sdirpath = os.path.join(args.vd, sdir)
                onlyfiles = [f for f in os.listdir(sdirpath) if os.path.isfile(os.path.join(sdirpath, f)) and f.endswith("mp4")]
                if len(onlyfiles) > 0:
                    onlydirnames.append(sdir)
                    onlydirs.append(sdirpath)
                    onlydirsfiles.append(onlyfiles)
        vid_out_path = os.path.join(outdir, "resultslanes")
        json_path = os.path.join(outdir, "jsontrans")
        if not os.path.exists(outdir):
            os.mkdir(outdir)
        if not os.path.exists(vid_out_path):
            os.mkdir(vid_out_path)
        if not os.path.exists(json_path):
            os.mkdir(json_path)
        fourcc = cv2.VideoWriter_fourcc(*'MP4V')
        # we are looking for a directory that contains directories of video files.
        # now see if each dir contains video files.

        for i in range(0,len(onlydirs)):
            if args.m is not None and len(onlydirsfiles[i]) < int(args.m):
                continue
            if args.x is not None and len(onlydirsfiles[i]) > int(args.x):
                continue
            outpathfile = os.path.join(vid_out_path , onlydirnames[i]+".mp4")
            if os.path.exists(outpathfile):
                continue
            jsonfile = os.path.join(json_path, onlydirnames[i] + ".json")
            if os.path.exists(jsonfile):
                continue
            # create the video writer for the results.
            vcap = SdeVideo(os.path.join(onlydirs[i],onlydirsfiles[i][0]))
            maxx, maxy = vcap.get_shape()
            vcap.release()

            # pr = cProfile.Profile()
            # pr.enable()

            # get the vanishing point and filtered list of files.
            vp, vr, ffiles = vanishing_point.get_first_pass_vanishing_point_files_mean(onlydirs[i],onlydirsfiles[i])
            if vp is not None:
                hdetect = hood_detect.HoodDetect([maxy,maxx])
                hood_scanline = hdetect.detect_hood_from_files(onlydirs[i],ffiles)
                # now get the transform.
                out = cv2.VideoWriter(outpathfile, fourcc, 20.0, (maxx, maxy))
                jsonresult =get_transform_multiple(onlydirs[i],ffiles, vp, vr, hood_scanline ,maxx, maxy, out, args.debug == '1')
                out.release()
                del out
            else:
                tmpproj = PerspectiveTransInfo()
                tmpproj.status_message = "No Vanishing Point"
                jsonresult = tmpproj.toJson()

            # pr.disable()
            # pr.print_stats(sort='cumtime')

            if jsonresult is not None:
                jsonfile = os.path.join(json_path,onlydirnames[i]+".json")
                with open(jsonfile,"w") as jf:
                    jf.write(jsonresult)

            gc.collect()
