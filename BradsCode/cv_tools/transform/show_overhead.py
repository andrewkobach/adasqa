import os
import argparse
import cv2
import numpy as np
from cv_utils.sdevideo import SdeVideo
from transform.PerspectiveTransInfo import PerspectiveTransInfo, pointhtrans
import json

from shapely.geometry import box, Polygon

def read_safety_file(fn):
    """
    Read the safety file into an np table,   Skip the headder.
    :param fn: file name
    :return:
    """
    # fid = open(fn, encoding='utf-8-sig')
    # safetyinfo = np.genfromtxt(io.BytesIO(fid.read().encode('ascii')),delimiter=',',usecols=(0,7),names=True)
    safetyinfo = np.genfromtxt(fn,delimiter=',',usecols=(0,7),skip_header=1)
    return safetyinfo


def straight_poly(torginvert, inpts):
    lnpts = np.dot(torginvert, np.array(inpts).transpose())
    l0 = [lnpts[0][0]/lnpts[2][0],lnpts[1][0]/lnpts[2][0]]
    r0 = [lnpts[0][1]/lnpts[2][1],lnpts[1][1]/lnpts[2][1]]
    l1 = [lnpts[0][2]/lnpts[2][2],lnpts[1][2]/lnpts[2][2]]
    r1 = [lnpts[0][3]/lnpts[2][3],lnpts[1][3]/lnpts[2][3]]
    l2 = [lnpts[0][4]/lnpts[2][4],lnpts[1][4]/lnpts[2][4]]
    r2 = [lnpts[0][5]/lnpts[2][5],lnpts[1][5]/lnpts[2][5]]
    l3 = [lnpts[0][6]/lnpts[2][6],lnpts[1][6]/lnpts[2][6]]
    r3 = [lnpts[0][7]/lnpts[2][7],lnpts[1][7]/lnpts[2][7]]
    return np.array([[l0,r0,r1,l1],[l1,r1,r2,l2],[l2,r2,r3,l3]],dtype=np.int32)

def get_close_follow(frame,model,distrect):
    result_rect = []
    other_rect = []
    bottom_seg = []
    if model is None:
        return result_rect, other_rect, bottom_seg
    ply = []
    for pl in distrect:
        ply.append( Polygon(pl))
    res = model.detect([frame], verbose=0)
    ids = np.searchsorted([1,4,5],res[0]['class_ids'])
    rois = res[0]['rois']
    for i in range(0,len(ids)):
        if ids[i] < 3:
            rect= rois[i]
            vehrect = box(rect[1],rect[0],rect[3],rect[2])
            mxy = max(rect[0],rect[2])
            bottom_seg.append([rect[1],mxy,rect[3],mxy])
            foundit = False
            for pl in ply:
                over = pl.intersection(vehrect)
                if over.area > 0 :
                    result_rect.append([[rect[1],rect[0]],[rect[3],rect[0]],[rect[3],rect[2]],[rect[1],rect[2]]])
                    foundit = True
                    break
            if not foundit:
                other_rect.append([[rect[1],rect[0]],[rect[3],rect[0]],[rect[3],rect[2]],[rect[1],rect[2]]])
    return result_rect, other_rect, bottom_seg


def generate_dual_view(vid_file_dir, safety_file_dir, trans_file, out_file, polysdir, overhead_feet=90, clip_overhead = True, show_guides = True, stats = True, fps = 4.0, model=None):
    """
    Generate the dual view where the left side is the overhead and the right side is
    the original view.

    :param vid_file_dir: If this is a directory then we look for the mp4 files in here.  We also
            look for the safety file for speed data.  If this is a file then we look for a csv file
            of the same name only ending in csv.
    :param trans_file: Transform file name.
    :param out_file: Output file name.
    :param overhead_feet: number of feet aheaad to show.
    :param clip_overhead: If true then clip the overhead view to only show the center.
    :param show_guides: If true then show the diagonal guides to measure the lane width and dashs.
    :return: Nothing
    """
    if not os.path.exists(vid_file_dir):
        return
    if not os.path.exists(trans_file):
        return
    # get list of input files.
    onlyfile =[]
    filedir = vid_file_dir
    if os.path.isfile(vid_file_dir):
        onlyfile.append(os.path.basename(vid_file_dir))
        filedir = os.path.dirname(vid_file_dir)
    else:
        onlyfile = [f for f in os.listdir(vid_file_dir) if os.path.isfile(os.path.join(vid_file_dir, f)) and f.endswith(".mp4")]

    perTrans = PerspectiveTransInfo()
    perTrans.fromJson(trans_file)

    if not (perTrans.status_message == 'SUCCESS' or perTrans.status_message == 'betas histogram') :
        return

    message_str = "tf:"+str(perTrans.lrn_tot_frame)+" lf:"+str(perTrans.lrn_frames_with_lanes)+" fd:"+\
                  str(perTrans.lrn_frames_dashs)+" td:"+str(perTrans.lrn_total_dash)+" dh:"+str(len(perTrans.lrn_dash_hist))+\
                    " wh:"+str(len(perTrans.lrn_width_hist))


    t = perTrans.getInvPerTrans()
    torg = t.copy()
    # get the inverse mapping.
    retinv, torginvert = cv2.invert(torg, flags=cv2.DECOMP_LU)


    # ptsdst2 = pointhtrans([perTrans.van_pt[0],perTrans.f_height], torg)
    # ptsdst1 = [ptsdst2[0], ptsdst2[1]-overhead_feet]
    # pts1 = pointhtrans(ptsdst1,torginvert)

    newscale = perTrans.f_height/overhead_feet

    scaletrans = np.array([[newscale, 0.0, 0.0],
                           [0.0, newscale, 0.0], [0.0, 0.0, 1.0]])
    # combine the scaling with the transform.
    t = np.dot(scaletrans, t)
    # now translate to the center of the screen.
    ptcentbot = pointhtrans([perTrans.f_width / 2, perTrans.f_height], t)
    justtrans = np.array([[1.0, 0.0, (perTrans.f_width / 2) - ptcentbot[0]],
                          [0.0, 1.0, (perTrans.f_height) - ptcentbot[1]], [0.0, 0.0, 1.0]])
    t = np.dot(justtrans, t)


    # find the center bottom to get the distance from.
    centbot = pointhtrans( [perTrans.van_pt[0], perTrans.f_height],torg)
    centx = centbot[0]
    bottom_offset = centbot[1]
    # find twenty feet from teh bottom.
    twenty = bottom_offset-20
    # combine the scale and translate.
    totaltransscale = np.dot(justtrans, scaletrans)
    over_xl = 0
    over_xr = perTrans.f_width
    if clip_overhead:
        sideclips =np.dot(totaltransscale, np.array([[centx - 24, bottom_offset,1], [centx + 24, bottom_offset,1]]).transpose())
        over_xl = int(sideclips[0][0]/sideclips[2][0])
        over_xr = int(sideclips[0][1]/sideclips[2][1])

    fourcc = cv2.VideoWriter_fourcc(*'MP4V')
    out = cv2.VideoWriter(out_file, fourcc, fps, ((over_xr-over_xl)+ perTrans.f_width, perTrans.f_height))
    for fl in onlyfile:
        vcap = SdeVideo(os.path.join(vid_file_dir,fl))
        maxx, maxy = vcap.get_shape()
        nframes = 0
        safety = None
        speedfile = os.path.join(safety_file_dir,fl.replace(".mp4","_safety.csv"))
        if os.path.isfile(speedfile):
            safety = read_safety_file(speedfile)
        polys_over = []
        polys_time = []
        while True:
            frame_time = vcap.get_time()
            ret, oframe = vcap.read()
            if not ret:
                break
            polys_time.append(frame_time/1000.0)
            # get the overhead view.
            dst = cv2.warpPerspective(oframe,t,(maxx,maxy))
            # put a circle at the vanishing point.
            if show_guides:
                cv2.circle(oframe, (int(perTrans.van_pt[0]), int(perTrans.van_pt[1])), 4, (0, 0, 0), -1, -1)
            tshape = []
            if safety is not None:
                # calculate the number of feet to the 1 second time.
                fps = 0.911344 *  safety[nframes][1]
                # the distance from the bottom to the one second mark.
                distsec = -fps+bottom_offset
                # get polygons to draw on the overlay
                plys = straight_poly(torginvert, [[centx - 6, bottom_offset,1], [centx + 6, bottom_offset,1],
                                                     [centx - 6, distsec,1], [centx + 6, distsec,1],
                                                     [centx - 6, distsec-fps,1], [centx + 6, distsec-fps,1],
                                                     [centx - 6, distsec - 2*fps,1], [centx + 6, distsec - 2*fps,1]])

                polys_over.append(plys.tolist())

                overlay = oframe.copy()

                narrow_width =2
                close_rect, other_rect, bot_seg = get_close_follow(oframe,model,straight_poly(torginvert, [[centx - narrow_width, bottom_offset,1], [centx + narrow_width, bottom_offset,1],
                                                     [centx - narrow_width, distsec,1], [centx + narrow_width, distsec,1],
                                                     [centx - narrow_width, distsec-fps,1], [centx + narrow_width, distsec-fps,1],
                                                     [centx - narrow_width, distsec - 2*fps,1], [centx + narrow_width, distsec - 2*fps,1]]))

                cv2.fillPoly(overlay, np.int32([plys[0]]), (0, 0, 255))
                cv2.fillPoly(overlay, np.int32([plys[1]]), (255, 0, 0))
                cv2.fillPoly(overlay, np.int32([plys[2]]), (0, 255, 0))
                if len(close_rect) > 0:
                    cv2.polylines(overlay, np.int32(close_rect), True, (0,255,255), 4)
                if len(other_rect) > 0:
                    cv2.polylines(overlay, np.int32(other_rect), True, (0, 255, 0), 4)
                cv2.addWeighted(overlay, 0.4, oframe, 0.6, 0, oframe)

                # draw zigzag on overhead view for reference.
                tshape = [[centx - 6, distsec, 1], [centx + 6, distsec, 1]]
                for sg in bot_seg:
                    sgpt1 = pointhtrans([sg[0],sg[1]],torg)
                    sgpt2 = pointhtrans([sg[2],sg[3]],torg)
                    tshape.extend([[sgpt1[0],sgpt1[1],1],[sgpt2[0],sgpt2[1],1]])
            if show_guides:
                tshape.extend([[centx - 6, twenty, 1], [centx - 6, twenty-10, 1],[centx + 6, twenty, 1], [centx + 6, twenty - 10, 1]])
                extra_offset = 0
                for k in range(0,10):
                    tshape.append([centx - 6, bottom_offset+extra_offset, 1])
                    extra_offset -= 10
                    tshape.append([centx + 6, bottom_offset+extra_offset, 1])
            if len(tshape) > 0:
                tview = np.dot(totaltransscale, np.array(tshape).transpose())
                for k in range(0,len(tshape),2):
                    cv2.line(dst, (int(tview[0, k]), int(tview[1, k])), (int(tview[0, k+1]), int(tview[1, k+1])), (0, 0, 255), 2)
            if stats:
                cv2.putText(oframe,message_str,(0,maxy),cv2.FONT_HERSHEY_PLAIN,2,(0,0,255),2)
            # piece together the images
            new_img = np.concatenate((dst[:,over_xl:over_xr], oframe), axis=1)
            # cv2.imshow("dst",new_img)
            # cv2.waitKey(60)
            out.write(new_img)
            nframes = nframes + 5

        if polysdir is not None:
            plysfile = os.path.join(polysdir, fl.replace(".mp4", ".json"))
            plysjson = get_poly_json(polys_over,polys_time,vcap.rawmaxx,vcap.rawmaxy,fl,[0,0,maxx,maxy],[maxx,0,vcap.rawmaxx,maxy])
            with open(plysfile, "w") as jf:
                jf.write(plysjson)

        vcap.release()
    out.release()

    return

def get_poly_json(poly_over,poly_time,maxx,maxy,file,cam1offset,cam2offset):
    """
    Create the json for the polygon file.
    :param poly_over: overlay polygons.
    :param poly_time: time for each frame and polygon.
    :param maxx: max x
    :param maxy: max y
    :return:
    """
    toplevel = {}
    frames_c1 = []
    frames_c2 = []
    duration = poly_time[1]-poly_time[0]
    for i in range(0,len(poly_time)):
        # arow = {'currenttime': poly_time[i], 'xresolution': maxx, 'yresolution': maxy}
        plylist = []
        if len(poly_over) > 0:
            for pl in poly_over[i]:
                plylist.append([v for sl in pl for v in sl])
        # arow['polygon']=plylist
        # videodata.append(arow)
        frames_c1.append( {'frame':i,'time':poly_time[i],'duration':duration,'track_dimension':{'width':maxx,'height':maxy},'offsets':cam1offset,'features':[{'name':'following distance','id':10001,'type':'road','confidence':100,'polygons':plylist}]})
        frames_c2.append({'frame':i,'time':poly_time[i],'duration':duration,'track_dimension':{'width':maxx,'height':maxy},'offsets':cam2offset})

    toplevel['tracks'] = [{'id':1,
                           'type':'video',
                           'resolution': {'min': [maxx, maxy], 'max': [maxx, maxy]}, 'sourceUrl': file,
                                      'cameras': [{'id':1, 'frames': frames_c1},
                                                  {'id':2, 'frames':frames_c2}]}]
                                                     # 'cameraId1':{'frames': frames_c1,'time_frame': dict(zip(poly_time,range(0,len(poly_time))))},
                                                     # 'cameraId2':{'frames':frames_c2,'time_frame': dict(zip(poly_time,range(0,len(poly_time))))}}}

    #toplevel['frames']= videodata

    #toplevel['time_frame']= dict(zip(poly_time,range(0,len(poly_time))))

    return json.dumps(toplevel, default=lambda o: o.__dict__, sort_keys=False, indent=4)


if __name__ == "__main__":

    # parse the arguments.
    parse = argparse.ArgumentParser()
    parse.add_argument("--vd", help="Directory containing directories of video files and speed data")
    parse.add_argument("--safety",help="Directory of safety files")
    parse.add_argument("--clip",help='Clip Overhead View')
    parse.add_argument("--dist",help='Overhead distance to display')
    parse.add_argument("--guide",help='Show Guides')
    parse.add_argument("--stats",help='Show Statistics.')
    parse.add_argument("--fps", help="Frames per second output.")
    parse.add_argument("--polys", help="Create polygon files")
    parse.add_argument("--close", help="close following seconds")

    args = parse.parse_args()

    safety_file_dir = args.vd if args.safety is None else args.safety
    do_clip = False if args.clip is None else True if args.clip == "True" else False
    dist = 180 if args.dist is None else int(args.dist)
    guide = False if args.guide is None else True if args.guide == "True" else False
    stats = False if args.stats is None else True if args.stats == "True" else False
    fps = 4.0 if args.fps is None else float(args.fps)
    polys = False if args.polys is None else True if args.polys == "True" else False
    close_follow = 1 if args.close is None else int(args.close)

    model_model = None
    if args.close is not None:
        from hood_detect import rcnn_model_wrap
        model_model = rcnn_model_wrap.load_model()

    if args.vd and os.path.isdir(args.vd):
        basevd = args.vd
        onlydirs = [f for f in os.listdir(basevd) if os.path.isdir(os.path.join(basevd, f))]
        if len(onlydirs) == 0:
            onlydirs = [os.path.basename(basevd)]
            basevd = os.path.dirname(basevd)
        outbasedir = basevd+"_output"
        overheaddir = os.path.join(outbasedir,"overhead")
        if not os.path.exists(overheaddir):
             os.mkdir(overheaddir)
        if polys:
            polysdir = os.path.join(outbasedir,"polys")
            if not os.path.exists(polysdir):
                os.mkdir(polysdir)
        else:
            polysdir = None

        jsontrans = os.path.join(outbasedir,"jsontrans")

        for d in onlydirs:
            if not os.path.exists(os.path.join(overheaddir,d+".mp4")):
                generate_dual_view(os.path.join(basevd,d),safety_file_dir ,os.path.join(jsontrans,d+".json"),
                               os.path.join(overheaddir,d+".mp4"),polysdir ,dist,do_clip,guide, stats, fps, model_model)



    # generate_dual_view("C:\\Users\\BradR\\Desktop\\VideoSet_20180128\\FE6BB073-112075",
    #                    "C:\\Users\\BradR\\Desktop\\VideoSet_20180128_output\\jsontrans\\FE6BB073-112075.json",
    #                    "C:\\Users\\BradR\\Desktop\\VideoSet_20180128_output\\overhead\\FE6BB073-112075.mp4",360,True,True)

    # if args.vf and os.path.isdir(args.vf):
        # # get just the mp4 files.
        # onlyfiles = [f for f in os.listdir(args.vf) if os.path.isfile(os.path.join(args.vf, f)) and f.endswith(".mp4")]
        # outpath = os.path.join(args.vf, "resultsoverhead")
        # jsonpath = os.path.join(args.vf, "jsontrans")
        # if not os.path.exists(outpath):
        #     os.mkdir(outpath)
        # if not os.path.exists(jsonpath):
        #     os.mkdir(jsonpath)
        # fourcc = cv2.VideoWriter_fourcc(*'MP4V')
        # for f in onlyfiles:
        #     vid = os.path.join(args.vf, f)
        #     outpathfile = os.path.join(outpath, f)
        #     if os.path.exists(outpathfile):
        #         continue
        #     jsonfile = os.path.join(jsonpath,f.replace(".mp4",".json"))
        #     if not os.path.isfile(jsonfile):
        #         continue
        #     safety = None
        #     speedfile = os.path.join(args.vf,f.replace(".mp4","_safety.csv"))
        #     if os.path.isfile(speedfile):
        #         safety = read_safety_file(speedfile)
        #
        #     perTrans = PerspectiveTransInfo()
        #     perTrans.fromJson(jsonfile)
        #     vcap = SdeVideo(vid)
        #     maxx, maxy = vcap.get_shape()
        #     out = cv2.VideoWriter(outpathfile, fourcc, 4.0, (2*maxx, maxy))
        #     t = None
        #     if args.persp is not None:
        #         t = perTrans.getPureProjective()
        #     else:
        #         t = perTrans.getInvPerTrans()
        #     torg = t.copy()
        #     # get the inverse mapping.
        #     retinv, torginvert = cv2.invert(torg,flags=cv2.DECOMP_LU)
        #     # calculate the scaling and translation.
        #     # We want to show 5/6 of the way to the vanishing point.
        #     midy = (5.0*perTrans.van_pt[1]+perTrans.f_height)/6.0
        #     xleft = perTrans.van_pt[0]-(perTrans.mid_width)
        #     xright = perTrans.van_pt[0]+(perTrans.mid_width)
        #     ptsdst1 = pointhtrans([xleft,midy], t)
        #     ptsdst2 = pointhtrans([xright,perTrans.f_height], t)
        #     newscale = min(abs(perTrans.f_width)/abs(ptsdst2[0]-ptsdst1[0]),(perTrans.f_height-midy)/abs(ptsdst2[1]-ptsdst1[1]))
        #
        #     scaletrans = np.array([[newscale,0.0,0.0],
        #                            [0.0,newscale,0.0],[0.0,0.0,1.0]])
        #     # combine the scaling with the transform.
        #     t = np.dot(scaletrans,t)
        #     # now translate to the center of the screen.
        #     ptcentbot = pointhtrans([(xleft+xright)/2,perTrans.f_height],t)
        #     justtrans = np.array([[1.0,0.0,(perTrans.f_width/2)-ptcentbot[0]],
        #                           [0.0,1.0,(perTrans.f_height)-ptcentbot[1]],[0.0,0.0,1.0]])
        #     t = np.dot(justtrans,t)
        #
        #     nframes = 0
        #     # fidn the center bottom to get the distance from.
        #     centbot = np.dot(torg,[perTrans.van_pt[0],perTrans.f_height,1.0])
        #     centx = centbot[0]/centbot[2]
        #     bottom_offset = centbot[1]/centbot[2]
        #     # combine the scale and translate.
        #     totaltransscale = np.dot(justtrans,scaletrans)
        #     # find twenty feet from teh bottom.
        #     twenty = bottom_offset-20
        #
        #     while True:
        #         ret, oframe = vcap.read()
        #         if not ret:
        #             break
        #         # get the overhead view.
        #         dst = cv2.warpPerspective(oframe,t,(maxx,maxy))
        #         # put a circle at the vanishing point.
        #         cv2.circle(oframe, (int(perTrans.van_pt[0]), int(perTrans.van_pt[1])), 4, (0, 0, 0), -1, -1)
        #         tshape = []
        #         if safety is not None:
        #             # calculate the number of feet to the 1 second time.
        #             fps = 0.911344 *  safety[nframes][1]
        #             # the distance from the bottom to the one second mark.
        #             distsec = -fps+bottom_offset
        #             # get polygons to draw on the overlay
        #             plys = straight_poly(torginvert, [[centx - 6, bottom_offset,1], [centx + 6, bottom_offset,1],
        #                                                  [centx - 6, distsec,1], [centx + 6, distsec,1],
        #                                                  [centx - 6, distsec-fps,1], [centx + 6, distsec-fps,1],
        #                                                  [centx - 6, distsec - 2*fps,1], [centx + 6, distsec - 2*fps,1]])
        #             overlay = oframe.copy()
        #             cv2.fillPoly(overlay, np.int32([plys[0]]), (0, 0, 255))
        #             cv2.fillPoly(overlay, np.int32([plys[1]]), (255, 0, 0))
        #             cv2.fillPoly(overlay, np.int32([plys[2]]), (0, 255, 0))
        #             cv2.addWeighted(overlay, 0.4, oframe, 0.6, 0, oframe)
        #
        #             # draw zigzag on overhead view for reference.
        #             tshape = [[centx - 6, distsec, 1], [centx + 6, distsec, 1]]
        #
        #         tshape.extend([[centx - 6, twenty, 1], [centx - 6, twenty-10, 1],[centx + 6, twenty, 1], [centx + 6, twenty - 10, 1]])
        #         extra_offset = 0
        #         for k in range(0,10):
        #             tshape.append([centx - 6, bottom_offset+extra_offset, 1])
        #             extra_offset -= 10
        #             tshape.append([centx + 6, bottom_offset+extra_offset, 1])
        #         tview = np.dot(totaltransscale, np.array(tshape).transpose())
        #         for k in range(0,len(tshape),2):
        #             cv2.line(dst, (int(tview[0, k]), int(tview[1, k])), (int(tview[0, k+1]), int(tview[1, k+1])), (0, 0, 255), 2)
        #
        #         # piece together the images
        #         new_img = np.concatenate((dst, oframe), axis=1)
        #         cv2.imshow("dst",new_img)
        #         cv2.waitKey(60)
        #         out.write(new_img)
        #         nframes = nframes + 5
        #     out.release()
        #     del out
        #     vcap.release()
        #
        #     gc.collect()
