"""
@author: Andrew Kobach

"""

import pandas as pd
import argparse
import pyodbc
from shutil import copyfile
import os
import glob
import numpy as np
from pathlib import Path
import tarfile
import xmltodict
import json
import requests

# running options 
parse = argparse.ArgumentParser()
parse.add_argument('--input', help='JSON file with input parameters')
parse.add_argument('--GetSDEs', action='store_true', default=False, help='queries External DW and copies over SDEs to local directory')
#parse.add_argument('--GetCaliParams', action='store_true', default=False, help='queries SRSetting database and creates calibrations.json in local directory with the needed format')
parse.add_argument('--remake', action='store_true', default=False, help="Remake all video files")
args = parse.parse_args()


if(args.input is not None): 
    inputjson = pd.read_json(args.input, typ='series')                            
else: 
    print('ERROR: Specify input json file!')
    exit()

print(inputjson)
                                                     
    
# exit if directory is taking up more than 1 GB of space
root_directory = Path('.')
size = sum(f.stat().st_size for f in root_directory.glob('**/*') if f.is_file() )

# stop everything if folder is going over size limit 
if(size >= 1e9*int(inputjson['FolderSizeLimit'])):
    print('Directory is taking up more than ' + str(size/1e9) + ' GB.  Delete some SDE files or ask for more space.')
    exit()
    


def GetListofSDEs():
    
    # make string for SQL query of the External Data Warehouse
    query = """
            SELECT s.company_name_current
            	, s.site_level_five_name_current
            	, elc.dim_captured_date_key
            	, sr.serial_number_current AS SR_serial_number
            	, v.serial_number_current
            	, t.trigger_type_name
            	, t.trigger_type_id 
                , e.event_number 
                , e.file_name
            FROM fact_Event_Lifecycle elc
                                    
            INNER JOIN dim_Site s ON s.dim_sitehierarchy_key = elc.dim_sitehierarchy_key
            INNER JOIN dim_Vehicle v ON v.dim_vehicle_key = elc.dim_vehicle_key
            INNER JOIN dim_SmartRecorder sr ON sr.dim_smartrecorder_key = elc.dim_smartrecorder_key
            INNER JOIN dim_Trigger_Type t ON t.dim_trigger_type_key = elc.dim_trigger_type_key
            INNER JOIN dim_Event e ON e.dim_event_key = elc.dim_event_key
                                    
            WHERE s.company_name_current IN (%s)
                AND elc.dim_captured_date_key >= %s
            	AND t.trigger_type_id IN (%s) 
            """ % (inputjson['Company'], inputjson['StartDate'], inputjson['TriggerIDs'])
            
            
    if('SRs' in inputjson):
        query += '\tAND sr.serial_number_current IN (%s) \n' % inputjson['SRs'] 
       
    if('Level5SiteName' in inputjson):
        query += '\tAND s.site_level_five_name_current IN (%s) \n' % inputjson['Level5SiteName'] 
      
        
    # specify the driver, server, and database
    driver = 'ODBC DRIVER 17 for SQL Server'
    server = 'db_externaldw.smartdrivesystems.com'
    database = 'sds_ExternalDataWarehouse'
     
    # choose a read-only node
    odbc = 'DRIVER={%s};SERVER=%s;DATABASE=%s;Trusted_Connection=Yes;ApplicationIntent=ReadOnly' % (driver, server, database)
                 
    # connect to the database
    sql_conn = pyodbc.connect(odbc)
    print('Connected to External Data Warehouse')
    
    # imports the data into a pandas dataframe, and automatically closes the connection
    data = pd.read_sql(query, sql_conn)
    print('Executed query')
    
    return data


def MkDirIfNoDir(dir):
    if (os.path.exists(dir) == False):
        os.mkdir(dir)    


def CopyOverSDEs(data):
    
    # get current directory
    cwd = os.getcwd()
    
    # specify paths of local directories
    SDE_dir = os.path.join(cwd, 'SDEs')
    dir_FCW = os.path.join(SDE_dir, 'FCW')
    dir_SF = os.path.join(SDE_dir, 'SF')
    dir_LDW = os.path.join(SDE_dir, 'LDW')
    
    # make local directories, if they don't already exist
    MkDirIfNoDir(SDE_dir)
    MkDirIfNoDir(dir_FCW)
    MkDirIfNoDir(dir_SF)
    MkDirIfNoDir(dir_LDW)
    
    # path to SDE files
    path_to_SDE_files = "\\" + inputjson['PathToSDEs']
    
    # make lists for SR SN's, SDE file names, and trigger types
    SR_SNs = data['SR_serial_number'].values
    SDE_files = data['file_name'].values
    trigger_type = data['trigger_type_id'].values
    
    
    for sr, f, t, i in zip(SR_SNs, SDE_files, trigger_type, data.index):
        src = os.path.join(path_to_SDE_files, sr, f)
        
        # do nothing if there is no SDE file to copy over
        if(os.path.exists(src)==False): continue
        
        if t in (456, 459, 460): # FCW
            dest = os.path.join(dir_FCW, sr)
            MkDirIfNoDir(dest)
            destf = os.path.join(dest, f)
            if (os.path.exists(destf)==False): 
                copyfile(src, destf)
                data.loc[i].to_json(os.path.join(dest, data.loc[i]['file_name'][:-4] + ".json"))
           
        if t in (457, 461, 462): # SF
            dest = os.path.join(dir_SF, sr)
            MkDirIfNoDir(dest)
            destf = os.path.join(dest, f)
            if (os.path.exists(destf)==False): 
                copyfile(src, destf)
                data.loc[i].to_json(os.path.join(dest, data.loc[i]['file_name'][:-4] + ".json"))
            
        if t in (458, 463, 464): # LDW
            dest = os.path.join(dir_LDW, sr)
            MkDirIfNoDir(dest)
            destf = os.path.join(dest, f)
            if (os.path.exists(destf)==False): 
                copyfile(src, destf)
                data.loc[i].to_json(os.path.join(dest, data.loc[i]['file_name'][:-4] + ".json"))
    
    print('Copied over SDEs')
    
    
    return 0


def GetCalibrationParameters():
    
    # get calibration parameters in SRSetting
    DRIVER = 'ODBC DRIVER 17 for SQL Server'
    server = 'db_SRSetting.smartdrivesystems.com'
    database = 'sds_SRSetting'
    
    odbc = 'DRIVER={%s};SERVER=%s;DATABASE=%s;Trusted_Connection=Yes;ApplicationIntent=ReadOnly' % (DRIVER, server, database)
                
    sql_conn = pyodbc.connect(odbc) 
    print('Connected to SRSetting')
    
    query = """ WITH CTE AS 
                (
                SELECT sr.SerialNumber,
                    d.Name AS HealthStatusProperty,
                    s.HealthPropertyDefinitionId,
                    s.ReportedValue,
                    s.CreatedDate,
                    RN = ROW_NUMBER() OVER (PARTITION BY sr.SerialNumber, d.Name ORDER BY s.CreatedDate DESC)
                FROM [sds_SRSetting].[msg].[SmartRecorderHealthData] s 
            
                INNER JOIN [sds_SRSetting].[dbo].[SyncedSmartRecorder] sr  ON sr.SmartRecorderId = s.SmartRecorderId
                INNER JOIN [sds_SRSetting].[msg].[HealthPropertyDefinition] d  ON s.HealthPropertyDefinitionId = d.HealthPropertyDefinitionId
                )
                SELECT SerialNumber, HealthStatusProperty, ReportedValue 
                FROM CTE
                WHERE HealthStatusProperty LIKE '%ADAS%'
                    AND RN = 1
                ORDER BY SerialNumber, HealthStatusProperty, ReportedValue
            """
    
    data = pd.read_sql(query, sql_conn)
    print('Executed query')
    
    # drop rows with ADASCamStatus ADASCalibrationStatus, and ADASCalibrationBoardHeight
    data = data[data['HealthStatusProperty'] != 'ADASCalibrationStatus']
    data = data[data['HealthStatusProperty'] != 'ADASCamStatus']
    data = data[data['HealthStatusProperty'] != 'ADASCalibrationBoardHeight']
    
    # drop rows with ReportedValue == 'Unknown'
    data = data[data['ReportedValue'] != 'Unknown']
    
    # what's left should be numbers, so convert them to numbers
    data['ReportedValue'] = pd.to_numeric(data['ReportedValue'])
    
    # flip the table 
    dataflipped = pd.pivot_table(data, index=['SerialNumber'], columns=['HealthStatusProperty'])
    
    # remove the extra column index
    dataflipped.columns = [dataflipped.columns[i][1] for i in range(len(dataflipped.columns))]
    
    # rename the columns
    dataflipped.rename({'ADASCalibrationRx':'ext_rx', 'ADASCalibrationRy':'ext_ry', 'ADASCalibrationRz':'ext_rz','ADASCalibrationTx':'ext_tx', 'ADASCalibrationTy':'ext_ty', 'ADASCalibrationTz':'ext_tz', 'ADASCalibrationVehicleWidth'
    :'car_width'}, axis=1, inplace=True)
    
    # set internal calibration parameters - these are constants
    dataflipped['int_fx'] = [1566 for i in range(len(dataflipped))]
    dataflipped['int_fy'] = [1566 for i in range(len(dataflipped))]
    dataflipped['int_cx'] = [648 for i in range(len(dataflipped))]
    dataflipped['int_cy'] = [328 for i in range(len(dataflipped))]
    
    # save calibration parameters to a local directory
    cwd = os.getcwd()
    calidir = os.path.join(cwd, str(inputjson['CalibrationFolder']))
    MkDirIfNoDir(calidir)
    dataflipped.to_json(os.path.join(calidir,'calibrations.json'), orient='index')
    
    # parse calibrations.json for each SR
    cal = pd.read_json(os.path.join(calidir, 'calibrations.json'))
    for x in cal.columns:
        cal[x].to_json(os.path.join(calidir, x+'.json'))
        
    print('Created json files with calibration parameters')


def GetCalibrationParametersFromSDE(f, SR):
    
    # open the SDE
    tar = tarfile.open(f)
    
    # get Header.XML
    header = xmltodict.parse(tar.extractfile("Header.XML")) 
    
    # if there are calibration parameters in the SDE file, then save calibration json file to CalibrationParameters
    if('ADASCalibrationStatus' in header['SdeHeader']['Debug']):
        
        # build calibration dictionary
        caliparams = {'ext_rx': float(header['SdeHeader']['Debug']['ADASCalibrationRx']),
                      'ext_ry': float(header['SdeHeader']['Debug']['ADASCalibrationRy']),
                      'ext_rz': float(header['SdeHeader']['Debug']['ADASCalibrationRz']),
                      'ext_tx': float(header['SdeHeader']['Debug']['ADASCalibrationTx']),
                      'ext_ty': float(header['SdeHeader']['Debug']['ADASCalibrationTy']),
                      'ext_tz': float(header['SdeHeader']['Debug']['ADASCalibrationTz']),
                      'car_width': float(header['SdeHeader']['Debug']['ADASCalibrationVehicleWidth']),
                      'int_fx': inputjson['int_fx'],
                      'int_fy': inputjson['int_fy'],
                      'int_cx': inputjson['int_cx'],
                      'int_cy': inputjson['int_cy']
                     }
        
        filename = os.path.join(os.getcwd(), str(inputjson['CalibrationFolder'])) + '\\' + SR + '.json' 
        
        with open(filename, 'w') as fp:
            json.dump(caliparams, fp)    
            
        # special case for ND8CB181 (Penske)
        if(SR == 'ND8CB181'):
            caliparams = {"int_fx": 1566,
                          "int_fy": 1566,
                          "int_cx": 648,
                          "int_cy": 328,
                          "ext_rx": -2.0752,
                          "ext_ry": 0.5,
                          "ext_rz": 0.0,
                          "ext_tx": 0.0,
                          "ext_ty": -2541.9956,
                          "ext_tz": -1480.0,
                          "car_width": 2350.0,
                          "car_height": 2350.0
                        }
            filename = os.path.join(os.getcwd(), str(inputjson['CalibrationFolder'])) + '\\' + SR + '.json' 
        
            with open(filename, 'w') as fp:
                json.dump(caliparams, fp) 
                
       
             
    return 
    

def GetCorrectedCalibrationParameters(cal_dir):
    
    # get corrected calibration parameters from Brad Rice's algorithm
    user = 'adas'
    password = 'G0Sl0w!!'
    url = 'http://d1y8b83d634g4x.cloudfront.net/corrected_calibrations.json'
    
    resp = requests.get(url, auth=(user, password))
    cals = resp.json()
    
    # save to file in CalibrationParamters
    with open(os.path.join(cal_dir, 'corrected_calibrations.json'), 'w') as f:
        json.dump(cals, f)
            
    return cals


def RunOverlayCode():
    
    # make list of SDEs currently saved in directory tree
    curdir = os.getcwd()
    
    '''
    SDEs3 = glob.glob(os.path.join(curdir, '*\\*\\*\\*.SDE'))
    SDEs2 = glob.glob(os.path.join(curdir, '*\\*\\*.SDE'))
    SDEs1 = glob.glob(os.path.join(curdir, '*\\*.SDE'))
    SDEs0 = glob.glob(os.path.join(curdir, '*.SDE'))
    '''
    
    SDEs3 = glob.glob('.\*\\*\\*\\*.SDE')
    SDEs2 = glob.glob('.\*\\*\\*.SDE')
    SDEs1 = glob.glob('.\*\\*.SDE')
    SDEs0 = glob.glob('.\*.SDE')
    
    SDEs = SDEs0 + SDEs1 + SDEs2 + SDEs3
    
    
    
    # set python path
    #os.environ['PYTHONPATH'] = os.path.join(curdir, 'BradsCode\\cv_tools')
    os.environ['PYTHONPATH'] = os.path.join(inputjson['PathToBradsCode'], 'BradsCode\\cv_tools')
    
    # location of overlay code
    #path_to_overlay = os.path.join(curdir, 'BradsCode\\cv_tools\\ADAS\\ADAS_overhead.py')
    path_to_overlay = os.path.join(inputjson['PathToBradsCode'], 'BradsCode\\cv_tools\\ADAS\\ADAS_overhead.py')

    # for firmware version >= 22p 
    offset = (inputjson['OffsetX'], inputjson['OffsetY'])
    
    # location of calibrations
    #cal_dir = os.path.join(curdir, str(inputjson['CalibrationFolder']))
    cal_dir = os.path.join(".", str(inputjson['CalibrationFolder']))

    for f in SDEs:
    
        # get SR SN from the found SDE File
        SR_SN = f.split(os.sep)[-1][:8]
        
        # go into SDE file, and if calibration parameters exist in Header.xml, rewrite calibration parameter json file in CalibrationParameters directory
        GetCalibrationParametersFromSDE(f, SR_SN)
                
        # continue if calibration parameters do not exist 
        if (os.path.exists(os.path.join(cal_dir, SR_SN+'.json')) == False): 
            print('No calibration parameters for ' + SR_SN + '!')
            continue # just don't make video
        
        # continue if overlay already made
        if (os.path.exists(f[:-4] + '.mp4')) and (args.remake is not True): continue

    
        # make overlay
        command = ('python ' + path_to_overlay  
            + ' --sde ' + '"'+f+'"'
            + ' --cal ' + cal_dir + '\\' + SR_SN + '.json' 
            + ' --mp4v' + ' --offset ' + str(offset[0]) + ',' + str(offset[1]) 
            + ' --shortfollowingS1 ' + str(inputjson['ShortFollowingS1']) 
            + ' --shortfollowingS2 ' + str(inputjson['ShortFollowingS2']) 
            + ' --lanedeparture ' + str(inputjson['LaneDeparture']) 
            + ' --lanelineconfidence ' + str(inputjson['LaneLineConfidence']) 
            + ' --laneslopemax ' + str(inputjson['LaneSlopeMax']) 
            + ' --lanelinedistsec ' + str(inputjson['LaneLineDistSec']) 
            + ' --maxinlanetime ' + str(inputjson['MaxInLaneTime']) 
            + ' --numframesSF ' + str(inputjson['ShortFollowingNumberOfFrames']) 
            + ' --numframesLDW ' + str(inputjson['LaneDepartureNumberOfFrames']) 
            + ' --TTCmin ' + str(inputjson['TTCmin'])
            + ' --vehicleconfidence ' + str(inputjson['VehicleConfidence'])
            )                  
        
        print(command)
        os.system(command)
        
              
            
def MakeExcelSheet():
    
    curdir = os.getcwd()
    
    # get corrected calibration parameters
    #corrcals_dict = GetCorrectedCalibrationParameters(os.path.join(curdir, str(inputjson['CalibrationFolder'])))
    #corrcals = pd.DataFrame.from_dict(corrcals_dict)
    
    # list of mp4 and json files
    mp4s = glob.glob(os.path.join(curdir, 'SDEs\\*\\*\\*.mp4'))
    jsons = glob.glob(os.path.join(curdir, 'SDEs\\*\\*\\*.json'))       
    distplot = glob.glob(os.path.join(curdir, 'SDEs\\*\\*\\*_vehicles.png'))   
    
    
    if(len(jsons) == 0):
        print('No events!')
        exit()
    
    
    if(len(mp4s) != len(distplot)):
        print('One of the SDE files is buggy.  Remove it, and try again.')
        for f in mp4s:
            if(os.path.exists(f[:-4]+'_vehicles.png') == False): print('Offending SDE:', f[:-4]+'.SDE')
        exit()
         
    # strip off mp4 suffix to get corresponding SDE and json files
    SDEnames = np.asarray([x.split(os.sep)[-1][:-4]+'.SDE' for x in mp4s])
    
    # new dataframe with location of mp4's
    viddf = pd.DataFrame(mp4s, columns=['mp4_path'])
    
    # new dataframe with location of distance plots
    distdf = pd.DataFrame(distplot, columns=['dist_plot'])
    
    # new column with corresponding SDE and json names - should be in same order as mp4's
    viddf['SDE_name'] = pd.Series(SDEnames).values
    
    # new column with corresponding SDE and json names - should be in same order as mp4's
    distdf['SDE_name'] = pd.Series(SDEnames).values
    
    sheet = pd.DataFrame()
    for j in jsons:
        dfh = pd.read_json(j, typ='series')
        sheet = sheet.append(dfh,ignore_index=True)    
    
    
    # merge on SDE name
    sheet = pd.merge(sheet, viddf, left_on='file_name', right_on='SDE_name', how='inner')
    sheet = pd.merge(sheet, distdf, left_on='SDE_name', right_on='SDE_name', how='inner')
            
    # create column with hyperlink to mp4
    sheet['video'] = sheet.apply(lambda row: '=HYPERLINK("' + row.mp4_path + '", "video link")', axis=1)    
    #sheet['FeetDiff'] = sheet.apply(lambda row: corrcals[row.SR_serial_number]['diff']/304.8 if(row.SR_serial_number in corrcals) else np.nan, axis=1)    
    #sheet['AngleDiff'] = sheet.apply(lambda row: corrcals[row.SR_serial_number]['angle_diff'] if(row.SR_serial_number in corrcals) else np.nan, axis=1) 
    sheet['distance_plot'] = sheet.apply(lambda row: '=HYPERLINK("' + row.dist_plot + '", "dist pdf")', axis=1)       
    
    # drop annoying columns
    sheet.drop(columns=['mp4_path', 'file_name', 'dist_plot'], inplace=True)
    
    # reorder columns
    #sheet = sheet[['FCW', 'LDW', 'SF', 'video', 'distance_plot', 'Firmware', 'dim_captured_date_key', 'SR_serial_number', 'company_name_current', 'site_level_five_name_current', 'serial_number_current', 'event_number', 'trigger_type_id', 'trigger_type_name', 'FeetDiff', 'AngleDiff', 'SDE_name']]
    sheet = sheet[['FCW', 'LDW', 'SF', 'video', 'distance_plot', 'Firmware', 'dim_captured_date_key', 'SR_serial_number', 'company_name_current', 'site_level_five_name_current', 'serial_number_current', 'event_number', 'trigger_type_id', 'trigger_type_name', 'SDE_name']]

    # sort by date
    sheet.sort_values(by=['dim_captured_date_key'], inplace=True)
    
    # split dataframe into three, one for each ADAS trigger type
    sheet_FCW = sheet[sheet['trigger_type_id'].isin([456, 459, 460])]
    sheet_SF = sheet[sheet['trigger_type_id'].isin([457, 461, 462])]
    sheet_LDW = sheet[sheet['trigger_type_id'].isin([458, 463, 464])]

    
    # calculate fake rate
    sftot = sheet_SF.shape[0]
    sf = sheet_SF[sheet_SF['SF'] != -1]
    sftrig = sf.shape[0]
    print('\nSHORT FOLLOWING')
    if(sftot != 0):
        print(str(sftrig) + ' out of ' + str(sftot) + ' (' + str(int(100*sftrig/sftot)) + '%) videos with a Short Following primary trigger had following time <= ' + str(inputjson['ShortFollowingS2'])[:4] + ' seconds for at least '+str(inputjson['ShortFollowingNumberOfFrames']) + ' consecutive frames' )
    else: print('No Short Following events yet!')
    
    ldwtot = sheet_LDW.shape[0]
    ldw = sheet_LDW[sheet_LDW['LDW'] != -1]
    ldwtrig = ldw.shape[0]
    print('\nLANE DEPARTURE')
    if(ldwtot != 0):
        print(str(ldwtrig) + ' out of ' + str(ldwtot) + ' (' + str(int(100*ldwtrig/ldwtot)) + '%) videos with a Lane Departure primary trigger had lane departure >= ' + str(inputjson['LaneDeparture']) + ' mm for at least ' +str(inputjson['ShortFollowingNumberOfFrames']) + ' consecutive frames' )
    else: print('No Lane Departure Warning events yet!')
    
    fcwtot = sheet_FCW.shape[0]
    fcw = sheet_FCW[sheet_FCW['FCW'] != -1]
    fcwtrig = fcw.shape[0]
    print('\nFORWARD COLLISION WARNING')
    if(fcwtot != 0):
        print(str(fcwtrig) + ' out of ' + str(fcwtot) + ' (' + str(int(100*fcwtrig/fcwtot)) + '%) videos with a Forward Collision Warning primary trigger had a time-to-collision < ' + str(inputjson['TTCmin']) + ' seconds')
        print('Note that the true-positive rate is likely lower than this percentage, due to phantom vehicles, etc.')
    else: print('No Forward Collision Warning events yet!')
    
    
    # clean up and export into csv files
    # there's a weird feature in pandas, which is circumvented by making new sheets
    sheet_FCW_new = sheet_FCW.replace(-1, 'No')
    sheet_SF_new = sheet_SF.replace(-1, 'No')
    sheet_LDW_new = sheet_LDW.replace(-1, 'No')
    
    sheet_FCW_newnew = sheet_FCW_new.replace(2, 'Yes')
    sheet_SF_newnew = sheet_SF_new.replace(2, 'Yes')
    sheet_LDW_newnew = sheet_LDW_new.replace(2, 'Yes')
    
    
    # create column that identifies the event as a true positive, false positive, or false negative
    
    
    
    sheet_FCW_newnew.to_csv('Videos_FCW.csv', index=False)
    sheet_SF_newnew.to_csv('Videos_SF.csv', index=False)
    sheet_LDW_newnew.to_csv('Videos_LDW.csv', index=False)








### MAIN ###
    

if(args.GetSDEs == True):
    data = GetListofSDEs() # SQL query of External Data Warehouse to get list of SDE files for ADAS triggers
    CopyOverSDEs(data) # Copy SDE files to local directories


# make CalibrationParameters direectory
MkDirIfNoDir(os.path.join(os.getcwd(), str(inputjson['CalibrationFolder'])))    

             
#if(args.GetCaliParams == True):
#    GetCalibrationParameters() # SQL query of SRSetting and make calibrations.json in local directory
    

# run overlay code on SDE files
RunOverlayCode()


# make excel sheets
MakeExcelSheet()




