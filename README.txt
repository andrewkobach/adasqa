Read this confluence page: https://smartdrivesystems.atlassian.net/wiki/spaces/AN/pages/643301686/Firmware+QA+for+ADAS

Make sure to change the field "PathToBradsCode" in Input.json to point to your local copy of the directory BradsCode\ 

Example of running when wanting to copy over the SDEs:

python Analyze.py --input Input.json --GetSDEs

If the SDEs are already copied over, run it like this:

python Analyze.py --input Input.json

